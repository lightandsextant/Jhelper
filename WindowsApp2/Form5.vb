﻿Public Class Form5
    Private Sub Form5_Load(sender As Object, e As EventArgs) Handles Me.Load
        '初始化groupbox1
        PictureBox1.Image = ImageList1.Images.Item(1)
        PictureBox2.Image = ImageList1.Images.Item(1)
        PictureBox3.Image = ImageList1.Images.Item(1)
        PictureBox4.Image = ImageList1.Images.Item(1)
        '补全修改应答人信息
        If GroupBox1.Text = "修改应答人信息" Then
            Form1.sqlcon1.sqlstr("
            SELECT facname,faccont,facline,facmail
            FROM factory
            WHERE factory.id = " & Form3.fac_id, 1)
            Form1.sqlcon1.dataf.Read()
            PictureBox1.Image = ImageList1.Images.Item(0)
            TextBox2.Text = Form1.sqlcon1.dataf.Item(1)
            TextBox3.Text = Form1.sqlcon1.dataf.Item(2)
            TextBox4.Text = Form1.sqlcon1.dataf.Item(3)
        End If
    End Sub

    Private Sub TextBox_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged,
    TextBox2.TextChanged, TextBox3.TextChanged, TextBox4.TextChanged
        '文本框验证输入图片
        Select Case sender.name
            Case "TextBox1"
                If TextBox1.Text <> "" Then
                    PictureBox1.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox1.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox2"
                If TextBox2.Text <> "" Then
                    PictureBox2.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox2.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox3"
                If TextBox3.Text <> "" Then
                    PictureBox3.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox3.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox4"
                If TextBox4.Text <> "" Then
                    PictureBox4.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox4.Image = ImageList1.Images.Item(1)
                End If
        End Select
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click

        '检查textbox不为空
        If TextBox1.Text = "" Or TextBox2.Text = "" Or TextBox3.Text = "" Or TextBox4.Text = "" Then
            MsgBox("警告：新增应答人信息不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        'sql更新或插入factory表
        Select Case GroupBox1.Text
            Case "新增应答人"
                Try
                    '检查facname是否存在
                    Form1.sqlcon1.sqlstr("
                    SELECT facname
                    FROM factory,factory_config
                    WHERE factory.id = factory_config.facid
                    AND factory_config.pkgid = " & Form3.pkg_id, 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        While Form1.sqlcon1.dataf.Read()
                            If Form1.sqlcon1.dataf.Item(0) = TextBox1.Text Then
                                MsgBox("警告：应答人已存在", 0 + 48, "warning")
                                Exit Sub
                            End If
                        End While
                    End If
                    '插入factory表数据
                    Form1.sqlcon1.sqlstr("
                    INSERT INTO 
                    [factory]([facname],[faccont],[facline],[facmail])
                    SELECT " &
                    Chr(34) & TextBox1.Text & Chr(34) & "," &
                    Chr(34) & TextBox2.Text & Chr(34) & "," &
                    Chr(34) & TextBox3.Text & Chr(34) & "," &
                    Chr(34) & TextBox4.Text & Chr(34) & "
                    WHERE NOT EXISTS (
                    SELECT * 
                    FROM [factory] 
                    WHERE [facname] = " &
                    Chr(34) & TextBox1.Text & Chr(34) & "
                    AND [faccont] = " &
                    Chr(34) & TextBox2.Text & Chr(34) & "
                    AND [facline] = " &
                    Chr(34) & TextBox3.Text & Chr(34) & "
                    AND [facmail] = " &
                    Chr(34) & TextBox4.Text & Chr(34) & ")")
                    '获取fac_id
                    Form1.sqlcon1.sqlstr("
                    SELECT id 
                    FROM factory
                    WHERE facname = " & Chr(34) & TextBox1.Text & Chr(34) & "
                    AND faccont = " & Chr(34) & TextBox2.Text & Chr(34) & "
                    AND facline = " & Chr(34) & TextBox3.Text & Chr(34) & "
                    AND facmail = " & Chr(34) & TextBox4.Text & Chr(34), 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        Form1.sqlcon1.dataf.Read()
                        Form3.fac_id = Form1.sqlcon1.dataf.Item(0)
                        '插入factory_config表数据
                        Form1.sqlcon1.sqlstr("
                        INSERT INTO 
                        factory_config(oveid,pkgid,facid,disst,repst)
                        VALUES (" &
                        Form3.pro_id & "," &
                        Form3.pkg_id & "," &
                        Form3.fac_id & "," &
                        Chr(34) & Chr(34) & "," &
                        Chr(34) & Chr(34) & ")")
                    Else
                        Throw New Exception
                    End If
                Catch ex As Exception
                    MsgBox("警告：发生未知错误 location:factory_config", 0 + 48, "warning")
                    Exit Sub
                End Try
            Case "修改应答人信息"
                Try
                    Form1.sqlcon1.sqlstr("
                    UPDATE factory SET
                    facname = " &
                    Chr(34) & TextBox1.Text.ToString & Chr(34) & ",
                    faccont = " &
                    Chr(34) & TextBox2.Text.ToString & Chr(34) & ",
                    facline = " &
                    Chr(34) & TextBox3.Text.ToString & Chr(34) & ",
                    facmail = " &
                    Chr(34) & TextBox4.Text.ToString & Chr(34) & "
                    WHERE id = " & Form3.fac_id)
                Catch ex As Exception
                    MsgBox("警告：发生未知错误 location:factory", 0 + 48, "warning")
                    Exit Sub
                End Try
        End Select
        MsgBox("提示：应答人已新增或更新完成", 0 + 64, "information")
        '刷新treeview
        Form3.SkinListView1.Clear()
        Call Form3.tvu(1)
        Me.Close()
    End Sub

End Class