﻿Imports DevExpress.XtraEditors.Controls

Public Class Form12

    Private Sub Form12_Load(sender As Object, e As EventArgs) Handles Me.Load
        '初始化combobox
        ComboBoxEdit1.Text = ""
        ComboBoxEdit1.Properties.Items.Clear()
        '初始化textbox
        TextBox1.Text = ""
        '查询cc_dossier，dosname
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT cc_dossier.dosname
        FROM cc_dossier", 1)
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                ComboBoxEdit1.Properties.Items.Add(Form1.sqlcon1.dataf.Item(0).ToString)
            End While
        End If

    End Sub

    Private Sub ComboBoxEdit1_TextChanged(sender As Object, e As EventArgs) Handles ComboBoxEdit1.TextChanged

        If ComboBoxEdit1.Text = "" Then
            TextBox1.Text = ""
        Else
            Form1.sqlcon1.sqlstr("
            SELECT DISTINCT dosname,dosno
            FROM cc_dossier
            WHERE cc_dossier.dosname = " &
            Chr(34) & ComboBoxEdit1.Text.ToString & Chr(34), 1)
            If Form1.sqlcon1.dataf.HasRows Then
                Form1.sqlcon1.dataf.Read()
                TextBox1.Text = Form1.sqlcon1.dataf.Item(1).ToString
            End If
        End If

    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click

        If ComboBoxEdit1.Text = "" Then
            MsgBox("警告：请选择项目", 0 + 48, "warning")
            Exit Sub
        Else
            If MsgBox("注意：是否确定删除选择项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                Form1.sqlcon1.sqlstr("
                DELETE FROM cc_dossier 
                WHERE cc_dossier.dosname = " &
                Chr(34) & ComboBoxEdit1.Text.ToString & Chr(34))
                '提示
                MsgBox("提示：已删除指定项目", 0 + 64, "information")

            End If
        End If
    End Sub

    '按项目导出excel
    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        Dim fo1 As New fileoutput

        If ButtonEdit1.Text = "" Then
            MsgBox("警告：导出路径不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '导出文件
        Try
            fo1.dc_danganxinxi(ButtonEdit1.Text.ToString)
        Catch ex As Exception
            '连接文件已打开报错
            MsgBox("警告：发生未知错误 location:xlscon", 0 + 48, "warning")
            Exit Sub
        End Try
    End Sub

    '导入格式文件excel
    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Dim wf1 As New wf

        '判断combobox，textbox空值
        If TextBox1.Text = "" Or ComboBoxEdit1.Text = "" Then
            MsgBox("警告：项目名称或档号不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '询问清楚数据
        If MsgBox("注意：是否清除已有数据并导入", 4 + 32, "caution") = MsgBoxResult.Yes Then
            wf1.wshow()
            Form1.sqlcon1.sqlstr("
            DELETE FROM cc_dossier 
            WHERE cc_dossier.dosname = " &
            Chr(34) & ComboBoxEdit1.Text.ToString & Chr(34))
            '选择导入后执行
            Form3.OpenFileDialog1.Filter = "Excel File (*.xls;*.xlsx)|*.xls;*.xlsx"
            Form3.OpenFileDialog1.ShowDialog()
            If Form3.OpenFileDialog1.FileName <> "" Then
                Try
                    '连接excel（文件名，sheet序号）
                    Form3.xlscon1.str(Form3.OpenFileDialog1.FileName, 1)
                Catch ex As Exception
                    wf1.wclose()
                    '连接文件已打开报错
                    MsgBox("警告：发生未知错误 location:xlscon", 0 + 48, "warning")
                    Exit Sub
                End Try
                '调用dr
                Call Form3.dr_ccdossier()
                '初始化OpenFileDialog1.FileName
                Form3.OpenFileDialog1.FileName = ""
            End If
            wf1.wclose()
        End If
    End Sub

    Private Sub ButtonEdit1_ButtonClick(sender As Object, e As ButtonPressedEventArgs) Handles ButtonEdit1.ButtonClick

        If Form3.FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            ButtonEdit1.Text = Form3.FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        Dim dr As DataRow
        Dim str As String
        Dim fwo1 As New filewdoutput
        Dim fo1 As New fileoutput
        Dim wf1 As New wf

        '检查填写内容是否为空
        If ButtonEdit1.Text = "" Then
            MsgBox("警告：导出路径不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        If TextBox1.Text = "" Then
            MsgBox("警告：项目名称或档号不得为空", 0 + 48, "warning")
            Exit Sub
        ElseIf ComboBoxEdit1.Text = "" Then
            MsgBox("警告：项目名称或档号不得为空", 0 + 48, "warning")
            Exit Sub
        Else
            Form1.sqlcon1.sqlstr("
            SELECT *
            FROM cc_dossier
            WHERE dosname = " &
            Chr(34) & ComboBoxEdit1.Text.ToString & Chr(34), 1)
            If Form1.sqlcon1.dataf.HasRows Then
            Else
                MsgBox("警告：未导入项目档案资料信息", 0 + 48, "warning")
                Exit Sub
            End If
        End If
        '询问是否到处
        If MsgBox("注意：是否确定生成", 4 + 32, "caution") = MsgBoxResult.No Then
            Exit Sub
        End If
        '清空cc_dossier_config
        Form1.sqlcon1.sqlstr("
        DELETE FROM cc_dossier_config
        WHERE dosname = " &
        Chr(34) & ComboBoxEdit1.Text & Chr(34))
        '清空cc_dossier_config_fm
        Form1.sqlcon1.sqlstr("
        DELETE FROM cc_dossier_config_fm
        WHERE dosname = " &
        Chr(34) & ComboBoxEdit1.Text & Chr(34))
        '导出脊背格式
        wf1.wshow()
        '创建路径
        System.IO.Directory.CreateDirectory(ButtonEdit1.Text.ToString & "\档案整理")
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT boxidx,dosname
        FROM cc_dossier
        WHERE dosname = " &
        Chr(34) & ComboBoxEdit1.Text.ToString & Chr(34) & "
        ORDER BY id", 2)
        For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
            str = ButtonEdit1.Text.ToString & "\档案整理\" & dr.Item(0)
            System.IO.Directory.CreateDirectory(str)
            '生成封面
            Try
                fwo1.dc_danganfm(str, dr.Item(0), dr.Item(1))
            Catch ex As Exception
                wf1.wclose()
                MsgBox("警告：盒号" & dr.Item(0) & "案卷封面生成错误", 0 + 48, "warning")
                Exit Sub
            End Try
            '生成目录
            Try
                fwo1.dc_danganml(str, dr.Item(0), dr.Item(1))
            Catch ex As Exception
                wf1.wclose()
                MsgBox("警告：盒号" & dr.Item(0) & "卷内目录生成错误", 0 + 48, "warning")
                Exit Sub
            End Try
            '生成备考表
            Try
                fwo1.dc_danganbkb(str, dr.Item(0), dr.Item(1))
            Catch ex As Exception
                wf1.wclose()
                MsgBox("警告：盒号" & dr.Item(0) & "备考表生成错误", 0 + 48, "warning")
                Exit Sub
            End Try
        Next
        '导出脊背格式
        Try
            fo1.dc_danganbj(ButtonEdit1.Text.ToString & "\档案整理")
        Catch ex As Exception
            wf1.wclose()
            MsgBox("警告：背脊格式生成错误", 0 + 48, "warning")
            Exit Sub
        End Try
        wf1.wclose()
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub


End Class