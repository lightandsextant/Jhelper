﻿Public Class Form6
    Public rep_id As Integer
    Public exp_id As Integer
    Private Sub Form6_Load(sender As Object, e As EventArgs) Handles Me.Load
        '赋值comobox1
        SkinComboBox1.Items.Clear()
        SkinComboBox1.Items.Add("资格不符")
        SkinComboBox1.Items.Add("实质不符")
        SkinComboBox1.Items.Add("保证金不符")
        SkinComboBox1.Items.Add("文件签署不符")
        SkinComboBox1.Items.Add("未选择")
        '赋值comobox2
        SkinComboBox2.Items.Clear()
        '赋值专家名称
        Form1.sqlcon1.sqlstr("
        SELECT char_gp,expname
        FROM expert,expert_config
        WHERE expert_config.expid = expert.id 
        AND (expert_config.char_gp LIKE " &
        Chr(34) & "商务%" & Chr(34) & "
        OR expert_config.char_gp LIKE " &
        Chr(34) & "技术%" & Chr(34) & ")
        AND oveid = " &
        Form3.pro_id, 1
                             )
        While Form1.sqlcon1.dataf.Read()
            SkinComboBox2.Items.Add(
            Form1.sqlcon1.dataf.Item(0) & " " &
            Form1.sqlcon1.dataf.Item(1))
        End While
        SkinComboBox2.Items.Add("未选择")
    End Sub

    Private Sub SkinComboBox1_SelectedValueChanged(sender As Object, e As EventArgs) Handles SkinComboBox1.SelectedValueChanged
        Dim str As String

        str = SkinComboBox1.Text
        Select Case str
            Case "资格不符"
                SkinTextBox1.Text = "采购文件第三章2.1.3在评审过程中发现下列情况之一的，应答将被否决：" &
                    "不满足本次采购文件要求的应答人资格条件的。第一章采购公告："
                SkinTextBox2.Text = "应答人应答文件中未提供***" & vbCrLf &
                    "，不满足采购文件要求，其应答被否决。"
            Case "实质不符"
                SkinTextBox1.Text = "采购文件第三章2.1.3在评审过程中发现下列情况之一的，应答将被否决：" &
                    "应答文件技术标准和要求不符合采购文件技术标准和要求的实质性规定的。"
                SkinTextBox2.Text = "应答人应答文件中未提供***" & vbCrLf &
                    "，不满足采购文件要求，其应答被否决。"
            Case "保证金不符"
                SkinTextBox1.Text = "采购文件第三章2.1.3在评审过程中发现下列情况之一的，应答将被否决：" &
                    "未按采购文件要求提交应答保证金。"
                SkinTextBox2.Text = "应答人应答文件中未提交应答保证金，不满足采购文件要求，其应答被否决。"
            Case "文件签署不符"
                SkinTextBox1.Text = "采购文件第三章2.1.3在评审过程中发现下列情况之一的，应答将被否决：" &
                    "纸质应答文件未经应答单位盖章和法定代表人（或法定代表人授权的委托代理人）签字（签名章）的。"
                SkinTextBox2.Text = "应答人应答文件中未提供***" & vbCrLf &
                    "，不满足采购文件要求，其应答被否决。"
            Case Else
                SkinTextBox1.Text = ""
                SkinTextBox2.Text = ""
        End Select

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim str As String

        '商务废标引用repeal_click
        str = Button1.Text.ToString
        '设置废标相关表
        Call repeal_click(str)
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim str As String

        '技术废标引用repeal_click
        str = Button2.Text.ToString
        Call repeal_click(str)
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim i As Integer
        Dim dr As DataRow
        Dim str As String

        If MsgBox("注意：是否确定取消废标", 4 + 32, "caution") = MsgBoxResult.Yes Then
            '获取repeal_config.id,repid
            Form1.sqlcon1.sqlstr("
            SELECT id,repid 
            FROM repeal_config 
            WHERE oveid = " & Form3.pro_id & "
            AND pkgid = " & Form3.pkg_id & "
            AND facid = " & Form3.fac_id, 2
                                 )
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                For i = 0 To 1
                    Select Case i
                        Case 0
                            '删除repeal_config
                            Form1.sqlcon1.sqlstr("
                            DELETE FROM repeal_config 
                            WHERE id = " & dr.Item(i)
                                                 )
                        Case 1
                            '删除repeael
                            Form1.sqlcon1.sqlstr("
                            DELETE FROM repeal 
                            WHERE id = " & dr.Item(i)
                                                 )
                    End Select
                Next
            Next
            '更新factory_config.repst
            Form1.sqlcon1.sqlstr("
            UPDATE factory_config 
            SET repst = " & Chr(34) & Chr(34) & "
            WHERE oveid = " & Form3.pro_id & "
            AND pkgid = " & Form3.pkg_id & "
            AND facid = " & Form3.fac_id
                                 )
            '更新form3.listview
            str = "SELECT tb1.facname,tb1.repst,tb2.couid,tb2.price
            FROM
            (SELECT factory.facname,factory_config.repst,factory_config.id
            FROM factory,factory_config
            WHERE factory_config.oveid = " &
            Form3.pro_id & "
            AND factory_config.pkgid = " &
            Form3.pkg_id & "
            AND factory_config.facid = factory.id
            AND factory_config.disst = " &
            Chr(34) & Chr(34) & ") AS tb1
            LEFT JOIN
            (SELECT price_config.fngid,price_config.couid,price_rounds.price
            FROM price_config,price_rounds
            WHERE price_config.id = price_rounds.preid
            AND price_config.couid = price_rounds.rouid) AS tb2
            ON tb1.id = tb2.fngid"
            '刷新slistview1
            's为sqlstr，i为列数
            Call Form3.slvu(str, 4)
            '退出form6
            Me.Close()
        End If

    End Sub

    Sub repeal_click(s As String)
        Dim i As Integer = 0
        Dim str As String = ""

        '判断选择项及废标描述不为空
        If SkinComboBox1.Text = "" Or SkinComboBox2.Text = "" Then
            MsgBox("警告：废标模板或提出专家不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        If SkinTextBox1.Text = "" Or SkinTextBox2.Text = "" Then
            MsgBox("警告：采购文件要求或存在问题不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '确定废标
        If MsgBox("注意：是否确定" & s, 4 + 32, "caution") = MsgBoxResult.Yes Then
            '查询是否存在repeal_config表id区分商务技术废标

            Try
                '查询存在repeal_config.id，repeal.id
                Form1.sqlcon1.sqlstr("
                SELECT repeal_config.id,repeal_config.repid
                FROM repeal,repeal_config 
                WHERE oveid = " &
                Form3.pro_id & "
                AND pkgid = " &
                Form3.pkg_id & "
                AND facid = " &
                Form3.fac_id & "
                AND repeal.id = repeal_config.repid 
                AND repeal.reptype = " &
                Chr(34) & s & Chr(34), 1
                                     )
                Form1.sqlcon1.dataf.Read()
                rep_id = Form1.sqlcon1.dataf.Item(0)
                i = Form1.sqlcon1.dataf.Item(1)
                '更新repeal表
                Form1.sqlcon1.sqlstr("
                UPDATE repeal 
                SET repname = " &
                Chr(34) & SkinComboBox1.Text & Chr(34) & "
                ,repqust = " &
                Chr(34) & SkinTextBox1.Text & Chr(34) & "
                ,repreal = " &
                Chr(34) & SkinTextBox2.Text & Chr(34) & "
                WHERE id = " &
                i
                                     )
                '更新repeal_config表
                Form1.sqlcon1.sqlstr("
                UPDATE repeal_config 
                SET ecoid = " &
                exp_id & "
                WHERE id = " &
                rep_id
                                     )
            Catch ex As Exception
                '不存在repeal.id,插入repeal表
                Form1.sqlcon1.sqlstr("
                INSERT INTO 
                repeal(repname,reptype,repqust,repreal) 
                VALUES 
                (" &
                Chr(34) & SkinComboBox1.Text & Chr(34) & "," &
                Chr(34) & s & Chr(34) & "," &
                Chr(34) & SkinTextBox1.Text & Chr(34) & "," &
                Chr(34) & SkinTextBox2.Text & Chr(34) &
                ")"
                                     )
                '赋值rep表id
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM repeal 
                WHERE repname = " &
                Chr(34) & SkinComboBox1.Text & Chr(34) & "
                AND reptype = " &
                Chr(34) & s & Chr(34) & "
                AND repqust = " &
                Chr(34) & SkinTextBox1.Text & Chr(34) & "
                AND repreal = " &
                Chr(34) & SkinTextBox2.Text & Chr(34), 1
                                     )
                Form1.sqlcon1.dataf.Read()
                i = Form1.sqlcon1.dataf.Item(0)
                '插入repeal_config表
                Form1.sqlcon1.sqlstr("
                INSERT INTO 
                repeal_config(oveid,pkgid,facid,repid,ecoid) 
                VALUES 
                (" &
                Form3.pro_id & "," &
                Form3.pkg_id & "," &
                Form3.fac_id & "," &
                i & "," &
                exp_id & "
                )"
                                     )
            End Try
            '更新factory_config.repst
            Form1.sqlcon1.sqlstr("
            SELECT reptype 
            FROM repeal,repeal_config 
            WHERE oveid = " &
            Form3.pro_id & "
            AND pkgid = " &
            Form3.pkg_id & "
            AND facid = " &
            Form3.fac_id & "
            AND repeal.id = repeal_config.repid ", 1
                                 )
            While Form1.sqlcon1.dataf.Read()
                str &= Form1.sqlcon1.dataf.Item(0)
            End While
            Form1.sqlcon1.sqlstr("
            UPDATE factory_config 
            SET repst = " &
            Chr(34) & str & Chr(34) & "
            WHERE oveid = " &
            Form3.pro_id & "
            AND pkgid = " &
            Form3.pkg_id & "
            AND facid = " &
            Form3.fac_id
                                 )
            '更新form3.listview
            str = "SELECT tb1.facname,tb1.repst,tb2.couid,tb2.price
            FROM
            (SELECT factory.facname,factory_config.repst,factory_config.id
            FROM factory,factory_config
            WHERE factory_config.oveid = " &
            Form3.pro_id & "
            AND factory_config.pkgid = " &
            Form3.pkg_id & "
            AND factory_config.facid = factory.id
            AND factory_config.disst = " &
            Chr(34) & Chr(34) & ") AS tb1
            LEFT JOIN
            (SELECT price_config.fngid,price_config.couid,price_rounds.price
            FROM price_config,price_rounds
            WHERE price_config.id = price_rounds.preid
            AND price_config.couid = price_rounds.rouid) AS tb2
            ON tb1.id = tb2.fngid"
            '刷新slistview1
            's为sqlstr，i为列数
            Call Form3.slvu(str, 4)
            '退出form6
            Me.Close()
        End If

    End Sub

    Private Sub SkinComboBox2_SelectedValueChanged(sender As Object, e As EventArgs) Handles SkinComboBox2.SelectedValueChanged
        '获取expert_config表id
        Dim ar() As String

        If SkinComboBox2.Text.ToString <> "未选择" Then
            ar = Split(SkinComboBox2.Text, " ")
            Try
                Form1.sqlcon1.sqlstr("
                SELECT expert_config.id 
                FROM expert,expert_config 
                WHERE oveid = " & Form3.pro_id & "
                AND expert.id = expert_config.expid 
                AND expert.expname = " &
                Chr(34) & ar(1) & Chr(34) & "
                AND expert_config.char_gp = " &
                Chr(34) & ar(0) & Chr(34), 1
                                        )
                Form1.sqlcon1.dataf.Read()
                exp_id = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：发生未知错误 location:expert_config", 0 + 48, "warning")
                Exit Sub
            End Try
        Else
            exp_id = 0
        End If
    End Sub

End Class