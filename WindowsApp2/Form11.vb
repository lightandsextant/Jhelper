﻿Public Class Form11

    Private Sub Form11_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        Form1.Show()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Form12.ShowDialog()

    End Sub

    Private Sub Form11_Load(sender As Object, e As EventArgs) Handles Me.Load
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS cc_dossier(
        id      integer     PRIMARY KEY     AUTOINCREMENT,
        dosname varchar(255)                NOT NULL,
        dosno   varchar(255)                NOT NULL,
        boxidx  varchar(255)                NOT NULL,
        fileno  varchar(255)                NOT NULL,
        fileow  varchar(255)                NOT NULL,
        filena  varchar(255)                NOT NULL,
        filetye varchar(255)                NOT NULL,
        filedat integer                     NOT NULL,
        filepag integer                     NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS cc_dossier_config(
        id      integer     PRIMARY KEY     AUTOINCREMENT,
        dosname varchar(255)                NOT NULL,
        boxidx  varchar(255)                NOT NULL,
        fmstr   varchar(255)                NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS cc_dossier_config_fm(
        id      integer     PRIMARY KEY     AUTOINCREMENT,
        dosname varchar(255)                NOT NULL,
        boxidx  varchar(255)                NOT NULL,
        bidname varchar(255)                NOT NULL,
        pkgname varchar(255)                NOT NULL,
        filetye varchar(255)                NOT NULL
        );")
    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        MsgBox("提示：此功能暂未开放", 0 + 64, "information")
    End Sub
End Class