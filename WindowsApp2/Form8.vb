﻿Public Class Form8
    Private Sub Form8_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim i As Integer

        '清空skincombox、textbox2
        SkinComboBox1.Items.Clear()
        TextBox2.Text = ""
        '获取price_config轮次
        Form1.sqlcon1.sqlstr("
        SELECT couid 
        FROM price_config
        WHERE fngid = 
        (SELECT id 
        FROM factory_config
        WHERE facid = " &
        Form3.fac_id & "
        AND pkgid = " &
        Form3.pkg_id & "
        AND oveid = " &
        Form3.pro_id & ")", 1)
        Form1.sqlcon1.dataf.Read()
        '添加skincombobox数据
        If Form1.sqlcon1.dataf.HasRows Then
            For i = 1 To Form1.sqlcon1.dataf.Item(0) + 1
                SkinComboBox1.Items.Add("第 " & i & " 轮")
            Next
        Else
            SkinComboBox1.Items.Add("第 1 轮")
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim i, preid As Integer
        Dim ar(), str As String

        '验证skincombobox数值
        If SkinComboBox1.Text = "" Then
            MsgBox("警告：未选择报价轮次", 0 + 48, "warning")
            Exit Sub
        End If
        '验证textbox2.text格式
        If TextBox2.Text <> "" Then
            If Not IsNumeric(TextBox2.Text) Then
                MsgBox("警告：填写报价格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        End If
        Try
            ar = Split(SkinComboBox1.Text, " ")
            '获取fngid
            Form1.sqlcon1.sqlstr("
            SELECT id 
            FROM factory_config
            WHERE facid = " &
            Form3.fac_id & "
            AND pkgid = " &
            Form3.pkg_id & "
            AND oveid = " &
            Form3.pro_id, 1)
            Form1.sqlcon1.dataf.Read()
            i = Form1.sqlcon1.dataf.Item(0)
            '更新couid
            Form1.sqlcon1.sqlstr("
            SELECT couid
            FROM price_config
            WHERE fngid = " & i, 1)
            Form1.sqlcon1.dataf.Read()
            '不存在price_config添加，大于couid刷新
            If Form1.sqlcon1.dataf.HasRows Then
                If ar(1) > Form1.sqlcon1.dataf.Item(0) Then
                    Form1.sqlcon1.sqlstr("
                    UPDATE price_config 
                    SET couid = " & CInt(ar(1)) & "
                    WHERE fngid = " & i)
                End If
            Else
                Form1.sqlcon1.sqlstr("
                INSERT INTO price_config(fngid,couid) 
                VALUES (" & i & "," &
                CInt(ar(1)) &
                ")")
            End If
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:price_config", 0 + 48, "warning")
            Exit Sub
        End Try
        '获取preid
        Form1.sqlcon1.sqlstr("
        SELECT id 
        FROM price_config 
        WHERE fngid = " & i, 1)
        Form1.sqlcon1.dataf.Read()
        preid = Form1.sqlcon1.dataf.Item(0)
        Try
            '更新price_rounds
            Form1.sqlcon1.sqlstr("
            SELECT id 
            FROM price_rounds 
            WHERE preid = " & preid & "
            AND rouid = " & CInt(ar(1)), 1)
            Form1.sqlcon1.dataf.Read()
            If Form1.sqlcon1.dataf.HasRows Then
                '判断textbox2是否为空值
                If TextBox2.Text = "" Then
                    '存在price_rounds数据删除
                    Form1.sqlcon1.sqlstr("
                    DELETE FROM price_rounds 
                    WHERE id = " &
                    Form1.sqlcon1.dataf.Item(0))
                Else
                    '存在price_rounds数据更新
                    Form1.sqlcon1.sqlstr("
                    UPDATE price_rounds 
                    SET price = " &
                    Val(TextBox2.Text) & "
                    WHERE preid = " & preid & "
                    AND rouid = " & ar(1))
                End If
            Else
                '不存在price_rounds数据插入
                If TextBox2.Text = "" Then
                Else
                    Form1.sqlcon1.sqlstr("
                    INSERT INTO price_rounds(preid,rouid,price) 
                    VALUES (" &
                    preid & "," &
                    ar(1) & "," &
                    Val(TextBox2.Text) & ")")
                End If
            End If
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:price_rounds", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已写入本轮次报价", 0 + 64, "information")
        '刷新slistview1
        str = "SELECT tb1.facname,tb1.repst,tb2.couid,tb2.price
        FROM
        (SELECT factory.facname,factory_config.repst,factory_config.id
        FROM factory,factory_config
        WHERE factory_config.oveid = " &
        Form3.pro_id & "
        AND factory_config.pkgid = " &
        Form3.pkg_id & "
        AND factory_config.facid = factory.id
        AND factory_config.disst = " &
        Chr(34) & Chr(34) & ") AS tb1
        LEFT JOIN
        (SELECT price_config.fngid,price_config.couid,price_rounds.price
        FROM price_config,price_rounds
        WHERE price_config.id = price_rounds.preid
        AND price_config.couid = price_rounds.rouid) AS tb2
        ON tb1.id = tb2.fngid"
        's为sqlstr，i为列数
        Call Form3.slvu(str, 4)
        Me.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim ar() As String
        Dim i, j As Integer

        '验证skincombobox数值
        If SkinComboBox1.Text = "" Then
            MsgBox("警告：未选择报价轮次", 0 + 48, "warning")
            Exit Sub
        End If
        '验证textbox2数值
        If TextBox2.Text = "" Then
            MsgBox("警告：应答人此轮未报价", 0 + 48, "warning")
            Exit Sub
        End If
        '是否确定删除
        If MsgBox("注意：是否确定删除此轮报价", 4 + 32, "caution") = MsgBoxResult.Yes Then
        Else
            Exit Sub
        End If
        '确定删除操作
        Try
            ar = Split(SkinComboBox1.Text, " ")
            '获取fngid为i
            Form1.sqlcon1.sqlstr("
            SELECT id 
            FROM factory_config
            WHERE facid = " &
            Form3.fac_id & "
            AND pkgid = " &
            Form3.pkg_id & "
            AND oveid = " &
            Form3.pro_id, 1)
            Form1.sqlcon1.dataf.Read()
            i = Form1.sqlcon1.dataf.Item(0)
            '获取preid为j
            Form1.sqlcon1.sqlstr("
            SELECT id 
            FROM price_config
            WHERE fngid = " &
            i, 1
                                 )
            Form1.sqlcon1.dataf.Read()
            j = Form1.sqlcon1.dataf.Item(0)
            '删除price_rounds数据
            Form1.sqlcon1.sqlstr("
            DELETE FROM price_rounds 
            WHERE preid = " & j & "
            AND rouid = " & CInt(ar(1))
                                )
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:price_config", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已删除此轮报价", 0 + 64, "information")
        '清空textbox2数据
        TextBox2.Text = ""
    End Sub

    Private Sub SkinComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles SkinComboBox1.SelectedIndexChanged
        Dim ar() As String
        Dim i, j As Integer

        '清空textbox2数据
        TextBox2.Text = ""
        Try
            ar = Split(SkinComboBox1.Text, " ")
            '获取fngid为i
            Form1.sqlcon1.sqlstr("
            SELECT id 
            FROM factory_config
            WHERE facid = " &
            Form3.fac_id & "
            AND pkgid = " &
            Form3.pkg_id & "
            AND oveid = " &
            Form3.pro_id, 1)
            Form1.sqlcon1.dataf.Read()
            If Form1.sqlcon1.dataf.HasRows Then
                i = Form1.sqlcon1.dataf.Item(0)
            End If
            '获取preid为j
            Form1.sqlcon1.sqlstr("
            SELECT id 
            FROM price_config
            WHERE fngid = " &
            i, 1
                                 )
            Form1.sqlcon1.dataf.Read()
            If Form1.sqlcon1.dataf.HasRows Then
                j = Form1.sqlcon1.dataf.Item(0)
            End If
            '获取price_rounds数据
            Form1.sqlcon1.sqlstr("
            SELECT price 
            FROM price_rounds 
            WHERE preid = " & j & "
            AND rouid = " & CInt(ar(1)), 1
                                )
            Form1.sqlcon1.dataf.Read()
            If Form1.sqlcon1.dataf.HasRows Then
                TextBox2.Text = Form1.sqlcon1.dataf.Item(0).ToString
            End If
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:price_config", 0 + 48, "warning")
            Exit Sub
        End Try
    End Sub

End Class