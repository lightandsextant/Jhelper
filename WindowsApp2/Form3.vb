﻿Imports NPOI
Imports NPOI.HSSF.UserModel
Imports NPOI.XSSF.UserModel
Imports System.IO
Imports Aspose.Words
Imports System.Data.SQLite
Imports System.Text.RegularExpressions

Public Class Form3
    Public pro_id As Integer
    Public bid_id As Integer
    Public pkg_id As Integer
    Public eng_id As Integer
    Public fac_id As Integer
    Public tvidx As Integer
    Public xlscon1 As New xlscon
    Public wf1 As New wf
    Public navbarstr As String

    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles Me.Load
        '测试
        GroupBox1.Top = 12
        '初始化form3
        GroupBox1.Visible = False
        GroupBox2.Visible = False
        Panel2.Visible = False
        Label12.Visible = True

        '获取pro_id值
        Form1.sqlcon1.sqlstr("SELECT id FROM overload 
        WHERE proname = " &
        Chr(34) & Form1.ListView1.SelectedItems(0).Text & Chr(34), 1
                             )
        Form1.sqlcon1.dataf.Read()
        pro_id = Form1.sqlcon1.dataf.Item(0)
        '检查bid表，不存在创建
        '检查package表，不存在创建
        '检查engineering表，不存在创建
        '检查factory表，不存在创建
        '检查repeal表，不存在创建
        '检查expert表，不存在创建
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bid(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        bididx  varchar(255)                    ,
        bidname varchar(255)            NOT NULL,
        bidno   varchar(255)            NOT NULL,
        oveid   integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS package(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        pkgidx  varchar(255)            NOT NULL,
        pkgname varchar(255)            NOT NULL,
        bidid   integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS engineering(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        engidx  varchar(255)                    ,
        engname varchar(255)            NOT NULL,
        pkgid   integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS factory(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        facname varchar(255)            NOT NULL,
        faccont varchar(255)            NOT NULL,
        facline varchar(255)            NOT NULL,
        facmail varchar(255)            NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS repeal(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        repname varchar(255)            NOT NULL,
        reptype varchar(255)            NOT NULL,
        repqust varchar(255)            NOT NULL,
        repreal varchar(255)            NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS expert(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        expname varchar(255)            NOT NULL,
        expiden varchar(255)            NOT NULL,
        expprof varchar(255)            NOT NULL,
        expadds varchar(255)            NOT NULL,
        expline varchar(255)            NOT NULL,
        expbnka varchar(255)            NOT NULL,
        expbnkn varchar(255)            NOT NULL
        );")
        '检查bid-config,nr-采购内容,xs-限授
        '检查package-config,gk-公开形式,fs-采购方式,bj-报价格式,ps-评审方法
        '检查engineering-config,gs-概算,xj-限价
        '检查factory-config
        '检查package_request,zz-资质要求,yj-业绩要求
        '检查price_config,fngid-factory_config应答人设置id,couid-应答报价数id
        '检查price_rounds,pre-price_config报价设置id,rouid-应答人报价轮次id
        '检查repeal_config
        '检查expert_config,gp-专家组别,po-专家职位
        '检查package_abort,aboid-流包类型（0-发售不足，1-递交不足，2-废标不足）
        '检查sorting表
        '检查sortig_price表
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bid_config(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        char_nr varchar(255)            NOT NULL,
        char_xs varchar(255)            NOT NULL,
        bidid   integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS package_config(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        char_gk varchar(255)            NOT NULL,
        char_fs varchar(255)            NOT NULL,
        char_bj varchar(255)            NOT NULL,
        char_ps varchar(255)            NOT NULL,
        pkgid   integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS engineering_config(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        char_gs numeric                 NOT NULL,
        char_xj numeric                 NOT NULL,
        engid   integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS factory_config(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        oveid   integer                 NOT NULL,
        pkgid   integer                 NOT NULL,
        facid   integer                 NOT NULL,
        disst   varchar(255)            NOT NULL,
        repst   varchar(255)            NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS package_request(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        oveid   integer                 NOT NULL,
        pkgid   integer                 NOT NULL,
        char_zz varchar(255)            NOT NULL,
        char_yj varchar(255)            NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS price_config(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        fngid   integer                 NOT NULL,
        couid   integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS price_rounds(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        preid   integer                 NOT NULL,
        rouid   integer                 NOT NULL,
        price   numeric                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS repeal_config(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        oveid   integer                 NOT NULL,
        pkgid   integer                 NOT NULL,
        facid   integer                 NOT NULL,
        repid   integer                 NOT NULL,
        ecoid   integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS expert_config(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        oveid   integer                 NOT NULL,
        expid   integer                 NOT NULL,
        char_gp varchar(255)            NOT NULL,
        char_po varchar(255)            NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS package_abort(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        pkgid   integer                 NOT NULL,
        aboid   integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS sorting(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        oveid   integer                 NOT NULL,
        pkgid   integer                 NOT NULL,
        facid   integer                 NOT NULL,
        char_sw numeric                 NOT NULL,
        char_js numeric                 NOT NULL,
        char_jg numeric                 NOT NULL,
        char_zf numeric                 NOT NULL,
        sort    integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS sorting_price(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        oveid   integer                 NOT NULL,
        pkgid   integer                 NOT NULL,
        facid   integer                 NOT NULL,
        char_bj numeric                 NOT NULL,
        sort    integer                 
        );")
        '检查bidoc_main表
        'mb-采购文件模板,ht-合同文本,cp-初评,xp-详评,jg-价格,chag-固定服务费,cou_gf-技术规范数量
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bidoc_main(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        oveid   integer                 NOT NULL,
        pkgid   integer                 NOT NULL,
        mbid    integer                 NOT NULL,
        htid    integer                 NOT NULL,
        swcpid  integer                 NOT NULL,
        jscpid  integer                 NOT NULL,
        swxpid  integer                 NOT NULL,
        jsxpid  integer                 NOT NULL,
        jgid    integer                 NOT NULL,
        chag    numeric                 NOT NULL,
        cou_gf  integer                 NOT NULL
        );")
    End Sub

    Private Sub Form3_Closed(sender As Object, e As EventArgs) Handles Me.Closed
        Form1.Show()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        GroupBox1.Text = "信息维护"
        GroupBox1.Visible = True
        GroupBox2.Visible = False
        Panel2.Visible = False
        Label12.Visible = False
        SkinListView1.Clear()
        TreeView1.Nodes.Clear()
        Call Me.tvu(1)
    End Sub

    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        GroupBox2.Text = "报表导出"
        GroupBox1.Visible = False
        GroupBox2.Visible = True
        Panel2.Visible = False
        Label12.Visible = False
        'SplitContainerControl1不显示
        SplitContainerControl1.Visible = False
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        GroupBox1.Text = "发售统计"
        GroupBox1.Visible = True
        GroupBox2.Visible = False
        Panel2.Visible = False
        Label12.Visible = False
        SkinListView1.Clear()
        TreeView1.Nodes.Clear()
        Call Me.tvu(1)
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        GroupBox1.Text = "专家管理"
        GroupBox1.Visible = True
        GroupBox2.Visible = False
        Panel2.Visible = False
        Label12.Visible = False
        SkinListView1.Items.Clear()
        TreeView1.Nodes.Clear()
        Call Me.tvu(2)
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        GroupBox1.Text = "评审设置"
        GroupBox1.Visible = True
        GroupBox2.Visible = False
        Panel2.Visible = False
        Label12.Visible = False
        SkinListView1.Clear()
        TreeView1.Nodes.Clear()
        Call Me.tvu(1)
    End Sub

    Private Sub PictureBox_MouseMove(sender As Object, e As MouseEventArgs) Handles PictureBox1.MouseMove,
    PictureBox2.MouseMove, PictureBox3.MouseMove, PictureBox4.MouseMove, PictureBox5.MouseMove
        Dim i As Integer
        i = Strings.Right(sender.name, 1)
        Me.Controls("label" & i).Font = New System.Drawing.Font("宋体", 10)
    End Sub

    Private Sub PictureBox_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox1.MouseLeave,
    PictureBox2.MouseLeave, PictureBox3.MouseLeave, PictureBox4.MouseLeave, PictureBox5.MouseLeave
        Dim i As Integer
        i = Strings.Right(sender.name, 1)
        Me.Controls("label" & i).Font = New System.Drawing.Font("宋体", 9)
    End Sub

    '刷新treeview
    Sub tvu(ByVal s As Int32)
        Dim str As String
        Dim i, j As Integer
        Dim dataf1 As SQLite.SQLiteDataReader
        '清空treeview
        TreeView1.Nodes.Clear()
        str = Form1.ListView1.SelectedItems(0).Text
        TreeView1.Nodes.Add("PRO", str, 0)
        Select Case s
            'case 1刷新treeview为分标-分包
            Case 1
                Try
                    '添加分标
                    Form1.sqlcon1.sqlstr("SELECT id,bididx,bidname FROM bid 
                    WHERE oveid = " &
                    pro_id, 1
                                         )
                    dataf1 = Form1.sqlcon1.dataf
                    'dataf1无数据时，抛出异常
                    If Form1.sqlcon1.dataf.HasRows = False Then
                        Throw New Exception
                    End If
                    While dataf1.Read()
                        '赋值i为bidid
                        i = dataf1.Item(0)
                        '赋值str为bididx & bidname
                        str = dataf1.Item(1) & " " & dataf1.Item(2)
                        TreeView1.Nodes("PRO").Nodes.Add("BID" & i, str, 0)
                        '添加分包
                        Form1.sqlcon1.sqlstr("SELECT * FROM package
                        WHERE bidid = " &
                        i, 1
                                                )
                        While Form1.sqlcon1.dataf.Read()
                            '赋值j为pkgid
                            j = Form1.sqlcon1.dataf.Item(0)
                            '赋值str为pkgidx & pkgname
                            str = Form1.sqlcon1.dataf.Item(1) & " " & Form1.sqlcon1.dataf.Item(2)
                            TreeView1.Nodes("PRO").Nodes("BID" & i).Nodes.Add("PKG" & j, str, 0)
                        End While
                    End While
                Catch ex As Exception
                    MsgBox("警告：请添加分标信息", 0 + 48, "warning")
                    Exit Sub
                End Try
            'case 2刷新treeview为组别-组内职位
            Case 2
                Try
                    '添加组别
                    Form1.sqlcon1.sqlstr("SELECT id,char_gp,char_po FROM expert_config 
                    WHERE oveid = " &
                    pro_id & "
                    GROUP BY char_gp,char_po
                    ORDER BY id", 1
                                         )
                    dataf1 = Form1.sqlcon1.dataf
                    'dataf1无数据时，抛出异常
                    If Form1.sqlcon1.dataf.HasRows = False Then
                        Throw New Exception
                    End If
                    While dataf1.Read()
                        '赋值i为expert_config id
                        i = dataf1.Item(0)
                        '赋值str为char_gp
                        str = dataf1.Item(1) & " " & dataf1.Item(2)
                        TreeView1.Nodes("PRO").Nodes.Add("GROUP" & i, str, 0)
                    End While
                Catch ex As Exception
                    MsgBox("警告：请添加评审专家信息", 0 + 48, "warning")
                    Exit Sub
                End Try
        End Select

    End Sub

    '选择treeview后
    Private Sub TreeView1_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles TreeView1.AfterSelect
        Dim str, n As String
        Dim ar() As String

        tvidx = TreeView1.SelectedNode.Level
        SkinListView1.Columns.Clear()
        '专家管理界面
        If GroupBox1.Text = "专家管理" Then
            Select Case tvidx
                Case 0
                    SkinListView1.Columns.Add("评审组名称", 250)
                    SkinListView1.Columns.Add("组内职位", 250)
                    str = "
                    SELECT char_gp,char_po 
                    FROM expert_config 
                    WHERE oveid = " & pro_id & "
                    GROUP BY char_gp,char_po"
                    '刷新slistview1
                    's为sqlstr，i为列数
                    Call slvu(str, 2)
                Case 1
                    SkinListView1.Columns.Add("专家姓名", 100)
                    SkinListView1.Columns.Add("身份证号", 250)
                    SkinListView1.Columns.Add("专业名称", 250)
                    SkinListView1.Columns.Add("工作单位", 250)
                    ar = Split(TreeView1.SelectedNode.Text, " ")
                    str = "
                    SELECT expname,expiden,expprof,expadds 
                    FROM expert,expert_config 
                    WHERE expert_config.expid = expert.id 
                    AND expert_config.char_gp = " &
                    Chr(34) & ar(0) & Chr(34) & "
                    AND expert_config.char_po = " &
                    Chr(34) & ar(1) & Chr(34) & "
                    AND expert_config.oveid = " &
                    pro_id
                    '刷新slistview1
                    's为sqlstr，i为列数
                    Call slvu(str, 4)
            End Select
            Exit Sub
        End If
        '除专家管理界面外
        '确定treeview选择级别
        Select Case tvidx
            Case 0
                SkinListView1.Columns.Add("分标号", 80)
                SkinListView1.Columns.Add("分标名称", 200)
                SkinListView1.Columns.Add("分标编号", 250)
                str = "
                SELECT bididx,bidname,bidno
                FROM bid
                WHERE oveid = " & pro_id & "
                ORDER BY id"
                '刷新slistview1
                's为sqlstr，i为列数
                Call slvu(str, 3)
            Case 1
                '赋值bid_id
                str = TreeView1.SelectedNode.Text
                n = str.Substring(InStr(str, " "), Len(str) - InStr(str, " ")).ToString
                Dim sqlq As New sqlqust



                Form1.sqlcon1.sqlstr("
                SELECT id
                FROM bid 
                WHERE bidname = " & Chr(34) & n & Chr(34) & "
                AND oveid = " & pro_id, 1)
                Form1.sqlcon1.dataf.Read()
                bid_id = Form1.sqlcon1.dataf.Item(0)
                '初始化skinlistview1
                SkinListView1.Columns.Add("分包号", 80)
                SkinListView1.Columns.Add("分包名称", 500)
                str = "
                SELECT pkgidx,pkgname
                FROM package
                WHERE bidid = " & bid_id & "
                ORDER BY id"
                '刷新slistview1
                's为sqlstr，i为列数
                Call slvu(str, 2)
            Case 2
                '赋值bid_id，防止误点击包
                str = TreeView1.SelectedNode.Parent.Text
                n = str.Substring(InStr(str, " "), Len(str) - InStr(str, " ")).ToString
                Form1.sqlcon1.sqlstr("
                SELECT id
                FROM bid 
                WHERE bidname = " & Chr(34) & n & Chr(34) & "
                AND oveid = " & pro_id, 1)
                Form1.sqlcon1.dataf.Read()
                bid_id = Form1.sqlcon1.dataf.Item(0)
                '赋值pkg_id
                str = TreeView1.SelectedNode.Text
                n = str.Substring(0, InStr(str, " ") - 1).ToString
                Form1.sqlcon1.sqlstr("
                SELECT id
                FROM package 
                WHERE pkgidx = " & Chr(34) & n & Chr(34) & "
                AND bidid = " & bid_id, 1)
                Form1.sqlcon1.dataf.Read()
                pkg_id = Form1.sqlcon1.dataf.Item(0)
                '按groupbox名称分别更新skinlistview1
                Select Case GroupBox1.Text
                    Case "信息维护"
                        '初始化skinlistview1
                        SkinListView1.Columns.Add("工程名称", 300)
                        SkinListView1.Columns.Add("概算金额", 80)
                        SkinListView1.Columns.Add("最高限价", 80)
                        str = "
                        SELECT engname,char_gs,char_xj
                        FROM engineering,engineering_config 
                        WHERE engineering.id = engineering_config.engid" & "
                        AND engineering.pkgid = " & pkg_id & "
                        ORDER BY engineering.id"
                        '刷新slistview1
                        's为sqlstr，i为列数
                        Call slvu(str, 3)
                    Case "发售统计"
                        '初始化skinlistview1
                        SkinListView1.Columns.Add("单位名称", 300)
                        SkinListView1.Columns.Add("弃标情况", 80)
                        SkinListView1.Columns.Add("联系人", 100)
                        SkinListView1.Columns.Add("电话", 160)
                        SkinListView1.Columns.Add("邮箱", 200)
                        str = "
                        SELECT facname,disst,faccont,facline,facmail 
                        FROM factory,factory_config 
                        WHERE oveid = " & pro_id & "
                        AND pkgid = " & pkg_id & "
                        AND factory_config.facid = factory.id"
                        '刷新slistview1
                        's为sqlstr，i为列数
                        Call slvu(str, 5)
                    Case "评审设置"
                        '初始化skinlistview1
                        SkinListView1.Columns.Add("单位名称", 300)
                        SkinListView1.Columns.Add("废标情况", 250)
                        SkinListView1.Columns.Add("已报价轮次", 120)
                        SkinListView1.Columns.Add("最新报价", 180)
                        str = "
                        SELECT tb1.facname,tb1.repst,tb2.couid,tb2.price
                        FROM (
                        SELECT factory.facname,factory_config.repst,factory_config.id
                        FROM factory,factory_config
                        WHERE factory_config.oveid = " & pro_id & "
                        AND factory_config.pkgid = " & pkg_id & "
                        AND factory_config.facid = factory.id
                        AND factory_config.disst = " & Chr(34) & Chr(34) & ") AS tb1
                        LEFT JOIN (
                        SELECT price_config.fngid,price_config.couid,price_rounds.price
                        FROM price_config,price_rounds
                        WHERE price_config.id = price_rounds.preid
                        AND price_config.couid = price_rounds.rouid) AS tb2
                        ON tb1.id = tb2.fngid"
                        '刷新slistview1
                        's为sqlstr，i为列数
                        Call slvu(str, 4)
                End Select
        End Select

    End Sub

    '新增button
    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        Dim str, n As String
        Dim strbid, strpkg As String()

        Try
            tvidx = TreeView1.SelectedNode.Level
            Select Case tvidx
                Case 0
                    Select Case GroupBox1.Text
                        Case "信息维护"
                            Form4.GroupBox1.Visible = True
                            Form4.GroupBox2.Visible = False
                            Form4.GroupBox3.Visible = False
                            '初始化form4单元格
                            Form4.TextBox1.Text = ""
                            Form4.TextBox2.Text = ""
                            Form4.TextBox3.Text = ""
                            '初始化form4combobox
                            Form4.SkinComboBox1.Items.Clear()
                            Form4.SkinComboBox2.Items.Clear()
                            Form4.SkinComboBox1.Items.Add("采购内容")
                            Form4.SkinComboBox1.Items.Add("限授情况")
                            Form4.ShowDialog()
                        Case "专家管理"
                            Form7.GroupBox1.Visible = True
                            Form7.GroupBox2.Visible = False
                            Form7.TextBox1.Text = ""
                            Form7.TextBox2.Text = ""
                            Form7.TextBox3.Text = ""
                            Form7.TextBox4.Text = ""
                            Form7.SkinListView1.Columns.Clear()
                            Form7.ShowDialog()
                        Case Else
                            MsgBox("警告：请在信息维护中新增项目", 0 + 48, "warning")
                            Exit Sub
                    End Select
                Case 1
                    Select Case GroupBox1.Text
                        Case "信息维护"
                            '赋值bid_id
                            str = TreeView1.SelectedNode.Text
                            n = str.Substring(InStr(str, " "), Len(str) - InStr(str, " ")).ToString
                            Form1.sqlcon1.sqlstr("SELECT id FROM bid 
                            WHERE bidname = " &
                            Chr(34) & n & Chr(34) &
                            "AND oveid = " &
                            pro_id, 1
                                                 )
                            Form1.sqlcon1.dataf.Read()
                            bid_id = Form1.sqlcon1.dataf.Item(0)
                            Form4.GroupBox1.Visible = False
                            Form4.GroupBox2.Visible = True
                            Form4.GroupBox3.Visible = False
                            '初始化form4单元格
                            Form4.TextBox5.Text = ""
                            Form4.TextBox6.Text = ""
                            '初始化form4combobox
                            Form4.SkinComboBox3.Items.Clear()
                            Form4.SkinComboBox4.Items.Clear()
                            Form4.SkinComboBox4.Items.Add("公开形式")
                            Form4.SkinComboBox4.Items.Add("采购方式")
                            Form4.SkinComboBox4.Items.Add("报价格式")
                            Form4.SkinComboBox4.Items.Add("评审方法")
                            Form4.ShowDialog()
                        Case "专家管理"
                            Form7.GroupBox1.Visible = True
                            Form7.GroupBox2.Visible = False
                            Form7.TextBox1.Text = ""
                            Form7.TextBox2.Text = ""
                            Form7.TextBox3.Text = ""
                            Form7.TextBox4.Text = ""
                            Form7.SkinListView1.Columns.Clear()
                            Form7.ShowDialog()
                        Case Else
                            MsgBox("警告：请在信息维护中新增项目", 0 + 48, "warning")
                            Exit Sub
                    End Select
                Case 2
                    '赋值pkg_id
                    str = TreeView1.SelectedNode.Text
                    '获取包号注意空格-1
                    n = str.Substring(0, InStr(str, " ") - 1).ToString
                    Form1.sqlcon1.sqlstr("SELECT id FROM package 
                    WHERE pkgidx = " &
                    Chr(34) & n & Chr(34) &
                    "AND bidid = " &
                    bid_id, 1
                                         )
                    Form1.sqlcon1.dataf.Read()
                    pkg_id = Form1.sqlcon1.dataf.Item(0)
                    Select Case GroupBox1.Text
                        Case "信息维护"
                            Form4.GroupBox1.Visible = False
                            Form4.GroupBox2.Visible = False
                            Form4.GroupBox3.Visible = True
                            '初始化form4单元格
                            Form4.TextBox4.Text = ""
                            Form4.TextBox7.Text = ""
                            '初始化form4combobox
                            Form4.SkinComboBox5.Items.Clear()
                            Form4.SkinComboBox6.Items.Clear()
                            Form4.SkinComboBox6.Items.Add("概算金额")
                            Form4.SkinComboBox6.Items.Add("最高限价")
                            Form4.ShowDialog()
                        Case "发售统计"
                            Form5.GroupBox1.Text = "新增应答人"
                            '获取bid_id,pkg_id
                            Try
                                strpkg = TreeView1.SelectedNode.Text.Split(" ")
                                strbid = TreeView1.SelectedNode.Parent.Text.Split(" ")
                                Form1.sqlcon1.sqlstr("
                                SELECT bid.id,package.id
                                FROM bid,package
                                WHERE bid.oveid = " & pro_id & "
                                AND bid.bidname = " & Chr(34) & strbid(1) & Chr(34) & "
                                AND package.pkgidx = " & Chr(34) & strpkg(0) & Chr(34) & "
                                AND package.bidid = bid.id", 1)
                                If Form1.sqlcon1.dataf.HasRows Then
                                    Form1.sqlcon1.dataf.Read()
                                    bid_id = Form1.sqlcon1.dataf.Item(0)
                                    pkg_id = Form1.sqlcon1.dataf.Item(1)
                                Else
                                    Throw New Exception
                                End If
                            Catch ex As Exception
                                MsgBox("警告：发生未知错误 location:pkg_id", 0 + 48, "warning")
                                Exit Sub
                            End Try
                            '初始化form5单元格
                            Form5.TextBox1.Text = ""
                            Form5.TextBox2.Text = ""
                            Form5.TextBox3.Text = ""
                            Form5.TextBox4.Text = ""
                            Form5.ShowDialog()
                        Case "评审设置"
                            MsgBox("警告：请在发售统计中新增项目", 0 + 48, "warning")
                            Exit Sub
                    End Select
            End Select
        Catch ex As Exception
            MsgBox("警告：请选择左侧要新增的项目", 0 + 48, "warning")
        End Try
    End Sub

    '删除button
    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        Dim str As String
        Dim i As Integer

        Try
            '确定treeview选择级别
            tvidx = TreeView1.SelectedNode.Level
            str = SkinListView1.SelectedItems(0).Text
            If MsgBox("注意：是否确定删除选择项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                Select Case tvidx
                    Case 0
                        str = SkinListView1.SelectedItems(0).SubItems(1).Text
                        Select Case GroupBox1.Text
                            Case "信息维护"
                                '检查bidid，删除bid_config表data
                                Form1.sqlcon1.sqlstr("SELECT id FROM bid 
                                WHERE bidname = " &
                                Chr(34) & str & Chr(34) & "
                                AND oveid = " &
                                pro_id, 1
                                                    )
                                Try
                                    Form1.sqlcon1.dataf.Read()
                                    i = Form1.sqlcon1.dataf.Item(0)
                                    '判断是否存在子项
                                    Form1.sqlcon1.sqlstr("SELECT id FROM package 
                                    WHERE bidid = " &
                                    i, 1
                                                         )
                                    If Form1.sqlcon1.dataf.HasRows Then
                                        MsgBox("警告：此项目存在子项无法删除", 0 + 48, "warning")
                                        Exit Sub
                                    End If
                                    Form1.sqlcon1.sqlstr("DELETE FROM bid_config
                                    WHERE bidid = " &
                                    i
                                                            )
                                Catch ex As Exception
                                    MsgBox("警告：发生未知错误 location:bid_config", 0 + 48, "warning")
                                    Exit Sub
                                End Try
                                Form1.sqlcon1.sqlstr("DELETE FROM bid
                                WHERE bidname = " &
                                Chr(34) & str & Chr(34) & "
                                AND oveid = " &
                                pro_id
                                                        )
                                '刷新slistview1
                                str = "SELECT bididx,bidname,bidno FROM bid
                                WHERE oveid = " &
                                pro_id & "
                                ORDER BY id"
                                Call slvu(str, 3)
                            Case "专家管理"
                                Form1.sqlcon1.sqlstr("
                                DELETE FROM expert_config 
                                WHERE oveid = " &
                                pro_id & "
                                AND char_gp = " &
                                Chr(34) & SkinListView1.SelectedItems(0).Text & Chr(34) & "
                                AND char_po = " &
                                Chr(34) & SkinListView1.SelectedItems(0).SubItems(1).Text & Chr(34))
                                str = "
                                SELECT char_gp,char_po 
                                FROM expert_config 
                                WHERE oveid = " & pro_id & "
                                GROUP BY char_gp,char_po"
                                '刷新slistview1
                                's为sqlstr，i为列数
                                Call slvu(str, 2)
                            Case Else
                                MsgBox("警告：请在信息维护中删除项目", 0 + 48, "warning")
                                Exit Sub
                        End Select
                    Case 1
                        str = SkinListView1.SelectedItems(0).Text
                        Select Case GroupBox1.Text
                            Case "信息维护"
                                '检查pkgid，删除package_config表data
                                Form1.sqlcon1.sqlstr("SELECT id FROM package 
                                WHERE pkgidx = " &
                                Chr(34) & str & Chr(34) & "
                                AND bidid = " &
                                bid_id, 1
                                                     )
                                Try
                                    Form1.sqlcon1.dataf.Read()
                                    i = Form1.sqlcon1.dataf.Item(0)
                                    '判断是否存在子项
                                    Form1.sqlcon1.sqlstr("SELECT id FROM engineering 
                                    WHERE pkgid = " &
                                    i, 1
                                                         )
                                    If Form1.sqlcon1.dataf.HasRows Then
                                        MsgBox("警告：此项目存在子项无法删除", 0 + 48, "warning")
                                        Exit Sub
                                    End If
                                    Form1.sqlcon1.sqlstr("DELETE FROM package_config
                                    WHERE pkgid = " &
                                    i
                                                         )
                                Catch ex As Exception
                                    MsgBox("警告：发生未知错误 location:package_config", 0 + 48, "warning")
                                    Exit Sub
                                End Try
                                Form1.sqlcon1.sqlstr("DELETE FROM package
                                WHERE pkgidx = " &
                                Chr(34) & str & Chr(34) & "
                                AND bidid = " &
                                bid_id
                                                     )
                                '刷新slistview1
                                str = "SELECT pkgidx,pkgname FROM package
                                WHERE bidid = " &
                                bid_id & "
                                ORDER BY id"
                                Call slvu(str, 2)
                            Case "专家管理"
                                '获取expert.id
                                Form1.sqlcon1.sqlstr("
                                SELECT id 
                                FROM expert 
                                WHERE expiden = " &
                                Chr(34) & SkinListView1.SelectedItems(0).SubItems(1).Text & Chr(34), 1)
                                Form1.sqlcon1.dataf.Read()
                                i = Form1.sqlcon1.dataf.Item(0)
                                '删除expert_config.expid
                                Form1.sqlcon1.sqlstr("
                                DELETE FROM expert_config 
                                WHERE expid = " & i)
                                str = "
                                SELECT char_gp,char_po 
                                FROM expert_config 
                                WHERE oveid = " & pro_id & "
                                GROUP BY char_gp,char_po"
                                '刷新slistview1
                                's为sqlstr，i为列数
                                Call slvu(str, 2)
                            Case Else
                                MsgBox("警告：请在信息维护中删除项目", 0 + 48, "warning")
                                Exit Sub
                        End Select
                    Case 2
                        str = SkinListView1.SelectedItems(0).Text
                        Select Case GroupBox1.Text
                            Case "信息维护"
                                '检查engid，删除engineering_config表data
                                Form1.sqlcon1.sqlstr("SELECT id FROM engineering 
                                WHERE engname = " &
                                Chr(34) & str & Chr(34) & "
                                AND pkgid = " &
                                pkg_id, 1
                                                     )
                                Try
                                    Form1.sqlcon1.dataf.Read()
                                    i = Form1.sqlcon1.dataf.Item(0)
                                    Form1.sqlcon1.sqlstr("DELETE FROM engineering_config
                                    WHERE engid = " &
                                    i
                                                         )
                                Catch ex As Exception
                                    MsgBox("警告：发生未知错误 location:package_config", 0 + 48, "warning")
                                    Exit Sub
                                End Try
                                Form1.sqlcon1.sqlstr("DELETE FROM engineering 
                                WHERE engname = " &
                                Chr(34) & str & Chr(34) & "
                                AND pkgid = " &
                                pkg_id
                                                     )
                                '刷新slistview1
                                str = "SELECT engname,char_gs,char_xj FROM engineering,engineering_config 
                                WHERE engineering.id = engineering_config.engid" &
                                " AND engineering.pkgid = " &
                                pkg_id &
                                " ORDER BY engineering.id"
                                Call slvu(str, 3)
                            Case "发售统计"
                                Try
                                    '删除factory_config表数据
                                    Form1.sqlcon1.sqlstr("
                                    DELETE FROM factory_config 
                                    WHERE oveid = " & pro_id & "
                                    AND pkgid = " & pkg_id & "
                                    AND facid IN (
                                    SELECT id FROM factory 
                                    WHERE facname = " &
                                    Chr(34) & str & Chr(34) & ")")
                                    str = "SELECT facname,disst,faccont,facline,facmail 
                                    FROM factory,factory_config 
                                    WHERE oveid = " &
                                                pro_id & "
                                    AND pkgid = " &
                                                pkg_id & "
                                    AND factory_config.facid = factory.id"
                                    '刷新slistview1
                                    's为sqlstr，i为列数
                                    Call slvu(str, 5)
                                Catch ex As Exception
                                    MsgBox("警告：发生未知错误 location:package_config", 0 + 48, "warning")
                                    Exit Sub
                                End Try
                            Case "评审设置"
                                MsgBox("警告：请在发售统计中删除项目", 0 + 48, "warning")
                                Exit Sub
                        End Select
                End Select
            Else
                Exit Sub
            End If
            '刷新treeview
            Select Case GroupBox1.Text
                Case "专家管理"
                    Call tvu(2)
                Case Else
                    Call tvu(1)
            End Select
            MsgBox("提示：已删除选择项目", 0 + 64, "information")
        Catch ex As Exception
            MsgBox("警告：请选择要删除的项目", 0 + 48, "warning")
        End Try
    End Sub

    '更新button
    Private Sub PictureBox8_Click(sender As Object, e As EventArgs) Handles PictureBox8.Click
        Dim strbid, strpkg As String()

        Select Case GroupBox1.Text
            Case "信息维护"
                Try
                    '判断treview选择级别
                    tvidx = TreeView1.SelectedNode.Level
                    Select Case tvidx
                        Case 0
                            Form4.TextBox1.Text = SkinListView1.SelectedItems(0).Text
                            Form4.TextBox2.Text = SkinListView1.SelectedItems(0).SubItems(1).Text
                            Form4.TextBox3.Text = SkinListView1.SelectedItems(0).SubItems(2).Text
                            Form4.GroupBox1.Visible = True
                            Form4.GroupBox2.Visible = False
                            Form4.GroupBox3.Visible = False
                            '初始化form4combobox
                            Form4.SkinComboBox1.Items.Clear()
                            Form4.SkinComboBox2.Items.Clear()
                            Form4.SkinComboBox1.Items.Add("采购内容")
                            Form4.SkinComboBox1.Items.Add("限授情况")
                        Case 1
                            Form4.TextBox6.Text = SkinListView1.SelectedItems(0).Text
                            Form4.TextBox5.Text = SkinListView1.SelectedItems(0).SubItems(1).Text
                            Form4.GroupBox1.Visible = False
                            Form4.GroupBox2.Visible = True
                            Form4.GroupBox3.Visible = False
                            '初始化form4combobox
                            Form4.SkinComboBox3.Items.Clear()
                            Form4.SkinComboBox4.Items.Clear()
                            Form4.SkinComboBox4.Items.Add("公开形式")
                            Form4.SkinComboBox4.Items.Add("采购方式")
                            Form4.SkinComboBox4.Items.Add("报价格式")
                            Form4.SkinComboBox4.Items.Add("评审方法")
                        Case 2
                            Form4.TextBox4.Text = SkinListView1.SelectedItems(0).Text
                            Form4.GroupBox1.Visible = False
                            Form4.GroupBox2.Visible = False
                            Form4.GroupBox3.Visible = True
                            '初始化form4combobox
                            Form4.SkinComboBox5.Items.Clear()
                            Form4.SkinComboBox6.Items.Clear()
                            Form4.SkinComboBox6.Items.Add("概算金额")
                            Form4.SkinComboBox6.Items.Add("最高限价")
                    End Select
                    Form4.GroupBox1.Text = "修改分标"
                    Form4.GroupBox2.Text = "修改分包"
                    Form4.GroupBox3.Text = "修改工程项目"
                    Form4.Text = "修改项目"
                    Form4.ShowDialog()
                Catch ex As Exception
                    MsgBox("警告：请选择要修改的项目", 0 + 48, "warning")
                    Exit Sub
                End Try
            Case "发售统计"
                Try
                    '判断treview选择级别
                    tvidx = TreeView1.SelectedNode.Level
                    Select Case tvidx
                        Case 2
                            '获取fac_id
                            strpkg = TreeView1.SelectedNode.Text.Split(" ")
                            strbid = TreeView1.SelectedNode.Parent.Text.Split(" ")
                            Form5.TextBox1.Text = SkinListView1.SelectedItems(0).Text
                            Try
                                Form1.sqlcon1.sqlstr("
                                SELECT facid 
                                FROM factory,factory_config,bid,package
                                WHERE bid.oveid = " & pro_id & "
                                AND bid.bidname = " & Chr(34) & strbid(1) & Chr(34) & "
                                AND package.pkgidx = " & Chr(34) & strpkg(0) & Chr(34) & "
                                AND package.bidid = bid.id
                                AND package.id = factory_config.pkgid
                                AND factory.facname = " &
                                Chr(34) & SkinListView1.SelectedItems(0).Text & Chr(34) & "
                                AND factory_config.facid = factory.id", 1)
                                If Form1.sqlcon1.dataf.HasRows Then
                                    Form1.sqlcon1.dataf.Read()
                                    fac_id = Form1.sqlcon1.dataf.Item(0)
                                Else
                                    Throw New Exception
                                End If
                            Catch ex As Exception
                                MsgBox("警告：发生未知错误 location:fac_id", 0 + 48, "warning")
                                Exit Sub
                            End Try
                            Form5.GroupBox1.Text = "修改应答人信息"
                            Form5.Text = "修改应答人信息"
                            Form5.ShowDialog()
                        Case Else
                            MsgBox("警告：请在信息维护中修改项目", 0 + 48, "warning")
                            Exit Sub
                    End Select
                Catch ex As Exception
                    MsgBox("警告：请选择要修改的项目", 0 + 48, "warning")
                    Exit Sub
                End Try
            Case "评审设置"
                Try
                    '判断treview选择级别
                    tvidx = TreeView1.SelectedNode.Level
                    Select Case tvidx
                        Case 2
                            MsgBox("警告：请在发售统计修改项目", 0 + 48, "warning")
                            Exit Sub
                        Case Else
                            MsgBox("警告：请在信息维护中修改项目", 0 + 48, "warning")
                            Exit Sub
                    End Select
                Catch ex As Exception
                    MsgBox("警告：请选择要修改的项目", 0 + 48, "warning")
                    Exit Sub
                End Try
            Case "专家管理"
                Try
                    '判断treview选择级别
                    tvidx = TreeView1.SelectedNode.Level
                    Form7.GroupBox1.Visible = True
                    Form7.GroupBox2.Visible = False
                    Form7.TextBox1.Text = ""
                    Form7.TextBox2.Text = ""
                    Form7.TextBox3.Text = ""
                    Form7.TextBox4.Text = ""
                    Form7.SkinListView1.Columns.Clear()
                    Form7.ShowDialog()
                Catch ex As Exception
                    MsgBox("警告：请选择要修改的项目", 0 + 48, "warning")
                    Exit Sub
                End Try
        End Select
    End Sub

    '设置button
    Private Sub PictureBox9_Click(sender As Object, e As EventArgs) Handles PictureBox9.Click
        Select Case GroupBox1.Text
            Case "信息维护"
                MsgBox("↑↑↓↓←→←→BABA", 0 + 64, "information")
            Case "发售统计"
                Panel2.Left = 150
                Panel2.Top = 75
                Button1.Visible = True
                Button2.Visible = False
                Button3.Visible = False
                Button4.Visible = False
                Button1.Text = "弃标"
                Panel2.Visible = True
            Case "评审设置"
                Panel2.Left = 150
                Panel2.Top = 75
                Button1.Visible = True
                Button2.Visible = True
                Button3.Visible = False
                Button4.Visible = False
                Button1.Text = "废标"
                Button2.Text = "报价"
                Panel2.Visible = True
            Case "专家管理"
                MsgBox("↑↑↓↓←→←→BABA", 0 + 64, "information")
        End Select

    End Sub

    '导入button
    Private Sub PictureBox10_Click(sender As Object, e As EventArgs) Handles PictureBox10.Click
        Select Case GroupBox1.Text
            Case "信息维护"
                '初始化panel2
                Panel2.Left = 200
                Panel2.Top = 75
                Button1.Visible = True
                Button2.Visible = True
                Button3.Visible = False
                Button4.Visible = False
                Button1.Text = "汇总"
                Button2.Text = "资质"
                Panel2.Visible = True
            Case "发售统计"
                '初始化panel2
                Panel2.Left = 200
                Panel2.Top = 75
                Button1.Visible = True
                Button2.Visible = True
                Button3.Visible = False
                Button4.Visible = False
                Button1.Text = "发售"
                Button2.Text = "弃标"
                Panel2.Visible = True
            Case "专家管理"
                '初始化panel2
                Panel2.Left = 200
                Panel2.Top = 75
                Button1.Visible = True
                Button2.Visible = False
                Button3.Visible = False
                Button4.Visible = False
                Button1.Text = "专家"
                Panel2.Visible = True
            Case "评审设置"
                '初始化panel2
                Panel2.Left = 200
                Panel2.Top = 75
                Button1.Visible = True
                Button2.Visible = True
                Button3.Visible = True
                Button4.Visible = False
                Button1.Text = "初评"
                Button2.Text = "报价"
                Button3.Text = "排序"
                Panel2.Visible = True
        End Select

    End Sub

    '查询button
    Private Sub PictureBox11_Click(sender As Object, e As EventArgs) Handles PictureBox11.Click
        MsgBox("提示：此功能暂未开放", 0 + 64, "information")
    End Sub

    Private Sub PictureBox12_Click(sender As Object, e As EventArgs) Handles PictureBox12.Click
        Panel2.Visible = False
    End Sub

    Private Sub Panel2_Leave(sender As Object, e As EventArgs) Handles Panel2.Leave
        Panel2.Visible = False
    End Sub

    Sub slvu(ByVal s As String, ByVal i As Int32)
        Dim dataf1 As SQLite.SQLiteDataReader

        SkinListView1.Items.Clear()
        Form1.sqlcon1.sqlstr(s, 1)
        dataf1 = Form1.sqlcon1.dataf
        While dataf1.Read()
            '定义新item类型
            Dim lvi As New ListViewItem(dataf1.Item(0).ToString, 0)
            Dim lvis0 As New ListViewItem.ListViewSubItem
            Dim lvis1 As New ListViewItem.ListViewSubItem
            Dim lvis2 As New ListViewItem.ListViewSubItem
            Dim lvis3 As New ListViewItem.ListViewSubItem

            Select Case i
                Case 2
                    lvis0.Text = dataf1.Item(1).ToString
                    lvi.SubItems.Add(lvis0)
                    SkinListView1.Items.Add(lvi)
                Case 3
                    lvis0.Text = dataf1.Item(1).ToString
                    lvi.SubItems.Add(lvis0)
                    lvis1.Text = dataf1.Item(2).ToString
                    lvi.SubItems.Add(lvis1)
                    SkinListView1.Items.Add(lvi)
                Case 4
                    lvis0.Text = dataf1.Item(1).ToString
                    lvi.SubItems.Add(lvis0)
                    lvis1.Text = dataf1.Item(2).ToString
                    lvi.SubItems.Add(lvis1)
                    lvis2.Text = dataf1.Item(3).ToString
                    lvi.SubItems.Add(lvis2)
                    SkinListView1.Items.Add(lvi)
                Case 5
                    lvis0.Text = dataf1.Item(1).ToString
                    lvi.SubItems.Add(lvis0)
                    lvis1.Text = dataf1.Item(2).ToString
                    lvi.SubItems.Add(lvis1)
                    lvis2.Text = dataf1.Item(3).ToString
                    lvi.SubItems.Add(lvis2)
                    lvis3.Text = dataf1.Item(4).ToString
                    lvi.SubItems.Add(lvis3)
                    SkinListView1.Items.Add(lvi)
            End Select
        End While
    End Sub

    Sub sqldel()
        Dim dr As DataRow
        Try
            '查询bidid，pkgid，engid汇总表,不存在为null
            Form1.sqlcon1.sqlstr("
            SELECT
            tb1.bidid,
            tb1.pkgid,
            engineering.id as engid
            FROM
            (SELECT
            bid.id as bidid,
            package.id as pkgid
            FROM
            bid 
            LEFT JOIN
            package
            ON 
            package.bidid = bid.id
            WHERE
            bid.oveid = " & pro_id & ")
            AS tb1
            LEFT JOIN 
            engineering
            ON 
            engineering.pkgid = tb1.pkgid
            ", 2
                        )
            Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                For i = 0 To 2
                    If dr.Item(i).ToString <> "" Then
                        Select Case i
                            Case 0
                                '删除bid，bid_config表
                                Form1.sqlcon1.sqlstr("DELETE FROM bid 
                                            WHERE id = " &
                                dr.Item(i)
                                           )
                                Form1.sqlcon1.sqlstr("DELETE FROM bid_config 
                                            WHERE bidid = " &
                                dr.Item(i)
                                           )
                            Case 1
                                '删除package，package_config表
                                Form1.sqlcon1.sqlstr("DELETE FROM package 
                                            WHERE id = " &
                                dr.Item(i)
                                           )
                                Form1.sqlcon1.sqlstr("DELETE FROM package_config 
                                            WHERE pkgid = " &
                                dr.Item(i)
                                           )
                            Case 2
                                '删除engineering，engineering_config表
                                Form1.sqlcon1.sqlstr("DELETE FROM engineering 
                                            WHERE id = " &
                                dr.Item(i)
                                           )
                                Form1.sqlcon1.sqlstr("DELETE FROM engineering_config 
                                            WHERE engid = " &
                                dr.Item(i)
                                           )
                        End Select
                    End If
                Next
            Next
            Form1.sqlcon1.sqlstr("COMMIT")
            '删除factory_config表
            Form1.sqlcon1.sqlstr("
                DELETE FROM factory_config 
                WHERE oveid =" &
                pro_id
                                 )
            '删除price

            '删除repeal/package_requst/排序


        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:sqlcon1.datas", 0 + 48, "warning")
            Exit Sub
        End Try
    End Sub

    Sub sqlfacdel()
        Try
            Form1.sqlcon1.sqlstr("
                DELETE FROM factory_config
                WHERE oveid = " &
                pro_id
                                 )
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:factory_config", 0 + 48, "warning")
            Exit Sub
        End Try
    End Sub

    Sub sqlexpdel()
        Try
            Form1.sqlcon1.sqlstr("
            DELETE FROM expert_config
            WHERE oveid = " &
            pro_id)
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:expert_config", 0 + 48, "warning")
            Exit Sub
        End Try
    End Sub

    Sub sqlrepdel()
        Dim str As String
        Dim dr As DataRow

        '获取并验证导入文件组别
        Try
            '获取expert_config表id
            str = xlscon1.wksheet.GetRow(1).GetCell(1).StringCellValue
            Form1.sqlcon1.sqlstr("
            SELECT id 
            FROM expert_config
            where oveid = " & pro_id & "
            AND char_gp = " & Chr(34) & str & Chr(34), 2)
            '判断expert_config.id是否存在
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                '验证组别
                If str Like "商务*" Then
                    str = "商务废标"
                ElseIf str Like "技术*" Then
                    str = "技术废标"
                Else
                    Throw New Exception
                End If
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件（组别）格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        '刷新factory_config表repst
        Try
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                '获取factory_config.repst
                Form1.sqlcon1.sqlstr("
                select factory_config.id,factory_config.repst
                FROM factory_config,repeal_config
                WHERE repeal_config.ecoid = " & dr.Item(0) & "
                AND repeal_config.pkgid = factory_config.pkgid
                AND repeal_config.facid = factory_config.facid", 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    While Form1.sqlcon1.dataf.Read()
                        Select Case Form1.sqlcon1.dataf.Item(1)
                            Case ""
                            Case "商务废标"
                                If str = "商务废标" Then
                                    Form1.sqlcon1.sqlstr("
                                    UPDATE factory_config
                                    SET repst= " & Chr(34) & Chr(34) & "
                                    WHERE factory_config.id = " &
                                    Form1.sqlcon1.dataf.Item(0))
                                End If
                            Case "技术废标"
                                If str = "技术废标" Then
                                    Form1.sqlcon1.sqlstr("
                                    UPDATE factory_config
                                    SET repst= " & Chr(34) & Chr(34) & "
                                    WHERE factory_config.id = " &
                                    Form1.sqlcon1.dataf.Item(0))
                                End If
                            Case "商务废标技术废标"
                                If str = "商务废标" Then
                                    Form1.sqlcon1.sqlstr("
                                    UPDATE factory_config
                                    SET repst= " & Chr(34) & "技术废标" & Chr(34) & "
                                    WHERE factory_config.id = " &
                                    Form1.sqlcon1.dataf.Item(0))
                                ElseIf str = "技术废标" Then
                                    Form1.sqlcon1.sqlstr("
                                    UPDATE factory_config
                                    SET repst= " & Chr(34) & "商务废标" & Chr(34) & "
                                    WHERE factory_config.id = " &
                                    Form1.sqlcon1.dataf.Item(0))
                                Else
                                    Throw New Exception
                                End If
                            Case "技术废标商务废标"
                                If str = "商务废标" Then
                                    Form1.sqlcon1.sqlstr("
                                    UPDATE factory_config
                                    SET repst= " & Chr(34) & "技术废标" & Chr(34) & "
                                    WHERE factory_config.id = " &
                                    Form1.sqlcon1.dataf.Item(0))
                                ElseIf str = "技术废标" Then
                                    Form1.sqlcon1.sqlstr("
                                    UPDATE factory_config
                                    SET repst= " & Chr(34) & "商务废标" & Chr(34) & "
                                    WHERE factory_config.id = " &
                                    Form1.sqlcon1.dataf.Item(0))
                                Else
                                    Throw New Exception
                                End If
                            Case Else
                                Throw New Exception
                        End Select
                    End While
                End If
                Try
                    '删除repeal_config表组别数据
                    Form1.sqlcon1.sqlstr("
                    DELETE FROM repeal_config
                    WHERE repeal_config.oveid = " & pro_id & "
                    AND repeal_config.ecoid = " & dr.Item(0) & "
                    AND repeal_config.repid IN
                    (SELECT id 
                    FROM repeal 
                    WHERE reptype = " & Chr(34) & str & Chr(34) & ")")
                Catch ex As Exception
                    MsgBox("警告：发生未知错误 location:repeal_config", 0 + 48, "warning")
                    Exit Sub
                End Try
            Next
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:factory_config.repst", 0 + 48, "warning")
            Exit Sub
        End Try
        '删除repeal_config表ecoid为0数据
        Try
            Form1.sqlcon1.sqlstr("
            DELETE FROM repeal_config 
            WHERE oveid = " & pro_id & "
            AND ecoid = 0"
                                )
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:repeal_config", 0 + 48, "warning")
            Exit Sub
        End Try

    End Sub

    Private Sub dr_huizong()
        Dim deep As Integer
        Dim xlsr, xlsc As Integer

        '验证1-9行标题名称
        Try
            If xlscon1.wksheet.GetRow(0).GetCell(0).StringCellValue <> "分标号" Or
               xlscon1.wksheet.GetRow(0).GetCell(1).StringCellValue <> "分标名称" Or
               xlscon1.wksheet.GetRow(0).GetCell(2).StringCellValue <> "分标编号" Or
               xlscon1.wksheet.GetRow(0).GetCell(3).StringCellValue <> "分包号" Or
               xlscon1.wksheet.GetRow(0).GetCell(4).StringCellValue <> "分包名称" Or
               xlscon1.wksheet.GetRow(0).GetCell(5).StringCellValue <> "项目单位" Or
               xlscon1.wksheet.GetRow(0).GetCell(6).StringCellValue <> "工程名称" Or
               xlscon1.wksheet.GetRow(0).GetCell(7).StringCellValue <> "工程概算" Or
               xlscon1.wksheet.GetRow(0).GetCell(8).StringCellValue <> "工程限价" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        'sql写入文件
        deep = xlscon1.wksheet.LastRowNum + 1
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 2 To deep
            '判断1-9列是否存在数据
            For xlsc = 1 To 9
                Try
                    If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).CellType = SS.UserModel.CellType.Blank Then
                        MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                        Form1.sqlcon1.sqlstr("END TRANSACTION")
                        Exit Sub
                    End If
                Catch ex As Exception
                    MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                    Form1.sqlcon1.sqlstr("END TRANSACTION")
                    Exit Sub
                End Try
            Next
            '添加bid表，不存在添加
            Form1.sqlcon1.sqlstr(
                "INSERT INTO 
                [bid]([bididx],[bidname],[bidno],[oveid]) 
                SELECT " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(0).StringCellValue.ToString & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue.ToString & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue.ToString & Chr(34) & "," &
                pro_id & " 
                WHERE NOT EXISTS 
                (SELECT 
                * 
                FROM 
                [bid] 
                WHERE 
                [bidname] = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue.ToString & Chr(34) & " 
                AND 
                [oveid] = " & pro_id &
                ")"
                                )
            '查询bid_id
            Form1.sqlcon1.sqlstr(
                "SELECT id FROM bid 
                WHERE bidname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue.ToString & Chr(34) &
                "AND oveid = " &
                pro_id, 1
                         )
            Form1.sqlcon1.dataf.Read()
            bid_id = Form1.sqlcon1.dataf.Item(0)
            '添加bid_config表
            Form1.sqlcon1.sqlstr(
                "INSERT INTO 
                [bid_config]([char_nr],[char_xs],[bidid]) 
                SELECT " &
                Chr(34) & "服务采购项目" & Chr(34) & "," &
                Chr(34) & "无限授" & Chr(34) & "," &
                bid_id & " 
                WHERE NOT EXISTS 
                (SELECT 
                * 
                FROM 
                [bid_config] 
                WHERE 
                [bidid] = " &
                bid_id &
                ")"
                                 )
            '添加package表，不存在添加
            Form1.sqlcon1.sqlstr(
                "INSERT INTO 
                [package]([pkgidx],[pkgname],[bidid]) 
                SELECT " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue.ToString & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue.ToString & Chr(34) & "," &
                bid_id & " 
                WHERE NOT EXISTS 
                (SELECT 
                * 
                FROM 
                [package] 
                WHERE 
                [pkgidx] = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue.ToString & Chr(34) & " 
                AND 
                [bidid] = " & bid_id &
                ")"
                                )
            '查询pkg_id
            Form1.sqlcon1.sqlstr(
                "SELECT id FROM package 
                WHERE pkgidx = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue.ToString & Chr(34) &
                "AND bidid = " &
                bid_id, 1
                         )
            Form1.sqlcon1.dataf.Read()
            pkg_id = Form1.sqlcon1.dataf.Item(0)
            '添加package_config表
            Form1.sqlcon1.sqlstr(
                "INSERT INTO 
                [package_config]([char_gk],[char_fs],[char_bj],[char_ps],[pkgid]) 
                SELECT " &
                Chr(34) & "公开" & Chr(34) & "," &
                Chr(34) & "竞争性谈判" & Chr(34) & "," &
                Chr(34) & "固定总价" & Chr(34) & "," &
                Chr(34) & "综合评审法" & Chr(34) & "," &
                pkg_id & " 
                WHERE NOT EXISTS 
                (SELECT 
                * 
                FROM 
                [package_config] 
                WHERE 
                [pkgid] = " &
                pkg_id &
                ")"
                                 )
            '判断工程名engname是否重复
            Form1.sqlcon1.sqlstr(
                "SELECT 
                *
                FROM 
                engineering 
                WHERE 
                engname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).StringCellValue.ToString & Chr(34) & " 
                AND 
                pkgid = " &
                pkg_id, 1
                                 )
            Form1.sqlcon1.dataf.Read()
            Try
                If Form1.sqlcon1.dataf.Item(0) Then
                    MsgBox("警告：导入文件（7列，" & xlsr & "行）格式错误:存在重复值", 0 + 48, "warning")
                    Exit Sub
                End If
            Catch ex As Exception

            End Try
            '添加engineering表，不存在添加
            Form1.sqlcon1.sqlstr(
                "INSERT INTO 
                [engineering]([engidx],[engname],[pkgid]) 
                SELECT " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue.ToString & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).StringCellValue.ToString & Chr(34) & "," &
                pkg_id & " 
                WHERE NOT EXISTS 
                (SELECT 
                * 
                FROM 
                [engineering] 
                WHERE 
                [engname] = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).StringCellValue.ToString & Chr(34) & " 
                AND 
                [pkgid] = " & pkg_id &
                ")"
                                )
            '查询eng_id
            Form1.sqlcon1.sqlstr(
                "SELECT id FROM engineering 
                WHERE engname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).StringCellValue.ToString & Chr(34) &
                "AND pkgid = " &
                pkg_id, 1
                         )
            Form1.sqlcon1.dataf.Read()
            eng_id = Form1.sqlcon1.dataf.Item(0)
            '添加engineering_config表
            Form1.sqlcon1.sqlstr(
                "INSERT INTO 
                [engineering_config]([char_gs],[char_xj],[engid]) 
                SELECT " &
                xlscon1.wksheet.GetRow(xlsr - 1).GetCell(7).NumericCellValue & "," &
                xlscon1.wksheet.GetRow(xlsr - 1).GetCell(8).NumericCellValue & "," &
                eng_id & " 
                WHERE NOT EXISTS 
                (SELECT 
                * 
                FROM 
                [engineering_config] 
                WHERE 
                [engid] = " &
                eng_id &
                ")"
                                 )
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        MsgBox("提示：导入文件完成", 0 + 64, "information")
        '刷新listview
        Call tvu(1)
    End Sub

    Sub dr_fashou()
        Dim deep As Integer
        Dim xlsr, xlsc As Integer
        Dim str0, str As String

        If xlscon1.wksheet.GetRow(1).GetCell(0).StringCellValue = "采购文件获取一览表" Then
            Call dr_ecp_fashou()
            Exit Sub
        End If

        '检查2-7标题行
        Try
            If xlscon1.wksheet.GetRow(1).GetCell(1).StringCellValue <> "分标名称" Or
               xlscon1.wksheet.GetRow(1).GetCell(2).StringCellValue <> "应答人" Or
               xlscon1.wksheet.GetRow(1).GetCell(3).StringCellValue <> "联系人" Or
               xlscon1.wksheet.GetRow(1).GetCell(4).StringCellValue <> "电话" Or
               xlscon1.wksheet.GetRow(1).GetCell(5).StringCellValue <> "邮箱" Or
               xlscon1.wksheet.GetRow(1).GetCell(6).StringCellValue <> "应答包号" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        deep = xlscon1.wksheet.LastRowNum + 1
        '开启sql事务
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 3 To deep
            '检查2-3列空值
            For xlsc = 2 To 3
                Try
                    If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).CellType = SS.UserModel.CellType.Blank Then
                        MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）填写错误", 0 + 48, "warning")
                        Form1.sqlcon1.sqlstr("END TRANSACTION")
                        Exit Sub
                    End If
                Catch ex As Exception
                    MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                    Form1.sqlcon1.sqlstr("END TRANSACTION")
                    Exit Sub
                End Try
            Next
            '获取bid_id
            Try
                Form1.sqlcon1.sqlstr("
                    SELECT id 
                    FROM bid 
                    WHERE bidname = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue & Chr(34) & "
                    AND oveid = " &
                    pro_id, 1)
                Form1.sqlcon1.dataf.Read()
                bid_id = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：导入文件（2列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            'sql插入或更新factory
            Try
                Form1.sqlcon1.sqlstr("
                    INSERT INTO 
                    [factory]([facname],[faccont],[facline],[facmail])
                    SELECT " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue & Chr(34) & "," &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34) & "," &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).NumericCellValue.ToString & Chr(34) & "," &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue & Chr(34) & "
                    WHERE NOT EXISTS (
                    SELECT 
                    * 
                    FROM 
                    [factory] 
                    WHERE 
                    [facname] = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue & Chr(34) & "
                    AND [faccont] = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34) & "
                    AND [facline] = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).NumericCellValue.ToString & Chr(34) & "
                    AND [facmail] = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue & Chr(34) &
                    ")"
                                     )
                Form1.sqlcon1.sqlstr("
                    SELECT id 
                    FROM factory 
                    WHERE 
                    facname = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue & Chr(34) & "
                    AND faccont = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34) & "
                    AND facline = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).NumericCellValue.ToString & Chr(34) & "
                    AND facmail = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue & Chr(34), 1
                                     )
                Form1.sqlcon1.dataf.Read()
                fac_id = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsr & "行应答人信息）填写错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '判断excel包号是否正确
            Try
                For xlsc = 7 To xlscon1.wksheet.GetRow(xlsr - 1).LastCellNum
                    If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).NumericCellValue = 0 Then
                        Continue For
                    Else
                        str0 = "包0" &
                            xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).NumericCellValue.ToString()
                        str = "包" &
                            xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).NumericCellValue.ToString()
                        Form1.sqlcon1.sqlstr("
                            SELECT id
                            FROM package 
                            WHERE bidid = " &
                            bid_id & "
                            AND pkgidx = " &
                            Chr(34) & str0 & Chr(34) & "
                            OR bidid = " &
                            bid_id & "
                            AND pkgidx = " &
                            Chr(34) & str & Chr(34), 1
                                                )
                        Form1.sqlcon1.dataf.Read()
                        pkg_id = Form1.sqlcon1.dataf.Item(0)
                    End If
                    '插入或更新factory_config记录
                    Form1.sqlcon1.sqlstr("
                        INSERT INTO 
                        [factory_config]([oveid],[pkgid],[facid],[disst],[repst])
                        SELECT " &
                        pro_id & "," &
                        pkg_id & "," &
                        fac_id & "," &
                        Chr(34) & Chr(34) & "," &
                        Chr(34) & Chr(34) & "
                        WHERE NOT EXISTS (
                        SELECT 
                        * 
                        FROM 
                        [factory_config] 
                        WHERE [oveid] = " &
                        pro_id & "
                        AND [pkgid] = " &
                        pkg_id & "
                        AND [facid] = " &
                        fac_id & "
                        )"
                                         )
                Next
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        MsgBox("提示：导入文件完成", 0 + 64, "information")
        '刷新listview
        Call tvu(1)
    End Sub

    Sub dr_ecp_fashou()
        Dim deep As Integer
        Dim xlsr, xlsc As Integer
        Dim str As String

        '检查标题行
        Try
            If xlscon1.wksheet.GetRow(4).GetCell(1).StringCellValue <> "分标名称" Or
               xlscon1.wksheet.GetRow(4).GetCell(3).StringCellValue <> "分包名称" Or
               xlscon1.wksheet.GetRow(4).GetCell(6).StringCellValue <> "投标厂商名称" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        deep = xlscon1.wksheet.LastRowNum + 1
        '开启sql事务
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 6 To deep
            '获取bid_id
            Try
                Form1.sqlcon1.sqlstr("
                    SELECT id 
                    FROM bid 
                    WHERE bidname = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue & Chr(34) & "
                    AND oveid = " &
                    pro_id, 1)
                Form1.sqlcon1.dataf.Read()
                bid_id = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：导入文件（2列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            'sql插入或更新factory
            Try
                Form1.sqlcon1.sqlstr("
                INSERT INTO 
                [factory]([facname],[faccont],[facline],[facmail])
                SELECT " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).StringCellValue & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(7).StringCellValue & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(8).StringCellValue & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(9).StringCellValue & Chr(34) & "
                WHERE NOT EXISTS (
                SELECT 
                * 
                FROM 
                [factory] 
                WHERE 
                [facname] = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).StringCellValue & Chr(34) & "
                AND [faccont] = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(7).StringCellValue & Chr(34) & "
                AND [facline] = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(8).StringCellValue & Chr(34) & "
                AND [facmail] = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(9).StringCellValue & Chr(34) &
                ")"
                                    )
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM factory 
                WHERE 
                facname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).StringCellValue & Chr(34) & "
                AND faccont = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(7).StringCellValue & Chr(34) & "
                AND facline = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(8).StringCellValue & Chr(34) & "
                AND facmail = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(9).StringCellValue & Chr(34), 1
                                    )
                Form1.sqlcon1.dataf.Read()
                fac_id = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsr & "行应答人信息）填写错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '判断excel包号是否正确
            Try
                str = xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue
                Form1.sqlcon1.sqlstr("
                SELECT id
                FROM package 
                WHERE bidid = " &
                bid_id & "
                AND pkgidx = " &
                Chr(34) & str & Chr(34), 1)
                Form1.sqlcon1.dataf.Read()
                pkg_id = Form1.sqlcon1.dataf.Item(0)
                '插入或更新factory_config记录
                Form1.sqlcon1.sqlstr("
                INSERT INTO 
                [factory_config]([oveid],[pkgid],[facid],[disst],[repst])
                SELECT " &
                pro_id & "," &
                pkg_id & "," &
                fac_id & "," &
                Chr(34) & Chr(34) & "," &
                Chr(34) & Chr(34) & "
                WHERE NOT EXISTS (
                SELECT 
                * 
                FROM 
                [factory_config] 
                WHERE [oveid] = " &
                pro_id & "
                AND [pkgid] = " &
                pkg_id & "
                AND [facid] = " &
                fac_id & "
                )")
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        MsgBox("提示：导入文件完成", 0 + 64, "information")
        '刷新listview
        Call tvu(1)
    End Sub

    Sub dr_qibiao()
        Dim deep As Integer
        Dim xlsr, xlsc As Integer

        '检查2-5标题行
        Try
            If xlscon1.wksheet.GetRow(0).GetCell(1).StringCellValue <> "分标名称" Or
               xlscon1.wksheet.GetRow(0).GetCell(2).StringCellValue <> "分包号" Or
               xlscon1.wksheet.GetRow(0).GetCell(3).StringCellValue <> "应答人" Or
               xlscon1.wksheet.GetRow(0).GetCell(4).StringCellValue <> "弃标情况" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        deep = xlscon1.wksheet.LastRowNum + 1
        '开启sql事务
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 2 To deep
            Try
                '检查弃标情况空值及其他
                If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue <> "弃标" Then
                    If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue <> "弃包" Then
                        Continue For
                    End If
                End If
            Catch ex As Exception
                Continue For
            End Try
            Try
                '检查2-4列空值
                For xlsc = 2 To 4
                    If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).CellType = SS.UserModel.CellType.Blank Then
                        MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                        Form1.sqlcon1.sqlstr("END TRANSACTION")
                        Exit Sub
                    End If
                Next
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            Try
                '获取bid_id
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM bid
                WHERE bidname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue & Chr(34) & "
                AND oveid = " &
                pro_id, 1)
                Form1.sqlcon1.dataf.Read()
                bid_id = Form1.sqlcon1.dataf.Item(0)
                '获取pkg_id
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM package 
                WHERE bidid = " &
                bid_id & "
                AND pkgidx = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue & Chr(34), 1)
                Form1.sqlcon1.dataf.Read()
                pkg_id = Form1.sqlcon1.dataf.Item(0)
                '获取fac_id
                Form1.sqlcon1.sqlstr("
                SELECT facid
                FROM factory,factory_config
                WHERE factory_config.pkgid = " & pkg_id & "
                AND factory.id = factory_config.facid
                AND factory.facname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34), 1)
                Form1.sqlcon1.dataf.Read()
                fac_id = Form1.sqlcon1.dataf.Item(0)
                '更新factory_config表disst列
                Form1.sqlcon1.sqlstr("
                UPDATE factory_config 
                SET disst = " & Chr(34) & "弃标" & Chr(34) & "
                WHERE oveid = " & pro_id & "
                AND pkgid = " & pkg_id & "
                AND facid = " & fac_id)
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        MsgBox("提示：导入文件完成", 0 + 64, "information")
        '刷新listview
        Call tvu(1)
    End Sub

    Sub dr_zizhi()
        Dim deep As Integer
        Dim xlsr, xlsc As Integer

        '检查2-5标题行
        Try
            If xlscon1.wksheet.GetRow(0).GetCell(1).StringCellValue <> "分标名称" Or
               xlscon1.wksheet.GetRow(0).GetCell(2).StringCellValue <> "分包号" Or
               xlscon1.wksheet.GetRow(0).GetCell(3).StringCellValue <> "资质要求" Or
               xlscon1.wksheet.GetRow(0).GetCell(4).StringCellValue <> "业绩要求" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        deep = xlscon1.wksheet.LastRowNum + 1
        '开启sql事务
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 2 To deep
            Try
                '检查2-5列空值
                For xlsc = 2 To 5
                    If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).CellType = SS.UserModel.CellType.Blank Then
                        MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）填写错误", 0 + 48, "warning")
                        Form1.sqlcon1.sqlstr("END TRANSACTION")
                        Exit Sub
                    End If
                Next
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            Try
                '获取bid_id
                Form1.sqlcon1.sqlstr("
                    SELECT id 
                    FROM bid
                    WHERE bidname = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue & Chr(34) & "
                    AND oveid = " &
                    pro_id, 1
                                     )
                Form1.sqlcon1.dataf.Read()
                bid_id = Form1.sqlcon1.dataf.Item(0)
                '获取pkg_id
                Form1.sqlcon1.sqlstr("
                    SELECT id 
                    FROM package 
                    WHERE bidid = " &
                    bid_id & "
                    AND pkgidx = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue & Chr(34), 1
                                     )
                Form1.sqlcon1.dataf.Read()
                pkg_id = Form1.sqlcon1.dataf.Item(0)
                '更新或插入package_request表
                Form1.sqlcon1.sqlstr("
                INSERT INTO 
                [package_request]([oveid],[pkgid],[char_zz],[char_yj])
                SELECT " &
                pro_id & "," &
                pkg_id & "," &
                Chr(34) & "-" & Chr(34) & "," &
                Chr(34) & "-" & Chr(34) & "
                WHERE NOT EXISTS (
                SELECT *
                FROM [package_request] 
                WHERE [oveid] = " &
                pro_id & "
                AND [pkgid] = " &
                pkg_id & "
                )"
                                    )
                Form1.sqlcon1.sqlstr("
                UPDATE package_request 
                SET char_zz = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34) & "
                ,char_yj = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue & Chr(34) & "
                WHERE oveid = " &
                pro_id & "
                AND pkgid = " &
                pkg_id
                                    )
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        MsgBox("提示：导入文件完成", 0 + 64, "information")
        '刷新listview
        Call tvu(1)
    End Sub

    Sub dr_zhuanjia()
        Dim deep, i As Integer
        Dim xlsr, xlsc As Integer

        '检查2-10标题行
        Try
            If xlscon1.wksheet.GetRow(0).GetCell(1).StringCellValue <> "组别" Or
               xlscon1.wksheet.GetRow(0).GetCell(2).StringCellValue <> "组内职位" Or
               xlscon1.wksheet.GetRow(0).GetCell(3).StringCellValue <> "姓名" Or
               xlscon1.wksheet.GetRow(0).GetCell(4).StringCellValue <> "身份证" Or
               xlscon1.wksheet.GetRow(0).GetCell(5).StringCellValue <> "专业" Or
               xlscon1.wksheet.GetRow(0).GetCell(6).StringCellValue <> "工作单位" Or
               xlscon1.wksheet.GetRow(0).GetCell(7).StringCellValue <> "电话" Or
               xlscon1.wksheet.GetRow(0).GetCell(8).StringCellValue <> "银行名称" Or
               xlscon1.wksheet.GetRow(0).GetCell(9).StringCellValue <> "银行账号" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        deep = xlscon1.wksheet.LastRowNum + 1
        '开启sql事务
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 2 To deep
            Try
                '检查2-10列空值
                For xlsc = 2 To 10
                    If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).CellType = SS.UserModel.CellType.Blank Then
                        MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）填写错误", 0 + 48, "warning")
                        Form1.sqlcon1.sqlstr("END TRANSACTION")
                        Exit Sub
                    End If
                Next
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                SkinListView1.Items.Clear()
                Exit Sub
            End Try
            Try
                '获取expid
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM expert 
                WHERE expiden = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue & Chr(34), 1)
                Form1.sqlcon1.dataf.Read()
                If Form1.sqlcon1.dataf.HasRows Then
                    i = Form1.sqlcon1.dataf.Item(0)
                    '刷新expert
                    Form1.sqlcon1.sqlstr("
                    UPDATE expert 
                    SET expname = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34) & "
                    ,expprof = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue & Chr(34) & "
                    ,expadds = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).StringCellValue & Chr(34) & "
                    ,expline = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(7).StringCellValue & Chr(34) & "
                    ,expbnka = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(8).StringCellValue & Chr(34) & "
                    ,expbnkn = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(9).StringCellValue & Chr(34) & "
                    WHERE id = " & i)
                Else
                    '插入expert
                    Form1.sqlcon1.sqlstr("
                    INSERT INTO expert(expname,expiden,expprof,expadds,expline,expbnka,expbnkn)
                    VALUES (" &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34) & "," &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue & Chr(34) & "," &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue & Chr(34) & "," &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).StringCellValue & Chr(34) & "," &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(7).StringCellValue & Chr(34) & "," &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(8).StringCellValue & Chr(34) & "," &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(9).StringCellValue & Chr(34) & ")")
                    Form1.sqlcon1.sqlstr("
                    SELECT id 
                    FROM expert 
                    WHERE expiden = " &
                    Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue & Chr(34), 1)
                    Form1.sqlcon1.dataf.Read()
                    i = Form1.sqlcon1.dataf.Item(0)
                End If
                '插入expert_config
                Form1.sqlcon1.sqlstr("
                INSERT INTO expert_config(oveid,expid,char_gp,char_po) 
                VALUES (" &
                pro_id & "," &
                i & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue & Chr(34) & ")")
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                SkinListView1.Items.Clear()
                Exit Sub
            End Try
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        SkinListView1.Items.Clear()
        MsgBox("提示：导入文件完成", 0 + 64, "information")
        '刷新treeview
        Call tvu(2)
    End Sub

    Sub dr_chuping()
        Dim deep, fng_id, i, j As Integer
        Dim xlsr, xlsc As Integer
        Dim str0, str As String

        '检查文件格式
        Try
            '检查2-10标题行
            If xlscon1.wksheet.GetRow(2).GetCell(1).StringCellValue <> "分标名称" Or
               xlscon1.wksheet.GetRow(2).GetCell(2).StringCellValue <> "包号" Or
               xlscon1.wksheet.GetRow(2).GetCell(3).StringCellValue <> "存在问题应答人" Or
               xlscon1.wksheet.GetRow(2).GetCell(4).StringCellValue <> "采购文件或法律要求" Or
               xlscon1.wksheet.GetRow(2).GetCell(5).StringCellValue <> "存在问题" Or
               xlscon1.wksheet.GetRow(2).GetCell(6).StringCellValue <> "处理意见" Or
               xlscon1.wksheet.GetRow(2).GetCell(7).StringCellValue <> "提出专家" Or
               xlscon1.wksheet.GetRow(2).GetCell(8).StringCellValue <> "原标包是否流包" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
            '检查商务技术废标reptype
            If xlscon1.wksheet.GetRow(1).GetCell(1).StringCellValue Like "*商务*" Then
                str0 = "商务废标"
            ElseIf xlscon1.wksheet.GetRow(1).GetCell(1).StringCellValue Like "*技术*" Then
                str0 = "技术废标"
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        deep = xlscon1.wksheet.LastRowNum + 1
        '开启sql事务
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 4 To deep
            '初始化str
            str = str0
            '检查2-9列空值
            Try
                For xlsc = 2 To 9
                    If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).CellType = SS.UserModel.CellType.Blank Then
                        MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）填写错误", 0 + 48, "warning")
                        Form1.sqlcon1.sqlstr("END TRANSACTION")
                        Exit Sub
                    End If
                Next
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '检查2-4列数据正确性
            Try
                '获取pkgid为pkg_id
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM package 
                WHERE pkgidx = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue & Chr(34) & "
                AND bidid = 
                (SELECT id 
                FROM bid 
                WHERE oveid = " & pro_id & "
                AND bidname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue & Chr(34) & ")", 1
                                     )
                Form1.sqlcon1.dataf.Read()
                pkg_id = Form1.sqlcon1.dataf.Item(0)
                '获取factory_config_id
                Form1.sqlcon1.sqlstr("
                SELECT factory_config.id,factory_config.facid
                FROM factory_config,factory
                WHERE factory_config.pkgid = 
                (SELECT id 
                FROM package 
                WHERE pkgidx = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue & Chr(34) & "
                AND bidid = 
                (SELECT id 
                FROM bid 
                WHERE oveid = " & pro_id & "
                AND bidname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue & Chr(34) & "))
                and factory.facname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34) & "
                and factory.id = factory_config.facid", 1
                                     )
                Form1.sqlcon1.dataf.Read()
                fng_id = Form1.sqlcon1.dataf.Item(0)
                '获取fac_id
                fac_id = Form1.sqlcon1.dataf.Item(1)
            Catch ex As Exception
                MsgBox("警告：导入文件（2-4列" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '检查8列数据正确性
            Try
                '获取ecoid为i
                Form1.sqlcon1.sqlstr("
                SELECT expert_config.id 
                FROM expert,expert_config
                WHERE expert_config.oveid = " & pro_id & "
                AND expert.expname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(7).StringCellValue & Chr(34) & "
                AND expert_config.char_gp = " &
                Chr(34) & xlscon1.wksheet.GetRow(1).GetCell(1).StringCellValue & Chr(34) & "
                AND expert_config.expid = expert.id", 1
                                     )
                Form1.sqlcon1.dataf.Read()
                i = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：导入文件（8列" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '获取repid为j
            Try
                Form1.sqlcon1.sqlstr("
                INSERT INTO 
                [repeal]([repname], [reptype], [repqust], [repreal])
                SELECT " &
                Chr(34) & "资格不符" & Chr(34) & "," &
                Chr(34) & str & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue.ToString & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue.ToString & Chr(34) & " 
                WHERE NOT EXISTS 
                (SELECT * 
                FROM [repeal] 
                WHERE [reptype] = " &
                Chr(34) & str & Chr(34) & "
                AND [repqust] = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue.ToString & Chr(34) & " 
                AND [repreal] = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue.ToString & Chr(34) & ")"
                                     )
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM repeal
                WHERE reptype = " &
                Chr(34) & str & Chr(34) & "
                AND repqust = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue.ToString & Chr(34) & "
                AND repreal = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue.ToString & Chr(34), 1
                )
                Form1.sqlcon1.dataf.Read()
                j = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：导入文件（5-6列" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '刷新或写入repeal_config表数据
            Try
                Form1.sqlcon1.sqlstr("
                INSERT INTO 
                [repeal_config]([oveid], [pkgid], [facid], [repid],[ecoid])
                SELECT " &
                pro_id & "," &
                pkg_id & "," &
                fac_id & "," &
                j & "," &
                i & "
                WHERE NOT EXISTS 
                (SELECT * 
                FROM [repeal_config] 
                WHERE [oveid] = " &
                pro_id & "
                AND [pkgid] = " &
                pkg_id & " 
                AND [facid] = " &
                fac_id & "
                AND [repid] = " &
                j & "
                AND [ecoid] = " &
                i & ")"
                                     )
            Catch ex As Exception
                MsgBox("警告：发生未知错误 location:repeal_config", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '刷新或写入factory_config.repst数据
            Try
                Form1.sqlcon1.sqlstr("
                SELECT repst 
                FROM factory_config 
                WHERE id = " &
                fng_id, 1
                                     )
                Form1.sqlcon1.dataf.Read()
                '判断已存在repeal-type
                If Form1.sqlcon1.dataf.HasRows Then
                    If Form1.sqlcon1.dataf.Item(0) = str Then
                    Else
                        str &= Form1.sqlcon1.dataf.Item(0)
                    End If
                End If
                Form1.sqlcon1.sqlstr("
                UPDATE factory_config 
                SET repst = " &
                Chr(34) & str & Chr(34) & "
                WHERE id =  " &
                fng_id
                                     )
                '清空skinlistview数据
                SkinListView1.Items.Clear()
            Catch ex As Exception
                MsgBox("警告：发生未知错误 location:factory_config.repst", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        MsgBox("提示：导入文件完成", 0 + 64, "information")
        '刷新listview
        Call tvu(1)
    End Sub

    Sub dr_paixu()
        Dim deep As Integer
        Dim xlsr, xlsc As Integer

        '检查2-10标题行
        Try
            If xlscon1.wksheet.GetRow(0).GetCell(0).StringCellValue <> "包号/子包号" Or
               xlscon1.wksheet.GetRow(0).GetCell(3).StringCellValue <> "投标人" Or
               xlscon1.wksheet.GetRow(0).GetCell(4).StringCellValue <> "技术加权分" Or
               xlscon1.wksheet.GetRow(0).GetCell(5).StringCellValue <> "商务加权分" Or
               xlscon1.wksheet.GetRow(0).GetCell(6).StringCellValue <> "价格加权分" Or
               xlscon1.wksheet.GetRow(0).GetCell(7).StringCellValue <> "总分" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        deep = xlscon1.wksheet.LastRowNum + 1
        '开启sql事务
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 2 To deep
            Try
                '检查2-9列空值
                For xlsc = 1 To 9
                    If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).CellType = SS.UserModel.CellType.Blank Then
                        MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）填写错误", 0 + 48, "warning")
                        Form1.sqlcon1.sqlstr("END TRANSACTION")
                        Exit Sub
                    End If
                Next
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            Try
                '获取pkgid,facid
                Form1.sqlcon1.sqlstr("
                SELECT package.id,factory_config.facid
                FROM package,bid,factory_config,factory
                WHERE 
                bid.oveid = " & pro_id & "
                AND 
                factory_config.pkgid = package.id
                AND 
                factory_config.facid = factory.id
                AND
                package.bidid = bid.id
                AND
                bid.bididx||bid.bidname||package.pkgidx = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(0).StringCellValue & Chr(34) & "
                AND
                factory.facname = " &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34), 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    pkg_id = Form1.sqlcon1.dataf.Item(0)
                    fac_id = Form1.sqlcon1.dataf.Item(1)
                    Form1.sqlcon1.sqlstr("
                    INSERT INTO sorting(oveid,pkgid,facid,char_js,char_sw,char_jg,char_zf,sort)
                    VALUES (" &
                    pro_id & "," &
                    pkg_id & "," &
                    fac_id & "," &
                    CDbl(xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue) & "," &
                    CDbl(xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue) & "," &
                    CDbl(xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).StringCellValue) & "," &
                    CDbl(xlscon1.wksheet.GetRow(xlsr - 1).GetCell(7).StringCellValue) & "," &
                    xlscon1.wksheet.GetRow(xlsr - 1).GetCell(8).NumericCellValue & ")")
                Else
                    MsgBox("警告：导入文件（" & xlsr & "行）数据错误", 0 + 48, "warning")
                    Form1.sqlcon1.sqlstr("END TRANSACTION")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("警告：导入文件（9列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        MsgBox("提示：导入文件完成", 0 + 64, "information")
        '刷新treeview
        Call tvu(2)
    End Sub

    Sub dr_paixuzdj()
        Dim dr As DataRow
        Dim i, j As Integer

        'sql插入或刷新sorting_price表
        Form1.sqlcon1.sqlstr("
        SELECT tb1.pkgid,tb1.facid,tb2.price
        FROM
        (SELECT factory_config.id as fngid,
        factory_config.pkgid,factory_config.facid 
        FROM factory_config
        WHERE factory_config.oveid =  " & pro_id & "
        AND factory_config.disst = " & Chr(34) & Chr(34) & "
        AND factory_config.repst = " & Chr(34) & Chr(34) & "
        ORDER BY factory_config.pkgid,factory_config.facid) as tb1
        LEFT JOIN 
        (SELECT price_config.fngid,
        max(price_rounds.rouid),price_rounds.price 
        FROM price_config,price_rounds 
        WHERE price_config.id = price_rounds.preid
        GROUP BY price_config.fngid) as tb2
        ON tb1.fngid = tb2.fngid
        ORDER BY tb1.pkgid,tb2.price", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                '设置序号i
                If j <> dr.Item(0) Then
                    j = dr.Item(0)
                    i = 1
                Else
                    i += 1
                End If
                Form1.sqlcon1.sqlstr("
                SELECT sorting_price.id 
                FROM sorting_price
                WHERE sorting_price.oveid = " & pro_id & "
                AND sorting_price.pkgid = " & dr.Item(0) & "
                AND sorting_price.facid = " & dr.Item(1), 1)
                '存在更新不存在插入
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    Form1.sqlcon1.sqlstr("
                    UPDATE sorting_price
                    SET char_bj = " & dr.Item(2) & ",sort = " & i & "
                    WHERE id = " & Form1.sqlcon1.dataf.Item(0))
                Else
                    Form1.sqlcon1.sqlstr("
                    INSERT INTO 
                    sorting_price(oveid,pkgid,facid,char_bj,sort)
                    VALUES (" & pro_id & "," & dr.Item(0) & "," &
                    dr.Item(1) & "," & dr.Item(2) & "," & i & ")")
                End If
            Next
        End If

    End Sub

    Sub dr_ccdossier()
        Dim deep As Integer
        Dim xlsr, xlsc As Integer

        '检查1-7标题行
        Try
            If xlscon1.wksheet.GetRow(0).GetCell(0).StringCellValue <> "盒号" Or
               xlscon1.wksheet.GetRow(0).GetCell(1).StringCellValue <> "文件编号" Or
               xlscon1.wksheet.GetRow(0).GetCell(2).StringCellValue <> "责任者" Or
               xlscon1.wksheet.GetRow(0).GetCell(3).StringCellValue <> "文件题目" Or
               xlscon1.wksheet.GetRow(0).GetCell(4).StringCellValue <> "文件类型" Or
               xlscon1.wksheet.GetRow(0).GetCell(5).StringCellValue <> "文件日期" Or
               xlscon1.wksheet.GetRow(0).GetCell(6).StringCellValue <> "页数" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        deep = xlscon1.wksheet.LastRowNum + 1
        '开启sql事务
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 2 To deep
            Try
                '检查1-7列空值
                For xlsc = 1 To 7
                    If xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).CellType = SS.UserModel.CellType.Blank Then
                        MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）填写错误", 0 + 48, "warning")
                        Form1.sqlcon1.sqlstr("END TRANSACTION")
                        Exit Sub
                    End If
                Next
            Catch ex As Exception
                MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                SkinListView1.Items.Clear()
                Exit Sub
            End Try
            Try
                Form1.sqlcon1.sqlstr("
                INSERT INTO cc_dossier(dosname,dosno,boxidx,fileno,fileow,filena,filetye,filedat,filepag)
                VALUES (" &
                Chr(34) & Form12.ComboBoxEdit1.Text.ToString & Chr(34) & "," &
                Chr(34) & Form12.TextBox1.Text.ToString & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(0).StringCellValue & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34) & "," &
                Chr(34) & xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue & Chr(34) & "," &
                xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).NumericCellValue & "," &
                xlscon1.wksheet.GetRow(xlsr - 1).GetCell(6).NumericCellValue & ")")
            Catch ex As Exception
            MsgBox("警告：导入文件（" & xlsr & "行）格式错误", 0 + 48, "warning")
            Form1.sqlcon1.sqlstr("END TRANSACTION")
            SkinListView1.Items.Clear()
            Exit Sub
            End Try
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        MsgBox("提示：导入文件完成", 0 + 64, "information")
    End Sub

    Sub up_pkgaboert()
        Dim dr As DataRow

        '清空package_abort
        Form1.sqlcon1.sqlstr("
        DELETE FROM package_abort
        WHERE package_abort.pkgid IN
        (SELECT package.id 
        FROM bid,package 
        WHERE package.bidid = bid.id 
        AND bid.oveid = " & pro_id & ")")
        '刷新package_abort表
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT package.id 
        FROM package,bid
        WHERE package.bidid = bid.id
        AND bid.oveid = " & pro_id, 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                '发售流包
                Form1.sqlcon1.sqlstr("
                SELECT COUNT(factory_config.facid)
                FROM factory_config,package
                WHERE package.id = factory_config.pkgid
                AND package.id = " & dr.Item(0), 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    Select Case Form1.sqlcon1.dataf.Item(0)
                        Case 0
                            Form1.sqlcon1.sqlstr("
                            INSERT INTO package_abort(pkgid,aboid) 
                            VALUES(" & dr.Item(0) & ",0)")
                            Continue For
                        Case 1
                            Form1.sqlcon1.sqlstr("
                            SELECT package_config.char_fs 
                            FROM package_config
                            WHERE package_config.pkgid = " & dr.Item(0), 1)
                            Form1.sqlcon1.dataf.Read()
                            If Form1.sqlcon1.dataf.Item(0) = "单一来源" Then
                            Else
                                Form1.sqlcon1.sqlstr("
                                INSERT INTO package_abort(pkgid,aboid) 
                                VALUES(" & dr.Item(0) & ",0)")
                                Continue For
                            End If
                    End Select
                Else
                    Form1.sqlcon1.sqlstr("
                    INSERT INTO package_abort(pkgid,aboid) 
                    VALUES(" & dr.Item(0) & ",0)")
                    Continue For
                End If
                '弃标流包
                Form1.sqlcon1.sqlstr("
                SELECT COUNT(factory_config.facid)
                FROM factory_config,package
                WHERE package.id = factory_config.pkgid
                AND factory_config.disst = " & Chr(34) & Chr(34) & "
                AND package.id = " & dr.Item(0), 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    Select Case Form1.sqlcon1.dataf.Item(0)
                        Case 0
                            Form1.sqlcon1.sqlstr("
                            INSERT INTO package_abort(pkgid,aboid) 
                            VALUES(" & dr.Item(0) & ",1)")
                            Continue For
                        Case 1
                            Form1.sqlcon1.sqlstr("
                            SELECT package_config.char_fs 
                            FROM package_config
                            WHERE package_config.pkgid = " & dr.Item(0), 1)
                            Form1.sqlcon1.dataf.Read()
                            If Form1.sqlcon1.dataf.Item(0) = "单一来源" Then
                            Else
                                Form1.sqlcon1.sqlstr("
                                INSERT INTO package_abort(pkgid,aboid) 
                                VALUES(" & dr.Item(0) & ",1)")
                                Continue For
                            End If
                    End Select
                End If
                '废标流包
                Form1.sqlcon1.sqlstr("
                SELECT COUNT(factory_config.facid)
                FROM factory_config,package
                WHERE package.id = factory_config.pkgid
                AND factory_config.disst = " & Chr(34) & Chr(34) & "
                AND factory_config.repst = " & Chr(34) & Chr(34) & "
                AND package.id = " & dr.Item(0), 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    Select Case Form1.sqlcon1.dataf.Item(0)
                        Case 0
                            Form1.sqlcon1.sqlstr("
                            INSERT INTO package_abort(pkgid,aboid) 
                            VALUES(" & dr.Item(0) & ",2)")
                            Continue For
                        Case 1
                            Form1.sqlcon1.sqlstr("
                            SELECT package_config.char_fs 
                            FROM package_config
                            WHERE package_config.pkgid = " & dr.Item(0), 1)
                            Form1.sqlcon1.dataf.Read()
                            If Form1.sqlcon1.dataf.Item(0) = "单一来源" Then
                            Else
                                Form1.sqlcon1.sqlstr("
                                INSERT INTO package_abort(pkgid,aboid) 
                                VALUES(" & dr.Item(0) & ",2)")
                                Continue For
                            End If
                    End Select
                End If
            Next
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim str As String
        Dim wf1 As New wf

        wf1.wshow()
        Select Case Panel2.Left.ToString
            '导入设置
            Case 200
                If MsgBox("注意：是否清除已有数据并导入", 4 + 32, "caution") = MsgBoxResult.No Then
                    wf1.wclose()
                    Exit Sub
                End If
                '选择导入后执行
                OpenFileDialog1.Filter = "Excel File (*.xls;*.xlsx)|*.xls;*.xlsx"
                OpenFileDialog1.ShowDialog()
                If OpenFileDialog1.FileName <> "" Then
                    Try
                        '连接excel（文件名，sheet序号）
                        xlscon1.str(OpenFileDialog1.FileName, 1)
                    Catch ex As Exception
                        wf1.wclose()
                        '连接文件已打开报错
                        MsgBox("警告：发生未知错误 location:xlscon", 0 + 48, "warning")
                        Exit Sub
                    End Try
                    '判断groupbox.name
                    Select Case GroupBox1.Text
                        Case "信息维护"
                            '清空数据
                            Call sqldel()
                            '写入文件
                            Call dr_huizong()
                        Case "发售统计"
                            '清空factory_config表
                            Call sqlfacdel()
                            '写入文件
                            Call dr_fashou()
                        Case "专家管理"
                            '清空expert_config表
                            Call sqlexpdel()
                            '写入文件
                            Call dr_zhuanjia()
                        Case "评审设置"
                            '按组删除repeal_config数据
                            Call sqlrepdel()
                            '写入文件
                            Call dr_chuping()
                    End Select
                    '初始化OpenFileDialog1.FileName
                    OpenFileDialog1.FileName = ""
                End If
            '个别设置
            Case 150
                Select Case GroupBox1.Text
                    Case "发售统计"
                        Try
                            '确定treeview选择级别
                            tvidx = TreeView1.SelectedNode.Level
                            Select Case tvidx
                                Case 2
                                    '获取factory_config.id
                                    str = SkinListView1.SelectedItems(0).Text
                                    Form1.sqlcon1.sqlstr("
                                    SELECT factory_config.facid
                                    FROM factory,factory_config
                                    WHERE factory_config.oveid = " & pro_id & "
                                    AND factory_config.pkgid = " & pkg_id & "
                                    AND factory_config.facid = factory.id
                                    AND factory.facname = " &
                                    Chr(34) & str & Chr(34), 1)
                                    Form1.sqlcon1.dataf.Read()
                                    fac_id = Form1.sqlcon1.dataf.Item(0)
                                    '更新弃标统计状态
                                    If SkinListView1.SelectedItems(0).SubItems(1).Text = "弃标" Then
                                        Form1.sqlcon1.sqlstr("
                                        UPDATE factory_config 
                                        SET disst = " & Chr(34) & Chr(34) & "
                                        WHERE oveid = " &
                                        pro_id & "
                                        AND pkgid = " &
                                        pkg_id & "
                                        AND facid = " &
                                        fac_id
                                                        )
                                    Else
                                        Form1.sqlcon1.sqlstr("
                                        UPDATE factory_config 
                                        SET disst = " &
                                        Chr(34) & "弃标" & Chr(34) & "
                                        WHERE oveid = " &
                                        pro_id & "
                                        AND pkgid = " &
                                        pkg_id & "
                                        AND facid = " &
                                        fac_id
                                                        )
                                    End If
                                Case Else
                                    wf1.wclose()
                                    MsgBox("警告：请选择要设置的的应答人", 0 + 48, "warning")
                                    Exit Sub
                            End Select
                            str = "
                            SELECT facname,disst,faccont,facline,facmail 
                            FROM factory,factory_config 
                            WHERE oveid = " & pro_id & "
                            AND pkgid = " & pkg_id & "
                            AND factory_config.facid = factory.id"
                            Call slvu(str, 5)
                        Catch ex As Exception
                            MsgBox("警告：请选择要设置的的应答人", 0 + 48, "warning")
                        End Try
                    Case "评审设置"
                        '显示废标设置form6
                        Try
                            Form6.TextBox1.Text = SkinListView1.SelectedItems(0).Text
                            Form1.sqlcon1.sqlstr("
                            SELECT bid.bidname,package.pkgidx
                            FROM bid,package
                            WHERE bid.oveid = " & pro_id & "
                            AND bid.id = " & bid_id & "
                            AND package.id = " & pkg_id & "
                            AND package.bidid = bid.id", 1)
                            Form1.sqlcon1.dataf.Read()
                            str = "（" & Form1.sqlcon1.dataf.Item(0) & "  " &
                                Form1.sqlcon1.dataf.Item(1) & "）"
                            Form6.Label2.Text = str
                            Form6.SkinTextBox1.Text = ""
                            Form6.SkinTextBox2.Text = ""
                            '获取facid
                            str = SkinListView1.SelectedItems(0).Text
                            Form1.sqlcon1.sqlstr("
                            SELECT factory_config.facid
                            FROM factory,factory_config
                            WHERE factory_config.oveid = " & pro_id & "
                            AND factory_config.pkgid = " & pkg_id & "
                            AND factory_config.facid = factory.id
                            AND factory.facname = " &
                            Chr(34) & str & Chr(34), 1)
                            Form1.sqlcon1.dataf.Read()
                            fac_id = Form1.sqlcon1.dataf.Item(0)
                            '显示form6废标界面
                            Form6.ShowDialog()
                        Catch ex As Exception
                            MsgBox("警告：请选择要设置的的应答人", 0 + 48, "warning")
                        End Try
                End Select
        End Select
        wf1.wclose()
        Panel2.Visible = False
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim str As String
        Dim wf1 As New wf

        wf1.wshow()
        Select Case Panel2.Left.ToString
            '导入设置
            Case 200
                If MsgBox("注意：是否清除已有数据并导入", 4 + 32, "caution") = MsgBoxResult.No Then
                    wf1.wclose()
                    Exit Sub
                End If
                '选择导入后执行
                OpenFileDialog1.Filter = "Excel File (*.xls;*.xlsx)|*.xls;*.xlsx"
                OpenFileDialog1.ShowDialog()
                If OpenFileDialog1.FileName <> "" Then
                    Try
                        '连接excel（文件名，sheet序号）
                        xlscon1.str(OpenFileDialog1.FileName, 1)
                    Catch ex As Exception
                        wf1.wclose()
                        '连接文件已打开报错
                        MsgBox("警告：发生未知错误 location:xlscon", 0 + 48, "warning")
                        Exit Sub
                    End Try
                    '判断groupbox.name
                    Select Case GroupBox1.Text
                        Case "信息维护"
                            '刷新fackage_request表char_zz,char_yj列
                            Form1.sqlcon1.sqlstr("
                            UPDATE package_request 
                            SET char_zz = " &
                            Chr(34) & "-" & Chr(34) & "
                            , char_yj = " &
                            Chr(34) & "-" & Chr(34) & "
                            WHERE oveid = " &
                            pro_id
                                                )
                            '写入文件
                            Call dr_zizhi()
                        Case "发售统计"
                            '刷新factory_config表disst列
                            Form1.sqlcon1.sqlstr("
                            UPDATE factory_config 
                            SET disst = " &
                            Chr(34) & Chr(34) & "
                            WHERE disst = " &
                            Chr(34) & "弃标" & Chr(34) & "
                            AND oveid = " &
                            pro_id
                                                )
                            '写入文件
                            Call dr_qibiao()
                        Case "评审设置"
                            '检查文件格式-线上文件
                            If xlscon1.wksheet.GetRow(0).GetCell(1).StringCellValue = "采购项目名称" Then
                                '按项目删除价格信息
                                Call Form9.sqlpredel(1)
                                '写入文件
                                Call Form9.dr_ecp_baojia()
                                wf1.wclose()
                                Exit Sub
                            End If
                            '显示报价导入form9-非线上文件
                            Form9.TextBox1.Text = ""
                            Form9.Label3.Text = "导入文件路径：" & OpenFileDialog1.FileName.ToString
                            '显示form9
                            Form9.ShowDialog()
                    End Select
                    '初始化OpenFileDialog1.FileName
                    OpenFileDialog1.FileName = ""
                End If
            '单独设置
            Case 150
                Select Case GroupBox1.Text
                    Case "评审设置"
                        '显示报价设置form8
                        Try
                            '填写textbox1,label2
                            Form8.TextBox1.Text = SkinListView1.SelectedItems(0).Text
                            Form1.sqlcon1.sqlstr("
                            SELECT bid.bidname,package.pkgidx
                            FROM bid,package
                            WHERE bid.oveid = " & pro_id & "
                            AND bid.id = " & bid_id & "
                            AND package.id = " & pkg_id & "
                            AND package.bidid = bid.id", 1
                                                 )
                            Form1.sqlcon1.dataf.Read()
                            str = "（" & Form1.sqlcon1.dataf.Item(0) & "  " &
                                Form1.sqlcon1.dataf.Item(1) & "）"
                            Form8.Label2.Text = str
                            '填写label4
                            Form1.sqlcon1.sqlstr("
                            SELECT char_bj 
                            FROM package_config 
                            WHERE pkgid = " &
                            pkg_id, 1)
                            Form1.sqlcon1.dataf.Read()
                            Select Case Form1.sqlcon1.dataf.Item(0)
                                Case "固定总价"
                                    Form8.Label4.Text = "固定总价（填写总价，单位万元）"
                                Case "折扣率"
                                    Form8.Label4.Text = "折扣率（填写折扣率小数）"
                                Case "综合单价"
                                    Form8.Label4.Text = "综合单价（填写单价合计，单位万元）"
                            End Select
                            '获取fac_id
                            Form1.sqlcon1.sqlstr("
                            SELECT factory_config.facid
                            FROM factory,factory_config
                            WHERE factory_config.oveid = " & pro_id & "
                            AND factory_config.pkgid = " & pkg_id & "
                            AND factory_config.facid = factory.id
                            AND factory.facname = " &
                            Chr(34) & SkinListView1.SelectedItems(0).Text & Chr(34), 1)
                            Form1.sqlcon1.dataf.Read()
                            fac_id = Form1.sqlcon1.dataf.Item(0)
                            '初始化form8.textbox2
                            Form8.TextBox2.Text = ""
                            '显示form8
                            Form8.ShowDialog()
                        Catch ex As Exception
                            MsgBox("警告：请选择要设置的的应答人", 0 + 48, "warning")
                        End Try
                End Select
        End Select
        wf1.wclose()
        Panel2.Visible = False
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Dim wf1 As New wf

        wf1.wshow()
        Select Case Panel2.Left.ToString
            Case 200
                If MsgBox("注意：是否清除已有数据并导入", 4 + 32, "caution") = MsgBoxResult.No Then
                    wf1.wclose()
                    Exit Sub
                End If
                '选择导入后执行
                OpenFileDialog1.Filter = "Excel File (*.xls;*.xlsx)|*.xls;*.xlsx"
                OpenFileDialog1.ShowDialog()
                If OpenFileDialog1.FileName <> "" Then
                    Try
                        '连接excel（文件名，sheet序号）

                        xlscon1.str(OpenFileDialog1.FileName, 1)
                    Catch ex As Exception
                        wf1.wclose()
                        '连接文件已打开报错
                        MsgBox("警告：发生未知错误 location:xlscon", 0 + 48, "warning")
                        Exit Sub
                    End Try
                    '判断groupbox.name
                    Select Case GroupBox1.Text
                        Case "评审设置"
                            '刷新package_abort
                            Call up_pkgaboert()
                            '写入sorting_price数据
                            Call dr_paixuzdj()
                            '清空sorting表
                            Form1.sqlcon1.sqlstr("
                            DELETE FROM sorting 
                            WHERE sorting.oveid = " & pro_id)
                            '写入sorting数据
                            Call dr_paixu()
                    End Select
                End If
        End Select
        wf1.wclose()
        Panel2.Visible = False
    End Sub

    Private Sub TextBox_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Select Case sender.name
            Case "TextBox1"
                If TextBox1.Text <> "" Then
                    PictureBox17.Image = Form2.ImageList1.Images.Item(0)
                Else
                    PictureBox17.Image = Form2.ImageList1.Images.Item(1)
                End If
        End Select
    End Sub

    Private Sub buttonedit_TextChanged(sender As Object, e As EventArgs) Handles ButtonEdit1.TextChanged
        Select Case sender.name
            Case "ButtonEdit1"
                If ButtonEdit1.Text <> "" Then
                    PictureBox18.Image = Form2.ImageList1.Images.Item(0)
                Else
                    PictureBox18.Image = Form2.ImageList1.Images.Item(1)
                End If
        End Select
    End Sub

    '横向-格式文件导出
    Private Sub PictureBox13_Click(sender As Object, e As EventArgs) Handles PictureBox13.Click
        Dim i As Integer
        Dim sli As New SortedList

        '显示waitform1
        wf1.wshow()
        '添加sli列表
        sli.Clear()
        sli.Add("1", "项目信息汇总表")
        sli.Add("2", "资质业绩要求汇总表")
        sli.Add("3", "发售记录（按应答人）")
        sli.Add("4", "弃标统计")
        sli.Add("5", "专家信息汇总表")
        sli.Add("6", "初评汇报表")
        sli.Add("7", "报价信息汇总表")
        sli.Add("8", "采购文件信息汇总表")
        sli.Add("9", "最终报价（186）")
        '清除group,items
        NavBarControl1.Items.Clear()
        NavBarControl1.Groups.Clear()
        '添加navbar-group
        NavBarControl1.Groups.Add()
        NavBarControl1.Groups(0).Caption = "格式文件导出"
        NavBarControl1.Groups(0).ImageOptions.SmallImageIndex = 0
        '添加navbar-items
        For i = 1 To 9
            NavBarControl1.Groups(0).AddItem()
            NavBarControl1.Items(i - 1).Caption = sli(CStr(i))
            NavBarControl1.Items(i - 1).ImageOptions.SmallImageIndex = 1
        Next
        '卸载waitform1
        wf1.wclose()
        '初始化Split
        PictureBox17.Image = Form2.ImageList1.Images.Item(1)
        PictureBox18.Image = Form2.ImageList1.Images.Item(0)
        TextBox1.Text = ""
        ButtonEdit1.Text = Form1.appath.ToString
        '显示SplitContainerControl1
        SplitContainerControl1.Visible = True
    End Sub

    '横向-资料导出
    Private Sub PictureBox14_Click(sender As Object, e As EventArgs) Handles PictureBox14.Click
        Dim i As Integer
        Dim sli As New SortedList

        '显示waitform1
        wf1.wshow()
        Application.DoEvents()
        '设置sortedlist
        sli.Clear()
        sli.Add("1", "发售资料")
        sli.Add("2", "项目准备资料")
        sli.Add("3", "专家资料")
        sli.Add("4", "评审过程资料")
        sli.Add("5", "定标资料")
        '清空navbarcontrol
        NavBarControl1.Items.Clear()
        NavBarControl1.Groups.Clear()
        '添加navbargroup
        For i = 1 To 5
            NavBarControl1.Groups.Add()
            NavBarControl1.Groups(i - 1).Caption = sli(CStr(i))
            NavBarControl1.Groups(i - 1).ImageOptions.SmallImageIndex = 0
        Next
        '添加navbaritems发售资料
        sli.Clear()
        sli.Add("1", "发售记录（按应答人）")
        sli.Add("2", "发售记录（按分包）")
        sli.Add("3", "递交登记表")
        sli.Add("4", "弃标统计")
        sli.Add("5", "应答人不足汇总审查")
        For i = 1 To 5
            NavBarControl1.Groups(0).AddItem()
            NavBarControl1.Items(i - 1).Caption = sli(CStr(i))
            If i = 5 Then
                NavBarControl1.Items(i - 1).ImageOptions.SmallImageIndex = 2
            Else
                NavBarControl1.Items(i - 1).ImageOptions.SmallImageIndex = 1
            End If
        Next
        '添加navbaritems项目准备资料
        sli.Add("11", "地签")
        sli.Add("12", "评审电话使用记录")
        sli.Add("13", "评审会场出入记录")
        sli.Add("14", "采购公告（表格）")
        For i = 11 To 14
            NavBarControl1.Groups(1).AddItem()
            NavBarControl1.Items(i - 6).Caption = sli(CStr(i))
            If i = 12 Or i = 13 Or i = 14 Then
                NavBarControl1.Items(i - 6).ImageOptions.SmallImageIndex = 2
            Else
                NavBarControl1.Items(i - 6).ImageOptions.SmallImageIndex = 1
            End If
        Next
        '添加navbaritems项目专家资料
        sli.Add("21", "手机保管记录")
        sli.Add("22", "专家签到表")
        sli.Add("23", "保密承诺函")
        sli.Add("24", "回避承诺函")
        sli.Add("25", "专家考勤表")
        sli.Add("26", "专家评价表")
        sli.Add("27", "专家费发放表")
        sli.Add("28", "专家费账号统计表")
        For i = 21 To 28
            NavBarControl1.Groups(2).AddItem()
            NavBarControl1.Items(i - 12).Caption = sli(CStr(i))
            If i = 25 Then
                NavBarControl1.Items(i - 12).ImageOptions.SmallImageIndex = 1
            Else
                NavBarControl1.Items(i - 12).ImageOptions.SmallImageIndex = 2
            End If
        Next
        '添加navbaritems过程资料
        sli.Add("31", "开封记录表")
        sli.Add("32", "授权代表身份验证")
        sli.Add("33", "阅标记录")
        sli.Add("34", "初评汇报表")
        sli.Add("35", "无效应答审批单")
        sli.Add("36", "有效应答不足审批单")
        sli.Add("37", "终止谈判通知单")
        sli.Add("38", "终止谈判通知单（按应答人）")
        sli.Add("39", "多轮报价记录表")
        sli.Add("40", "评审报告签字页")
        For i = 31 To 40
            NavBarControl1.Groups(3).AddItem()
            NavBarControl1.Items(i - 14).Caption = sli(CStr(i))
            If i = 37 Or i = 38 Or i = 40 Then
                NavBarControl1.Items(i - 14).ImageOptions.SmallImageIndex = 2
            Else
                NavBarControl1.Items(i - 14).ImageOptions.SmallImageIndex = 1
            End If
        Next
        '添加navbaritems定标资料
        sli.Add("41", "定标审批单")
        sli.Add("42", "评审报告（表格）")
        sli.Add("43", "领导小组审批单")
        sli.Add("44", "重大事项审批单")
        sli.Add("45", "成交候选人公示")
        sli.Add("46", "成交结果公告")
        sli.Add("47", "成交通知书")
        sli.Add("48", "成交人联系方式")
        sli.Add("49", "评审报告（专家）")
        For i = 41 To 49
            NavBarControl1.Groups(4).AddItem()
            NavBarControl1.Items(i - 14).Caption = sli(CStr(i))
            If i = 48 Then
                NavBarControl1.Items(i - 14).ImageOptions.SmallImageIndex = 1
            Else
                NavBarControl1.Items(i - 14).ImageOptions.SmallImageIndex = 2
            End If
        Next
        '卸载waitform1
        wf1.wclose()
        '初始化Split
        PictureBox17.Image = Form2.ImageList1.Images.Item(1)
        PictureBox18.Image = Form2.ImageList1.Images.Item(0)
        TextBox1.Text = ""
        ButtonEdit1.Text = Form1.appath.ToString
        '显示SplitContainerControl1
        SplitContainerControl1.Visible = True
    End Sub

    '横向-采购文件制作
    Private Sub PictureBox15_Click(sender As Object, e As EventArgs) Handles PictureBox15.Click

        '显示waitform1
        wf1.wshow()
        Application.DoEvents()
        'sql获取owner，worker表数据
        Form1.sqlcon1.sqlstr("
        SELECT 
        overload.proname,owner.owname,worker.wkcallm,worker.wkcalln 
        FROM overload,owner,worker 
        WHERE overload.id = " & pro_id & "
        AND overload.owid = owner.id
        AND overload.wkid = worker.id", 1)
        Form1.sqlcon1.dataf.Read()
        '初始化textbox
        Form10.TextBox1.Text = Form1.sqlcon1.dataf.Item(0)
        Form10.TextBox2.Text = Form1.sqlcon1.dataf.Item(1)
        Form10.TextBox3.Text = Form1.sqlcon1.dataf.Item(2)
        Form10.TextBox4.Text = Form1.sqlcon1.dataf.Item(3)
        Form10.TextBox5.Text = Format(Now(), "yyyy年MM月").ToString
        '初始化picturebox
        Form10.PictureBox7.Image = Form4.ImageList1.Images.Item(2)
        Form10.PictureBox8.Image = Form4.ImageList1.Images.Item(2)
        Form10.PictureBox9.Image = Form4.ImageList1.Images.Item(2)
        Form10.PictureBox10.Image = Form4.ImageList1.Images.Item(2)
        Form10.PictureBox11.Image = Form4.ImageList1.Images.Item(2)
        Form10.PictureBox12.Image = Form4.ImageList1.Images.Item(2)
        '初始化groupbox
        Form10.GroupBox1.Visible = True
        Form10.GroupBox2.Visible = False
        '卸载waitform1
        wf1.wclose()
        '显示form10
        Form10.ShowDialog()

    End Sub

    '文件下载图标
    Private Sub PictureBox19_Click(sender As Object, e As EventArgs) Handles PictureBox19.Click
        Dim fo1 As New fileoutput
        Dim fwo1 As New filewdoutput
        Dim dr As DataRow

        '检查textbox1和buttonedit1
        If TextBox1.Text = "" Or ButtonEdit1.Text = "" Then
            MsgBox("警告：导出文件名或导出路径不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '检查路径是否存在
        If Not Dir(ButtonEdit1.Text, vbDirectory) <> "" Then
            MsgBox("警告：指定文件夹路径不存在", 0 + 48, "warning")
            Exit Sub
        End If
        '显示waitform1
        wf1.wshow()
        Application.DoEvents()
        'select文件名call类fileoutput
        Select Case navbarstr
            Case "项目信息汇总表"
                fo1.dc_xinxi(ButtonEdit1.Text)
            Case "资质业绩要求汇总表"
                fo1.dc_zizhi(ButtonEdit1.Text)
            Case "发售记录（按应答人）"
                fo1.dc_fashou6(ButtonEdit1.Text)
            Case "弃标统计"
                fo1.dc_qibiao(ButtonEdit1.Text)
            Case "专家信息汇总表"
                fo1.dc_zjxinxi(ButtonEdit1.Text)
            Case "阅标记录"
                '判断文件路径
                If Not Dir(ButtonEdit1.Text & "\阅标记录\商务组", vbDirectory) <> "" Then
                    MkDir(ButtonEdit1.Text & "\阅标记录\商务组")
                End If
                If Not Dir(ButtonEdit1.Text & "\阅标记录\技术组", vbDirectory) <> "" Then
                    MkDir(ButtonEdit1.Text & "\阅标记录\技术组")
                End If
                If Not Dir(ButtonEdit1.Text & "\阅标记录\示例", vbDirectory) <> "" Then
                    MkDir(ButtonEdit1.Text & "\阅标记录\示例")
                End If
                '判断示例文件
                If Not Dir(ButtonEdit1.Text & "\阅标记录\示例\商务响应情况汇总表-示例.xlsx") <> "" Then
                    MsgBox("警告：不存在商务示例文件", 0 + 48, "warning")
                    Exit Select
                End If
                If Not Dir(ButtonEdit1.Text & "\阅标记录\示例\技术响应情况汇总表-示例.xlsx") <> "" Then
                    MsgBox("警告：不存在技术示例文件", 0 + 48, "warning")
                    Exit Select
                End If
                'sql获取bid.id,bidname表数据
                Form1.sqlcon1.sqlstr("
                SELECT DISTINCT 
                bid.id,bid.bidname,overload.proname
                FROM bid,overload
                WHERE overload.id = " & pro_id & "
                AND overload.id = bid.oveid", 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    While Form1.sqlcon1.dataf.Read()
                        Try
                            '商务阅标记录
                            xlscon1.str(ButtonEdit1.Text & "\阅标记录\示例\商务响应情况汇总表-示例.xlsx", 1)
                            fo1.dc_swyuebiao(ButtonEdit1.Text & "\阅标记录\商务组", Form1.sqlcon1.dataf.Item(0))
                            '技术阅标记录
                            xlscon1.str(ButtonEdit1.Text & "\阅标记录\示例\技术响应情况汇总表-示例.xlsx", 1)
                            fo1.dc_jsyuebiao(ButtonEdit1.Text & "\阅标记录\技术组", Form1.sqlcon1.dataf.Item(0))
                        Catch ex As Exception
                            MsgBox("警告：发生未知错误 location:xlscon", 0 + 48, "warning")
                            Exit Select
                        End Try
                    End While
                Else
                    MsgBox("警告：未找到项目信息", 0 + 48, "warning")
                    Exit Select
                End If
                MsgBox("提示：已导出指定文件", 0 + 64, "information")
            Case "初评汇报表"
                fo1.dc_chuping(ButtonEdit1.Text)
            Case "报价信息汇总表"
                fo1.dc_baojia(ButtonEdit1.Text)
            Case "发售记录（按分包）"
                fo1.dc_fashou7(ButtonEdit1.Text)
            Case "递交登记表"
                fo1.dc_dijiao(ButtonEdit1.Text)
            Case "应答人不足汇总审查"
                fwo1.dc_kailiu(ButtonEdit1.Text)
            Case "地签"
                fo1.dc_diqian(ButtonEdit1.Text)
            Case "评审电话使用记录"
                fwo1.dc_dianhua(ButtonEdit1.Text)
            Case "评审会场出入记录"
                fwo1.dc_churu(ButtonEdit1.Text)
            Case "开封记录表"
                fo1.dc_kaifeng(ButtonEdit1.Text)
            Case "授权代表身份验证"
                fo1.dc_yanzheng(ButtonEdit1.Text)
            Case "无效应答审批单"
                fo1.dc_wuxiao(ButtonEdit1.Text)
            Case "有效应答不足审批单"
                fo1.dc_youxiao(ButtonEdit1.Text)
            Case "多轮报价记录表"
                fo1.dc_duolun(ButtonEdit1.Text)
            'Case "评审委员会组成"
            '    fwo1.dc_zucheng(ButtonEdit1.Text)
            Case "手机保管记录"
                fwo1.dc_baodao(ButtonEdit1.Text)
            Case "专家签到表"
                fwo1.dc_qiandao(ButtonEdit1.Text)
            Case "保密承诺函"
                fwo1.dc_baomi(ButtonEdit1.Text)
            Case "回避承诺函"
                fwo1.dc_huibi(ButtonEdit1.Text)
            Case "专家考勤表"
                fo1.dc_kaoqin(ButtonEdit1.Text)
            Case "专家评价表"
                fwo1.dc_pingjia(ButtonEdit1.Text)
            Case "终止谈判通知单"
                fwo1.dc_zhongzhi(ButtonEdit1.Text)
            Case "终止谈判通知单（按应答人）"
                '判断文件路径
                If Not Dir(ButtonEdit1.Text & "\终止谈判通知单（按应答人）", vbDirectory) <> "" Then
                    MkDir(ButtonEdit1.Text & "\终止谈判通知单（按应答人）")
                End If
                Dim i As Integer = 1
                'sql获取bid.id，package.id表数据
                Form1.sqlcon1.sqlstr("
                SELECT DISTINCT 
                repeal_config.id 
                FROM 
                overload,owner,bid,package,factory,repeal_config,repeal
                WHERE overload.id = " & pro_id & "
                AND overload.owid = owner.id 
                AND bid.oveid = overload.id 
                AND package.bidid = bid.id 
                AND repeal_config.pkgid = package.id 
                AND repeal_config.repid = repeal.id
                AND repeal_config.facid = factory.id 
                ORDER BY factory.facname,bid.id,package.id", 2)
                If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                    Try
                        For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                            fwo1.dc_zhongzhi_ydr(ButtonEdit1.Text & "\终止谈判通知单（按应答人）", dr.Item(0), i)
                            i += 1
                        Next
                    Catch ex As Exception
                        MsgBox("警告：发生未知错误 location:sorting", 0 + 48, "warning")
                        Exit Select
                    End Try
                Else
                    MsgBox("警告：未找到终止谈判信息", 0 + 48, "warning")
                    Exit Select
                End If
                MsgBox("提示：已导出指定文件", 0 + 64, "information")
            Case "评审报告签字页"
                fwo1.dc_qianziye(ButtonEdit1.Text)
            Case "定标审批单"
                Try
                    fwo1.dc_shenpi(ButtonEdit1.Text)
                Catch ex As Exception
                    MsgBox("警告：发生未知错误 location:sorting", 0 + 48, "warning")
                    Exit Select
                End Try
            Case "评审报告（表格）"
                Try
                    fwo1.dc_baogao(ButtonEdit1.Text)
                Catch ex As Exception
                    MsgBox("警告：发生未知错误 location:sorting", 0 + 48, "warning")
                    Exit Select
                End Try
            Case "领导小组审批单"
                Try
                    fwo1.dc_ldshenpi(ButtonEdit1.Text)
                Catch ex As Exception
                    MsgBox("警告：发生未知错误 location:sorting", 0 + 48, "warning")
                    Exit Select
                End Try
            Case "重大事项审批单"
                Try
                    fwo1.dc_zdshenpi(ButtonEdit1.Text)
                Catch ex As Exception
                    MsgBox("警告：发生未知错误 location:sorting", 0 + 48, "warning")
                    Exit Select
                End Try
            Case "成交候选人公示"
                Try
                    fwo1.dc_houxuan(ButtonEdit1.Text)
                Catch ex As Exception
                    MsgBox("警告：发生未知错误 location:sorting", 0 + 48, "warning")
                    Exit Select
                End Try
            Case "成交结果公告"
                Try
                    fwo1.dc_gonggao(ButtonEdit1.Text)
                Catch ex As Exception
                    MsgBox("警告：发生未知错误 location:sorting", 0 + 48, "warning")
                    Exit Select
                End Try
            Case "成交通知书"
                '判断文件路径
                If Not Dir(ButtonEdit1.Text & "\通知书", vbDirectory) <> "" Then
                    MkDir(ButtonEdit1.Text & "\通知书")
                End If
                'sql获取bid.id，package.id表数据
                Form1.sqlcon1.sqlstr("
                SELECT DISTINCT 
                bid.id,package.id 
                FROM bid,package
                WHERE bid.oveid = " & pro_id & "
                AND bid.id= package.bidid
                AND package.id NOT IN 
                (SELECT pkgid 
                FROM package_abort)
                ORDER BY bid.id,package.id", 2)
                If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                    Try
                        For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                            fwo1.dc_tongzhishu(ButtonEdit1.Text & "\通知书", dr.Item(1))
                        Next
                    Catch ex As Exception
                        MsgBox("警告：发生未知错误 location:sorting", 0 + 48, "warning")
                        Exit Select
                    End Try
                Else
                    MsgBox("警告：未找到中标信息", 0 + 48, "warning")
                    Exit Select
                End If
                MsgBox("提示：已导出指定文件", 0 + 64, "information")
            Case "成交人联系方式"
                fo1.dc_chengjiaolx(ButtonEdit1.Text)
            Case "采购文件信息汇总表"
                fo1.dc_cgwjhuizong(ButtonEdit1.Text)
            Case "最终报价（186）"
                fo1.dc_186baojia(ButtonEdit1.Text)
            Case "采购公告（表格）"
                fwo1.dc_cggonggao(ButtonEdit1.Text)
            Case "评审报告（专家）"
                fwo1.dc_baogaozj(ButtonEdit1.Text)
            Case "专家费发放表"
                fwo1.dc_zhuanjiafei(ButtonEdit1.Text)
            Case "专家费账号统计表"
                fwo1.dc_zhuanjiafeizh(ButtonEdit1.Text)
        End Select
        '卸载waitform1
        wf1.wclose()

    End Sub

    '点击buttonedit的button
    Sub ButtonEdit1_buttonclick(sender As Object, e As EventArgs) Handles ButtonEdit1.ButtonClick

        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            ButtonEdit1.Text = FolderBrowserDialog1.SelectedPath
        End If

    End Sub

    Sub navbaritem_click(sender As Object, e As DevExpress.XtraNavBar.NavBarLinkEventArgs) Handles NavBarControl1.LinkClicked
        Dim str As String

        str = e.Link.Item.Caption
        navbarstr = str
        TextBox1.Text = str

    End Sub
End Class

Public Class xlscon
    Public wksheet As NPOI.SS.UserModel.ISheet
    Public wkbk As NPOI.SS.UserModel.IWorkbook
    Sub str(ByVal s As String, ByVal i As Integer)
        '判断文件名，wkbook打开文件流
        If s.IndexOf(".xlsx") > -1 Then
            Call exlsx(s, i)
        ElseIf s.IndexOf(".xls") > -1 Then
            Call exls(s, i)
        End If

    End Sub
    Sub exls(ByVal s As String, ByVal i As Integer)
        '新建文件流（文件全路径，文件流模式，文件流读取模式）
        Dim fs As New FileStream(s, FileMode.Open, FileAccess.Read)
        Dim wkbook As New HSSFWorkbook(fs)
        wkbk = wkbook
        wksheet = wkbook.GetSheetAt(i - 1)
    End Sub
    Sub exlsx(ByVal s As String, ByVal i As Integer)
        '新建文件流（文件全路径，文件流模式，文件流读取模式）
        Dim fs As New FileStream(s, FileMode.Open, FileAccess.Read)
        Dim wkbook As New XSSFWorkbook(fs)
        wkbk = wkbook
        wksheet = wkbook.GetSheetAt(i - 1)
    End Sub
End Class

Public Class fileoutput
    Dim wok As New XSSFWorkbook
    Dim sht As XSSFSheet
    Dim row As SS.UserModel.IRow
    Dim cel As XSSFCell
    Dim cstyle As SS.UserModel.ICellStyle
    Dim cfont As XSSFFont

    'fp为文件保存位置
    Public Sub dc_xinxi(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        '设置sheet
        sht = wok.CreateSheet("项目信息汇总")
        '设置标题行
        sli.Clear()
        sli.Add("1", "分标号")
        sli.Add("2", "分标名称")
        sli.Add("3", "分标编号")
        sli.Add("4", "分包号")
        sli.Add("5", "分包名称")
        sli.Add("6", "项目单位")
        sli.Add("7", "工程名称")
        sli.Add("8", "工程概算")
        sli.Add("9", "工程限价")
        '插入标题行
        row = sht.CreateRow(0)
        '行高
        row.Height = 21 * 20
        cstyle = wok.CreateCellStyle
        '对齐方式
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = True
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        For xlcol = 1 To 9
            cel = row.CreateCell(xlcol - 1)
            '列宽
            sht.SetColumnWidth(xlcol - 1, 18 * 275)
            '数据
            cel.SetCellValue(sli(CStr(xlcol)))
            cel.CellStyle = cstyle
        Next
        '获取数据dataset
        Form1.sqlcon1.sqlstr("
        SELECT 
        bid.bididx,bid.bidname,bid.bidno,
        package.pkgidx,package.pkgname,
        engineering.engidx,engineering.engname,
        engineering_config.char_gs,engineering_config.char_xj
        FROM 
        bid,package,engineering,engineering_config
        WHERE 
        bid.oveid = " & Form3.pro_id & "
        AND
        bid.id = package.bidid
        AND 
        package.id = engineering.pkgid
        AND 
        engineering.id = engineering_config.engid
        ORDER BY 
        bid.id,package.id,engineering.id", 2)
        '初始化xlrow数据开始行
        xlrow = 1
        '判断是否存在数据
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                row = sht.CreateRow(xlrow)
                For i = 1 To 9
                    cel = row.CreateCell(i - 1)
                    If i = 8 Or i = 9 Then
                        cel.SetCellValue(dr.Item(i - 1))
                    Else
                        cel.SetCellValue(dr.Item(i - 1).ToString)
                    End If
                Next
                xlrow += 1
            Next
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")

    End Sub

    Public Sub dc_cgwjhuizong(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow
        Dim str As String

        '设置sheet
        sht = wok.CreateSheet("采购文件信息汇总")
        '设置标题行
        sli.Clear()
        sli.Add("1", "分标号")
        sli.Add("2", "分标名称")
        sli.Add("3", "分包号")
        sli.Add("4", "分包名称")
        sli.Add("5", "采购文件模板")
        sli.Add("6", "合同模板")
        sli.Add("7", "商务初评模板")
        sli.Add("8", "技术初评模板")
        sli.Add("9", "商务详评模板")
        sli.Add("10", "技术详评模板")
        sli.Add("11", "价格模板")
        sli.Add("12", "固定服务费（万元）")
        '插入标题行
        row = sht.CreateRow(0)
        '行高
        row.Height = 35 * 20
        cstyle = wok.CreateCellStyle
        '对齐方式
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = True
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        For xlcol = 1 To 12
            cel = row.CreateCell(xlcol - 1)
            '列宽
            sht.SetColumnWidth(xlcol - 1, 15 * 275)
            '数据
            cel.SetCellValue(sli(CStr(xlcol)))
            cel.CellStyle = cstyle
        Next
        '获取数据dataset
        Try
            Form1.sqlcon1.sqlstr("
            SELECT 
            tb1.bididx,tb1.bidname,tb1.pkgidx,tb1.pkgname,
            bidoc_mb.char_na,bidoc_ht.char_na,
            bidoc_swcp.char_na,bidoc_jscp.char_na,
            bidoc_swxp.char_na,bidoc_jsxp.char_na,
            bidoc_jg.char_na,
            tb1.chag
            FROM
            (SELECT
            bid.bididx,
            bid.bidname,
            bid.bidno,
            package.pkgidx,
            package.pkgname,
            bidoc_main.mbid,
            bidoc_main.htid,
            bidoc_main.swcpid,
            bidoc_main.jscpid,
            bidoc_main.swxpid,
            bidoc_main.jsxpid,
            bidoc_main.jgid,
            bidoc_main.chag
            FROM 
            bid,package,bidoc_main
            WHERE bidoc_main.oveid = " & Form3.pro_id & "
            AND bid.oveid = bidoc_main.oveid
            AND bid.id = package.bidid
            AND package.id = bidoc_main.pkgid
            ORDER BY bid.id,package.id) as tb1
            LEFT JOIN bidoc_mb ON tb1.mbid = bidoc_mb.id
            LEFT JOIN bidoc_ht ON tb1.htid = bidoc_ht.id
            LEFT JOIN bidoc_swcp ON tb1.swcpid = bidoc_swcp.id
            LEFT JOIN bidoc_jscp ON tb1.jscpid = bidoc_jscp.id
            LEFT JOIN bidoc_swxp ON tb1.swxpid = bidoc_swxp.id
            LEFT JOIN bidoc_jsxp ON tb1.jsxpid = bidoc_jsxp.id
            LEFT JOIN bidoc_jg ON tb1.jgid = bidoc_jg.id", 2)
        Catch ex As Exception
            MsgBox("警告：未选择采购文件导出路径", 0 + 48, "warning")
            Exit Sub
        End Try
        '初始化xlrow数据开始行
        xlrow = 1
        '判断是否存在数据
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                row = sht.CreateRow(xlrow)
                For i = 1 To 12
                    cel = row.CreateCell(i - 1)
                    If i < 5 Then
                        cel.SetCellValue(dr.Item(i - 1).ToString)
                    Else
                        If i = 12 Then
                            cel.SetCellValue(dr.Item(i - 1))
                        Else
                            If dr.Item(i - 1).ToString = "" Then
                                str = ""
                            Else
                                str = dr.Item(i - 1).ToString
                                str = str.Substring(0, Len(str) - 5)
                            End If
                            cel.SetCellValue(str)
                        End If
                    End If
                Next
                xlrow += 1
            Next
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")

    End Sub

    Public Sub dc_zizhi(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        '设置sheet
        sht = wok.CreateSheet("资质业绩汇总")
        '设置标题行
        sli.Clear()
        sli.Add("1", "分标号")
        sli.Add("2", "分标名称")
        sli.Add("3", "分包号")
        sli.Add("4", "资质要求")
        sli.Add("5", "业绩要求")
        '插入标题行
        row = sht.CreateRow(0)
        '行高
        row.Height = 21 * 20
        cstyle = wok.CreateCellStyle
        '对齐方式
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = True
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        For xlcol = 1 To 5
            cel = row.CreateCell(xlcol - 1)
            '列宽
            sht.SetColumnWidth(xlcol - 1, 20 * 275)
            '数据
            cel.SetCellValue(sli(CStr(xlcol)))
            cel.CellStyle = cstyle
        Next
        '获取数据dataset
        Form1.sqlcon1.sqlstr("
        SELECT 
        bid.bididx,bid.bidname,
        package.pkgidx,
        package_request.char_zz,package_request.char_yj
        FROM 
        bid,package,package_request
        WHERE 
        bid.oveid = " & Form3.pro_id & "
        AND
        bid.id = package.bidid
        AND 
        package.id = package_request.pkgid
        ORDER BY 
        bid.id,package.id", 2)
        '初始化xlrow数据开始行
        xlrow = 1
        '判断是否存在数据
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                row = sht.CreateRow(xlrow)
                For i = 1 To 5
                    cel = row.CreateCell(i - 1)
                    cel.SetCellValue(dr.Item(i - 1).ToString)
                Next
                xlrow += 1
            Next
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")

    End Sub

    Public Sub dc_fashou6(ByVal fp As String)
        Dim xlcol, xlrow, factmax As Integer
        Dim str As String
        Dim sli As New SortedList
        Dim dr As DataRow

        '设置sheet
        sht = wok.CreateSheet("发售统计")
        '设置标题行
        sli.Clear()
        sli.Add("1", "序号")
        sli.Add("2", "分标名称")
        sli.Add("3", "应答人")
        sli.Add("4", "联系人")
        sli.Add("5", "电话")
        sli.Add("6", "邮箱")
        sli.Add("7", "应答包号")
        '插入标题行-大标题
        Form1.sqlcon1.sqlstr("
        SELECT proname 
        FROM overload 
        WHERE id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        row = sht.CreateRow(0)
        row.Height = 30 * 20
        cel = row.CreateCell(0)
        cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
        '对齐方式
        cstyle = wok.CreateCellStyle
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = True
        cfont.FontHeightInPoints = 16
        cstyle.SetFont(cfont)
        cel.CellStyle = cstyle
        '插入标题行-小标题
        row = sht.CreateRow(1)
        '行高
        row.Height = 30 * 20
        '边框
        cstyle = wok.CreateCellStyle
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
        cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
        cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
        cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = True
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        For xlcol = 1 To 7
            cel = row.CreateCell(xlcol - 1)
            '列宽
            Select Case xlcol
                Case 1
                    sht.SetColumnWidth(xlcol - 1, 6 * 275)
                Case 2
                    sht.SetColumnWidth(xlcol - 1, 18 * 275)
                Case 3
                    sht.SetColumnWidth(xlcol - 1, 27 * 275)
                Case 4
                    sht.SetColumnWidth(xlcol - 1, 11 * 275)
                Case 5
                    sht.SetColumnWidth(xlcol - 1, 15 * 275)
                Case 6
                    sht.SetColumnWidth(xlcol - 1, 15 * 275)
            End Select
            '数据
            cel.SetCellValue(sli(CStr(xlcol)))
            cel.CellStyle = cstyle
        Next
        cstyle = wok.CreateCellStyle
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
        cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
        cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
        cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
        cstyle.WrapText = True
        '自动换行
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = False
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        '获取数据dataset
        Form1.sqlcon1.sqlstr("
        SELECT 
        bid.bidname,
        factory.facname,factory.faccont,factory.facline,factory.facmail,
        count(factory.facname) AS faccount
        FROM 
        bid,package,factory,factory_config
        WHERE 
        bid.oveid = " & Form3.pro_id & "
        AND
        bid.id = package.bidid
        AND 
        factory.id = factory_config.facid
        AND 
        package.id = factory_config.pkgid
        GROUP BY 
        bid.bidname,factory.facname
        ORDER BY 
        factory.facname,package.id", 2)
        '初始化xlrow数据开始行，初始化factmax最大行
        xlrow = 2
        factmax = 1
        '判断是否存在数据
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                row = sht.CreateRow(xlrow)
                row.Height = 40 * 20
                For i = 1 To 6
                    cel = row.CreateCell(i - 1)
                    cel.CellStyle = cstyle
                    If i = 1 Then
                        cel.SetCellValue(xlrow - 1)
                    Else
                        cel.SetCellValue(dr.Item(i - 2).ToString)
                    End If
                Next
                xlrow += 1
                '设置最大包数faccountmax
                If dr.Item(5) > factmax Then
                    factmax = dr.Item(5)
                End If
            Next
            '初始化xlrow数据开始行
            xlrow = 2
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                '初始化xlcol列
                xlcol = 6
                '设置格式
                For i = 7 To 7 + factmax - 1
                    cel = sht.GetRow(xlrow).CreateCell(i - 1)
                    cel.CellStyle = cstyle
                Next
                '填入包数据
                Form1.sqlcon1.sqlstr("
                SELECT package.pkgidx 
                FROM package,bid,factory_config 
                WHERE package.bidid = bid.id 
                AND bid.oveid = " & Form3.pro_id & "
                AND bid.bidname = " & Chr(34) & dr.Item(0) & Chr(34) & "
                AND factory_config.pkgid = package.id 
                AND factory_config.facid IN 
                (SELECT id 
                FROM factory 
                WHERE facname = " & Chr(34) & dr.Item(1) & Chr(34) & ")
                ORDER BY package.id", 1)
                While Form1.sqlcon1.dataf.Read()
                    str = Form1.sqlcon1.dataf.Item(0)
                    str = str.Substring(1, Len(str) - 1)
                    cel = sht.GetRow(xlrow).GetCell(xlcol)
                    cel.SetCellValue(CInt(str))
                    xlcol += 1
                End While
                xlrow += 1
            Next
        End If
        cstyle = wok.CreateCellStyle
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
        cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
        cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
        cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
        '设置小标题字体
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = True
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        If factmax = 1 Then
            '合并单元格
            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 6))
        Else
            For i = 8 To 7 + factmax - 1
                cel = sht.GetRow(1).CreateCell(i - 1)
                cel.CellStyle = cstyle
            Next
            '合并单元格
            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 6 + factmax - 1))
            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 6, 6 + factmax - 1))
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")

    End Sub

    Public Sub dc_qibiao(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        '设置sheet
        sht = wok.CreateSheet("弃标统计")
        '设置标题行
        sli.Clear()
        sli.Add("1", "分标号")
        sli.Add("2", "分标名称")
        sli.Add("3", "分包号")
        sli.Add("4", "应答人")
        sli.Add("5", "弃标情况")
        '插入标题行
        row = sht.CreateRow(0)
        '行高
        row.Height = 21 * 20
        cstyle = wok.CreateCellStyle
        '对齐方式
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = True
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        For xlcol = 1 To 5
            cel = row.CreateCell(xlcol - 1)
            '列宽
            If xlcol = 4 Then
                sht.SetColumnWidth(xlcol - 1, 32 * 275)
            Else
                sht.SetColumnWidth(xlcol - 1, 18 * 275)
            End If
            '填入数据
            cel.SetCellValue(sli(CStr(xlcol)))
            cel.CellStyle = cstyle
        Next
        cstyle = wok.CreateCellStyle
        '对齐方式
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = False
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        '获取数据dataset
        Form1.sqlcon1.sqlstr("
        SELECT 
        *
        FROM
        (SELECT 
        bid.bididx as bidid,bid.bidname as bidname,
        package.pkgidx as pkgidx,package.id as pkgid
        FROM
         bid,package
        WHERE
        bid.oveid = " & Form3.pro_id & "
        AND
        bid.id = package.bidid
        ORDER BY 
        bid.id,package.id) AS tb1
        LEFT JOIN 
        (SELECT
        factory_config.pkgid as pkgid,factory.facname as facname,factory_config.disst as disst
        FROM 
        factory,factory_config
        WHERE 
        factory_config.facid = factory.id) AS tb2
        ON
        tb1.pkgid = tb2.pkgid ", 2)
        '初始化xlrow数据开始行
        xlrow = 1
        '判断是否存在数据
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                row = sht.CreateRow(xlrow)
                For i = 1 To 5
                    cel = row.CreateCell(i - 1)
                    If i = 4 Or i = 5 Then
                        cel.SetCellValue(dr.Item(i + 1).ToString)
                    Else
                        cel.SetCellValue(dr.Item(i - 1).ToString)
                    End If
                    cel.CellStyle = cstyle
                Next
                xlrow += 1
            Next
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")

    End Sub

    Public Sub dc_zjxinxi(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        '设置sheet
        sht = wok.CreateSheet("专家信息汇总")
        '设置标题行
        sli.Clear()
        sli.Add("1", "序号")
        sli.Add("2", "组别")
        sli.Add("3", "组内职位")
        sli.Add("4", "姓名")
        sli.Add("5", "身份证")
        sli.Add("6", "专业")
        sli.Add("7", "工作单位")
        sli.Add("8", "电话")
        sli.Add("9", "银行名称")
        sli.Add("10", "银行账号")
        '插入标题行
        row = sht.CreateRow(0)
        '行高
        row.Height = 21 * 20
        cstyle = wok.CreateCellStyle
        '对齐方式
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = True
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        For xlcol = 1 To 10
            cel = row.CreateCell(xlcol - 1)
            '列宽
            sht.SetColumnWidth(xlcol - 1, 20 * 275)
            '数据
            cel.SetCellValue(sli(CStr(xlcol)))
            cel.CellStyle = cstyle
        Next
        cstyle = wok.CreateCellStyle
        '对齐方式
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = False
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        '获取数据dataset
        Form1.sqlcon1.sqlstr("
        SELECT 
        expert_config.char_gp,expert_config.char_po,
        expert.expname,expert.expiden,expert.expprof,
        expert.expadds,expert.expline,expert.expbnka,expert.expbnkn
        FROM 
        expert,expert_config
        WHERE 
        expert.id = expert_config.expid
        AND 
        expert_config.oveid = " & Form3.pro_id & "
        ORDER BY
        expert_config.char_gp,expert_config.char_po DESC", 2)
        '初始化xlrow数据开始行
        xlrow = 1
        '判断是否存在数据
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                row = sht.CreateRow(xlrow)
                For i = 1 To 10
                    cel = row.CreateCell(i - 1)
                    If i = 1 Then
                        cel.SetCellValue(xlrow)
                    Else
                        cel.SetCellValue(dr.Item(i - 2).ToString)
                    End If
                    cel.CellStyle = cstyle
                Next
                xlrow += 1
            Next
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")

    End Sub

    Public Sub dc_baojia(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        '设置sheet
        sht = wok.CreateSheet("报价信息汇总")
        '设置标题行
        sli.Clear()
        sli.Add("1", "序号")
        sli.Add("2", "分标号")
        sli.Add("3", "分标名称")
        sli.Add("4", "分包号")
        sli.Add("5", "应答人")
        sli.Add("6", "报价（万元）")
        '插入标题行
        row = sht.CreateRow(0)
        '行高
        row.Height = 21 * 20
        cstyle = wok.CreateCellStyle
        '对齐方式
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = True
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        For xlcol = 1 To 6
            cel = row.CreateCell(xlcol - 1)
            '列宽
            sht.SetColumnWidth(xlcol - 1, 18 * 275)
            '数据
            cel.SetCellValue(sli(CStr(xlcol)))
            cel.CellStyle = cstyle
        Next
        cstyle = wok.CreateCellStyle
        '对齐方式
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        '字体格式
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.IsBold = False
        cfont.FontHeightInPoints = 13
        cstyle.SetFont(cfont)
        '获取数据dataset
        Form1.sqlcon1.sqlstr("
        SELECT 
        bid.bididx,bid.bidname,
        package.pkgidx,
        factory.facname
        FROM 
        bid,package,factory,factory_config
        WHERE 
        bid.oveid = " & Form3.pro_id & "
        AND
        bid.id = package.bidid
        AND 
        package.id = factory_config.pkgid
        AND 
        factory_config.facid = factory.id
        AND 
        factory_config.disst = " & Chr(34) & Chr(34) & "
        AND 
        factory_config.repst = " & Chr(34) & Chr(34) & "
        ORDER BY 
        bid.id,package.id", 2)
        '初始化xlrow数据开始行
        xlrow = 1
        '判断是否存在数据
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                row = sht.CreateRow(xlrow)
                row.Height = 21 * 20
                For i = 1 To 5
                    cel = row.CreateCell(i - 1)
                    If i = 1 Then
                        cel.SetCellValue(xlrow)
                    Else
                        cel.SetCellValue(dr.Item(i - 2).ToString)
                    End If
                    cel.CellStyle = cstyle
                Next
                xlrow += 1
            Next
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")

    End Sub

    Public Sub dc_diqian(ByVal fp As String)
        Dim xlcol, xlrow, i As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        '设置sheet
        sht = wok.CreateSheet("地签")
        '获取数据dataset
        Form1.sqlcon1.sqlstr("
        SELECT 
        bid.bidname,package.pkgidx,package.pkgname,bid.bididx
        FROM 
        bid,package
        WHERE 
        bid.oveid = " & Form3.pro_id & "
        AND 
        bid.id = package.bidid", 2)
        '判断是否存在数据
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            cstyle = wok.CreateCellStyle
            cfont = wok.CreateFont
            cfont.FontName = "宋体"
            cfont.FontHeightInPoints = 40
            cfont.IsBold = True
            cstyle.SetFont(cfont)
            cstyle.WrapText = True
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            '初始化xlrow数据开始行
            xlrow = 0
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                row = sht.CreateRow(xlrow)
                row.Height = 409 * 20
                i = 1
                cel = row.CreateCell(i - 1)
                cel.SetCellValue(dr.Item(i + 2).ToString & " " & dr.Item(i - 1).ToString)
                cel.CellStyle = cstyle
                xlrow += 1
            Next
            cstyle = wok.CreateCellStyle
            cfont = wok.CreateFont
            cfont.FontName = "宋体"
            cfont.FontHeightInPoints = 50
            cfont.IsBold = True
            cstyle.SetFont(cfont)
            cstyle.WrapText = True
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            '初始化xlrow数据开始行
            xlrow = 0
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                row = sht.GetRow(xlrow)
                i = 2
                cel = row.CreateCell(i - 1)
                cel.SetCellValue(dr.Item(i - 1).ToString & " " & dr.Item(i).ToString)
                cel.CellStyle = cstyle
                xlrow += 1
            Next
            '设置列宽
            For xlcol = 1 To 2
                If xlcol = 1 Then
                    sht.SetColumnWidth(xlcol - 1, 19 * 275)
                Else
                    sht.SetColumnWidth(xlcol - 1, 100 * 275)
                End If
            Next
            '设置纸张方向
            sht.PrintSetup.Landscape = True
            sht.PrintSetup.PaperSize = 9
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")

    End Sub

    Public Sub dc_fashou7(ByVal fp As String)
        Dim xlcol, xlrow, i As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        'sql获取proname,bidname并创建
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono,bid.bididx,bid.bidname,bid.bidno
        FROM overload,bid
        WHERE overload.id = " & Form3.pro_id & "
        AND overload.id = bid.oveid 
        ORDER BY bid.id", 1)
        '创建表格式及标题行
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                sht = wok.CreateSheet(Form1.sqlcon1.dataf.Item(2).ToString & Form1.sqlcon1.dataf.Item(3).ToString)
                For xlrow = 0 To 2
                    Select Case xlrow
                        Case 0
                            row = sht.CreateRow(xlrow)
                            row.Height = 19 * 20
                            For xlcol = 0 To 6
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue("采购文件发售记录")
                                End If
                                cstyle = wok.CreateCellStyle
                                cfont = wok.CreateFont
                                cfont.FontName = "方正仿宋_GBK"
                                cfont.FontHeightInPoints = 11
                                cfont.IsBold = True
                                cstyle.Alignment = HorizontalAlignment.Center
                                cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 6))
                        Case 1
                            row = sht.CreateRow(xlrow)
                            row.Height = 65 * 20
                            For xlcol = 0 To 6
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue("项目名称：" & Form1.sqlcon1.dataf.Item(0) & vbCrLf &
                                                     "采购编号：" & Form1.sqlcon1.dataf.Item(1) & vbCrLf &
                                                     "分标名称：" & Form1.sqlcon1.dataf.Item(2) & " " &
                                                     Form1.sqlcon1.dataf.Item(3) & vbCrLf &
                                                     "分标编号：" & Form1.sqlcon1.dataf.Item(4))
                                End If
                                cstyle = wok.CreateCellStyle
                                cfont = wok.CreateFont
                                cfont.FontName = "方正仿宋_GBK"
                                cfont.FontHeightInPoints = 11
                                cfont.IsBold = True
                                cstyle.WrapText = True
                                cstyle.Alignment = HorizontalAlignment.Left
                                cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 0, 6))
                        Case 2
                            sli.Clear()
                            sli.Add("1", "序号")
                            sli.Add("2", "包号")
                            sli.Add("3", "包名称")
                            sli.Add("4", "应答单位")
                            sli.Add("5", "联系人")
                            sli.Add("6", "电话")
                            sli.Add("7", "电子邮件")
                            row = sht.CreateRow(xlrow)
                            row.Height = 32 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 11
                            cfont.IsBold = True
                            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 6
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(sli(CStr(xlcol + 1)))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                    End Select
                Next
                '设置纸张方向
                sht.PrintSetup.Landscape = True
                sht.PrintSetup.PaperSize = 9
                '页脚
                sht.Footer.Left = "发售记录人："
                '缩放90%
                sht.PrintSetup.Scale = 90
                '设置列宽
                For xlcol = 0 To 6
                    Select Case xlcol
                        Case 0
                            sht.SetColumnWidth(xlcol, 6 * 275)
                        Case 1
                            sht.SetColumnWidth(xlcol, 8 * 275)
                        Case 2
                            sht.SetColumnWidth(xlcol, 37 * 275)
                        Case 3
                            sht.SetColumnWidth(xlcol, 35 * 275)
                        Case 4
                            sht.SetColumnWidth(xlcol, 11 * 275)
                        Case 5
                            sht.SetColumnWidth(xlcol, 15 * 275)
                        Case 6
                            sht.SetColumnWidth(xlcol, 25 * 275)
                    End Select
                Next
            End While
            'sql获取
            Form1.sqlcon1.sqlstr("
            SELECT * 
            FROM  
            (SELECT 
            bid.bididx,bid.bidname,package.pkgidx,package.pkgname,package.id as pkgid
            FROM bid,package
            WHERE bid.oveid = " & Form3.pro_id & "
            AND bid.id = package.bidid
            ORDER BY bid.id,package.id) as tb1
            LEFT JOIN 
            (SELECT 
            factory.facname,factory.faccont,factory.facline,factory.facmail,factory_config.pkgid as pkgid
            FROM factory_config,factory
            WHERE factory_config.facid = factory.id) as tb2
            ON 
            tb1.pkgid = tb2.pkgid", 2)
            '设置格式
            cstyle = wok.CreateCellStyle
            cfont = wok.CreateFont
            cfont.FontName = "方正仿宋_GBK"
            cfont.FontHeightInPoints = 11
            cfont.IsBold = False
            cstyle.WrapText = True
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            '填写数据
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(0).ToString & dr.Item(1).ToString)
                    xlrow = sht.LastRowNum + 1
                    row = sht.CreateRow(xlrow)
                    row.Height = 32 * 20
                    For xlcol = 0 To 6
                        cel = row.CreateCell(xlcol)
                        Select Case xlcol
                            Case 0
                                cel.SetCellValue(xlrow - 2)
                            Case 1
                                cel.SetCellValue(dr.Item(2).ToString)
                            Case 2
                                cel.SetCellValue(dr.Item(3).ToString)
                            Case Else
                                cel.SetCellValue(dr.Item(xlcol + 2).ToString)
                        End Select
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Next
                Next
                'sql获取bidname
                Form1.sqlcon1.sqlstr("
                SELECT bididx,bidname 
                FROM bid 
                WHERE oveid = " & Form3.pro_id, 2)
                '合并单元格
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(0).ToString & dr.Item(1).ToString)
                    i = 2
                    For xlrow = 3 To sht.LastRowNum
                        Select Case xlrow
                            Case sht.LastRowNum
                                If sht.GetRow(xlrow - 1).GetCell(1).StringCellValue <>
                                    sht.GetRow(xlrow).GetCell(1).StringCellValue Then
                                    '最后一行只有一行时
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 1, 1))
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 2, 2))
                                Else
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow, 1, 1))
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow, 2, 2))
                                End If
                            Case Else
                                If sht.GetRow(xlrow - 1).GetCell(1).StringCellValue <>
                                    sht.GetRow(xlrow).GetCell(1).StringCellValue Then
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 1, 1))
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 2, 2))
                                    i = xlrow
                                End If
                        End Select
                    Next
                Next
            End If
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_dijiao(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow
        Dim datestr As String

        'sql获取proname、prono、protime3
        Form1.sqlcon1.sqlstr("
        SELECT proname,prono,protm3 
        FROM overload 
        WHERE id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        '设置sheet
        sht = wok.CreateSheet("应答文件递交记录")
        '设置标题行
        For xlrow = 0 To 4
            row = sht.CreateRow(xlrow)
            Select Case xlrow
                Case 0
                    row.Height = 27 * 20
                    cstyle = wok.CreateCellStyle
                    cfont = wok.CreateFont
                    cfont.FontName = "方正仿宋_GBK"
                    cfont.FontHeightInPoints = 20
                    cstyle.Alignment = HorizontalAlignment.Center
                    cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                    For xlcol = 0 To 13
                        cel = row.CreateCell(xlcol)
                        If xlcol = 0 Then
                            cel.SetCellValue("应答文件递交登记列表")
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        End If
                    Next
                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 13))
                Case 1
                    row.Height = 30 * 20
                    For xlcol = 0 To 13
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 10
                        cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                        cstyle.Alignment = HorizontalAlignment.Left
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        Select Case xlcol
                            Case 0
                                cfont.IsBold = True
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("采购编号：")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 1
                                cfont.IsBold = True
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("采购编号：")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 0, 1))
                            Case 2
                                cfont.IsBold = False
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(Form1.sqlcon1.dataf.Item(1))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 3, 4
                                cfont.IsBold = False
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(Form1.sqlcon1.dataf.Item(1))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 5
                                cfont.IsBold = True
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("采购编号：")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 6
                                cfont.IsBold = False
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case Else
                                cfont.IsBold = False
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                        End Select
                    Next
                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 2, 4))
                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 6, 13))
                Case 2
                    row.Height = 30 * 20
                    For xlcol = 0 To 13
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 10
                        cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                        cstyle.Alignment = HorizontalAlignment.Left
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        Select Case xlcol
                            Case 0
                                cfont.IsBold = True
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("递交时间：")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 1
                                cfont.IsBold = True
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("递交时间：")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(2, 2, 0, 1))
                            Case 2
                                cfont.IsBold = False
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(Format(Form1.sqlcon1.dataf.Item(2), "yyyy年MM月dd日HH:mm") & "前")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 3, 4
                                cfont.IsBold = False
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(Format(Form1.sqlcon1.dataf.Item(2), "yyyy年MM月dd日HH:mm") & "前")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 5
                                cfont.IsBold = True
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("递交地点：")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 6
                                cfont.IsBold = False
                                cel = row.CreateCell(xlcol)
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case Else
                                cfont.IsBold = False
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                        End Select
                    Next
                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(2, 2, 2, 4))
                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(2, 2, 6, 13))
                Case 3
                    row.Height = 25 * 20
                    sli.Clear()
                    sli.Add("1", "序号")
                    sli.Add("2", "应答人名称")
                    sli.Add("3", "所投包号")
                    sli.Add("4", "所投包号")
                    sli.Add("5", "包数")
                    sli.Add("6", "正/副/电子版")
                    sli.Add("7", "联系人")
                    sli.Add("8", "手机及固定电话")
                    sli.Add("9", "电子邮件")
                    sli.Add("10", "提交文件")
                    sli.Add("11", "密封情况")
                    sli.Add("12", "递交时间")
                    sli.Add("13", "递交人签字")
                    sli.Add("14", "联系电话")
                    '单元格格式
                    cstyle = wok.CreateCellStyle
                    cfont = wok.CreateFont
                    cfont.FontName = "方正仿宋_GBK"
                    cfont.FontHeightInPoints = 10
                    cfont.IsBold = True
                    cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                    cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                    cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                    cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                    cstyle.Alignment = HorizontalAlignment.Center
                    cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                    For xlcol = 0 To 13
                        cel = row.CreateCell(xlcol)
                        cel.SetCellValue(sli(CStr(xlcol + 1)))
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Next
                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(3, 3, 2, 3))
                Case 4
                    row.Height = 60 * 20
                    For xlcol = 0 To 13
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 10
                        cfont.IsBold = True
                        cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                        cstyle.Alignment = HorizontalAlignment.Center
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        cstyle.WrapText = True
                        cel = row.CreateCell(xlcol)
                        If xlcol = 2 Or xlcol = 3 Or xlcol = 4 Then
                            Select Case xlcol
                                Case 2
                                    cel.SetCellValue("分标名称")
                                Case 3
                                    cel.SetCellValue("（对已购买但没有投的包打×，并修改小计）")
                                Case 4
                                    cel.SetCellValue("小计")
                            End Select
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Else
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(3, 4, xlcol, xlcol))
                        End If
                    Next
            End Select
        Next
        '设置纸张方向
        sht.PrintSetup.Landscape = True
        sht.PrintSetup.PaperSize = 9
        '页脚
        sht.Footer.Left = "接收记录人："
        sht.Footer.Center = "监督人："
        '缩放90%
        sht.PrintSetup.Scale = 72
        '设置列宽
        For xlcol = 0 To 13
            Select Case xlcol
                Case 0
                    sht.SetColumnWidth(xlcol, 4 * 275)
                Case 1
                    sht.SetColumnWidth(xlcol, 23 * 275)
                Case 2
                    sht.SetColumnWidth(xlcol, 13 * 275)
                Case 3
                    sht.SetColumnWidth(xlcol, 11 * 275)
                Case 4
                    sht.SetColumnWidth(xlcol, 5 * 275)
                Case 13
                    sht.SetColumnWidth(xlcol, 20 * 275)
                Case Else
                    sht.SetColumnWidth(xlcol, 12 * 275)
            End Select
        Next
        datestr = Format(Form1.sqlcon1.dataf.Item(2), "yyyy年MM月dd日")
        'sql获取表数据
        Form1.sqlcon1.sqlstr("
        SELECT
        factory.facname,bid.bidname,package.pkgidx,factory.faccont,factory.facline,factory.facmail
        FROM
        factory,factory_config,bid,package
        WHERE
        bid.oveid = " & Form3.pro_id & "
        AND 
        bid.id = package.bidid
        AND
        package.id = factory_config.pkgid
        AND 
        factory.id = factory_config.facid
        ORDER BY
        factory.facname", 2)
        '设置cell格式
        cstyle = wok.CreateCellStyle
        cfont = wok.CreateFont
        cfont.FontName = "方正仿宋_GBK"
        cfont.FontHeightInPoints = 10
        cfont.IsBold = False
        cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
        cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
        cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
        cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        cstyle.WrapText = True
        For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
            xlrow = sht.LastRowNum + 1
            row = sht.CreateRow(xlrow)
            For xlcol = 0 To 13
                cel = row.CreateCell(xlcol)
                row.Height = 35 * 20
                Select Case xlcol
                    Case 0
                        cel.SetCellValue(xlrow - 4)
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Case 1
                        cel.SetCellValue(dr.Item(0))
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Case 2
                        cel.SetCellValue(dr.Item(1))
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Case 3
                        cel.SetCellValue(dr.Item(2))
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Case 5
                        cel.SetCellValue("正_副_电_")
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Case 6
                        cel.SetCellValue(dr.Item(3))
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Case 7
                        cel.SetCellValue(dr.Item(4))
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Case 8
                        cel.SetCellValue(dr.Item(5))
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Case 9
                        cel.SetCellValue("应答文件")
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Case 11
                        cel.SetCellValue(datestr & "_时_分")
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Case Else
                        cel.SetCellValue("")
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                End Select
            Next
        Next
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_kaifeng(ByVal fp As String)
        Dim xlcol, xlrow, i As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        'sql获取proname,bidname并创建
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono,bid.bididx,bid.bidname,bid.bidno
        FROM overload,bid
        WHERE overload.id = " & Form3.pro_id & "
        AND overload.id = bid.oveid 
        ORDER BY bid.id", 1)
        '创建表格式及标题行
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                sht = wok.CreateSheet(Form1.sqlcon1.dataf.Item(2).ToString & Form1.sqlcon1.dataf.Item(3).ToString)
                For xlrow = 0 To 2
                    Select Case xlrow
                        Case 0
                            row = sht.CreateRow(xlrow)
                            row.Height = 19 * 20
                            For xlcol = 0 To 6
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue("应答文件开封记录")
                                End If
                                cstyle = wok.CreateCellStyle
                                cfont = wok.CreateFont
                                cfont.FontName = "方正仿宋_GBK"
                                cfont.FontHeightInPoints = 11
                                cfont.IsBold = True
                                cstyle.Alignment = HorizontalAlignment.Center
                                cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 6))
                        Case 1
                            row = sht.CreateRow(xlrow)
                            row.Height = 65 * 20
                            For xlcol = 0 To 6
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue("项目名称：" & Form1.sqlcon1.dataf.Item(0) & vbCrLf &
                                                     "采购编号：" & Form1.sqlcon1.dataf.Item(1) & vbCrLf &
                                                     "分标名称：" & Form1.sqlcon1.dataf.Item(2) & " " &
                                                     Form1.sqlcon1.dataf.Item(3) & vbCrLf &
                                                     "分标编号：" & Form1.sqlcon1.dataf.Item(4))
                                End If
                                cstyle = wok.CreateCellStyle
                                cfont = wok.CreateFont
                                cfont.FontName = "方正仿宋_GBK"
                                cfont.FontHeightInPoints = 11
                                cfont.IsBold = True
                                cstyle.WrapText = True
                                cstyle.Alignment = HorizontalAlignment.Left
                                cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 0, 6))
                        Case 2
                            sli.Clear()
                            sli.Add("1", "序号")
                            sli.Add("2", "包号")
                            sli.Add("3", "包名称")
                            sli.Add("4", "应答人")
                            sli.Add("5", "首轮报价" & vbCrLf & "（万元）")
                            sli.Add("6", "应答保证金")
                            sli.Add("7", "存在问题")
                            row = sht.CreateRow(xlrow)
                            row.Height = 32 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 11
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 6
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(sli(CStr(xlcol + 1)))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                    End Select
                Next
                '设置纸张方向
                sht.PrintSetup.Landscape = True
                sht.PrintSetup.PaperSize = 9
                '页脚
                sht.Footer.Left = "开封人：                       记录人："
                sht.Footer.Center = "监督人："
                sht.Footer.Right = "时间：" & Format(Now(), "yyyy年MM月dd日")
                '缩放90%
                sht.PrintSetup.Scale = 99
                '设置列宽
                For xlcol = 0 To 6
                    Select Case xlcol
                        Case 0
                            sht.SetColumnWidth(xlcol, 6 * 275)
                        Case 1
                            sht.SetColumnWidth(xlcol, 8 * 275)
                        Case 2
                            sht.SetColumnWidth(xlcol, 37 * 275)
                        Case 3
                            sht.SetColumnWidth(xlcol, 35 * 275)
                        Case 4
                            sht.SetColumnWidth(xlcol, 13 * 275)
                        Case 5
                            sht.SetColumnWidth(xlcol, 13 * 275)
                        Case 6
                            sht.SetColumnWidth(xlcol, 13 * 275)
                    End Select
                Next
            End While
            'sql获取
            Form1.sqlcon1.sqlstr("
                SELECT * 
                FROM  
                (SELECT 
                bid.bididx,bid.bidname,package.pkgidx,package.pkgname,package.id as pkgid
                FROM bid,package
                WHERE bid.oveid = " & Form3.pro_id & "
                AND bid.id = package.bidid
                ORDER BY bid.id,package.id) as tb1
                LEFT JOIN 
                (SELECT 
                factory.facname,factory_config.pkgid as pkgid
                FROM factory_config,factory
                WHERE factory_config.facid = factory.id) as tb2
                ON 
                tb1.pkgid = tb2.pkgid", 2)
            '设置格式
            cstyle = wok.CreateCellStyle
            cfont = wok.CreateFont
            cfont.FontName = "方正仿宋_GBK"
            cfont.FontHeightInPoints = 11
            cfont.IsBold = False
            cstyle.WrapText = True
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            '填写数据
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(0).ToString & dr.Item(1).ToString)
                    xlrow = sht.LastRowNum + 1
                    row = sht.CreateRow(xlrow)
                    row.Height = 32 * 20
                    For xlcol = 0 To 4
                        cel = row.CreateCell(xlcol)
                        Select Case xlcol
                            Case 0
                                cel.SetCellValue(xlrow - 2)
                            Case 1
                                cel.SetCellValue(dr.Item(2).ToString)
                            Case 2
                                cel.SetCellValue(dr.Item(3).ToString)
                            Case 3
                                cel.SetCellValue(dr.Item(5).ToString)
                            Case 4
                                '获取pkg_id
                                Form1.sqlcon1.sqlstr("
                                SELECT package.id 
                                FROM bid,package 
                                WHERE bid.oveid = " & Form3.pro_id & "
                                AND bid.id = package.bidid 
                                AND bid.bidname = " & Chr(34) & dr.Item(1) & Chr(34) & "
                                AND package.pkgidx = " & Chr(34) & dr.Item(2) & Chr(34), 1)
                                Form1.sqlcon1.dataf.Read()
                                Form3.pkg_id = Form1.sqlcon1.dataf.Item(0)
                                '获取包应答人个数
                                Form1.sqlcon1.sqlstr("
                                SELECT COUNT(facid) 
                                FROM factory_config 
                                WHERE oveid = " & Form3.pro_id & "
                                AND pkgid = " & Form3.pkg_id & "
                                AND disst = " & Chr(34) & Chr(34), 1)
                                If Form1.sqlcon1.dataf.HasRows Then
                                    Form1.sqlcon1.dataf.Read()
                                    Select Case Form1.sqlcon1.dataf.Item(0)
                                        Case 0
                                            cel.SetCellValue("流包")
                                            cstyle.SetFont(cfont)
                                            cel.CellStyle = cstyle
                                            cel = row.CreateCell(5)
                                            cel.SetCellValue("")
                                            cstyle.SetFont(cfont)
                                            cel.CellStyle = cstyle
                                            cel = row.CreateCell(6)
                                            cel.SetCellValue("")
                                        Case 1
                                            '获取标包采购方式
                                            Form1.sqlcon1.sqlstr("
                                            SELECT package_config.char_fs 
                                            FROM package_config 
                                            WHERE package_config.pkgid = " & Form3.pkg_id, 1)
                                            Form1.sqlcon1.dataf.Read()
                                            If Form1.sqlcon1.dataf.Item(0) = "单一来源" Then
                                                Form1.sqlcon1.sqlstr("
                                                SELECT price_rounds.price 
                                                FROM price_rounds,price_config,factory,factory_config
                                                WHERE price_rounds.rouid = 1
                                                AND price_rounds.preid = price_config.id
                                                AND price_config.fngid = factory_config.id
                                                AND factory_config.pkgid = " & Form3.pkg_id & "
                                                AND factory_config.oveid = " & Form3.pro_id & "
                                                AND factory_config.facid IN 
                                                (SELECT id 
                                                FROM factory 
                                                WHERE facname = " & Chr(34) & dr.Item(5) & Chr(34) & ")", 1)
                                                If Form1.sqlcon1.dataf.HasRows Then
                                                    Form1.sqlcon1.dataf.Read()
                                                    cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                                    cstyle.SetFont(cfont)
                                                    cel.CellStyle = cstyle
                                                    cel = row.CreateCell(5)
                                                    cel.SetCellValue("有")
                                                    cstyle.SetFont(cfont)
                                                    cel.CellStyle = cstyle
                                                    cel = row.CreateCell(6)
                                                    cel.SetCellValue("无")
                                                Else
                                                    cel.SetCellValue("未报价")
                                                    cstyle.SetFont(cfont)
                                                    cel.CellStyle = cstyle
                                                    cel = row.CreateCell(5)
                                                    cel.SetCellValue("")
                                                    cstyle.SetFont(cfont)
                                                    cel.CellStyle = cstyle
                                                    cel = row.CreateCell(6)
                                                    cel.SetCellValue("")
                                                End If
                                            Else
                                                cel.SetCellValue("流包")
                                                cstyle.SetFont(cfont)
                                                cel.CellStyle = cstyle
                                                cel = row.CreateCell(5)
                                                cel.SetCellValue("")
                                                cstyle.SetFont(cfont)
                                                cel.CellStyle = cstyle
                                                cel = row.CreateCell(6)
                                                cel.SetCellValue("")
                                            End If
                                        Case Else
                                            Form1.sqlcon1.sqlstr("
                                            SELECT price_rounds.price
                                            FROM price_rounds,price_config,factory,factory_config
                                            WHERE price_rounds.rouid = 1
                                            AND price_rounds.preid = price_config.id
                                            AND price_config.fngid = factory_config.id
                                            AND factory_config.pkgid = " & Form3.pkg_id & "
                                            AND factory_config.oveid = " & Form3.pro_id & "
                                            AND factory_config.facid IN 
                                            (SELECT id 
                                            FROM factory 
                                            WHERE facname = " & Chr(34) & dr.Item(5) & Chr(34) & ")", 1)
                                            If Form1.sqlcon1.dataf.HasRows Then
                                                Form1.sqlcon1.dataf.Read()
                                                cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                                cstyle.SetFont(cfont)
                                                cel.CellStyle = cstyle
                                                cel = row.CreateCell(5)
                                                cel.SetCellValue("有")
                                                cstyle.SetFont(cfont)
                                                cel.CellStyle = cstyle
                                                cel = row.CreateCell(6)
                                                cel.SetCellValue("无")
                                            Else
                                                '未流包项目判断是否弃标
                                                Form1.sqlcon1.sqlstr("
                                                SELECT factory_config.disst
                                                FROM factory,factory_config
                                                WHERE factory_config.pkgid = " & Form3.pkg_id & "
                                                AND factory_config.oveid = " & Form3.pro_id & "
                                                AND factory_config.facid = factory.id
                                                AND factory.facname = " & Chr(34) & dr.Item(5) & Chr(34), 1)
                                                If Form1.sqlcon1.dataf.HasRows Then
                                                    Form1.sqlcon1.dataf.Read()
                                                    cel.SetCellValue("弃包")
                                                    cstyle.SetFont(cfont)
                                                    cel.CellStyle = cstyle
                                                    cel = row.CreateCell(5)
                                                    cel.SetCellValue("")
                                                    cstyle.SetFont(cfont)
                                                    cel.CellStyle = cstyle
                                                    cel = row.CreateCell(6)
                                                    cel.SetCellValue("")
                                                Else
                                                    cel.SetCellValue("未报价")
                                                    cstyle.SetFont(cfont)
                                                    cel.CellStyle = cstyle
                                                    cel = row.CreateCell(5)
                                                    cel.SetCellValue("")
                                                    cstyle.SetFont(cfont)
                                                    cel.CellStyle = cstyle
                                                    cel = row.CreateCell(6)
                                                    cel.SetCellValue("")
                                                End If

                                            End If
                                    End Select
                                Else
                                    cel.SetCellValue("流包")
                                    cstyle.SetFont(cfont)
                                    cel.CellStyle = cstyle
                                    cel = row.CreateCell(5)
                                    cel.SetCellValue("")
                                    cstyle.SetFont(cfont)
                                    cel.CellStyle = cstyle
                                    cel = row.CreateCell(6)
                                    cel.SetCellValue("")
                                End If
                        End Select
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Next
                Next
                'sql获取bidname
                Form1.sqlcon1.sqlstr("
                    SELECT bididx,bidname 
                    FROM bid 
                    WHERE oveid = " & Form3.pro_id, 2)
                '合并单元格
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(0).ToString & dr.Item(1).ToString)
                    i = 2
                    For xlrow = 3 To sht.LastRowNum
                        Select Case xlrow
                            Case sht.LastRowNum
                                If sht.GetRow(xlrow - 1).GetCell(1).StringCellValue <>
                                    sht.GetRow(xlrow).GetCell(1).StringCellValue Then
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 1, 1))
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 2, 2))
                                Else
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow, 1, 1))
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow, 2, 2))
                                End If
                            Case Else
                                If sht.GetRow(xlrow - 1).GetCell(1).StringCellValue <>
                                    sht.GetRow(xlrow).GetCell(1).StringCellValue Then
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 1, 1))
                                    sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 2, 2))
                                    i = xlrow
                                End If
                        End Select
                    Next
                Next
            End If
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_yanzheng(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        'sql获取proname,bidname并创建
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono,bid.bididx,bid.bidname,bid.bidno
        FROM overload,bid
        WHERE overload.id = " & Form3.pro_id & "
        AND overload.id = bid.oveid 
        ORDER BY bid.id", 1)
        '创建表格式及标题行
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                sht = wok.CreateSheet(Form1.sqlcon1.dataf.Item(2).ToString & Form1.sqlcon1.dataf.Item(3).ToString)
                For xlrow = 0 To 2
                    Select Case xlrow
                        Case 0
                            row = sht.CreateRow(xlrow)
                            row.Height = 45 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 14
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            cstyle.SetFont(cfont)
                            For xlcol = 0 To 4
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue(Form1.sqlcon1.dataf.Item(0) & vbCrLf &
                                                     Form1.sqlcon1.dataf.Item(2) & Form1.sqlcon1.dataf.Item(3) &
                                                     "应答人资格验证")
                                End If
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 4))
                        Case 1
                            sli.Clear()
                            sli.Add("1", "序号")
                            sli.Add("2", "应答人")
                            sli.Add("3", "身份证明文件")
                            sli.Add("4", "授权委托书")
                            sli.Add("5", "备注")
                            row = sht.CreateRow(xlrow)
                            row.Height = 25 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 12
                            cfont.IsBold = True
                            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 4
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(sli(CStr(xlcol + 1)))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                    End Select
                Next
                '设置纸张方向
                sht.PrintSetup.Landscape = False
                sht.PrintSetup.PaperSize = 9
                '页脚
                sht.Footer.Left = "监督人："
                sht.Footer.Right = "时间：" & Format(Now(), "yyyy年MM月dd日")
                '缩放90%
                sht.PrintSetup.Scale = 90
                '设置列宽
                For xlcol = 0 To 6
                    Select Case xlcol
                        Case 0
                            sht.SetColumnWidth(xlcol, 6 * 275)
                        Case 1
                            sht.SetColumnWidth(xlcol, 45 * 275)
                        Case 2
                            sht.SetColumnWidth(xlcol, 14 * 275)
                        Case 3
                            sht.SetColumnWidth(xlcol, 14 * 275)
                        Case 4
                            sht.SetColumnWidth(xlcol, 8 * 275)
                    End Select
                Next
            End While
            'sql获取
            Form1.sqlcon1.sqlstr("
            SELECT DISTINCT(bid.bidname),bid.bididx,factory.facname
            FROM bid,package,factory,factory_config
            WHERE bid.oveid = " & Form3.pro_id & "
            AND package.bidid = bid.id
            AND factory_config.pkgid = package.id 
            AND factory_config.facid = factory.id
            AND factory_config.disst = " & Chr(34) & Chr(34) & "
            ORDER BY bid.id,factory.facname", 2)
            '设置格式
            cstyle = wok.CreateCellStyle
            cfont = wok.CreateFont
            cfont.FontName = "方正仿宋_GBK"
            cfont.FontHeightInPoints = 12
            cfont.IsBold = False
            cstyle.WrapText = True
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            '填写数据
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(1).ToString & dr.Item(0).ToString)
                    xlrow = sht.LastRowNum + 1
                    row = sht.CreateRow(xlrow)
                    row.Height = 25 * 20
                    For xlcol = 0 To 4
                        cel = row.CreateCell(xlcol)
                        Select Case xlcol
                            Case 0
                                cel.SetCellValue(xlrow - 1)
                            Case 1
                                cel.SetCellValue(dr.Item(2).ToString)
                            Case 2
                                cel.SetCellValue("√")
                            Case 3
                                cel.SetCellValue("√")
                        End Select
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Next
                Next
            End If
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_chuping(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        'sql获取expert_config.char_gp并创建
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT(expert_config.char_gp)
        FROM expert_config,repeal_config
        WHERE expert_config.oveid = " & Form3.pro_id & "
        AND expert_config.id = repeal_config.ecoid
        ORDER BY expert_config.char_gp", 1)
        '创建表格式及标题行
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                sht = wok.CreateSheet(Form1.sqlcon1.dataf.Item(0))
                For xlrow = 0 To 2
                    Select Case xlrow
                        Case 0
                            row = sht.CreateRow(xlrow)
                            row.Height = 30 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 15
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 8
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("初评汇报表")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 8))
                        Case 1
                            row = sht.CreateRow(xlrow)
                            row.Height = 30 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 12
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.Alignment = HorizontalAlignment.Left
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 8
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue("组别")
                                Else
                                    cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                End If
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 1, 8))
                        Case 2
                            sli.Clear()
                            sli.Add("1", "序号")
                            sli.Add("2", "分标名称")
                            sli.Add("3", "包号")
                            sli.Add("4", "存在问题应答人")
                            sli.Add("5", "采购文件或法律要求")
                            sli.Add("6", "存在问题")
                            sli.Add("7", "处理意见")
                            sli.Add("8", "提出专家")
                            sli.Add("9", "原标包是否流包")
                            row = sht.CreateRow(xlrow)
                            row.Height = 35 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 12
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 8
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(sli(CStr(xlcol + 1)))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                    End Select
                Next
                '设置纸张方向
                sht.PrintSetup.Landscape = True
                sht.PrintSetup.PaperSize = 9
                '页脚
                sht.Footer.Left = "填表人（代理机构）："
                sht.Footer.Center = "评审专家："
                '缩放90%
                sht.PrintSetup.Scale = 80
                '设置列宽
                For xlcol = 0 To 8
                    Select Case xlcol
                        Case 0
                            sht.SetColumnWidth(xlcol, 5 * 275)
                        Case 1
                            sht.SetColumnWidth(xlcol, 10 * 275)
                        Case 2
                            sht.SetColumnWidth(xlcol, 5 * 275)
                        Case 3
                            sht.SetColumnWidth(xlcol, 20 * 275)
                        Case 4
                            sht.SetColumnWidth(xlcol, 42 * 275)
                        Case 5
                            sht.SetColumnWidth(xlcol, 42 * 275)
                        Case 6
                            sht.SetColumnWidth(xlcol, 10 * 275)
                        Case 7
                            sht.SetColumnWidth(xlcol, 10 * 275)
                        Case 8
                            sht.SetColumnWidth(xlcol, 10 * 275)
                    End Select
                Next
            End While
            'sql获取
            Form1.sqlcon1.sqlstr("
            SELECT 
            expert_config.char_gp,bid.bidname,package.pkgidx,factory.facname,
            repeal.repqust,repeal.repreal,expert.expname,package.id
            FROM 
            bid,package,factory,repeal,repeal_config,expert,expert_config
            WHERE 
            repeal_config.oveid = " & Form3.pro_id & "
            AND 
            repeal_config.ecoid = expert_config.id
            AND 
            expert_config.expid = expert.id
            AND 
            repeal_config.facid = factory.id
            AND 
            repeal_config.pkgid = package.id
            AND 
            bid.id = package.bidid
            AND 
            repeal_config.repid = repeal.id
            ORDER BY 
            expert_config.char_gp,factory.facname,bid.id,package.id", 2)
            '设置格式
            cstyle = wok.CreateCellStyle
            cfont = wok.CreateFont
            cfont.FontName = "方正仿宋_GBK"
            cfont.FontHeightInPoints = 12
            cfont.IsBold = False
            cstyle.WrapText = True
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            '填写数据
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(0).ToString)
                    xlrow = sht.LastRowNum + 1
                    row = sht.CreateRow(xlrow)
                    row.Height = 65 * 20
                    For xlcol = 0 To 8
                        cel = row.CreateCell(xlcol)
                        Select Case xlcol
                            Case 0
                                cel.SetCellValue(xlrow - 2)
                            Case 1
                                cel.SetCellValue(dr.Item(1).ToString)
                            Case 2
                                cel.SetCellValue(dr.Item(2).ToString)
                            Case 3
                                cel.SetCellValue(dr.Item(3).ToString)
                            Case 4
                                cel.SetCellValue(dr.Item(4).ToString)
                            Case 5
                                cel.SetCellValue(dr.Item(5).ToString)
                            Case 6
                                cel.SetCellValue("建议终止谈判")
                            Case 7
                                cel.SetCellValue(dr.Item(6).ToString)
                            Case 8
                                '判断原标包是否流包pkgid
                                Form1.sqlcon1.sqlstr("
                                SELECT COUNT(factory_config.facid),package_config.char_fs 
                                FROM factory_config,package_config
                                WHERE factory_config.oveid = " & Form3.pro_id & "
                                AND factory_config.pkgid = " & dr.Item(7) & "
                                AND factory_config.disst = " & Chr(34) & Chr(34) & "
                                AND factory_config.repst = " & Chr(34) & Chr(34) & "
                                AND factory_config.pkgid = package_config.pkgid", 1)
                                If Form1.sqlcon1.dataf.HasRows Then
                                    Form1.sqlcon1.dataf.Read()
                                    Select Case Form1.sqlcon1.dataf.Item(0)
                                        Case 0
                                            cel.SetCellValue("是")
                                        Case 1
                                            If Form1.sqlcon1.dataf.Item(1) = "单一来源" Then
                                                cel.SetCellValue("否")
                                            Else
                                                cel.SetCellValue("是")
                                            End If
                                        Case Else
                                            cel.SetCellValue("否")
                                    End Select
                                Else
                                    cel.SetCellValue("是")
                                End If
                        End Select
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Next
                Next
            End If
        Else
            '无信息导出格式
            sht = wok.CreateSheet("格式文件")
            For xlrow = 0 To 2
                Select Case xlrow
                    Case 0
                        row = sht.CreateRow(xlrow)
                        row.Height = 30 * 20
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 15
                        cfont.IsBold = True
                        cstyle.WrapText = True
                        cstyle.Alignment = HorizontalAlignment.Center
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        For xlcol = 0 To 8
                            cel = row.CreateCell(xlcol)
                            cel.SetCellValue("初评汇报表")
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Next
                        sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 8))
                    Case 1
                        row = sht.CreateRow(xlrow)
                        row.Height = 30 * 20
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 12
                        cfont.IsBold = True
                        cstyle.WrapText = True
                        cstyle.Alignment = HorizontalAlignment.Left
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        For xlcol = 0 To 8
                            cel = row.CreateCell(xlcol)
                            If xlcol = 0 Then
                                cel.SetCellValue("组别")
                            End If
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Next
                        sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 1, 8))
                    Case 2
                        sli.Clear()
                        sli.Add("1", "序号")
                        sli.Add("2", "分标名称")
                        sli.Add("3", "包号")
                        sli.Add("4", "存在问题应答人")
                        sli.Add("5", "采购文件或法律要求")
                        sli.Add("6", "存在问题")
                        sli.Add("7", "处理意见")
                        sli.Add("8", "提出专家")
                        sli.Add("9", "原标包是否流包")
                        row = sht.CreateRow(xlrow)
                        row.Height = 35 * 20
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 12
                        cfont.IsBold = True
                        cstyle.WrapText = True
                        cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                        cstyle.Alignment = HorizontalAlignment.Center
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        For xlcol = 0 To 8
                            cel = row.CreateCell(xlcol)
                            cel.SetCellValue(sli(CStr(xlcol + 1)))
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Next
                End Select
            Next
            '设置纸张方向
            sht.PrintSetup.Landscape = True
            sht.PrintSetup.PaperSize = 9
            '页脚
            sht.Footer.Left = "填表人（代理机构）："
            sht.Footer.Center = "评审专家："
            '缩放90%
            sht.PrintSetup.Scale = 80
            '设置列宽
            For xlcol = 0 To 8
                Select Case xlcol
                    Case 0
                        sht.SetColumnWidth(xlcol, 5 * 275)
                    Case 1
                        sht.SetColumnWidth(xlcol, 10 * 275)
                    Case 2
                        sht.SetColumnWidth(xlcol, 5 * 275)
                    Case 3
                        sht.SetColumnWidth(xlcol, 20 * 275)
                    Case 4
                        sht.SetColumnWidth(xlcol, 42 * 275)
                    Case 5
                        sht.SetColumnWidth(xlcol, 42 * 275)
                    Case 6
                        sht.SetColumnWidth(xlcol, 10 * 275)
                    Case 7
                        sht.SetColumnWidth(xlcol, 10 * 275)
                    Case 8
                        sht.SetColumnWidth(xlcol, 10 * 275)
                End Select
            Next
        End If
        'sql获取repeal_config.ecoid= 0数据
        Form1.sqlcon1.sqlstr("
        SELECT 
        bid.bidname,package.pkgidx,factory.facname,repeal.repqust,repeal.repreal,package.id 
        FROM bid,package,factory,repeal,repeal_config 
        WHERE repeal_config.oveid = " & Form3.pro_id & "
        AND repeal_config.ecoid = 0
        AND repeal_config.pkgid = package.id
        AND bid.id = package.bidid
        AND repeal_config.facid = factory.id
        AND repeal_config.repid = repeal.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            sht = wok.CreateSheet("未分组")
            For xlrow = 0 To 2
                Select Case xlrow
                    Case 0
                        row = sht.CreateRow(xlrow)
                        row.Height = 30 * 20
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 15
                        cfont.IsBold = True
                        cstyle.WrapText = True
                        cstyle.Alignment = HorizontalAlignment.Center
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        For xlcol = 0 To 8
                            cel = row.CreateCell(xlcol)
                            cel.SetCellValue("初评汇报表")
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Next
                        sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 8))
                    Case 1
                        row = sht.CreateRow(xlrow)
                        row.Height = 30 * 20
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 12
                        cfont.IsBold = True
                        cstyle.WrapText = True
                        cstyle.Alignment = HorizontalAlignment.Left
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        For xlcol = 0 To 8
                            cel = row.CreateCell(xlcol)
                            If xlcol = 0 Then
                                cel.SetCellValue("组别")
                            Else
                                cel.SetCellValue("")
                            End If
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Next
                        sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 1, 8))
                    Case 2
                        sli.Clear()
                        sli.Add("1", "序号")
                        sli.Add("2", "分标名称")
                        sli.Add("3", "包号")
                        sli.Add("4", "存在问题应答人")
                        sli.Add("5", "采购文件或法律要求")
                        sli.Add("6", "存在问题")
                        sli.Add("7", "处理意见")
                        sli.Add("8", "提出专家")
                        sli.Add("9", "原标包是否流包")
                        row = sht.CreateRow(xlrow)
                        row.Height = 35 * 20
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 12
                        cfont.IsBold = True
                        cstyle.WrapText = True
                        cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                        cstyle.Alignment = HorizontalAlignment.Center
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        For xlcol = 0 To 8
                            cel = row.CreateCell(xlcol)
                            cel.SetCellValue(sli(CStr(xlcol + 1)))
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Next
                End Select
            Next
            sht.PrintSetup.Landscape = True
            sht.PrintSetup.PaperSize = 9
            '页脚
            sht.Footer.Left = "填表人（代理机构）："
            sht.Footer.Center = "评审专家："
            '缩放90%
            sht.PrintSetup.Scale = 80
            '设置列宽
            For xlcol = 0 To 8
                Select Case xlcol
                    Case 0
                        sht.SetColumnWidth(xlcol, 5 * 275)
                    Case 1
                        sht.SetColumnWidth(xlcol, 10 * 275)
                    Case 2
                        sht.SetColumnWidth(xlcol, 5 * 275)
                    Case 3
                        sht.SetColumnWidth(xlcol, 20 * 275)
                    Case 4
                        sht.SetColumnWidth(xlcol, 42 * 275)
                    Case 5
                        sht.SetColumnWidth(xlcol, 42 * 275)
                    Case 6
                        sht.SetColumnWidth(xlcol, 10 * 275)
                    Case 7
                        sht.SetColumnWidth(xlcol, 10 * 275)
                    Case 8
                        sht.SetColumnWidth(xlcol, 10 * 275)
                End Select
            Next
            '填写未分组数据
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                xlrow = sht.LastRowNum + 1
                row = sht.CreateRow(xlrow)
                row.Height = 65 * 20
                cstyle = wok.CreateCellStyle
                cfont = wok.CreateFont
                cfont.FontName = "方正仿宋_GBK"
                cfont.FontHeightInPoints = 12
                cfont.IsBold = False
                cstyle.WrapText = True
                cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                cstyle.Alignment = HorizontalAlignment.Center
                cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                For xlcol = 0 To 8
                    cel = row.CreateCell(xlcol)
                    Select Case xlcol
                        Case 0
                            cel.SetCellValue(xlrow - 2)
                        Case 1
                            cel.SetCellValue(dr.Item(0).ToString)
                        Case 2
                            cel.SetCellValue(dr.Item(1).ToString)
                        Case 3
                            cel.SetCellValue(dr.Item(2).ToString)
                        Case 4
                            cel.SetCellValue(dr.Item(3).ToString)
                        Case 5
                            cel.SetCellValue(dr.Item(4).ToString)
                        Case 6
                            cel.SetCellValue("建议终止谈判")
                        Case 7
                            cel.SetCellValue("")
                        Case 8
                            '判断原标包是否流包pkgid
                            Form1.sqlcon1.sqlstr("
                                SELECT COUNT(factory_config.facid),package_config.char_fs 
                                FROM factory_config,package_config
                                WHERE factory_config.oveid = " & Form3.pro_id & "
                                AND factory_config.pkgid = " & dr.Item(5) & "
                                AND factory_config.disst = " & Chr(34) & Chr(34) & "
                                AND factory_config.repst = " & Chr(34) & Chr(34) & "
                                AND factory_config.pkgid = package_config.pkgid", 1)
                            If Form1.sqlcon1.dataf.HasRows Then
                                Form1.sqlcon1.dataf.Read()
                                Select Case Form1.sqlcon1.dataf.Item(0)
                                    Case 0
                                        cel.SetCellValue("是")
                                    Case 1
                                        If Form1.sqlcon1.dataf.Item(1) = "单一来源" Then
                                            cel.SetCellValue("否")
                                        Else
                                            cel.SetCellValue("是")
                                        End If
                                    Case Else
                                        cel.SetCellValue("否")
                                End Select
                            Else
                                cel.SetCellValue("是")
                            End If
                    End Select
                    cstyle.SetFont(cfont)
                    cel.CellStyle = cstyle
                Next
            Next
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_wuxiao(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        'sql获取expert_config.char_gp并创建
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT(expert_config.char_gp)
        FROM expert_config,repeal_config
        WHERE expert_config.oveid = " & Form3.pro_id & "
        AND expert_config.id = repeal_config.ecoid
        ORDER BY expert_config.char_gp", 1)
        '创建表格式及标题行
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                sht = wok.CreateSheet(Form1.sqlcon1.dataf.Item(0))
                For xlrow = 0 To 2
                    Select Case xlrow
                        Case 0
                            row = sht.CreateRow(xlrow)
                            row.Height = 30 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 15
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 7
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("无效应答审批单")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 7))
                        Case 1
                            row = sht.CreateRow(xlrow)
                            row.Height = 30 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 12
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.Alignment = HorizontalAlignment.Left
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 7
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue("组别")
                                Else
                                    cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                End If
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 1, 7))
                        Case 2
                            sli.Clear()
                            sli.Add("1", "序号")
                            sli.Add("2", "分标名称")
                            sli.Add("3", "包号及包名称")
                            sli.Add("4", "应答人")
                            sli.Add("5", "采购文件或法律要求")
                            sli.Add("6", "应答文件情况")
                            sli.Add("7", "无效应答事项（结论）")
                            sli.Add("8", "提出专家")
                            row = sht.CreateRow(xlrow)
                            row.Height = 35 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 12
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 7
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(sli(CStr(xlcol + 1)))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                    End Select
                Next
                '设置纸张方向
                sht.PrintSetup.Landscape = True
                sht.PrintSetup.PaperSize = 9
                '页脚
                sht.Footer.Left = "评审组长：               评审委员会："
                sht.Footer.Right = "法律顾问："
                '缩放90%
                sht.PrintSetup.Scale = 73
                '设置列宽
                For xlcol = 0 To 7
                    Select Case xlcol
                        Case 0
                            sht.SetColumnWidth(xlcol, 5 * 275)
                        Case 1
                            sht.SetColumnWidth(xlcol, 10 * 275)
                        Case 2
                            sht.SetColumnWidth(xlcol, 25 * 275)
                        Case 3
                            sht.SetColumnWidth(xlcol, 20 * 275)
                        Case 4
                            sht.SetColumnWidth(xlcol, 42 * 275)
                        Case 5
                            sht.SetColumnWidth(xlcol, 42 * 275)
                        Case 6
                            sht.SetColumnWidth(xlcol, 13 * 275)
                        Case 7
                            sht.SetColumnWidth(xlcol, 13 * 275)
                    End Select
                Next
            End While
            'sql获取
            Form1.sqlcon1.sqlstr("
            SELECT 
            expert_config.char_gp,bid.bidname,package.pkgidx,package.pkgname,factory.facname,
            repeal.repqust,repeal.repreal,expert.expname,package.id
            FROM 
            bid,package,factory,repeal,repeal_config,expert,expert_config
            WHERE 
            repeal_config.oveid = " & Form3.pro_id & "
            AND 
            repeal_config.ecoid = expert_config.id
            AND 
            expert_config.expid = expert.id
            AND 
            repeal_config.facid = factory.id
            AND 
            repeal_config.pkgid = package.id
            AND 
            bid.id = package.bidid
            AND 
            repeal_config.repid = repeal.id
            ORDER BY 
            expert_config.char_gp,factory.facname,bid.id,package.id", 2)
            '设置格式
            cstyle = wok.CreateCellStyle
            cfont = wok.CreateFont
            cfont.FontName = "方正仿宋_GBK"
            cfont.FontHeightInPoints = 12
            cfont.IsBold = False
            cstyle.WrapText = True
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            '填写数据
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(0).ToString)
                    xlrow = sht.LastRowNum + 1
                    row = sht.CreateRow(xlrow)
                    row.Height = 65 * 20
                    For xlcol = 0 To 7
                        cel = row.CreateCell(xlcol)
                        Select Case xlcol
                            Case 0
                                cel.SetCellValue(xlrow - 2)
                            Case 1
                                cel.SetCellValue(dr.Item(1).ToString)
                            Case 2
                                cel.SetCellValue(dr.Item(2).ToString & " " &
                                                 dr.Item(3).ToString)
                            Case 3
                                cel.SetCellValue(dr.Item(4).ToString)
                            Case 4
                                cel.SetCellValue(dr.Item(5).ToString)
                            Case 5
                                cel.SetCellValue(dr.Item(6).ToString)
                            Case 6
                                cel.SetCellValue("终止谈判")
                            Case 7
                                cel.SetCellValue(dr.Item(7).ToString)
                        End Select
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Next
                Next
            End If
        End If
        'sql获取repeal_config.ecoid= 0数据
        Form1.sqlcon1.sqlstr("
        SELECT 
        bid.bidname,package.pkgidx,package.pkgname,factory.facname,repeal.repqust,repeal.repreal,package.id 
        FROM bid,package,factory,repeal,repeal_config 
        WHERE repeal_config.oveid = " & Form3.pro_id & "
        AND repeal_config.ecoid = 0
        AND repeal_config.pkgid = package.id
        AND bid.id = package.bidid
        AND repeal_config.facid = factory.id
        AND repeal_config.repid = repeal.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            sht = wok.CreateSheet("未分组")
            For xlrow = 0 To 2
                Select Case xlrow
                    Case 0
                        row = sht.CreateRow(xlrow)
                        row.Height = 30 * 20
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 15
                        cfont.IsBold = True
                        cstyle.WrapText = True
                        cstyle.Alignment = HorizontalAlignment.Center
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        For xlcol = 0 To 7
                            cel = row.CreateCell(xlcol)
                            cel.SetCellValue("无效应答审批单")
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Next
                        sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 7))
                    Case 1
                        row = sht.CreateRow(xlrow)
                        row.Height = 30 * 20
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 12
                        cfont.IsBold = True
                        cstyle.WrapText = True
                        cstyle.Alignment = HorizontalAlignment.Left
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        For xlcol = 0 To 7
                            cel = row.CreateCell(xlcol)
                            If xlcol = 0 Then
                                cel.SetCellValue("组别")
                            Else
                                cel.SetCellValue("")
                            End If
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Next
                        sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 1, 7))
                    Case 2
                        sli.Clear()
                        sli.Add("1", "序号")
                        sli.Add("2", "分标名称")
                        sli.Add("3", "包号及包名称")
                        sli.Add("4", "应答人")
                        sli.Add("5", "采购文件或法律要求")
                        sli.Add("6", "应答文件情况")
                        sli.Add("7", "无效应答事项（结论）")
                        sli.Add("8", "提出专家")
                        row = sht.CreateRow(xlrow)
                        row.Height = 35 * 20
                        cstyle = wok.CreateCellStyle
                        cfont = wok.CreateFont
                        cfont.FontName = "方正仿宋_GBK"
                        cfont.FontHeightInPoints = 12
                        cfont.IsBold = True
                        cstyle.WrapText = True
                        cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                        cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                        cstyle.Alignment = HorizontalAlignment.Center
                        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                        For xlcol = 0 To 7
                            cel = row.CreateCell(xlcol)
                            cel.SetCellValue(sli(CStr(xlcol + 1)))
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Next
                End Select
            Next
            sht.PrintSetup.Landscape = True
            sht.PrintSetup.PaperSize = 9
            '页脚
            sht.Footer.Left = "评审组长：               评审委员会："
            sht.Footer.Right = "法律顾问："
            '缩放90%
            sht.PrintSetup.Scale = 80
            '设置列宽
            For xlcol = 0 To 7
                Select Case xlcol
                    Case 0
                        sht.SetColumnWidth(xlcol, 5 * 275)
                    Case 1
                        sht.SetColumnWidth(xlcol, 10 * 275)
                    Case 2
                        sht.SetColumnWidth(xlcol, 25 * 275)
                    Case 3
                        sht.SetColumnWidth(xlcol, 20 * 275)
                    Case 4
                        sht.SetColumnWidth(xlcol, 42 * 275)
                    Case 5
                        sht.SetColumnWidth(xlcol, 42 * 275)
                    Case 6
                        sht.SetColumnWidth(xlcol, 12 * 275)
                    Case 7
                        sht.SetColumnWidth(xlcol, 12 * 275)
                End Select
            Next
            '填写未分组数据
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                xlrow = sht.LastRowNum + 1
                row = sht.CreateRow(xlrow)
                row.Height = 65 * 20
                cstyle = wok.CreateCellStyle
                cfont = wok.CreateFont
                cfont.FontName = "方正仿宋_GBK"
                cfont.FontHeightInPoints = 12
                cfont.IsBold = False
                cstyle.WrapText = True
                cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                cstyle.Alignment = HorizontalAlignment.Center
                cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                For xlcol = 0 To 7
                    cel = row.CreateCell(xlcol)
                    Select Case xlcol
                        Case 0
                            cel.SetCellValue(xlrow - 2)
                        Case 1
                            cel.SetCellValue(dr.Item(0).ToString)
                        Case 2
                            cel.SetCellValue(dr.Item(1).ToString & " " &
                                             dr.Item(2).ToString)
                        Case 3
                            cel.SetCellValue(dr.Item(3).ToString)
                        Case 4
                            cel.SetCellValue(dr.Item(4).ToString)
                        Case 5
                            cel.SetCellValue(dr.Item(5).ToString)
                        Case 6
                            cel.SetCellValue("终止谈判")
                        Case 7
                            cel.SetCellValue("")
                    End Select
                    cstyle.SetFont(cfont)
                    cel.CellStyle = cstyle
                Next
            Next
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_youxiao(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        'sql获取proname,bidname并创建
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono
        FROM overload
        WHERE overload.id = " & Form3.pro_id, 1)
        '创建表格式及标题行
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                sht = wok.CreateSheet("有效应答不足被否决审批单")
                For xlrow = 0 To 2
                    Select Case xlrow
                        Case 0
                            row = sht.CreateRow(xlrow)
                            row.Height = 30 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 15
                            cfont.IsBold = True
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 5
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue("有效应答不足被否决审批单")
                                End If
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 5))
                        Case 1
                            row = sht.CreateRow(xlrow)
                            row.Height = 40 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 11
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.Alignment = HorizontalAlignment.Left
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 5
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue("项目名称：" & Form1.sqlcon1.dataf.Item(0) & vbCrLf &
                                                     "采购编号：" & Form1.sqlcon1.dataf.Item(1))
                                End If
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 0, 5))
                        Case 2
                            sli.Clear()
                            sli.Add("1", "序号")
                            sli.Add("2", "分标名称")
                            sli.Add("3", "包号及包名称")
                            sli.Add("4", "有效应答人")
                            sli.Add("5", "有效应答数量")
                            sli.Add("6", "是否流包")
                            row = sht.CreateRow(xlrow)
                            row.Height = 40 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 11
                            cfont.IsBold = True
                            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 5
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(sli(CStr(xlcol + 1)))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                    End Select
                Next
                '设置纸张方向
                sht.PrintSetup.Landscape = True
                sht.PrintSetup.PaperSize = 9
                '页脚
                sht.Footer.Left = "评审组长：               法律顾问："
                sht.Footer.Center = "评审委员会："
                '缩放90%
                sht.PrintSetup.Scale = 95
                '设置列宽
                For xlcol = 0 To 5
                    Select Case xlcol
                        Case 0
                            sht.SetColumnWidth(xlcol, 6 * 275)
                        Case 1
                            sht.SetColumnWidth(xlcol, 25 * 275)
                        Case 2
                            sht.SetColumnWidth(xlcol, 37 * 275)
                        Case 3
                            sht.SetColumnWidth(xlcol, 35 * 275)
                        Case 4
                            sht.SetColumnWidth(xlcol, 13 * 275)
                        Case 5
                            sht.SetColumnWidth(xlcol, 13 * 275)
                    End Select
                Next
            End While
            'sql获取
            Form1.sqlcon1.sqlstr("
            SELECT factory_config.pkgid,count(facid),package_config.char_fs
            FROM factory_config,package_config
            WHERE factory_config.oveid = " & Form3.pro_id & "
            AND factory_config.disst = " & Chr(34) & Chr(34) & "
            AND factory_config.pkgid = package_config.pkgid
            GROUP BY factory_config.pkgid", 2)
            '设置格式
            cstyle = wok.CreateCellStyle
            cfont = wok.CreateFont
            cfont.FontName = "方正仿宋_GBK"
            cfont.FontHeightInPoints = 11
            cfont.IsBold = False
            cstyle.WrapText = True
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            '填写数据
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    If dr.Item(1) = 0 Then
                    ElseIf dr.Item(1) = 1 And dr.Item(2) <> "单一来源" Then
                    Else
                        Form1.sqlcon1.sqlstr("
                        SELECT * 
                        FROM
                        (SELECT 
                        bid.bidname,package.pkgidx,package.pkgname,
                        package.id as pkgid
                        FROM 
                        bid,package
                        WHERE 
                        package.id = " & dr.Item(0) & "
                        AND 
                        bid.id = package.bidid)
                        LEFT JOIN 
                        (SELECT 
                        factory.facname,count(factory_config.facid)
                        FROM 
                        factory,factory_config
                        WHERE 
                        factory_config.pkgid = " & dr.Item(0) & "
                        AND 
                        factory_config.facid = factory.id
                        AND 
                        factory_config.disst = " & Chr(34) & Chr(34) & "
                        AND 
                        factory_config.repst  = " & Chr(34) & Chr(34) & ")", 1)
                        Form1.sqlcon1.dataf.Read()
                        Select Case Form1.sqlcon1.dataf.Item(5)
                            Case 0
                                xlrow = sht.LastRowNum + 1
                                row = sht.CreateRow(xlrow)
                                row.Height = 55 * 20
                                For xlcol = 0 To 5
                                    cel = row.CreateCell(xlcol)
                                    Select Case xlcol
                                        Case 0
                                            cel.SetCellValue(xlrow - 2)
                                        Case 1
                                            cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                        Case 2
                                            cel.SetCellValue(Form1.sqlcon1.dataf.Item(1) & " " &
                                                             Form1.sqlcon1.dataf.Item(2))
                                        Case 3
                                            cel.SetCellValue("")
                                        Case 4
                                            cel.SetCellValue(Form1.sqlcon1.dataf.Item(5))
                                        Case 5
                                            cel.SetCellValue("是")
                                    End Select
                                    cstyle.SetFont(cfont)
                                    cel.CellStyle = cstyle
                                Next
                            Case 1
                                xlrow = sht.LastRowNum + 1
                                row = sht.CreateRow(xlrow)
                                row.Height = 55 * 20
                                For xlcol = 0 To 5
                                    cel = row.CreateCell(xlcol)
                                    Select Case xlcol
                                        Case 0
                                            cel.SetCellValue(xlrow - 2)
                                        Case 1
                                            cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                        Case 2
                                            cel.SetCellValue(Form1.sqlcon1.dataf.Item(1) & " " &
                                                             Form1.sqlcon1.dataf.Item(2))
                                        Case 3
                                            cel.SetCellValue(Form1.sqlcon1.dataf.Item(4))
                                        Case 4
                                            cel.SetCellValue(Form1.sqlcon1.dataf.Item(5))
                                        Case 5
                                            cel.SetCellValue("是")
                                    End Select
                                    cstyle.SetFont(cfont)
                                    cel.CellStyle = cstyle
                                Next
                        End Select
                    End If
                Next
            End If
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_duolun(ByVal fp As String)
        Dim xlcol, xlrow， bidmaxrd, pkgmaxrd， i As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        'sql获取proname,bidname并创建
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono,bid.bididx,bid.bidname,bid.bidno
        FROM overload,bid
        WHERE overload.id = " & Form3.pro_id & "
        AND overload.id = bid.oveid 
        ORDER BY bid.id", 1)
        '创建表格式及标题行
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                sht = wok.CreateSheet(Form1.sqlcon1.dataf.Item(2).ToString & Form1.sqlcon1.dataf.Item(3).ToString)
                For xlrow = 0 To 2
                    Select Case xlrow
                        Case 0
                            row = sht.CreateRow(xlrow)
                            row.Height = 25 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 11
                            cfont.IsBold = True
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 6
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue("多轮报价记录表")
                                End If
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                        Case 1
                            row = sht.CreateRow(xlrow)
                            row.Height = 65 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 11
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.Alignment = HorizontalAlignment.Left
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 6
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue("项目名称：" & Form1.sqlcon1.dataf.Item(0) & vbCrLf &
                                                     "采购编号：" & Form1.sqlcon1.dataf.Item(1) & vbCrLf &
                                                     "分标名称：" & Form1.sqlcon1.dataf.Item(2) & " " &
                                                     Form1.sqlcon1.dataf.Item(3) & vbCrLf &
                                                     "分标编号：" & Form1.sqlcon1.dataf.Item(4))
                                End If
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                        Case 2
                            sli.Clear()
                            sli.Add("1", "序号")
                            sli.Add("2", "包号")
                            sli.Add("3", "包名称")
                            sli.Add("4", "包限价" & vbCrLf & "（万元）")
                            sli.Add("5", "应答人")
                            row = sht.CreateRow(xlrow)
                            row.Height = 32 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "方正仿宋_GBK"
                            cfont.FontHeightInPoints = 11
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 4
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(sli(CStr(xlcol + 1)))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                    End Select
                Next
                '设置纸张方向
                sht.PrintSetup.Landscape = True
                sht.PrintSetup.PaperSize = 9
                '页脚
                sht.Footer.Left = "评审专家："
                sht.Footer.Center = "监督人："
                sht.Footer.Right = "时间：" & Format(Now(), "yyyy年MM月dd日")
                '缩放90%
                sht.PrintSetup.Scale = 80
                '设置列宽
                For xlcol = 0 To 4
                    Select Case xlcol
                        Case 0
                            sht.SetColumnWidth(xlcol, 6 * 275)
                        Case 1
                            sht.SetColumnWidth(xlcol, 6 * 275)
                        Case 2
                            sht.SetColumnWidth(xlcol, 32 * 275)
                        Case 3
                            sht.SetColumnWidth(xlcol, 10 * 275)
                        Case 4
                            sht.SetColumnWidth(xlcol, 40 * 275)
                    End Select
                Next
            End While
            '转换大小写
            sli.Clear()
            sli.Add("1", "首")
            sli.Add("2", "二")
            sli.Add("3", "三")
            sli.Add("4", "四")
            sli.Add("5", "五")
            sli.Add("6", "六")
            sli.Add("7", "七")
            sli.Add("8", "八")
            sli.Add("9", "九")
            sli.Add("10", "十")
            Form1.sqlcon1.sqlstr("
            SELECT bid.bididx,bid.bidname 
            FROM bid 
            WHERE bid.oveid = " & Form3.pro_id, 2)
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(0).ToString & dr.Item(1).ToString)
                    '获取分标最大报价轮次
                    Form1.sqlcon1.sqlstr("
                    SELECT bid.bidname,MAX(price_config.couid)
                    FROM price_config,bid,package,factory_config
                    WHERE bid.oveid = " & Form3.pro_id & "
                    AND bid.bidname = " & Chr(34) & dr.Item(1) & Chr(34) & "
                    AND bid.id = package.bidid
                    AND package.id = factory_config.pkgid
                    AND factory_config.id = price_config.fngid", 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        Form1.sqlcon1.dataf.Read()
                        Try
                            bidmaxrd = Form1.sqlcon1.dataf.Item(1)
                        Catch ex As Exception
                            bidmaxrd = 3
                        End Try
                        row = sht.GetRow(2)
                        If bidmaxrd < 4 Then
                            For xlcol = 5 To 7
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(sli(CStr(xlcol - 4)) & "轮报价" &
                                             vbCrLf & "（万元）")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                                sht.SetColumnWidth(xlcol, 20 * 275)
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 7))
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 0, 7))
                        Else
                            For xlcol = 5 To 4 + bidmaxrd
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(sli(CStr(xlcol - 4)) & "轮报价" &
                                             vbCrLf & "（万元）")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                                sht.SetColumnWidth(xlcol, Int(60 / bidmaxrd) * 275)
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 4 + bidmaxrd))
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 0, 4 + bidmaxrd))
                        End If
                    End If
                Next
            End If
            'sql获取报价数据
            Form1.sqlcon1.sqlstr("
            SELECT * 
            FROM  
            (SELECT 
            bid.bididx,bid.bidname,package.pkgidx,package.pkgname,package.id as pkgid
            FROM bid,package
            WHERE bid.oveid = " & Form3.pro_id & "
            AND bid.id = package.bidid
            ORDER BY bid.id,package.id) as tb1
            LEFT JOIN 
            (SELECT 
            factory.facname,factory_config.pkgid as pkgid
            FROM factory_config,factory
            WHERE factory_config.facid = factory.id) as tb2
            ON 
            tb1.pkgid = tb2.pkgid", 2)
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                '设置数据行格式
                cstyle = wok.CreateCellStyle
                cfont = wok.CreateFont
                cfont.FontName = "方正仿宋_GBK"
                cfont.FontHeightInPoints = 11
                cfont.IsBold = False
                cstyle.WrapText = True
                cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                cstyle.Alignment = HorizontalAlignment.Center
                cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                '插入数据行
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(0) & dr.Item(1))
                    xlrow = sht.LastRowNum + 1
                    row = sht.CreateRow(xlrow)
                    row.Height = 32 * 20
                    '填写包限价
                    Form1.sqlcon1.sqlstr("
                    SELECT SUM(engineering_config.char_xj) 
                    FROM engineering,engineering_config 
                    WHERE engineering.pkgid = " & dr.Item(4) & "
                    AND engineering.id = engineering_config.engid", 1)
                    Form1.sqlcon1.dataf.Read()
                    For xlcol = 0 To 4
                        Select Case xlcol
                            Case 0
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(xlrow - 2)
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 1
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(dr.Item(2))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 2
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(dr.Item(3))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 3
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case 4
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(dr.Item(5).ToString)
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                        End Select
                    Next
                    '获取分标最大报价轮次
                    Form1.sqlcon1.sqlstr("
                    SELECT bid.bidname,MAX(price_config.couid)
                    FROM price_config,bid,package,factory_config
                    WHERE bid.oveid = " & Form3.pro_id & "
                    AND bid.bidname = " & Chr(34) & dr.Item(1) & Chr(34) & "
                    AND bid.id = package.bidid
                    AND package.id = factory_config.pkgid
                    AND factory_config.id = price_config.fngid", 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        Form1.sqlcon1.dataf.Read()
                        Try
                            bidmaxrd = Form1.sqlcon1.dataf.Item(1)
                        Catch ex As Exception
                            bidmaxrd = 3
                        End Try
                    End If
                    'sql获取pkgmaxrd
                    Form1.sqlcon1.sqlstr("
                    SELECT MAX(price_config.couid) 
                    FROM price_config,factory_config
                    WHERE factory_config.pkgid = " & dr.Item(4) & "
                    AND factory_config.id = price_config.fngid", 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        Form1.sqlcon1.dataf.Read()
                        Try
                            pkgmaxrd = Form1.sqlcon1.dataf.Item(0)
                        Catch ex As Exception
                            pkgmaxrd = bidmaxrd
                        End Try
                    Else
                        pkgmaxrd = bidmaxrd
                    End If
                    '填写弃包
                    Form1.sqlcon1.sqlstr("
                    SELECT factory_config.disst 
                    FROM factory,factory_config 
                    WHERE factory_config.pkgid = " & dr.Item(4) & "
                    AND factory_config.facid = factory.id 
                    AND factory.id IN 
                    (SELECT id 
                    FROM factory 
                    WHERE factory.facname = " & Chr(34) & dr.Item(5).ToString & Chr(34) &
                    ")", 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        Form1.sqlcon1.dataf.Read()
                        If Form1.sqlcon1.dataf.Item(0) <> "" Then
                            For xlcol = 5 To 4 + bidmaxrd
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("弃包")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            Continue For
                        End If
                    End If
                    '填写废标
                    Form1.sqlcon1.sqlstr("
                    SELECT factory_config.repst 
                    FROM factory,factory_config 
                    WHERE factory_config.pkgid = " & dr.Item(4) & "
                    AND factory_config.facid = factory.id 
                    AND factory.id IN 
                    (SELECT id 
                    FROM factory 
                    WHERE factory.facname = " & Chr(34) & dr.Item(5).ToString & Chr(34) &
                    ")", 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        Form1.sqlcon1.dataf.Read()
                        If Form1.sqlcon1.dataf.Item(0) <> "" Then
                            For xlcol = 5 To 4 + bidmaxrd
                                cel = row.CreateCell(xlcol)
                                Select Case xlcol
                                    Case 5
                                        '填写首轮报价
                                        Form1.sqlcon1.sqlstr("
                                        SELECT price_rounds.price
                                        FROM factory_config,price_config,price_rounds
                                        WHERE factory_config.pkgid = " & dr.Item(4) & "
                                        AND factory_config.id =  price_config.fngid
                                        AND price_config.id = price_rounds.preid
                                        AND price_rounds.rouid = " & xlcol - 4 & "
                                        AND factory_config.facid IN
                                        (SELECT id 
                                        FROM factory 
                                        WHERE factory.facname = " & Chr(34) & dr.Item(5).ToString & Chr(34) &
                                        ")", 1)
                                        If Form1.sqlcon1.dataf.HasRows Then
                                            Form1.sqlcon1.dataf.Read()
                                            If Form1.sqlcon1.dataf.Item(0).ToString <> "" Then
                                                cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                            Else
                                                cel.SetCellValue("此轮未报价")
                                            End If
                                        Else
                                            cel.SetCellValue("此轮未报价")
                                        End If
                                        cstyle.SetFont(cfont)
                                        cel.CellStyle = cstyle
                                    Case Else
                                        cel.SetCellValue("终止谈判")
                                        cstyle.SetFont(cfont)
                                        cel.CellStyle = cstyle
                                End Select
                            Next
                            Continue For
                        End If
                    End If
                    '判断流包
                    Form1.sqlcon1.sqlstr("
                    SELECT COUNT(factory_config.facid),package_config.char_fs 
                    FROM factory_config,package_config
                    WHERE factory_config.pkgid = " & dr.Item(4) & "
                    AND factory_config.disst = " & Chr(34) & Chr(34) & "
                    AND factory_config.repst = " & Chr(34) & Chr(34) & "
                    AND factory_config.pkgid = package_config.pkgid", 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        Form1.sqlcon1.dataf.Read()
                        If Form1.sqlcon1.dataf.Item(0) > 1 Then
                        ElseIf Form1.sqlcon1.dataf.Item(0) = 1 And
                        Form1.sqlcon1.dataf.Item(1).ToString = "单一来源" Then
                        Else
                            For xlcol = 5 To 4 + bidmaxrd
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("流包")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            Continue For
                        End If
                    End If
                    '填写报价数据
                    For xlcol = 5 To 4 + bidmaxrd
                        cel = row.CreateCell(xlcol)
                        If xlcol < 5 + pkgmaxrd Then
                            Form1.sqlcon1.sqlstr("
                            SELECT price_rounds.price
                            FROM factory_config,price_config,price_rounds
                            WHERE factory_config.pkgid = " & dr.Item(4) & "
                            AND factory_config.id =  price_config.fngid
                            AND price_config.id = price_rounds.preid
                            AND price_rounds.rouid = " & xlcol - 4 & "
                            AND factory_config.facid IN
                            (SELECT id 
                            FROM factory 
                            WHERE factory.facname = " & Chr(34) & dr.Item(5).ToString & Chr(34) &
                            ")", 1)
                            If Form1.sqlcon1.dataf.HasRows Then
                                Form1.sqlcon1.dataf.Read()
                                If Form1.sqlcon1.dataf.Item(0).ToString <> "" Then
                                    cel.SetCellValue(Form1.sqlcon1.dataf.Item(0))
                                Else
                                    cel.SetCellValue("此轮未报价")
                                End If
                            Else
                                cel.SetCellValue("此轮未报价")
                            End If
                        End If
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Next
                Next
            End If
            'sql获取bidname
            Form1.sqlcon1.sqlstr("
            SELECT bididx,bidname 
            FROM bid 
            WHERE oveid = " & Form3.pro_id, 2)
            '合并单元格
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                sht = wok.GetSheet(dr.Item(0).ToString & dr.Item(1).ToString)
                i = 2
                For xlrow = 3 To sht.LastRowNum
                    Select Case xlrow
                        Case sht.LastRowNum
                            If sht.GetRow(xlrow - 1).GetCell(1).StringCellValue <>
                                    sht.GetRow(xlrow).GetCell(1).StringCellValue Then
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 1, 1))
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 2, 2))
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 3, 3))
                            Else
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow, 1, 1))
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow, 2, 2))
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow, 3, 3))
                            End If
                        Case Else
                            If sht.GetRow(xlrow - 1).GetCell(1).StringCellValue <>
                                    sht.GetRow(xlrow).GetCell(1).StringCellValue Then
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 1, 1))
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 2, 2))
                                sht.AddMergedRegion(New SS.Util.CellRangeAddress(i, xlrow - 1, 3, 3))
                                i = xlrow
                            End If
                    End Select
                Next
            Next
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_kaoqin(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        'sql获取proname,bidname并创建
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT overload.proname,expert_config.char_gp
        FROM overload,expert_config
        WHERE overload.id = " & Form3.pro_id & "
        AND expert_config.oveid = overload.id
        ORDER BY expert_config.id", 1)
        '创建表格式及标题行
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                sht = wok.CreateSheet(Form1.sqlcon1.dataf.Item(1).ToString)
                For xlrow = 0 To 2
                    Select Case xlrow
                        Case 0
                            row = sht.CreateRow(xlrow)
                            row.Height = 45 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "宋体"
                            cfont.FontHeightInPoints = 16
                            cfont.IsBold = True
                            cstyle.WrapText = True
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 7
                                cel = row.CreateCell(xlcol)
                                If xlcol = 0 Then
                                    cel.SetCellValue(Form1.sqlcon1.dataf.Item(0) & vbCrLf &
                                                     Form1.sqlcon1.dataf.Item(1) & "考勤表")
                                End If
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(0, 0, 0, 7))
                        Case 1
                            row = sht.CreateRow(xlrow)
                            row.Height = 30 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "宋体"
                            cfont.FontHeightInPoints = 14
                            cfont.IsBold = False
                            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 7
                                cel = row.CreateCell(xlcol)
                                Select Case xlcol
                                    Case 0
                                        cel.SetCellValue("序号")
                                    Case 1
                                        cel.SetCellValue("姓名")
                                    Case 2
                                        cel.SetCellValue(Format(Now(), "yyyy年MM月dd日"))
                                    Case 5
                                        cel.SetCellValue(Format(DateAdd("d", 1, Now), "yyyy年MM月dd日"))
                                    Case Else
                                        cel.SetCellValue("")
                                End Select
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 2, 4))
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 1, 5, 7))
                        Case 2
                            sli.Clear()
                            sli.Add("1", "序号")
                            sli.Add("2", "姓名")
                            sli.Add("3", "上午")
                            sli.Add("4", "下午")
                            sli.Add("5", "晚上")
                            sli.Add("6", "上午")
                            sli.Add("7", "下午")
                            sli.Add("8", "晚上")
                            row = sht.CreateRow(xlrow)
                            row.Height = 30 * 20
                            cstyle = wok.CreateCellStyle
                            cfont = wok.CreateFont
                            cfont.FontName = "宋体"
                            cfont.FontHeightInPoints = 14
                            cfont.IsBold = False
                            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                            cstyle.Alignment = HorizontalAlignment.Center
                            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                            For xlcol = 0 To 7
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(sli(CStr(xlcol + 1)))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 2, 0, 0))
                            sht.AddMergedRegion(New SS.Util.CellRangeAddress(1, 2, 1, 1))
                    End Select
                Next
                '设置纸张方向
                sht.PrintSetup.Landscape = True
                sht.PrintSetup.PaperSize = 9
                '页脚
                sht.Footer.Left = "考勤人："
                '缩放90%
                sht.PrintSetup.Scale = 80
                '设置列宽
                For xlcol = 0 To 7
                    Select Case xlcol
                        Case 0
                            sht.SetColumnWidth(xlcol, 6 * 275)
                        Case 1
                            sht.SetColumnWidth(xlcol, 18 * 275)
                        Case Else
                            sht.SetColumnWidth(xlcol, 20 * 275)
                    End Select
                Next
            End While
            'sql获取
            Form1.sqlcon1.sqlstr("
            SELECT expert_config.char_gp,expert.expname 
            FROM expert,expert_config
            WHERE expert_config.oveid = " & Form3.pro_id & "
            AND expert_config.expid = expert.id
            ORDER BY expert_config.char_gp,expert_config.id", 2)
            '设置格式
            cstyle = wok.CreateCellStyle
            cfont = wok.CreateFont
            cfont.FontName = "宋体"
            cfont.FontHeightInPoints = 14
            cfont.IsBold = False
            cstyle.WrapText = True
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            '填写数据
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(0).ToString)
                    xlrow = sht.LastRowNum + 1
                    row = sht.CreateRow(xlrow)
                    row.Height = 45 * 20
                    For xlcol = 0 To 7
                        cel = row.CreateCell(xlcol)
                        Select Case xlcol
                            Case 0
                                cel.SetCellValue(xlrow - 2)
                            Case 1
                                cel.SetCellValue(dr.Item(1))
                            Case Else
                                cel.SetCellValue("")
                        End Select
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Next
                Next
            End If
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_chengjiaolx(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        'sql获取expert_config.char_gp并创建
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT 
        bid.bididx,bid.bidname,package.pkgidx,package.pkgname,package.id 
        FROM 
        bid,package 
        WHERE 
        bid.oveid = " & Form3.pro_id & "
        AND 
        bid.id = package.bidid
        ORDER BY 
        bid.id,package.id", 2)
        '创建表格式及标题行
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            sht = wok.CreateSheet("成交人联系方式")
            sli.Clear()
            sli.Add("1", "分标名称")
            sli.Add("2", "包号")
            sli.Add("3", "包名称")
            sli.Add("4", "成交人")
            sli.Add("5", "联系人")
            sli.Add("6", "联系电话")
            sli.Add("7", "邮箱")
            '插入行
            row = sht.CreateRow(0)
            row.Height = 35 * 20
            '设置格式
            cstyle = wok.CreateCellStyle
            cstyle.WrapText = True
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cfont = wok.CreateFont
            cfont.FontName = "方正仿宋_GBK"
            cfont.FontHeightInPoints = 11
            cfont.IsBold = True
            For xlcol = 0 To 6
                cel = row.CreateCell(xlcol)
                cel.SetCellValue(sli(CStr(xlcol + 1)))
                cstyle.SetFont(cfont)
                cel.CellStyle = cstyle
            Next
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                xlrow = sht.LastRowNum + 1
                row = sht.CreateRow(xlrow)
                row.Height = 45 * 20
                For xlcol = 0 To 2
                    Select Case xlcol
                        Case 0
                            cel = row.CreateCell(xlcol)
                            cel.SetCellValue(dr.Item(0) & dr.Item(1))
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Case 1
                            cel = row.CreateCell(xlcol)
                            cel.SetCellValue(dr.Item(2))
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                        Case 2
                            cel = row.CreateCell(xlcol)
                            cel.SetCellValue(dr.Item(3))
                            cstyle.SetFont(cfont)
                            cel.CellStyle = cstyle
                    End Select
                Next
                'sql获取package_abort表数据
                Form1.sqlcon1.sqlstr("
                SELECT package_abort.aboid 
                FROM package_abort 
                WHERE package_abort.pkgid = " & dr.Item(4), 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    For xlcol = 3 To 6
                        Select Case xlcol
                            Case 3
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("流包")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Case Else
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue("")
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                        End Select
                    Next
                Else
                    'sql获取package_config表数据
                    Form1.sqlcon1.sqlstr("
                    SELECT package_config.char_ps 
                    FROM package_config 
                    WHERE package_config.pkgid = " & dr.Item(4), 1)
                    Form1.sqlcon1.dataf.Read()
                    If Form1.sqlcon1.dataf.Item(0) = "最低价法" Then
                        'sql获取sorting表数据
                        Form1.sqlcon1.sqlstr("
                        SELECT 
                        factory.facname,factory.faccont,factory.facline,factory.facmail
                        FROM factory,sorting_price 
                        WHERE sorting_price.oveid = " & Form3.pro_id & "
                        AND sorting_price.pkgid = " & dr.Item(4) & "
                        AND sorting_price.facid = factory.id 
                        AND sorting_price.sort = 1", 1)
                        If Form1.sqlcon1.dataf.HasRows Then
                            Form1.sqlcon1.dataf.Read()
                            For xlcol = 3 To 6
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(Form1.sqlcon1.dataf.Item(xlcol - 3))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                        End If
                    Else
                        Form1.sqlcon1.sqlstr("
                        SELECT 
                        factory.facname,factory.faccont,factory.facline,factory.facmail
                        FROM factory,sorting_price,sorting
                        WHERE sorting.oveid = " & Form3.pro_id & "
                        AND sorting.pkgid = " & dr.Item(4) & "
                        AND sorting.facid = factory.id 
                        AND sorting.facid = sorting_price.facid
                        AND sorting.pkgid = sorting_price.pkgid
                        AND sorting.sort = 1", 1)
                        If Form1.sqlcon1.dataf.HasRows Then
                            Form1.sqlcon1.dataf.Read()
                            For xlcol = 3 To 6
                                cel = row.CreateCell(xlcol)
                                cel.SetCellValue(Form1.sqlcon1.dataf.Item(xlcol - 3))
                                cstyle.SetFont(cfont)
                                cel.CellStyle = cstyle
                            Next
                        End If
                    End If
                End If
            Next
            '设置纸张方向
            sht.PrintSetup.Landscape = True
            sht.PrintSetup.PaperSize = 9
            '缩放比例
            sht.PrintSetup.Scale = 85
            '设置列宽
            For xlcol = 0 To 6
                Select Case xlcol
                    Case 0
                        sht.SetColumnWidth(xlcol, 25 * 275)
                    Case 1
                        sht.SetColumnWidth(xlcol, 8 * 275)
                    Case 2
                        sht.SetColumnWidth(xlcol, 33 * 275)
                    Case 3
                        sht.SetColumnWidth(xlcol, 33 * 275)
                    Case 4
                        sht.SetColumnWidth(xlcol, 8 * 275)
                    Case 5
                        sht.SetColumnWidth(xlcol, 19 * 275)
                    Case 6
                        sht.SetColumnWidth(xlcol, 19 * 275)
                End Select
            Next
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_186baojia(ByVal fp As String)
        Dim xlcol, xlrow As Integer
        Dim sli As New SortedList
        Dim dr As DataRow

        'sql获取proname,bidname并创建
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono,bid.bididx,bid.bidname,bid.bidno
        FROM overload,bid
        WHERE overload.id = " & Form3.pro_id & "
        AND overload.id = bid.oveid 
        ORDER BY bid.id", 1)
        '创建表格式及标题行
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                sht = wok.CreateSheet(Form1.sqlcon1.dataf.Item(2).ToString & Form1.sqlcon1.dataf.Item(3).ToString)
                sli.Clear()
                sli.Add("1", "包号")
                sli.Add("2", "包名称")
                sli.Add("3", "应答单位")
                sli.Add("4", "最终报价")
                row = sht.CreateRow(xlrow)
                row.Height = 35 * 20
                cstyle = wok.CreateCellStyle
                cfont = wok.CreateFont
                cfont.FontName = "方正仿宋_GBK"
                cfont.FontHeightInPoints = 13
                cfont.IsBold = True
                cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
                cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
                cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
                cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
                cstyle.Alignment = HorizontalAlignment.Center
                cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
                For xlcol = 0 To 3
                    cel = row.CreateCell(xlcol)
                    cel.SetCellValue(sli(CStr(xlcol + 1)))
                    cstyle.SetFont(cfont)
                    cel.CellStyle = cstyle
                Next
                '设置纸张方向
                sht.PrintSetup.Landscape = True
                sht.PrintSetup.PaperSize = 9
                '缩放90%
                sht.PrintSetup.Scale = 95
                '设置列宽
                For xlcol = 0 To 3
                    Select Case xlcol
                        Case 0
                            sht.SetColumnWidth(xlcol, 8 * 275)
                        Case 1
                            sht.SetColumnWidth(xlcol, 50 * 275)
                        Case 2
                            sht.SetColumnWidth(xlcol, 40 * 275)
                        Case 3
                            sht.SetColumnWidth(xlcol, 35 * 275)
                    End Select
                Next
            End While
            'sql获取
            Form1.sqlcon1.sqlstr("
            SELECT 
            tb1.bididx,tb1.bidname,tb1.pkgidx,tb1.pkgname,tb1.facname,
            tb2.price
            FROM
            (SELECT 
            bid.bididx,bid.bidname,
            package.pkgidx,package.pkgname,
            factory.facname,factory_config.id as id1
            FROM bid,package,factory,factory_config
            WHERE bid.oveid = " & Form3.pro_id & "
            AND bid.id = package.bidid
            AND factory_config.pkgid = package.id
            AND factory_config.facid = factory.id
            AND factory_config.disst = " & Chr(34) & Chr(34) & "
            AND factory_config.repst = " & Chr(34) & Chr(34) & "
            ORDER BY bid.id,package.id) as tb1
            LEFT join 
            (SELECT 
            price_rounds.price,factory_config.id as id2
            FROM price_config,price_rounds,factory_config
            WHERE factory_config.id = price_config.fngid
            AND price_config.id = price_rounds.preid
            AND price_config.couid = price_rounds.rouid) as tb2
            ON tb1.id1 = tb2.id2", 2)
            '设置格式
            cstyle = wok.CreateCellStyle
            cfont = wok.CreateFont
            cfont.FontName = "方正仿宋_GBK"
            cfont.FontHeightInPoints = 11
            cfont.IsBold = False
            cstyle.WrapText = True
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            '填写数据
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    sht = wok.GetSheet(dr.Item(0).ToString & dr.Item(1).ToString)
                    xlrow = sht.LastRowNum + 1
                    row = sht.CreateRow(xlrow)
                    row.Height = 32 * 20
                    For xlcol = 0 To 3
                        cel = row.CreateCell(xlcol)
                        Select Case xlcol
                            Case 3
                                If dr.Item(5).ToString = "" Then
                                    cel.SetCellValue(0)
                                Else
                                    cel.SetCellValue(dr.Item(5))
                                End If
                            Case Else
                                cel.SetCellValue(dr.Item(xlcol + 2).ToString)
                        End Select
                        cstyle.SetFont(cfont)
                        cel.CellStyle = cstyle
                    Next
                Next
            End If
        End If
        Try
            Dim fs As New FileStream(fp & "\" & Form3.TextBox1.Text.ToString & ".xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_swyuebiao(ByVal fp As String, ByVal bidids As Integer)
        Dim xlcol, xlrow, i, j As Integer
        Dim dr As DataRow

        '填写标题行
        cel = Form3.xlscon1.wksheet.GetRow(1).GetCell(0)
        cel.SetCellValue("批次名称：" & Form1.sqlcon1.dataf.Item(2))
        cel = Form3.xlscon1.wksheet.GetRow(1).GetCell(4)
        cel.SetCellValue("分标名称：" & Form1.sqlcon1.dataf.Item(1))
        'sql获取bid,package
        Form1.sqlcon1.sqlstr("
        SELECT tb1.pkgidx,tb1.char_zz,char_yj,tb2.facname
        FROM
        (SELECT 
        package.pkgidx,package.id,
        package_request.char_zz,package_request.char_yj
        FROM bid,package,package_request
        WHERE bid.id = " & bidids & "
        AND package.bidid = bid.id
        AND package_request.pkgid = package.id
        ORDER BY package.id)as tb1
        LEFT JOIN 
        (SELECT package.id,factory.facname
        FROM package,factory,factory_config
        WHERE factory_config.facid = factory.id
        AND factory_config.pkgid = package.id
        AND factory_config.disst = " & Chr(34) & Chr(34) & ") as tb2
        ON tb1.id = tb2.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            i = 1
            j = Form3.xlscon1.wksheet.LastRowNum
            '设置单元格格式
            cstyle = Form3.xlscon1.wkbk.CreateCellStyle
            cfont = Form3.xlscon1.wkbk.CreateFont
            cfont.FontName = "宋体"
            cfont.FontHeightInPoints = 10
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            cstyle.WrapText = True
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                xlrow = Form3.xlscon1.wksheet.LastRowNum + 1
                row = Form3.xlscon1.wksheet.CreateRow(xlrow)
                row.Height = 45 * 20
                For xlcol = 1 To 28
                    '新建单元格
                    cel = row.CreateCell(xlcol - 1)
                    '填入值
                    Select Case xlcol
                        Case 1
                            cel.SetCellValue(i)
                        Case 2
                            cel.SetCellValue(dr.Item(0).ToString)
                        Case 3
                            cel.SetCellValue(dr.Item(1).ToString & vbCrLf & dr.Item(2).ToString)
                        Case 4
                            cel.SetCellValue(dr.Item(3).ToString)
                        Case Else
                            cel.SetCellValue("")
                    End Select
                    cstyle.SetFont(cfont)
                    cel.CellStyle = cstyle
                Next
                i += 1
            Next
            '合并单元格
            For xlrow = j To Form3.xlscon1.wksheet.LastRowNum
                Select Case xlrow
                    Case Form3.xlscon1.wksheet.LastRowNum
                        Form3.xlscon1.wksheet.AddMergedRegion(New SS.Util.CellRangeAddress(j, xlrow, 2, 2))
                    Case Else
                        cel = Form3.xlscon1.wksheet.GetRow(xlrow).GetCell(1)
                        If cel.StringCellValue <> Form3.xlscon1.wksheet.GetRow(xlrow - 1).GetCell(1).ToString Then
                            Form3.xlscon1.wksheet.AddMergedRegion(New SS.Util.CellRangeAddress(j, xlrow - 1, 2, 2))
                            j = xlrow
                        End If
                End Select
            Next
            '设置打印区域
            Form3.xlscon1.wkbk.SetPrintArea(0, 0, 27, 0, xlrow)
        End If
        Try
            Dim fs As New FileStream(fp & "\商务响应情况汇总表-" & Form1.sqlcon1.dataf.Item(1) & ".xlsx", FileMode.Create)
            Form3.xlscon1.wkbk.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try

    End Sub

    Public Sub dc_jsyuebiao(ByVal fp As String, ByVal bidids As Integer)
        Dim xlcol, xlrow, i, j As Integer
        Dim dr As DataRow

        '填写标题行
        cel = Form3.xlscon1.wksheet.GetRow(1).GetCell(0)
        cel.SetCellValue("批次名称：" & Form1.sqlcon1.dataf.Item(2))
        cel = Form3.xlscon1.wksheet.GetRow(1).GetCell(4)
        cel.SetCellValue("分标名称：" & Form1.sqlcon1.dataf.Item(1))
        'sql获取bid,package
        Form1.sqlcon1.sqlstr("
        SELECT tb1.pkgidx,tb1.char_zz,char_yj,tb2.facname
        FROM
        (SELECT 
        package.pkgidx,package.id,
        package_request.char_zz,package_request.char_yj
        FROM bid,package,package_request
        WHERE bid.id = " & bidids & "
        AND package.bidid = bid.id
        AND package_request.pkgid = package.id
        ORDER BY package.id)as tb1
        LEFT JOIN 
        (SELECT package.id,factory.facname
        FROM package,factory,factory_config
        WHERE factory_config.facid = factory.id
        AND factory_config.pkgid = package.id
        AND factory_config.disst = " & Chr(34) & Chr(34) & ") as tb2
        ON tb1.id = tb2.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            i = 1
            j = Form3.xlscon1.wksheet.LastRowNum
            '设置单元格格式
            cstyle = Form3.xlscon1.wkbk.CreateCellStyle
            cfont = Form3.xlscon1.wkbk.CreateFont
            cfont.FontName = "宋体"
            cfont.FontHeightInPoints = 10
            cstyle.BorderTop = SS.UserModel.BorderStyle.Thin
            cstyle.BorderBottom = SS.UserModel.BorderStyle.Thin
            cstyle.BorderLeft = SS.UserModel.BorderStyle.Thin
            cstyle.BorderRight = SS.UserModel.BorderStyle.Thin
            cstyle.Alignment = HorizontalAlignment.Center
            cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
            cstyle.WrapText = True
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                xlrow = Form3.xlscon1.wksheet.LastRowNum + 1
                row = Form3.xlscon1.wksheet.CreateRow(xlrow)
                row.Height = 45 * 20
                For xlcol = 1 To 28
                    '新建单元格
                    cel = row.CreateCell(xlcol - 1)
                    '填入值
                    Select Case xlcol
                        Case 1
                            cel.SetCellValue(i)
                        Case 2
                            cel.SetCellValue(dr.Item(0).ToString)
                        Case 3
                            cel.SetCellValue(dr.Item(1).ToString & vbCrLf & dr.Item(2).ToString)
                        Case 4
                            cel.SetCellValue(dr.Item(3).ToString)
                        Case Else
                            cel.SetCellValue("")
                    End Select
                    cstyle.SetFont(cfont)
                    cel.CellStyle = cstyle
                Next
                i += 1
            Next
            '合并单元格
            For xlrow = j To Form3.xlscon1.wksheet.LastRowNum
                Select Case xlrow
                    Case Form3.xlscon1.wksheet.LastRowNum
                        Form3.xlscon1.wksheet.AddMergedRegion(New SS.Util.CellRangeAddress(j, xlrow, 2, 2))
                    Case Else
                        cel = Form3.xlscon1.wksheet.GetRow(xlrow - 1).GetCell(1)
                        If cel.StringCellValue <> Form3.xlscon1.wksheet.GetRow(xlrow).GetCell(1).ToString Then
                            Form3.xlscon1.wksheet.AddMergedRegion(New SS.Util.CellRangeAddress(j, xlrow - 1, 2, 2))
                            j = xlrow
                        End If
                End Select
            Next
            '设置打印区域
            Form3.xlscon1.wkbk.SetPrintArea(0, 0, 27, 0, xlrow)
        End If
        Try
            Dim fs As New FileStream(fp & "\技术响应情况汇总表-" & Form1.sqlcon1.dataf.Item(1) & ".xlsx", FileMode.Create)
            Form3.xlscon1.wkbk.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try

    End Sub

    Public Sub dc_danganxinxi(ByVal fp As String)
        Dim xlcol, xlrow As Integer

        sht = wok.CreateSheet("档案信息汇总")
        row = sht.CreateRow(0)
        row.Height = 25 * 20
        cstyle = wok.CreateCellStyle
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.FontHeightInPoints = 11
        cfont.IsBold = True
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        For xlcol = 0 To 6
            cel = row.CreateCell(xlcol)
            Select Case xlcol
                Case 0
                    cel.SetCellValue("盒号")
                Case 1
                    cel.SetCellValue("文件编号")
                Case 2
                    cel.SetCellValue("责任者")
                Case 3
                    cel.SetCellValue("文件题目")
                Case 4
                    cel.SetCellValue("文件类型")
                Case 5
                    cel.SetCellValue("文件日期")
                Case 6
                    cel.SetCellValue("页数")
            End Select
            cstyle.SetFont(cfont)
            cel.CellStyle = cstyle
        Next
        '获取数据
        Form1.sqlcon1.sqlstr("
        SELECT cc_dossier.boxidx,cc_dossier.fileno,cc_dossier.fileow,cc_dossier.filena,cc_dossier.filetye,cc_dossier.filedat,cc_dossier.filepag
        FROM cc_dossier
        WHERE cc_dossier.dosname = " & Chr(34) &
        Form12.ComboBoxEdit1.Text.ToString & Chr(34) & "
        ORDER BY id", 1)
        cstyle = wok.CreateCellStyle
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.FontHeightInPoints = 11
        cfont.IsBold = False
        cstyle.Alignment = HorizontalAlignment.Left
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        xlrow = 1
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                row = sht.CreateRow(xlrow)
                For xlcol = 0 To 6
                    cel = row.CreateCell(xlcol)
                    cel.SetCellValue(Form1.sqlcon1.dataf.Item(xlcol))
                    cstyle.SetFont(cfont)
                    cel.CellStyle = cstyle
                Next
                xlrow += 1
            End While
        End If
        '设置纸张方向
        sht.PrintSetup.Landscape = True
        sht.PrintSetup.PaperSize = 9
        '设置列宽
        For xlcol = 0 To 6
            sht.SetColumnWidth(xlcol, 30 * 275)
        Next
        Try
            Dim fs As New FileStream(fp & "\档案整理信息汇总表.xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_danganbj(ByVal fp As String)
        Dim xlcol, xlrow As Integer

        sht = wok.CreateSheet("封面内容目录")
        row = sht.CreateRow(0)
        row.Height = 25 * 20
        cstyle = wok.CreateCellStyle
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.FontHeightInPoints = 11
        cfont.IsBold = True
        cstyle.Alignment = HorizontalAlignment.Center
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        For xlcol = 0 To 1
            cel = row.CreateCell(xlcol)
            Select Case xlcol
                Case 0
                    cel.SetCellValue("盒号")
                Case 1
                    cel.SetCellValue("封面内容")
            End Select
            cstyle.SetFont(cfont)
            cel.CellStyle = cstyle
        Next
        '获取数据
        Form1.sqlcon1.sqlstr("
        SELECT boxidx,fmstr
        FROM cc_dossier_config
        WHERE dosname = " & Chr(34) &
        Form12.ComboBoxEdit1.Text.ToString & Chr(34) & "
        ORDER BY id", 1)
        cstyle = wok.CreateCellStyle
        cfont = wok.CreateFont
        cfont.FontName = "宋体"
        cfont.FontHeightInPoints = 11
        cfont.IsBold = False
        cstyle.Alignment = HorizontalAlignment.Left
        cstyle.VerticalAlignment = SS.UserModel.VerticalAlignment.Center
        xlrow = 1
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                row = sht.CreateRow(xlrow)
                For xlcol = 0 To 1
                    cel = row.CreateCell(xlcol)
                    cel.SetCellValue(Form1.sqlcon1.dataf.Item(xlcol))
                    cstyle.SetFont(cfont)
                    cel.CellStyle = cstyle
                Next
                xlrow += 1
            End While
        End If
        '设置纸张方向
        sht.PrintSetup.Landscape = True
        sht.PrintSetup.PaperSize = 9
        '设置列宽
        For xlcol = 0 To 6
            sht.SetColumnWidth(xlcol, 30 * 275)
        Next
        Try
            Dim fs As New FileStream(fp & "\背脊-封面内容目录.xlsx", FileMode.Create)
            wok.Write(fs)
            fs.Close()
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
    End Sub



End Class

Class filewdoutput
    Dim doc As New Document
    Dim builder As New DocumentBuilder(doc)
    Dim table As New Tables.Table(doc)

    Public Sub dc_dianhua(ByVal fp As String)

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        '纸张方向
        builder.PageSetup.Orientation = Orientation.Landscape
        '段落对其方式
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        'sql数据
        Form1.sqlcon1.sqlstr("
        SELECT proname 
        FROM overload 
        WHERE id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        '设置字体
        builder.Font.Size = 26
        '设置回车
        builder.Writeln(ControlChar.CrLf)
        '填写文字
        builder.Writeln(Form1.sqlcon1.dataf.Item(0))
        '标题
        builder.Writeln(ControlChar.CrLf)
        builder.Font.Size = 36
        builder.Writeln("电话使用记录")
        '分页第二页
        builder.InsertBreak(BreakType.SectionBreakNewPage)
        '设置页眉
        builder.MoveToHeaderFooter(HeaderFooterType.HeaderPrimary)
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.Font.Size = 18
        builder.Write(Form1.sqlcon1.dataf.Item(0))
        '设置页码
        builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary)
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.Font.Size = 9
        builder.InsertField("PAGE", "")
        '文档末尾
        builder.MoveToDocumentEnd()
        '设置表字体格式
        builder.Font.Size = 14
        builder.Font.Bold = True
        '插入表格
        table = builder.StartTable()
        'cell垂直居中
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.RowFormat.Height = 40
        '第一行
        builder.InsertCell()
        builder.CellFormat.Width = 45
        builder.Write("序号")
        builder.InsertCell()
        builder.CellFormat.Width = 82
        builder.Write("姓名")
        builder.InsertCell()
        builder.CellFormat.Width = 144
        builder.Write("使用事由")
        builder.InsertCell()
        builder.CellFormat.Width = 144
        builder.Write("对方电话")
        builder.InsertCell()
        builder.CellFormat.Width = 82
        builder.Write("使用时间")
        builder.InsertCell()
        builder.CellFormat.Width = 82
        builder.Write("签字确认")
        builder.InsertCell()
        builder.CellFormat.Width = 82
        builder.Write("监督人")
        builder.InsertCell()
        builder.CellFormat.Width = 82
        builder.Write("代理机构")
        builder.EndRow().RowFormat.HeadingFormat = True
        '第二行后
        For i = 1 To 30
            builder.InsertCell()
            builder.CellFormat.Width = 45
            builder.InsertCell()
            builder.CellFormat.Width = 82
            builder.InsertCell()
            builder.CellFormat.Width = 144
            builder.InsertCell()
            builder.CellFormat.Width = 144
            builder.InsertCell()
            builder.CellFormat.Width = 82
            builder.InsertCell()
            builder.CellFormat.Width = 82
            builder.InsertCell()
            builder.CellFormat.Width = 82
            builder.InsertCell()
            builder.CellFormat.Width = 82
            builder.EndRow().RowFormat.HeadingFormat = False
        Next
        '结束表格
        table = builder.EndTable()
        '设置表格格式
        table.Alignment = Tables.TableAlignment.Center
        table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")

    End Sub

    Public Sub dc_churu(ByVal fp As String)

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        '纸张方向
        builder.PageSetup.Orientation = Orientation.Landscape
        '段落对其方式
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        'sql数据
        Form1.sqlcon1.sqlstr("
        SELECT proname 
        FROM overload 
        WHERE id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        '设置字体
        builder.Font.Size = 26
        '设置回车
        builder.Writeln(ControlChar.CrLf)
        '填写文字
        builder.Writeln(Form1.sqlcon1.dataf.Item(0))
        '标题
        builder.Writeln(ControlChar.CrLf)
        builder.Font.Size = 36
        builder.Writeln("出入会场记录")
        '分页第二页
        builder.InsertBreak(BreakType.SectionBreakNewPage)
        '设置页眉
        builder.MoveToHeaderFooter(HeaderFooterType.HeaderPrimary)
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.Font.Size = 18
        builder.Write(Form1.sqlcon1.dataf.Item(0))
        '设置页码
        builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary)
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.Font.Size = 9
        builder.InsertField("PAGE", "")
        '文档末尾
        builder.MoveToDocumentEnd()
        '设置表字体格式
        builder.Font.Size = 14
        builder.Font.Bold = True
        '插入表格
        table = builder.StartTable()
        'cell垂直居中
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.RowFormat.Height = 40
        '第一行
        builder.InsertCell()
        builder.CellFormat.Width = 45
        builder.Write("序号")
        builder.InsertCell()
        builder.CellFormat.Width = 82
        builder.Write("姓名")
        builder.InsertCell()
        builder.CellFormat.Width = 370
        builder.Write("事由")
        builder.InsertCell()
        builder.CellFormat.Width = 123
        builder.Write("离开时间")
        builder.InsertCell()
        builder.CellFormat.Width = 123
        builder.Write("返回时间")
        builder.EndRow().RowFormat.HeadingFormat = True
        '第二行后
        For i = 1 To 30
            builder.InsertCell()
            builder.CellFormat.Width = 45
            builder.InsertCell()
            builder.CellFormat.Width = 82
            builder.InsertCell()
            builder.CellFormat.Width = 370
            builder.InsertCell()
            builder.CellFormat.Width = 123
            builder.InsertCell()
            builder.CellFormat.Width = 123
            builder.EndRow().RowFormat.HeadingFormat = False
        Next
        '结束表格
        table = builder.EndTable()
        '设置表格格式
        table.Alignment = Tables.TableAlignment.Center
        table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")

    End Sub

    Public Sub dc_kailiu(ByVal fp As String)
        Dim dr As DataRow

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        'sql数据
        Form1.sqlcon1.sqlstr("
        SELECT proname 
        FROM overload 
        WHERE id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        builder.Font.Size = 16
        builder.Write("应答人不足汇总审查表")
        '第二段
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.Font.Size = 10
        builder.Write(ControlChar.CrLf)
        builder.Write("项目名称：" & Form1.sqlcon1.dataf.Item(0))
        '第三段
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        '插入表格
        table = builder.StartTable()
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.Font.Bold = True
        builder.RowFormat.Height = 35
        '第一行标题
        builder.InsertCell()
        builder.CellFormat.Width = 105
        builder.Write("分标名称")
        builder.InsertCell()
        builder.CellFormat.Width = 45
        builder.Write("包号")
        builder.InsertCell()
        builder.CellFormat.Width = 195
        builder.Write("包名称")
        builder.InsertCell()
        builder.CellFormat.Width = 75
        builder.Write("应答人数量")
        builder.InsertCell()
        builder.CellFormat.Width = 90
        builder.Write("采购形式")
        builder.EndRow()
        '第二行以后
        'sql查询
        Form1.sqlcon1.sqlstr("
        SELECT *
        FROM
        (SELECT 
        bid.bidname,package.pkgidx,package.pkgname,package_config.char_fs,package.id as pkgid
        FROM bid,package,package_config
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.id =package.bidid
        AND package.id = package_config.pkgid
        ORDER BY bid.id,package.id)as tb1
        LEFT JOIN
        (SELECT COUNT(factory_config.facid),factory_config.pkgid as pkgid
        FROM factory_config 
        WHERE factory_config.disst = " & Chr(34) & Chr(34) & "
        GROUP BY factory_config.pkgid) as tb2
        ON tb1.pkgid = tb2.pkgid", 2)
        '设置字体
        builder.Font.Bold = False
        builder.Font.Size = 10
        For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
            '填写数据
            If IsDBNull(dr.Item(5)) Then
                builder.InsertCell()
                builder.CellFormat.Width = 105
                builder.Write(dr.Item(0).ToString)
                builder.InsertCell()
                builder.CellFormat.Width = 45
                builder.Write(dr.Item(1).ToString)
                builder.InsertCell()
                builder.CellFormat.Width = 195
                builder.Write(dr.Item(2).ToString)
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write(0)
                builder.InsertCell()
                builder.CellFormat.Width = 90
                builder.Write(dr.Item(3).ToString)
                builder.EndRow()
            ElseIf dr.item(5) < 2 Then
                If dr.Item(5) = 1 And dr.Item(3) = "单一来源" Then
                Else
                    builder.InsertCell()
                    builder.CellFormat.Width = 105
                    builder.Write(dr.Item(0).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 45
                    builder.Write(dr.Item(1).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 195
                    builder.Write(dr.Item(2).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 75
                    builder.Write(dr.Item(5))
                    builder.InsertCell()
                    builder.CellFormat.Width = 90
                    builder.Write(dr.Item(3).ToString)
                    builder.EndRow()
                End If
            End If
        Next
        '末尾行
        builder.InsertCell()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.CellFormat.Width = 510
        builder.Write("项目经理签字：")
        builder.EndRow()
        builder.InsertCell()
        builder.CellFormat.Width = 255
        builder.Write("监督人签字：")
        builder.InsertCell()
        builder.CellFormat.Width = 255
        builder.Write("法律顾问签字：")
        builder.EndRow()
        table = builder.EndTable
        '设置表格格式
        table.Alignment = Tables.TableAlignment.Center
        table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
        '第四段
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.Font.Bold = False
        builder.Write("填写说明：根据《应答文件递交记录》编制，在应答文件递交截止后、开封前填写。")
        '保存文件
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_baodao(ByVal fp As String)
        Dim dr As DataRow
        Dim i As Integer
        Dim str As String

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Landscape
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center

        'sql获取char_gp数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT expert_config.char_gp,overload.proname,overload.prono
        FROM expert_config,overload
        WHERE expert_config.oveid = " & Form3.pro_id & "
        AND expert_config.oveid=overload.id
        ORDER BY expert_config.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                '第一段
                builder.Font.Name = "宋体"
                builder.Font.Size = 16
                builder.Write("评审组织机构人员手机保管记录" &
                              "（" & dr.Item(0) & "）")
                '第二段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.Font.Size = 11
                builder.InsertBreak(BreakType.ParagraphBreak)
                str = "项目名称：" & dr.Item(1).ToString
                For i = 1 To 65 - Len(dr.Item(1).ToString) - Len(dr.Item(2)).ToString
                    str += " "
                Next
                str += "采购编号：" & dr.Item(2).ToString
                builder.Write(str)
                '第三段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                '插入表格
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.RowFormat.Height = 35
                '第一行标题
                builder.InsertCell()
                builder.CellFormat.Width = 45
                builder.Write("序号")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("姓名")
                builder.InsertCell()
                builder.CellFormat.Width = 180
                builder.Write("身份证号")
                builder.InsertCell()
                builder.CellFormat.Width = 110
                builder.Write("联系电话")
                builder.InsertCell()
                builder.CellFormat.Width = 90
                builder.Write("手机型号")
                builder.InsertCell()
                builder.CellFormat.Width = 180
                builder.Write("取回签字")
                builder.EndRow()
                'sql获取专家信息
                Form1.sqlcon1.sqlstr("
                SELECT expert.expname,expert.expiden,expert.expline
                FROM expert,expert_config
                WHERE expert.id = expert_config.expid
                AND expert_config.oveid = " & Form3.pro_id & "
                AND expert_config.char_gp = " &
                Chr(34) & dr.Item(0).ToString & Chr(34) & "
                ORDER BY expert_config.id", 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    i = 1
                    While Form1.sqlcon1.dataf.Read()
                        builder.InsertCell()
                        builder.CellFormat.Width = 45
                        builder.Write(i)
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write(Form1.sqlcon1.dataf.Item(0).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 180
                        builder.Write(Form1.sqlcon1.dataf.Item(1).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 110
                        builder.Write(Form1.sqlcon1.dataf.Item(2).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 90
                        builder.Write("")
                        builder.InsertCell()
                        builder.CellFormat.Width = 180
                        builder.Write("")
                        builder.EndRow()
                        i += 1
                    End While
                End If
                table = builder.EndTable
                '设置表格格式
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                '设置页脚
                builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary)
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.Font.Size = 11
                builder.Write("监督人：")
                '文档末尾
                builder.MoveToDocumentEnd()
                builder.InsertBreak(BreakType.SectionBreakNewPage)
            Next
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_qiandao(ByVal fp As String)
        Dim dr As DataRow
        Dim i As Integer
        Dim str As String

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Landscape
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center

        'sql获取char_gp数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT expert_config.char_gp,overload.proname,overload.prono
        FROM expert_config,overload
        WHERE expert_config.oveid = " & Form3.pro_id & "
        AND expert_config.oveid=overload.id
        ORDER BY expert_config.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                '第一段
                builder.Font.Name = "方正仿宋_GBK"
                builder.Font.Bold = False
                builder.Font.Size = 16
                builder.Write("评审委员会成员名单及签字表")
                '第二段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.Font.Size = 11
                str = "项目名称：" & dr.Item(1).ToString
                For i = 1 To 65 - Len(dr.Item(1).ToString) - Len(dr.Item(2)).ToString
                    str += " "
                Next
                str += "采购编号：" & dr.Item(2).ToString
                builder.Write(str)
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("组别：" & dr.Item(0).ToString)
                '第三段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                '插入表格
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.Font.Bold = True
                builder.RowFormat.Height = 35
                '第一行标题
                builder.InsertCell()
                builder.CellFormat.Width = 80
                builder.Write("姓名")
                builder.InsertCell()
                builder.CellFormat.Width = 180
                builder.Write("工作单位")
                builder.InsertCell()
                builder.CellFormat.Width = 105
                builder.Write("现从事专业")
                builder.InsertCell()
                builder.CellFormat.Width = 180
                builder.Write("身份证号")
                builder.InsertCell()
                builder.CellFormat.Width = 60
                builder.Write("评委岗位")
                builder.InsertCell()
                builder.CellFormat.Width = 80
                builder.Write("专家签字确认")
                builder.EndRow()
                'sql获取专家信息
                Form1.sqlcon1.sqlstr("
                SELECT 
                expert.expname,expert.expadds,expert.expprof,expert.expiden,
                expert_config.char_po
                FROM expert,expert_config
                WHERE expert.id = expert_config.expid
                AND expert_config.oveid = " & Form3.pro_id & "
                AND expert_config.char_gp = " &
                Chr(34) & dr.Item(0).ToString & Chr(34) & "
                ORDER BY expert_config.id", 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    builder.Font.Bold = False
                    While Form1.sqlcon1.dataf.Read()
                        builder.InsertCell()
                        builder.CellFormat.Width = 80
                        builder.Write(Form1.sqlcon1.dataf.Item(0).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 180
                        builder.Write(Form1.sqlcon1.dataf.Item(1).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 105
                        builder.Write(Form1.sqlcon1.dataf.Item(2).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 180
                        builder.Write(Form1.sqlcon1.dataf.Item(3).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 60
                        builder.Write(Form1.sqlcon1.dataf.Item(4).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 80
                        builder.Write("")
                        builder.EndRow()
                    End While
                End If
                table = builder.EndTable
                '设置表格格式
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                '设置页脚
                builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary)
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.Font.Name = "方正仿宋_GBK"
                builder.Font.Size = 11
                str = "监督人:"
                For i = 1 To 100
                    str += " "
                Next
                str += "时间：" & Format(Now(), "yyyy年MM月dd日")
                builder.Write(str)
                '文档末尾
                builder.MoveToDocumentEnd()
                builder.InsertBreak(BreakType.SectionBreakNewPage)
            Next
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_zhuanjiafei(ByVal fp As String)
        Dim dr As DataRow
        Dim i， idx As Integer
        Dim str As String

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Landscape
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center

        'sql获取char_gp数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT expert_config.char_gp,overload.proname,overload.prono
        FROM expert_config,overload
        WHERE expert_config.oveid = " & Form3.pro_id & "
        AND expert_config.oveid=overload.id
        ORDER BY expert_config.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                '第一段
                builder.Font.Name = "宋体"
                builder.Font.Bold = True
                builder.Font.Size = 16
                builder.Write("评标专家咨询费发放单（" & dr.Item(0) & "）")
                '第二段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.Font.Bold = False
                builder.Font.Size = 12
                str = "项目名称：" & dr.Item(1).ToString
                For i = 1 To 65 - Len(dr.Item(1).ToString) - Len(dr.Item(2)).ToString
                    str += " "
                Next
                str += "共1页，第1页"
                builder.Write(str)
                builder.InsertBreak(BreakType.ParagraphBreak)
                str = "发放时间：" & Format(Now(), "yyyy年MM月dd日")
                For i = 1 To 65 - Len(dr.Item(1).ToString) - Len(dr.Item(2)).ToString
                    str += " "
                Next
                str += "金额单位：人民币元"
                builder.Write(str)
                '第三段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                '插入表格
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.Font.Bold = True
                builder.RowFormat.Height = 30
                '第一行标题
                builder.InsertCell()
                builder.CellFormat.Width = 35
                builder.Write("序号")
                builder.InsertCell()
                builder.CellFormat.Width = 125
                builder.Write("姓名")
                builder.InsertCell()
                builder.CellFormat.Width = 125
                builder.Write("金额")
                builder.InsertCell()
                builder.CellFormat.Width = 125
                builder.Write("签收")
                builder.InsertCell()
                builder.CellFormat.Width = 260
                builder.Write("身份证号码")
                builder.EndRow()
                idx = 1
                'sql获取专家信息
                Form1.sqlcon1.sqlstr("
                SELECT 
                expert.expname,expert.expadds,expert.expprof,expert.expiden,
                expert_config.char_po
                FROM expert,expert_config
                WHERE expert.id = expert_config.expid
                AND expert_config.oveid = " & Form3.pro_id & "
                AND expert_config.char_gp = " &
                Chr(34) & dr.Item(0).ToString & Chr(34) & "
                ORDER BY expert_config.id", 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    builder.Font.Bold = False
                    While Form1.sqlcon1.dataf.Read()
                        builder.InsertCell()
                        builder.CellFormat.Width = 35
                        builder.Write(idx.ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 125
                        builder.Write(Form1.sqlcon1.dataf.Item(0).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 125
                        builder.InsertCell()
                        builder.CellFormat.Width = 125
                        builder.InsertCell()
                        builder.CellFormat.Width = 260
                        builder.Write(Form1.sqlcon1.dataf.Item(3).ToString)
                        builder.EndRow()
                        idx += 1
                    End While
                End If
                builder.Font.Bold = True
                For i = 1 To 2
                    builder.InsertCell()
                    builder.CellFormat.Width = 35
                    builder.InsertCell()
                    builder.CellFormat.Width = 125
                    If i = 1 Then
                        builder.Write("本页小计")
                    Else i = 2
                        builder.Write("合计")
                    End If
                    builder.InsertCell()
                    builder.CellFormat.Width = 125
                    builder.InsertCell()
                    builder.CellFormat.Width = 125
                    builder.InsertCell()
                    builder.CellFormat.Width = 260
                    builder.EndRow()
                Next
                table = builder.EndTable
                '设置表格格式
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                '设置页脚
                builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary)
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.Font.Name = "宋体"
                builder.Font.Size = 12
                str = "发放人:"
                For i = 1 To 100
                    str += " "
                Next
                str += "项目负责人："
                builder.Write(str)
                '文档末尾
                builder.MoveToDocumentEnd()
                builder.InsertBreak(BreakType.SectionBreakNewPage)
            Next
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_zhuanjiafeizh(ByVal fp As String)
        Dim dr As DataRow
        Dim i， idx As Integer
        Dim str As String

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Landscape

        'sql获取char_gp数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT expert_config.char_gp,overload.proname,overload.prono
        FROM expert_config,overload
        WHERE expert_config.oveid = " & Form3.pro_id & "
        AND expert_config.oveid=overload.id
        ORDER BY expert_config.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                '第一段
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                builder.Font.Name = "宋体"
                builder.Font.Bold = True
                builder.Font.Size = 16
                builder.Write("评标专家账户信息统计表（" & dr.Item(0) & "）")
                '第二段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.Font.Bold = False
                builder.Font.Size = 12
                str = "项目名称：" & dr.Item(1).ToString
                For i = 1 To 65 - Len(dr.Item(1).ToString) - Len(dr.Item(2)).ToString
                    str += " "
                Next
                builder.Write(str)
                builder.InsertBreak(BreakType.ParagraphBreak)
                str = "评审开始时间：" & Format(Now(), "yyyy年MM月dd日")
                For i = 1 To 65 - Len(dr.Item(1).ToString) - Len(dr.Item(2)).ToString
                    str += " "
                Next
                str += "评审会议室："
                builder.Write(str)
                '第三段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                '插入表格
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.Font.Bold = True
                builder.RowFormat.Height = 30
                '第一行标题
                builder.InsertCell()
                builder.CellFormat.Width = 35
                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                builder.Write("序号")
                builder.InsertCell()
                builder.CellFormat.Width = 85
                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                builder.Write("姓名")
                builder.InsertCell()
                builder.CellFormat.Width = 160
                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                builder.Write("身份证号码")
                builder.InsertCell()
                builder.CellFormat.Width = 200
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
                builder.Write("评标专家个人银行账户信息")
                builder.InsertCell()
                builder.CellFormat.Width = 200
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                builder.Write("")
                builder.InsertCell()
                builder.CellFormat.Width = 85
                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                builder.Write("联系电话")
                builder.EndRow()
                '第二行标题
                builder.InsertCell()
                builder.CellFormat.Width = 35
                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                builder.InsertCell()
                builder.CellFormat.Width = 85
                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                builder.InsertCell()
                builder.CellFormat.Width = 160
                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                builder.InsertCell()
                builder.CellFormat.Width = 200
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                builder.Write("开户银行")
                builder.InsertCell()
                builder.CellFormat.Width = 200
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                builder.Write("银行账号")
                builder.InsertCell()
                builder.CellFormat.Width = 85
                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                builder.EndRow()
                idx = 1
                'sql获取专家信息
                Form1.sqlcon1.sqlstr("
                SELECT 
                expert.expname,expert.expiden,expert.expbnka,expert.expbnkn,expert.expline,
                expert_config.char_po
                FROM expert,expert_config
                WHERE expert.id = expert_config.expid
                AND expert_config.oveid = " & Form3.pro_id & "
                AND expert_config.char_gp = " &
                Chr(34) & dr.Item(0).ToString & Chr(34) & "
                ORDER BY expert_config.id", 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    builder.Font.Bold = False
                    While Form1.sqlcon1.dataf.Read()
                        builder.InsertCell()
                        builder.CellFormat.Width = 35
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                        builder.Write(idx.ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 85
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                        builder.Write(Form1.sqlcon1.dataf.Item(0).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 160
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                        builder.Write(Form1.sqlcon1.dataf.Item(1).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 200
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                        builder.Write(Form1.sqlcon1.dataf.Item(2).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 200
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                        builder.Write(Form1.sqlcon1.dataf.Item(3).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 85
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                        builder.Write(Form1.sqlcon1.dataf.Item(4).ToString)
                        builder.EndRow()
                        idx += 1
                    End While
                End If
                table = builder.EndTable
                '设置表格格式
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                '最后段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Right
                builder.Font.Bold = False
                builder.Font.Size = 12
                builder.Write("专家费负责人：于老师   0531-5869968")
                '设置页脚
                builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary)
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.Font.Name = "宋体"
                builder.Font.Size = 12
                str = "评标项目经理签字:"
                builder.Write(str)
                '文档末尾
                builder.MoveToDocumentEnd()
                builder.InsertBreak(BreakType.SectionBreakNewPage)
            Next
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_baomi(ByVal fp As String)
        Dim sli As New SortedList
        Dim i As Integer
        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait

        'sql获取char_gp数据
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono
        FROM overload
        WHERE overload.id = " & Form3.pro_id, 1)
        If Form1.sqlcon1.dataf.HasRows Then
            Form1.sqlcon1.dataf.Read()
            '填写第一页
            builder.Font.Name = "方正仿宋_GBK"
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
            builder.ParagraphFormat.FirstLineIndent = 0
            builder.Font.Size = 18
            builder.Font.Bold = True
            builder.Write("廉政保密承诺书")
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.ParagraphFormat.ClearFormatting()
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
            builder.ParagraphFormat.FirstLineIndent = 28
            builder.Font.Size = 12
            builder.Font.Bold = False
            builder.Write("本人参加" & Form1.sqlcon1.dataf.Item(0) &
                          "评审现场工作，现就廉洁保密作如下承诺：")
            sli.Clear()
            sli.Add("1", "一、本人知晓有关评审工作的回避制度，不存在下列情况之一：")
            sli.Add("2", "1.是应答人或应答人主要负责人的近亲属；")
            sli.Add("3", "2.与应答人有经济利益关系；")
            sli.Add("4", "3.曾在招投标有关活动中从事违法行为而受过行政处罚或刑事处罚。")
            sli.Add("5", "二、严格遵守党的纪律、国家法律法规和公司各项规章制度，在评审期间及评审活动结束后严格遵守廉洁从业各项规定。")
            sli.Add("6", "1.不利用职权索取、接受应答人、供应商、中介人以及其他与行使职权有关的单位或者个人的财物或者其他好处；")
            sli.Add("7", "2.不接受应答人、供应商的礼品、宴请以及旅游、健身、娱乐等可能影响招投标等公正性的活动安排；")
            sli.Add("8", "3.不在招标采购有关工作中以各种形式谋取不正当利益；")
            sli.Add("9", "4.在评审中坚持公平公正原则，认真履行评审专家职责；")
            sli.Add("10", "5.不以知悉或者掌握的内部信息谋取利益。")
            sli.Add("11", "三、严格遵守《中华人民共和国保密法》及公司有关保密规定。")
            sli.Add("12", "1.党员要自觉遵守党的纪律，严守党的秘密，充分发挥党员干部先锋模范带头作用，确保不发生失泄密事件；")
            sli.Add("13", "2.严格遵守公司各项保密制度，确保不发生失泄密事件，不发生有重大政治影响和重大经济损失的责任问题；")
            sli.Add("14", "3.评审过程中发现有泄密风险和泄密情况，及时汇报，主动制止，绝不隐瞒；")
            sli.Add("15", "4.评审会议结束后，不泄露与评审有关的应当保密的任何内容。")
            sli.Add("16", "四、认真公正履行评委会及评审专家职责。")
            sli.Add("17", "1.严格遵守国家法律、法规及公司有关规定，严格遵循" & Chr(34) & "公开、公平、公正和诚实信用" & Chr(34) & "原则，严格按照工作程序合法、合规、合理开展各项工作，不发生违法、违规、违反工作纪律的行为；")
            sli.Add("18", "2.在评审期间自觉接受现场监督人员及所在党组织的监督，严格执行评审现场各项管理要求，不擅离职守，服从评审现场统一安排；")
            sli.Add("19", "3.本着对国家、采购人、应答人负责的精神，尽职尽责、高效准确地完成所承担的工作任务")
            sli.Add("20", "五、若有违反上述承诺行为，本人愿承担相应责任或后果。")
            sli.Add("21", "六、本《承诺书》未尽事宜按国家有关法律法规和公司规定执行。")
            '填入内容
            For i = 1 To 21
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write(sli(CStr(i)).ToString)
            Next
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.Write("承诺人：                          日期：    年  月  日")
            builder.InsertBreak(BreakType.ParagraphBreak)
            '插入分页符
            builder.InsertBreak(BreakType.PageBreak)
            '填写第二页
            builder.Font.Name = "方正仿宋_GBK"
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
            builder.ParagraphFormat.FirstLineIndent = 0
            builder.Font.Size = 18
            builder.Font.Bold = True
            builder.Write("廉政保密承诺书（监督）")
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.ParagraphFormat.ClearFormatting()
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
            builder.ParagraphFormat.FirstLineIndent = 28
            builder.Font.Size = 12
            builder.Font.Bold = False
            builder.Write("本人参加" & Form1.sqlcon1.dataf.Item(0) &
                          "评审现场工作，现就廉洁保密作如下承诺：")
            sli.Clear()
            sli.Add("1", "一、严格遵守党的纪律、国家法律法规和公司各项规章制度，在评审期间及评审活动结束后严格遵守廉洁从业各项规定。")
            sli.Add("2", "1.不利用职权索取、接受应答人、供应商、中介人以及其他与行使职权有关的单位或者个人的财物或者其他好处；")
            sli.Add("3", "2.不接受应答人、供应商的礼品、宴请以及旅游、健身、娱乐等可能影响招投标等公正性的活动安排；")
            sli.Add("4", "3.不在招标采购有关工作中以各种形式谋取不正当利益；")
            sli.Add("5", "4.不以知悉或者掌握的内部信息谋取利益。")
            sli.Add("6", "二、严格遵守《中华人民共和国保密法》及公司有关保密规定。")
            sli.Add("7", "1.党员要自觉遵守党的纪律，严守党的秘密，充分发挥党员干部先锋模范带头作用，确保不发生失泄密事件；")
            sli.Add("8", "2.严格遵守公司各项保密制度，确保不发生失泄密事件，不发生有重大政治影响和重大经济损失的责任问题；")
            sli.Add("9", "3.评审现场中发现有泄密风险和泄密情况，及时汇报，主动制止，绝不隐瞒；")
            sli.Add("10", "4.评审会议结束后，不泄露与评审有关的应当保密的任何内容。")
            sli.Add("11", "三、在评审现场监督期间严格执行评审现场各项管理要求，服从评审现场统一安排。")
            sli.Add("12", "四、坚持原则，忠于职守，严格履行监督职责，监督所有评审现场人员严格遵守廉洁及保密相关规定，发现问题及时纠正错误，并提出工作建议。")
            sli.Add("13", "五、若有违反上述承诺行为，本人愿承担相应责任或后果。")
            sli.Add("14", "六、本《承诺书》未尽事宜按国家有关法律法规和公司规定执行。")
            '填入内容
            For i = 1 To 14
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write(sli(CStr(i)).ToString)
            Next
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.Write("承诺人：                          日期：    年  月  日")
            builder.InsertBreak(BreakType.ParagraphBreak)
            '插入分页符
            builder.InsertBreak(BreakType.PageBreak)
            '填写第三页
            builder.Font.Name = "方正仿宋_GBK"
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
            builder.ParagraphFormat.FirstLineIndent = 0
            builder.Font.Size = 18
            builder.Font.Bold = True
            builder.Write("廉政保密承诺书（法务）")
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.ParagraphFormat.ClearFormatting()
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
            builder.ParagraphFormat.FirstLineIndent = 28
            builder.Font.Size = 12
            builder.Font.Bold = False
            builder.Write("本人参加" & Form1.sqlcon1.dataf.Item(0) &
                          "评审现场工作，现就廉洁保密作如下承诺：")
            sli.Clear()
            sli.Add("1", "一、严格遵守党的纪律、国家法律法规和公司各项规章制度，在评审期间及评审活动结束后严格遵守廉洁从业各项规定。")
            sli.Add("2", "1.不利用职权索取、接受应答人、供应商、中介人以及其他与行使职权有关的单位或者个人的财物或者其他好处；")
            sli.Add("3", "2.不接受应答人、供应商的礼品、宴请以及旅游、健身、娱乐等可能影响招投标等公正性的活动安排；")
            sli.Add("4", "3.不在招标采购有关工作中以各种形式谋取不正当利益；")
            sli.Add("5", "4.不以知悉或者掌握的内部信息谋取利益。")
            sli.Add("6", "二、严格遵守《中华人民共和国保密法》及公司有关保密规定。")
            sli.Add("7", "1.党员要自觉遵守党的纪律，严守党的秘密，充分发挥党员干部先锋模范带头作用，确保不发生失泄密事件；")
            sli.Add("8", "2.严格遵守公司各项保密制度，确保不发生失泄密事件，不发生有重大政治影响和重大经济损失的责任问题；")
            sli.Add("9", "3.评审现场中发现有泄密风险和泄密情况，及时汇报，主动制止，绝不隐瞒；")
            sli.Add("10", "4.评审会议结束后，不泄露与评审有关的应当保密的任何内容。")
            sli.Add("11", "三、要严格遵守评审现场管理规定。法律人员要严格依照国家招投标采购法律法规提供法律支撑服务，依法依规，恪守公平公正原则；代理机构业务人员要严格按照代理业务人员职责提供支撑服务，不得违规干预评审专家评审工作；技术支持人员要按照技术管理及审批要求进行系统维护等技术支持相关工作；会务人员要忠于职守，认真履行各项工作职责，保障会议正常和有序的进行。")
            sli.Add("12", "四、在评审期间自觉接受现场监督人员及所在党组织的监督，严格执行评审现场各项管理要求，不擅离职守，服从评审现场统一安排。")
            sli.Add("13", "五、若有违反上述承诺行为，本人愿承担相应责任或后果。")
            sli.Add("14", "六、本《承诺书》未尽事宜按国家有关法律法规和公司规定执行。")
            '填入内容
            For i = 1 To 14
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write(sli(CStr(i)).ToString)
            Next
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.Write("承诺人：                          日期：    年  月  日")
            builder.InsertBreak(BreakType.ParagraphBreak)
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_huibi(ByVal fp As String)
        Dim sli As New SortedList
        Dim i As Integer
        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        '设置1.5倍行距
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 18
        'sql获取char_gp数据
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono
        FROM overload
        WHERE overload.id = " & Form3.pro_id, 1)
        If Form1.sqlcon1.dataf.HasRows Then
            Form1.sqlcon1.dataf.Read()
            builder.Font.Name = "方正仿宋_GBK"
            builder.Font.Size = 16
            builder.Font.Bold = True
            builder.Write("评审专家回避承诺书")
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.ParagraphFormat.ClearFormatting()
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
            builder.ParagraphFormat.FirstLineIndent = 28
            '设置1.5倍行距
            builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
            builder.ParagraphFormat.LineSpacing = 18
            builder.Font.Size = 14
            builder.Font.Bold = False
            builder.Write("本人受邀请，担任" & Form1.sqlcon1.dataf.Item(0) &
                          "的评委。本人郑重承诺，有下列情形之一的，应主动申请回避，如有违反，同意取消本人评审专家资格，并承担相应的法律责任：")
            sli.Clear()
            sli.Add("1", "一、采购人或应答人主要负责人的近亲属；")
            sli.Add("2", "二、项目主管部门或监督管理部门的人员；")
            sli.Add("3", "三、与应答人有其他社会关系或者经济利益关系，可能影响对投标公正评审的；")
            sli.Add("4", "四、曾因在招标、评标以及其他与招标投标活动有关活动中从事违法行为而受过行政处罚或刑事处罚的；")
            sli.Add("5", "五、其他依法应当回避的人员。")
            sli.Add("6", "承诺人（签名）")
            For i = 1 To 6
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write(sli(CStr(i)).ToString)
            Next
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.ParagraphFormat.ClearFormatting()
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Right
            '设置1.5倍行距
            builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
            builder.ParagraphFormat.LineSpacing = 18
            builder.Font.Size = 14
            builder.Font.Bold = False
            builder.Write("日期：    年  月  日")
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_pingjia(ByVal fp As String)
        Dim sli As New SortedList
        Dim dr As DataRow
        Dim i As Integer

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        '第一部分
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono 
        FROM overload 
        WHERE overload.id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        '第一段
        builder.Font.Name = "方正仿宋_GBK"
        builder.Bold = True
        builder.Font.Size = 22
        builder.Write("评审专家履责情况评价表")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Font.Size = 14
        builder.Write("（专业水平及表达能力方面）")
        '第二段
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        builder.Bold = False
        builder.Font.Size = 14
        builder.Write("项目编号：" & Form1.sqlcon1.dataf.Item(1))
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Write("评标时间：" & Format(Now(), "yyyy年MM月dd日"))
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Write("项目名称：" & Form1.sqlcon1.dataf.Item(0))
        '第三段
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        '插入表格
        table = builder.StartTable()
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.RowFormat.Height = 25
        '第一行标题
        builder.InsertCell()
        builder.CellFormat.Width = 75
        builder.Write("组别")
        builder.InsertCell()
        builder.CellFormat.Width = 75
        builder.Write("角色")
        builder.InsertCell()
        builder.CellFormat.Width = 75
        builder.Write("姓名")
        builder.InsertCell()
        builder.CellFormat.Width = 165
        builder.Write("评价结论")
        builder.InsertCell()
        builder.CellFormat.Width = 75
        builder.Write("备注")
        builder.EndRow()
        'sql获取专家信息
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT expert_config.char_gp,expert_config.char_po,expert.expname
        FROM overload,expert_config,expert
        WHERE expert_config.oveid = " & Form3.pro_id & "
        AND expert_config.expid = expert.id
        AND expert_config.char_po = " & Chr(34) & "组长" & Chr(34), 1)
        If Form1.sqlcon1.dataf.HasRows Then
            i = 1
            While Form1.sqlcon1.dataf.Read()
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write(Form1.sqlcon1.dataf.Item(0).ToString)
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write(Form1.sqlcon1.dataf.Item(1).ToString)
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write(Form1.sqlcon1.dataf.Item(2).ToString)
                builder.InsertCell()
                builder.CellFormat.Width = 165
                builder.Write("")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("")
                builder.EndRow()
                i += 1
            End While
        End If
        table = builder.EndTable
        '设置表格格式
        table.Alignment = Tables.TableAlignment.Center
        table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
        '文档末尾
        builder.MoveToDocumentEnd()
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        sli.Clear()
        sli.Add("1", "评价人（监督人）签名：                         评价时间：")
        sli.Add("2", "评价规则：")
        sli.Add("3", "1.此表格适用于评标委员会对组长的评价；")
        sli.Add("4", "2.评价结论分为“"A、B、C"”三个等级：专业能力强且表达能力强评为“"A等"”；专业能力、表达能力一般评为“"B等"”；专业能力不足且表达不清评为“"C等"”。")
        sli.Add("5", "3.评价为“"A"”的组长人数不超过组长总人数的1/3；")
        sli.Add("6", "4.评价为“"C"”的组长，必须在备注栏说明具体情况。")
        For i = 1 To 6
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.Write(sli(CStr(i)).ToString)
        Next
        '设置其他页
        builder.InsertBreak(BreakType.SectionBreakNewPage)
        'sql获取expert数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT expert_config.char_gp
        FROM overload,expert_config,expert
        WHERE expert_config.oveid = " & Form3.pro_id & "
        AND (expert_config.char_gp LIKE " & Chr(34) & "%商务%" & Chr(34) & "
        OR expert_config.char_gp LIKE " & Chr(34) & "%技术%" & Chr(34) & ")
        ORDER BY expert_config.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            '第二部分
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                Form1.sqlcon1.sqlstr("
                SELECT overload.proname,overload.prono 
                FROM overload 
                WHERE overload.id = " & Form3.pro_id, 1)
                Form1.sqlcon1.dataf.Read()
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                '第一段
                builder.Font.Name = "方正仿宋_GBK"
                builder.Bold = True
                builder.Font.Size = 22
                builder.Write("评审专家履责情况评价表")
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Font.Size = 14
                builder.Write("（专业水平及表达能力方面）")
                '第二段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                builder.Bold = False
                builder.Font.Size = 14
                builder.Write("项目编号：" & Form1.sqlcon1.dataf.Item(1))
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("评标时间：" & Format(Now(), "yyyy年MM月dd日"))
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("项目名称：" & Form1.sqlcon1.dataf.Item(0))
                '第三段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                '插入表格
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.RowFormat.Height = 25
                '第一行标题
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("组别")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("角色")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("姓名")
                builder.InsertCell()
                builder.CellFormat.Width = 165
                builder.Write("评价结论")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("备注")
                builder.EndRow()
                'sql获取专家信息
                Form1.sqlcon1.sqlstr("
                SELECT expert_config.char_gp,expert_config.char_po,expert.expname
                FROM expert_config,expert
                WHERE expert_config.oveid = " & Form3.pro_id & "
                AND expert_config.expid = expert.id
                AND expert_config.char_gp = " & Chr(34) & dr.Item(0) & Chr(34) & "
                AND expert_config.char_po = " & Chr(34) & "组员" & Chr(34) & "
                ORDER BY expert_config.id", 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    i = 1
                    While Form1.sqlcon1.dataf.Read()
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write(Form1.sqlcon1.dataf.Item(0).ToString)
                        If i = 1 Then
                            builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        Else
                            builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                        End If
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write(Form1.sqlcon1.dataf.Item(1).ToString)
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write(Form1.sqlcon1.dataf.Item(2).ToString)
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.InsertCell()
                        builder.CellFormat.Width = 165
                        builder.Write("")
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write("")
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.EndRow()
                        i += 1
                    End While
                End If
                table = builder.EndTable
                '设置表格格式
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                '文档末尾
                builder.MoveToDocumentEnd()
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                sli.Clear()
                sli.Add("1", "评价人（组长）签名：                         评价时间：")
                sli.Add("2", "评价规则：")
                sli.Add("3", "1.此表格适用于组长对组员的评价；")
                sli.Add("4", "2.评价结论分为“"A、B、C"”三个等级：专业能力强且表达能力强评为“"A等"”；专业能力、表达能力一般评为“"B等"”；专业能力不足且表达不清评为“"C等"”。")
                sli.Add("5", "3.评价为“"A"”的组长人数不超过组长总人数的1/3；")
                sli.Add("6", "4.评价为“"C"”的组长，必须在备注栏说明具体情况。")
                For i = 1 To 6
                    builder.InsertBreak(BreakType.ParagraphBreak)
                    builder.Write(sli(CStr(i)).ToString)
                Next
                builder.InsertBreak(BreakType.SectionBreakNewPage)
            Next
            '第三部分
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                Form1.sqlcon1.sqlstr("
                SELECT overload.proname,overload.prono 
                FROM overload 
                WHERE overload.id = " & Form3.pro_id, 1)
                Form1.sqlcon1.dataf.Read()
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                '第一段
                builder.Font.Name = "方正仿宋_GBK"
                builder.Bold = True
                builder.Font.Size = 22
                builder.Write("评审专家履责情况评价表")
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Font.Size = 14
                builder.Write("（工作配合及任务完成方面）")
                '第二段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                builder.Bold = False
                builder.Font.Size = 14
                builder.Write("项目编号：" & Form1.sqlcon1.dataf.Item(1))
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("评标时间：" & Format(Now(), "yyyy年MM月dd日"))
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("项目名称：" & Form1.sqlcon1.dataf.Item(0))
                '第三段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                '插入表格
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.RowFormat.Height = 25
                '第一行标题
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("组别")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("角色")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("姓名")
                builder.InsertCell()
                builder.CellFormat.Width = 165
                builder.Write("评价结论")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("备注")
                builder.EndRow()
                'sql获取专家信息
                Form1.sqlcon1.sqlstr("
                SELECT expert_config.char_gp,expert_config.char_po,expert.expname
                FROM expert_config,expert
                WHERE expert_config.oveid = " & Form3.pro_id & "
                AND expert_config.expid = expert.id
                AND expert_config.char_gp = " & Chr(34) & dr.Item(0) & Chr(34) & "
                ORDER BY expert_config.id", 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    i = 1
                    While Form1.sqlcon1.dataf.Read()
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write(Form1.sqlcon1.dataf.Item(0).ToString)
                        If i = 1 Then
                            builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        Else
                            builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                        End If
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write(Form1.sqlcon1.dataf.Item(1).ToString)
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write(Form1.sqlcon1.dataf.Item(2).ToString)
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.InsertCell()
                        builder.CellFormat.Width = 165
                        builder.Write("")
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write("")
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.EndRow()
                        i += 1
                    End While
                End If
                table = builder.EndTable
                '设置表格格式
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                '文档末尾
                builder.MoveToDocumentEnd()
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                sli.Clear()
                sli.Add("1", "评价人（代理机构）签名：                         评价时间：")
                sli.Add("2", "评价规则：")
                sli.Add("3", "1.此表格适用于招标代理机构对评标组长、组员的评价；")
                sli.Add("4", "2.评价结论分为“"A、B、C"”三个等级：按时出勤且成果完成情况好的评为“"A等"”；按时出勤，成果一般的评为“"B等"”；出勤迟到或缺勤且工作完成敷衍了事的评为“"C等"”。")
                sli.Add("5", "3.评价为“"C"”的组长或组员，必须在备注栏说明具体情况。")
                For i = 1 To 5
                    builder.InsertBreak(BreakType.ParagraphBreak)
                    builder.Write(sli(CStr(i)).ToString)
                Next
                builder.InsertBreak(BreakType.SectionBreakNewPage)
            Next
            '第四部分
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                Form1.sqlcon1.sqlstr("
                SELECT overload.proname,overload.prono 
                FROM overload 
                WHERE overload.id = " & Form3.pro_id, 1)
                Form1.sqlcon1.dataf.Read()
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                '第一段
                builder.Font.Name = "方正仿宋_GBK"
                builder.Bold = True
                builder.Font.Size = 22
                builder.Write("评审专家履责情况评价表")
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Font.Size = 14
                builder.Write("（纪律遵守及评审意见方面）")
                '第二段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                builder.Bold = False
                builder.Font.Size = 14
                builder.Write("项目编号：" & Form1.sqlcon1.dataf.Item(1))
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("评标时间：" & Format(Now(), "yyyy年MM月dd日"))
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("项目名称：" & Form1.sqlcon1.dataf.Item(0))
                '第三段
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                '插入表格
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.RowFormat.Height = 25
                '第一行标题
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("组别")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("角色")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("姓名")
                builder.InsertCell()
                builder.CellFormat.Width = 165
                builder.Write("评价结论")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write("备注")
                builder.EndRow()
                'sql获取专家信息
                Form1.sqlcon1.sqlstr("
                SELECT expert_config.char_gp,expert_config.char_po,expert.expname
                FROM expert_config,expert
                WHERE expert_config.oveid = " & Form3.pro_id & "
                AND expert_config.expid = expert.id
                AND expert_config.char_gp = " & Chr(34) & dr.Item(0) & Chr(34) & "
                ORDER BY expert_config.id", 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    i = 1
                    While Form1.sqlcon1.dataf.Read()
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write(Form1.sqlcon1.dataf.Item(0).ToString)
                        If i = 1 Then
                            builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        Else
                            builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                        End If
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write(Form1.sqlcon1.dataf.Item(1).ToString)
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write(Form1.sqlcon1.dataf.Item(2).ToString)
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.InsertCell()
                        builder.CellFormat.Width = 165
                        builder.Write("")
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.Write("")
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.EndRow()
                        i += 1
                    End While
                End If
                table = builder.EndTable
                '设置表格格式
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                '文档末尾
                builder.MoveToDocumentEnd()
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                sli.Clear()
                sli.Add("1", "评价人（监督人）签名：                         评价时间：")
                sli.Add("2", "评价规则：")
                sli.Add("3", "1.此表格适用于监督人员对评标组长、组员的评价；")
                sli.Add("4", "2.评价结论分为“"A、B、C"”三个等级：遵守通讯工具上交、封闭等纪律要求，意见表达时无个人倾向性意见，评为“"A等"”；遵守通讯工具上交、封闭等纪律要求，意见表达时有一定个人倾向性意见，经提醒加以修正评为“"B等"”；未遵守通讯工具上交、封闭等纪律要求，或意见表达时有一定个人倾向性意见，经提醒不加修正，评为“"C等"”。")
                sli.Add("5", "3.评价为“"C"”的组长或组员，必须在备注栏说明具体情况。")
                For i = 1 To 5
                    builder.InsertBreak(BreakType.ParagraphBreak)
                    builder.Write(sli(CStr(i)).ToString)
                Next
                builder.InsertBreak(BreakType.SectionBreakNewPage)
            Next
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_zhongzhi(ByVal fp As String)
        Dim str As String

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        'sql获取repeal数据
        Form1.sqlcon1.sqlstr("
        SELECT 
        overload.proname,overload.prono,owner.owname,bid.bidname,package.pkgidx,
        factory.facname,repeal.repqust,repeal.repreal
        FROM 
        overload,owner,bid,package,factory,repeal_config,repeal
        WHERE overload.id = " & Form3.pro_id & "
        AND overload.owid = owner.id 
        AND bid.oveid = overload.id 
        AND package.bidid = bid.id 
        AND repeal_config.pkgid = package.id 
        AND repeal_config.repid = repeal.id
        AND repeal_config.facid = factory.id 
        ORDER BY factory.facname,bid.id,package.id", 1)
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                '设置段落格式
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                '标题
                builder.Font.Name = "方正仿宋_GBK"
                builder.Font.Size = 18
                builder.Font.Bold = True
                builder.Write("终止谈判通知单")
                builder.InsertBreak(BreakType.ParagraphBreak)
                '内容
                builder.Font.Size = 14
                builder.Font.Bold = False
                str = "分标名称：" & Form1.sqlcon1.dataf.Item(3)
                For i = 1 To 30 - Len(Form1.sqlcon1.dataf.Item(3)) - Len(Form1.sqlcon1.dataf.Item(4))
                    str += " "
                Next
                str += "包号：" & Form1.sqlcon1.dataf.Item(4)
                builder.Write(str)
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 12
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.RowFormat.Height = 35
                '插入行
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("项目名称")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write(Form1.sqlcon1.dataf.Item(0))
                builder.EndRow()
                '插入行
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("分标编号")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write(Form1.sqlcon1.dataf.Item(1))
                builder.EndRow()
                '插入行
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("采购人")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write(Form1.sqlcon1.dataf.Item(2))
                builder.EndRow()
                '插入行
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("应答人")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write(Form1.sqlcon1.dataf.Item(5))
                builder.EndRow()
                '插入行
                builder.RowFormat.Height = 150
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("终止原因")
                builder.Font.Size = 10.5
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write(Form1.sqlcon1.dataf.Item(6) &
                                vbCrLf & Form1.sqlcon1.dataf.Item(7))
                builder.EndRow()
                '插入行
                builder.RowFormat.Height = 55
                builder.Font.Size = 14
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("应答人法定代表人或被授权人签字")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write("")
                builder.EndRow()
                '插入行
                builder.RowFormat.Height = 55
                builder.Font.Size = 14
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("法定代表人或被授权人身份证号")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write("")
                builder.EndRow()
                '插入行
                builder.RowFormat.Height = 55
                builder.Font.Size = 14
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("法定代表人或被授权人联系电话")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write("")
                builder.EndRow()
                '设置表格格式
                table = builder.EndTable
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                '结尾
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                builder.Write("发出日期及时间：" &
                                Format(Now(), "yyyy年MM月dd日") &
                                "   时")
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("回复日期及时间：" &
                                Format(Now(), "yyyy年MM月dd日") &
                                "   时")
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("采购代理机构：山东诚信工程建设监理有限公司")
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("评审专家：               监督人：")
                builder.InsertBreak(BreakType.SectionBreakNewPage)
            End While
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")

        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_zhongzhi_ydr(ByVal fp As String, ByVal repconid As Integer, ByVal fileidx As Integer)
        Dim str As String

        '定义新实例
        doc = New Document
        builder = New DocumentBuilder(doc)
        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        'sql获取repeal数据
        Form1.sqlcon1.sqlstr("
        SELECT 
        overload.proname,overload.prono,owner.owname,bid.bidname,package.pkgidx,
        factory.facname,repeal.repqust,repeal.repreal
        FROM 
        overload,owner,bid,package,factory,repeal_config,repeal
        WHERE overload.id = " & Form3.pro_id & "
        AND repeal_config.id = " & repconid & "
        AND overload.owid = owner.id 
        AND bid.oveid = overload.id 
        AND package.bidid = bid.id 
        AND repeal_config.pkgid = package.id 
        AND repeal_config.repid = repeal.id
        AND repeal_config.facid = factory.id 
        ORDER BY factory.facname,bid.id,package.id", 1)
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                '设置段落格式
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                '标题
                builder.Font.Name = "方正仿宋_GBK"
                builder.Font.Size = 18
                builder.Font.Bold = True
                builder.Write("终止谈判通知单")
                builder.InsertBreak(BreakType.ParagraphBreak)
                '内容
                builder.Font.Size = 14
                builder.Font.Bold = False
                str = "分标名称：" & Form1.sqlcon1.dataf.Item(3)
                For i = 1 To 30 - Len(Form1.sqlcon1.dataf.Item(3)) - Len(Form1.sqlcon1.dataf.Item(4))
                    str += " "
                Next
                str += "包号：" & Form1.sqlcon1.dataf.Item(4)
                builder.Write(str)
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 12
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.RowFormat.Height = 35
                '插入行
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("项目名称")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write(Form1.sqlcon1.dataf.Item(0))
                builder.EndRow()
                '插入行
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("分标编号")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write(Form1.sqlcon1.dataf.Item(1))
                builder.EndRow()
                '插入行
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("采购人")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write(Form1.sqlcon1.dataf.Item(2))
                builder.EndRow()
                '插入行
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("应答人")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write(Form1.sqlcon1.dataf.Item(5))
                builder.EndRow()
                '插入行
                builder.RowFormat.Height = 150
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("终止原因")
                builder.Font.Size = 10.5
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write(Form1.sqlcon1.dataf.Item(6) &
                                vbCrLf & Form1.sqlcon1.dataf.Item(7))
                builder.EndRow()
                '插入行
                builder.RowFormat.Height = 55
                builder.Font.Size = 14
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("应答人法定代表人或被授权人签字")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write("")
                builder.EndRow()
                '插入行
                builder.RowFormat.Height = 55
                builder.Font.Size = 14
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("法定代表人或被授权人身份证号")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write("")
                builder.EndRow()
                '插入行
                builder.RowFormat.Height = 55
                builder.Font.Size = 14
                builder.InsertCell()
                builder.CellFormat.Width = 130
                builder.Write("法定代表人或被授权人联系电话")
                builder.InsertCell()
                builder.CellFormat.Width = 320
                builder.Write("")
                builder.EndRow()
                '设置表格格式
                table = builder.EndTable
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                '结尾
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 15
                builder.Write("发出日期及时间：" &
                                Format(Now(), "yyyy年MM月dd日") &
                                "   时")
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("回复日期及时间：" &
                                Format(Now(), "yyyy年MM月dd日") &
                                "   时")
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("采购代理机构：山东诚信工程建设监理有限公司")
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Write("评审专家：               监督人：")
                '保存文件
                Try
                    '定义文件命名规则
                    str = fileidx.ToString & "、" &
                    Form1.sqlcon1.dataf.Item(5) & "-" &
                    Form1.sqlcon1.dataf.Item(3) & "-" &
                    Form1.sqlcon1.dataf.Item(4)
                    str = Left(str, 200)
                    doc.Save(fp & "\" & str & ".docx")
                Catch ex As Exception
                    MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
                    Exit Sub
                End Try
            End While
        End If


    End Sub

    Public Sub dc_qianziye(ByVal fp As String)

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 18
        '标题
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Bold = True
        builder.Font.Size = 18
        builder.Write("评审报告签字页")
        '获取overload表
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono
        FROM overload
        WHERE overload.id =" & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        '第二段
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 18
        builder.Font.Bold = False
        builder.Font.Size = 10.5
        builder.Write("项目名称：" & Form1.sqlcon1.dataf.Item(0))
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Write("采购编号：" & Form1.sqlcon1.dataf.Item(1))
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Write("评审委员会")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Write("主任：")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Write("成员：")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Write("评审专家组")
        builder.InsertBreak(BreakType.ParagraphBreak)
        '获取epxert_config表
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT expert_config.char_gp
        FROM expert_config
        WHERE expert_config.oveid =" & Form3.pro_id & "
        AND (expert_config.char_gp LIKE " & Chr(34) & "%商务%" & Chr(34) & "
        OR expert_config.char_gp LIKE " & Chr(34) & "%技术%" & Chr(34) & ")
        ORDER BY expert_config.id", 1)
        '第三段
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                builder.Write(Form1.sqlcon1.dataf.Item(0) & "：")
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.InsertBreak(BreakType.ParagraphBreak)
            End While
        End If
        builder.Write("法律顾问：")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Write("监督人：")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 18
        builder.Write("日期：" & Format(Now(), " yyyy年MM月dd日"))
        '保存文件
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_baogao(ByVal fp As String)
        Dim str As String = ""
        Dim abortstr As String
        Dim i, sortmax As Integer
        Dim dr As DataRow

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        'sql获取bid,package表数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT 
        bid.bididx,bid.bidname,package.pkgidx,package.pkgname,bid.id,package.id
        FROM bid,package
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.id = package.bidid 
        ORDER BY bid.id,package.id ", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                '设置标题格式
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                '填写分标号分标名
                If str <> dr.Item(1) Then
                    str = dr.Item(1)
                    builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                    builder.ParagraphFormat.LineSpacing = 18
                    builder.InsertBreak(BreakType.ParagraphBreak)
                    builder.Font.Bold = False
                    builder.Font.Name = "方正仿宋_GBK"
                    builder.Font.Size = 12
                    builder.Writeln(dr.Item(0) & " " & dr.Item(1))
                End If
                '填写分包号分包名
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 12
                builder.Font.Bold = True
                builder.Font.Size = 10.5
                builder.Write(dr.Item(2) & " " & dr.Item(3))
                builder.InsertBreak(BreakType.ParagraphBreak)
                '设置格式
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 12
                '按包填写排序表
                Form1.sqlcon1.sqlstr("
                SELECT package_config.char_ps 
                FROM package_config 
                WHERE package_config.pkgid = " & dr.Item(5), 1)
                Form1.sqlcon1.dataf.Read()
                If Form1.sqlcon1.dataf.Item(0) = "最低价法" Then
                    '设置表格式
                    builder.Font.Bold = True
                    table = builder.StartTable()
                    builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                    builder.RowFormat.Height = 25
                    '插入标题行
                    builder.InsertCell()
                    builder.CellFormat.Width = 35
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write("排序")
                    builder.InsertCell()
                    builder.CellFormat.Width = 200
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write("应答人")
                    builder.InsertCell()
                    builder.CellFormat.Width = 185
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write("应答总价" & vbCrLf & "（万元）")
                    builder.InsertCell()
                    builder.CellFormat.Width = 80
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                    builder.Write("推荐的成交人")
                    builder.EndRow()
                    '填写表数据
                    builder.Font.Bold = False
                    'sql获取package_abort表数据
                    Form1.sqlcon1.sqlstr("
                    SELECT package_abort.aboid 
                    FROM package_abort 
                    WHERE package_abort.pkgid = " & dr.Item(5), 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        Form1.sqlcon1.dataf.Read()
                        If Form1.sqlcon1.dataf.Item(0) < 2 Then
                            abortstr = "递交应答文件不足两家，此包流包"
                        Else
                            abortstr = "进入详评应答人不足两家，此包流包"
                        End If
                        For i = 1 To 3
                            builder.InsertCell()
                            builder.CellFormat.Width = 35
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                            builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(i)
                            builder.InsertCell()
                            builder.CellFormat.Width = 200
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                            builder.Write(abortstr)
                            builder.InsertCell()
                            builder.CellFormat.Width = 185
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(abortstr)
                            builder.InsertCell()
                            builder.CellFormat.Width = 80
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                            builder.Write(abortstr)
                            builder.EndRow()
                        Next
                        '设置表格格式
                        table = builder.EndTable
                        table.Alignment = Tables.TableAlignment.Center
                        table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                        builder.InsertBreak(BreakType.ParagraphBreak)
                        Continue For
                    End If
                    'sql获取sortmax
                    Form1.sqlcon1.sqlstr("
                    SELECT MAX(sorting_price.sort) 
                    FROM sorting_price 
                    WHERE sorting_price.pkgid = " & dr.Item(5), 1)
                    Form1.sqlcon1.dataf.Read()
                    sortmax = Form1.sqlcon1.dataf.Item(0)
                    'sql获取sorting表数据
                    Form1.sqlcon1.sqlstr("
                    SELECT 
                    factory.facname,sorting_price.char_bj,sorting_price.sort
                    FROM factory,sorting_price
                    WHERE factory.id = sorting_price.facid
                    AND sorting_price.pkgid = " & dr.Item(5) & "
                    ORDER BY sorting_price.sort", 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        i = 1
                        While Form1.sqlcon1.dataf.Read()
                            builder.InsertCell()
                            builder.CellFormat.Width = 35
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                            builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(i)
                            builder.InsertCell()
                            builder.CellFormat.Width = 200
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                            builder.Write(Form1.sqlcon1.dataf.Item(0))
                            builder.InsertCell()
                            builder.CellFormat.Width = 185
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(Form1.sqlcon1.dataf.Item(1))
                            builder.InsertCell()
                            builder.CellFormat.Width = 80
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                            If i = 1 Then
                                builder.Write(Form1.sqlcon1.dataf.Item(0))
                            Else
                                builder.Write("")
                            End If
                            builder.EndRow()
                            i += 1
                        End While
                    End If
                    '设置表格格式
                    table = builder.EndTable
                    table.Alignment = Tables.TableAlignment.Center
                    table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                    builder.InsertBreak(BreakType.ParagraphBreak)
                Else
                    '设置表格式
                    builder.Font.Bold = True
                    table = builder.StartTable()
                    builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                    builder.RowFormat.Height = 25
                    '插入标题行
                    builder.InsertCell()
                    builder.CellFormat.Width = 35
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write("排序")
                    builder.InsertCell()
                    builder.CellFormat.Width = 105
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write("应答人")
                    builder.InsertCell()
                    builder.CellFormat.Width = 60
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write("应答总价" & vbCrLf & "（万元）")
                    builder.InsertCell()
                    builder.CellFormat.Width = 55
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write("技术权分")
                    builder.InsertCell()
                    builder.CellFormat.Width = 55
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write("商务权分")
                    builder.InsertCell()
                    builder.CellFormat.Width = 55
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write("价格权分")
                    builder.InsertCell()
                    builder.CellFormat.Width = 55
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write("总分")
                    builder.InsertCell()
                    builder.CellFormat.Width = 80
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                    builder.Write("推荐的成交人")
                    builder.EndRow()
                    '填写表数据
                    builder.Font.Bold = False
                    'sql获取package_abort表数据
                    Form1.sqlcon1.sqlstr("
                    SELECT package_abort.aboid 
                    FROM package_abort 
                    WHERE package_abort.pkgid = " & dr.Item(5), 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        Form1.sqlcon1.dataf.Read()
                        If Form1.sqlcon1.dataf.Item(0) < 2 Then
                            abortstr = "递交应答文件不足两家，此包流包"
                        Else
                            abortstr = "进入详评应答人不足两家，此包流包"
                        End If
                        For i = 1 To 3
                            builder.InsertCell()
                            builder.CellFormat.Width = 35
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                            builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(i)
                            builder.InsertCell()
                            builder.CellFormat.Width = 105
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                            builder.Write(abortstr)
                            builder.InsertCell()
                            builder.CellFormat.Width = 60
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(abortstr)
                            builder.InsertCell()
                            builder.CellFormat.Width = 55
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(abortstr)
                            builder.InsertCell()
                            builder.CellFormat.Width = 55
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(abortstr)
                            builder.InsertCell()
                            builder.CellFormat.Width = 55
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(abortstr)
                            builder.InsertCell()
                            builder.CellFormat.Width = 55
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(abortstr)
                            builder.InsertCell()
                            builder.CellFormat.Width = 80
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = 3 Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                            builder.Write(abortstr)
                            builder.EndRow()
                        Next
                        '设置表格格式
                        table = builder.EndTable
                        table.Alignment = Tables.TableAlignment.Center
                        table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                        builder.InsertBreak(BreakType.ParagraphBreak)
                        Continue For
                    End If
                    'sql获取sortmax
                    Form1.sqlcon1.sqlstr("
                    SELECT MAX(sorting.sort) 
                    FROM sorting 
                    WHERE sorting.pkgid = " & dr.Item(5), 1)
                    Form1.sqlcon1.dataf.Read()
                    sortmax = Form1.sqlcon1.dataf.Item(0)
                    'sql获取sorting表数据
                    Form1.sqlcon1.sqlstr("
                    SELECT 
                    factory.facname,sorting_price.char_bj,
                    sorting.char_js,sorting.char_sw,
                    sorting.char_jg,sorting.char_zf,sorting.sort
                    FROM factory,sorting,sorting_price
                    WHERE factory.id = sorting.facid
                    AND sorting_price.facid = sorting.facid
                    AND sorting_price.pkgid = sorting.pkgid
                    AND sorting.pkgid = " & dr.Item(5) & "
                    ORDER BY sorting.sort", 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        i = 1
                        While Form1.sqlcon1.dataf.Read()
                            builder.InsertCell()
                            builder.CellFormat.Width = 35
                            builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
                            builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(i)
                            builder.InsertCell()
                            builder.CellFormat.Width = 105
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                            builder.Write(Form1.sqlcon1.dataf.Item(0))
                            builder.InsertCell()
                            builder.CellFormat.Width = 60
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(Form1.sqlcon1.dataf.Item(1))
                            builder.InsertCell()
                            builder.CellFormat.Width = 55
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(Form1.sqlcon1.dataf.Item(2))
                            builder.InsertCell()
                            builder.CellFormat.Width = 55
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(Form1.sqlcon1.dataf.Item(3))
                            builder.InsertCell()
                            builder.CellFormat.Width = 55
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(Form1.sqlcon1.dataf.Item(4))
                            builder.InsertCell()
                            builder.CellFormat.Width = 55
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                            builder.Write(Form1.sqlcon1.dataf.Item(5))
                            builder.InsertCell()
                            builder.CellFormat.Width = 80
                            If i = 1 Then
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                            Else
                                builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                            End If
                            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                            If i = sortmax Then
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                            Else
                                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                            End If
                            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                            If i = 1 Then
                                builder.Write(Form1.sqlcon1.dataf.Item(0))
                            Else
                                builder.Write("")
                            End If
                            builder.EndRow()
                            i += 1
                        End While
                    End If
                    '设置表格格式
                    table = builder.EndTable
                    table.Alignment = Tables.TableAlignment.Center
                    table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                    builder.InsertBreak(BreakType.ParagraphBreak)
                End If
            Next
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")

        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_baogaozj(ByVal fp As String)
        Dim str As String = ""
        Dim sortmax, idx As Integer
        Dim dr As DataRow

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        'sql获取bid,package表数据
        Form1.sqlcon1.sqlstr("
        SELECT
        expert.expname,expert.expadds,expert.expprof,expert.expiden,
        expert_config.char_gp,expert_config.char_po
        FROM expert,expert_config
        WHERE expert_config.oveid = " & Form3.pro_id & "
        AND expert_config.expid = expert.id
        ORDER BY expert_config.id,expert_config.char_gp", 2)
        sortmax = Form1.sqlcon1.datas.Tables(0).Rows.Count
        If sortmax > 0 Then
            idx = 1
            '设置标题格式
            builder.ParagraphFormat.ClearFormatting()
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
            builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
            builder.ParagraphFormat.LineSpacing = 12
            '设置表格式
            builder.Font.Bold = True
            builder.Font.Name = "方正仿宋_GBK"
            builder.Font.Size = 10.5
            table = builder.StartTable()
            builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
            builder.RowFormat.Height = 25
            '插入标题行
            builder.RowFormat.HeadingFormat = True
            builder.InsertCell()
            builder.CellFormat.Width = 35
            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
            builder.Write("序号")
            builder.InsertCell()
            builder.CellFormat.Width = 65
            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
            builder.Write("专家姓名")
            builder.InsertCell()
            builder.CellFormat.Width = 100
            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
            builder.Write("工作单位")
            builder.InsertCell()
            builder.CellFormat.Width = 65
            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
            builder.Write("现从事专业")
            builder.InsertCell()
            builder.CellFormat.Width = 120
            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
            builder.Write("证件号码")
            builder.InsertCell()
            builder.CellFormat.Width = 65
            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Double
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
            builder.Write("评委岗位")
            builder.EndRow()
            builder.RowFormat.HeadingFormat = False
            builder.Font.Bold = False
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                '填写分标号分标名
                If str <> dr.Item(4) Then
                    str = dr.Item(4)
                    '插入组别行
                    builder.InsertCell()
                    builder.CellFormat.Width = 35
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
                    builder.Write(str)
                    builder.InsertCell()
                    builder.CellFormat.Width = 65
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                    builder.InsertCell()
                    builder.CellFormat.Width = 100
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                    builder.InsertCell()
                    builder.CellFormat.Width = 65
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                    builder.InsertCell()
                    builder.CellFormat.Width = 120
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                    builder.InsertCell()
                    builder.CellFormat.Width = 65
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                    builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
                    builder.EndRow()
                    '插入数据行
                    builder.InsertCell()
                    builder.CellFormat.Width = 35
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx = sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write(idx.ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 65
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx = sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write(dr.Item(0).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 100
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx = sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write(dr.Item(1).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 65
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx = sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write(dr.Item(2).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 120
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx >= sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write(dr.Item(3).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 65
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx = sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                    builder.Write(dr.Item(5).ToString)
                    builder.EndRow()
                Else
                    '插入数据行
                    builder.InsertCell()
                    builder.CellFormat.Width = 35
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx = sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write(idx.ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 65
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx = sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write(dr.Item(0).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 100
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx = sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write(dr.Item(1).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 65
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx = sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write(dr.Item(2).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 120
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx >= sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                    builder.Write(dr.Item(3).ToString)
                    builder.InsertCell()
                    builder.CellFormat.Width = 65
                    builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                    If idx = sortmax Then
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Double
                    Else
                        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                    End If
                    builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                    builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
                    builder.Write(dr.Item(5).ToString)
                    builder.EndRow()
                End If
                idx += 1
            Next
            '设置表格格式
            table = builder.EndTable
            table.Alignment = Tables.TableAlignment.Center
            table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
            builder.InsertBreak(BreakType.ParagraphBreak)
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_shenpi(ByVal fp As String)
        Dim i, j As Integer
        Dim dr As DataRow
        Dim sli As New SortedList

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        '填写标题
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 18
        builder.Font.Bold = False
        builder.Font.Name = "方正小标宋_GBK"
        builder.Font.Size = 22
        'sql获取overload表数据
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname 
        FROM overload 
        WHERE overload.id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        builder.Writeln(Form1.sqlcon1.dataf.Item(0) & "审批单")
        'sql获取bid表数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT bid.id 
        FROM bid 
        WHERE bid.oveid = " & Form3.pro_id & "
        ORDER BY bid.id", 1)
        i = 1
        While Form1.sqlcon1.dataf.Read()
            sli.Add(i.ToString, Form1.sqlcon1.dataf.Item(0).ToString)
            i += 1
        End While
        '按分标填写数据
        For j = 1 To i - 1
            '填写分标名
            Form1.sqlcon1.sqlstr("
            SELECT DISTINCT bid.bididx,bid.bidname 
            FROM bid 
            WHERE bid.id = " & CInt(sli(j.ToString)), 1)
            Form1.sqlcon1.dataf.Read()
            builder.ParagraphFormat.ClearFormatting()
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
            builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
            builder.ParagraphFormat.LineSpacing = 12
            builder.Font.Bold = False
            builder.Font.Name = "方正小标宋_GBK"
            builder.Font.Size = 16
            builder.InsertBreak(BreakType.ParagraphBreak)
            builder.Write(j.ToString & "、" & Form1.sqlcon1.dataf.Item(0) &
                          " " & Form1.sqlcon1.dataf.Item(1))
            builder.InsertBreak(BreakType.ParagraphBreak)
            '设置表格式
            builder.ParagraphFormat.ClearFormatting()
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
            builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
            builder.ParagraphFormat.LineSpacing = 12
            builder.Font.Bold = True
            builder.Font.Name = "方正仿宋_GBK"
            builder.Font.Size = 11
            '填写分标表数据
            Form1.sqlcon1.sqlstr("
            SELECT DISTINCT 
            bid.bidno,package.pkgidx,package.pkgname,package.id
            FROM bid,package
            WHERE bid.id = " & CInt(sli(j.ToString)) & "
            AND package.bidid = bid.id", 2)
            If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.RowFormat.Height = 10
                '插入第一行
                builder.InsertCell()
                builder.CellFormat.Width = 550
                builder.Write("招投标领导小组审批意见：")
                builder.EndRow()
                '插入标题行
                builder.RowFormat.Height = 25
                builder.InsertCell()
                builder.CellFormat.Width = 120
                builder.Write("分标编号(包号)")
                builder.InsertCell()
                builder.CellFormat.Width = 80
                builder.Write("项目单位")
                builder.InsertCell()
                builder.CellFormat.Width = 160
                builder.Write("项目名称")
                builder.InsertCell()
                builder.CellFormat.Width = 110
                builder.Write("成交候选人")
                builder.InsertCell()
                builder.CellFormat.Width = 80
                builder.Write("成交价格" & vbCrLf & "(万元)")
                builder.EndRow()
                builder.Font.Bold = False
                For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                    builder.RowFormat.Height = 35
                    builder.InsertCell()
                    builder.CellFormat.Width = 120
                    builder.Write(dr.Item(0) & "(" &
                                  dr.Item(1) & ")")
                    builder.InsertCell()
                    builder.CellFormat.Width = 80
                    'sql获取engineering.engidx表信息
                    Form1.sqlcon1.sqlstr("
                    SELECT *,count(tb1.engidx)
                    FROM 
                    (SELECT DISTINCT engineering.engidx
                    FROM package,engineering
                    WHERE package.id = " & dr.Item(3) & "
                    AND engineering.pkgid = package.id) AS tb1", 1)
                    Form1.sqlcon1.dataf.Read()
                    If Form1.sqlcon1.dataf.Item(1) < 2 Then
                        builder.Write(Form1.sqlcon1.dataf.Item(0))
                    Else
                        builder.Write(Form1.sqlcon1.dataf.Item(0) & "等" &
                                      Form1.sqlcon1.dataf.Item(1) & "个项目单位")
                    End If
                    builder.InsertCell()
                    builder.CellFormat.Width = 160
                    builder.Write(dr.Item(2))
                    builder.InsertCell()
                    builder.CellFormat.Width = 110
                    '获取package_abort表数据
                    Form1.sqlcon1.sqlstr("
                    SELECT package_abort.aboid 
                    FROM package_abort
                    WHERE package_abort.pkgid = " & dr.Item(3), 1)
                    If Form1.sqlcon1.dataf.HasRows Then
                        Form1.sqlcon1.dataf.Read()
                        builder.Write("流包")
                        builder.InsertCell()
                        builder.CellFormat.Width = 80
                        builder.Write("")
                        builder.EndRow()
                    Else
                        Form1.sqlcon1.sqlstr("
                        SELECT package_config.char_ps 
                        FROM package_config 
                        WHERE package_config.pkgid = " & dr.Item(3), 1)
                        Form1.sqlcon1.dataf.Read()
                        If Form1.sqlcon1.dataf.Item(0) = "最低价法" Then
                            'sql获取sorting_price表数据
                            Form1.sqlcon1.sqlstr("
                            SELECT factory.facname,sorting_price.char_bj 
                            FROM factory,sorting_price 
                            WHERE sorting_price.facid = factory.id 
                            AND sorting_price.sort = 1 
                            AND sorting_price.pkgid = " & dr.Item(3), 1)
                            Form1.sqlcon1.dataf.Read()
                            builder.Write(Form1.sqlcon1.dataf.Item(0))
                            builder.InsertCell()
                            builder.CellFormat.Width = 80
                            builder.Write(Form1.sqlcon1.dataf.Item(1))
                            builder.EndRow()
                        Else
                            'sql获取sorting表数据
                            Form1.sqlcon1.sqlstr("
                            SELECT factory.facname,sorting_price.char_bj
                            FROM factory,sorting,sorting_price 
                            WHERE sorting.facid = factory.id 
                            AND sorting.pkgid = sorting_price.pkgid
                            AND sorting.facid = sorting_price.facid
                            AND sorting.sort = 1
                            AND sorting.pkgid = " & dr.Item(3), 1)
                            Form1.sqlcon1.dataf.Read()
                            builder.Write(Form1.sqlcon1.dataf.Item(0))
                            builder.InsertCell()
                            builder.CellFormat.Width = 80
                            builder.Write(Form1.sqlcon1.dataf.Item(1))
                            builder.EndRow()
                        End If
                    End If
                Next
                '设置表格格式
                table = builder.EndTable
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
            End If
        Next
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")

        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_ldshenpi(ByVal fp As String)
        Dim i As Integer
        Dim str As String = ""
        Dim dr As DataRow
        Dim sli As New SortedList

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        '填写标题
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 12
        builder.Font.Bold = False
        builder.Font.Name = "方正小标宋_GBK"
        builder.Font.Size = 18
        'sql获取overload表数据
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname 
        FROM overload 
        WHERE overload.id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        builder.Writeln(Form1.sqlcon1.dataf.Item(0) & "采购领导小组定标审批单")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Font.Bold = False
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 15
        builder.Writeln("授权采购项目评审结果")
        builder.Font.Bold = True
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 10.5
        '表标题
        table = builder.StartTable()
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.RowFormat.Height = 25
        '插入第一行
        builder.InsertCell()
        builder.CellFormat.Width = 35
        builder.Write("序号")
        builder.InsertCell()
        builder.CellFormat.Width = 100
        builder.Write("项目单位")
        builder.InsertCell()
        builder.CellFormat.Width = 150
        builder.Write("项目名称")
        builder.InsertCell()
        builder.CellFormat.Width = 120
        builder.Write("成交人")
        builder.InsertCell()
        builder.CellFormat.Width = 90
        builder.Write("成交价格" & vbCrLf & "（万元）")
        builder.EndRow()
        'sql获取bid表数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT 
        bid.bididx,bid.bidname,package.pkgidx,package.pkgname,
        package_config.char_ps,bid.id,package.id 
        FROM bid,package,package_config 
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.id = package.bidid
        AND package.id = package_config.pkgid 
        ORDER BY bid.id,package.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            i = 1
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                If dr.Item(1) <> str Then
                    builder.Font.Bold = True
                    builder.Font.Name = "方正仿宋_GBK"
                    builder.Font.Size = 10.5
                    builder.RowFormat.Height = 25
                    builder.InsertCell()
                    builder.CellFormat.Width = 495
                    builder.Write(dr.Item(0) & " " & dr.Item(1))
                    builder.EndRow()
                    str = dr.Item(1)
                End If
                builder.Font.Bold = False
                builder.Font.Name = "方正仿宋_GBK"
                builder.Font.Size = 10.5
                builder.RowFormat.Height = 40
                builder.InsertCell()
                builder.CellFormat.Width = 35
                builder.Write(i)
                i += 1
                builder.InsertCell()
                builder.CellFormat.Width = 100
                'sql获取engineering.engidx表数据
                Form1.sqlcon1.sqlstr("
                SELECT *,count(tb1.engidx)
                FROM 
                (SELECT DISTINCT engineering.engidx
                FROM package,engineering
                WHERE package.id = " & dr.Item(6) & "
                AND engineering.pkgid = package.id) AS tb1", 1)
                Form1.sqlcon1.dataf.Read()
                If Form1.sqlcon1.dataf.Item(1) < 2 Then
                    builder.Write(Form1.sqlcon1.dataf.Item(0))
                Else
                    builder.Write(Form1.sqlcon1.dataf.Item(0) & "等" &
                                  Form1.sqlcon1.dataf.Item(1) & "个项目单位")
                End If
                builder.InsertCell()
                builder.CellFormat.Width = 150
                builder.Write(dr.Item(3))
                builder.InsertCell()
                builder.CellFormat.Width = 120
                '获取package_abort表数据
                Form1.sqlcon1.sqlstr("
                SELECT package_abort.aboid 
                FROM package_abort
                WHERE package_abort.pkgid = " & dr.Item(6), 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    builder.Write("流包")
                    builder.InsertCell()
                    builder.CellFormat.Width = 90
                    builder.Write("")
                    builder.EndRow()
                Else
                    Form1.sqlcon1.sqlstr("
                    SELECT package_config.char_ps 
                    FROM package_config 
                    WHERE package_config.pkgid = " & dr.Item(6), 1)
                    Form1.sqlcon1.dataf.Read()
                    If Form1.sqlcon1.dataf.Item(0) = "最低价法" Then
                        'sql获取sorting_price表数据
                        Form1.sqlcon1.sqlstr("
                        SELECT factory.facname,sorting_price.char_bj 
                        FROM factory,sorting_price 
                        WHERE sorting_price.facid = factory.id 
                        AND sorting_price.sort = 1 
                        AND sorting_price.pkgid = " & dr.Item(6), 1)
                        Form1.sqlcon1.dataf.Read()
                        builder.Write(Form1.sqlcon1.dataf.Item(0))
                        builder.InsertCell()
                        builder.CellFormat.Width = 90
                        builder.Write(Form1.sqlcon1.dataf.Item(1))
                        builder.EndRow()
                    Else
                        'sql获取sorting表数据
                        Form1.sqlcon1.sqlstr("
                        SELECT factory.facname,sorting_price.char_bj
                        FROM factory,sorting,sorting_price 
                        WHERE sorting.facid = factory.id 
                        AND sorting.pkgid = sorting_price.pkgid
                        AND sorting.facid = sorting_price.facid
                        AND sorting.sort = 1
                        AND sorting.pkgid = " & dr.Item(6), 1)
                        Form1.sqlcon1.dataf.Read()
                        builder.Write(Form1.sqlcon1.dataf.Item(0))
                        builder.InsertCell()
                        builder.CellFormat.Width = 90
                        builder.Write(Form1.sqlcon1.dataf.Item(1))
                        builder.EndRow()
                    End If
                End If
            Next
        End If
        '设置表格格式
        table = builder.EndTable
        table.Alignment = Tables.TableAlignment.Center
        table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 12
        builder.Font.Bold = False
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 15
        builder.Write("    " & Format(Now(), " yyyy年MM月dd日") &
                        "，国网山东省电力公司枣庄供电公司会议听取了公司授权采购项目评审情况的汇报，认为评审结果有效，同意本次评审意见。")
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 12
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Write("授权采购工作领导小组审批签字：")
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")

        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_zdshenpi(ByVal fp As String)
        Dim i As Integer
        Dim str As String = ""
        Dim dr As DataRow
        Dim sli As New SortedList

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        '填写标题
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 12
        builder.Font.Bold = False
        builder.Font.Name = "方正小标宋_GBK"
        builder.Font.Size = 18
        'sql获取overload表数据
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname 
        FROM overload 
        WHERE overload.id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        builder.Writeln(Form1.sqlcon1.dataf.Item(0) & "重大事项决策程序审批单")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Font.Bold = False
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 15
        builder.Writeln("授权采购项目评审结果")
        builder.Font.Bold = True
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 10.5
        '表标题
        table = builder.StartTable()
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.RowFormat.Height = 25
        '插入第一行
        builder.InsertCell()
        builder.CellFormat.Width = 35
        builder.Write("序号")
        builder.InsertCell()
        builder.CellFormat.Width = 100
        builder.Write("项目单位")
        builder.InsertCell()
        builder.CellFormat.Width = 150
        builder.Write("项目名称")
        builder.InsertCell()
        builder.CellFormat.Width = 120
        builder.Write("成交人")
        builder.InsertCell()
        builder.CellFormat.Width = 90
        builder.Write("成交价格" & vbCrLf & "（万元）")
        builder.EndRow()
        'sql获取bid表数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT 
        bid.bididx,bid.bidname,package.pkgidx,package.pkgname,
        package_config.char_ps,bid.id,package.id 
        FROM bid,package,package_config 
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.id = package.bidid
        AND package.id = package_config.pkgid 
        ORDER BY bid.id,package.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            i = 1
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                If dr.Item(1) <> str Then
                    builder.Font.Bold = True
                    builder.Font.Name = "方正仿宋_GBK"
                    builder.Font.Size = 10.5
                    builder.RowFormat.Height = 25
                    builder.InsertCell()
                    builder.CellFormat.Width = 495
                    builder.Write(dr.Item(0) & " " & dr.Item(1))
                    builder.EndRow()
                    str = dr.Item(1)
                End If
                builder.Font.Bold = False
                builder.Font.Name = "方正仿宋_GBK"
                builder.Font.Size = 10.5
                builder.RowFormat.Height = 40
                builder.InsertCell()
                builder.CellFormat.Width = 35
                builder.Write(i)
                i += 1
                builder.InsertCell()
                builder.CellFormat.Width = 100
                'sql获取engineering.engidx表数据
                Form1.sqlcon1.sqlstr("
                SELECT *,count(tb1.engidx)
                FROM 
                (SELECT DISTINCT engineering.engidx
                FROM package,engineering
                WHERE package.id = " & dr.Item(6) & "
                AND engineering.pkgid = package.id) AS tb1", 1)
                Form1.sqlcon1.dataf.Read()
                If Form1.sqlcon1.dataf.Item(1) < 2 Then
                    builder.Write(Form1.sqlcon1.dataf.Item(0))
                Else
                    builder.Write(Form1.sqlcon1.dataf.Item(0) & "等" &
                                  Form1.sqlcon1.dataf.Item(1) & "个项目单位")
                End If
                builder.InsertCell()
                builder.CellFormat.Width = 150
                builder.Write(dr.Item(3))
                builder.InsertCell()
                builder.CellFormat.Width = 120
                '获取package_abort表数据
                Form1.sqlcon1.sqlstr("
                SELECT package_abort.aboid 
                FROM package_abort
                WHERE package_abort.pkgid = " & dr.Item(6), 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    builder.Write("流包")
                    builder.InsertCell()
                    builder.CellFormat.Width = 90
                    builder.Write("")
                    builder.EndRow()
                Else
                    Form1.sqlcon1.sqlstr("
                    SELECT package_config.char_ps 
                    FROM package_config 
                    WHERE package_config.pkgid = " & dr.Item(6), 1)
                    Form1.sqlcon1.dataf.Read()
                    If Form1.sqlcon1.dataf.Item(0) = "最低价法" Then
                        'sql获取sorting_price表数据
                        Form1.sqlcon1.sqlstr("
                        SELECT factory.facname,sorting_price.char_bj 
                        FROM factory,sorting_price 
                        WHERE sorting_price.facid = factory.id 
                        AND sorting_price.sort = 1 
                        AND sorting_price.pkgid = " & dr.Item(6), 1)
                        Form1.sqlcon1.dataf.Read()
                        builder.Write(Form1.sqlcon1.dataf.Item(0))
                        builder.InsertCell()
                        builder.CellFormat.Width = 90
                        builder.Write(Form1.sqlcon1.dataf.Item(1))
                        builder.EndRow()
                    Else
                        'sql获取sorting表数据
                        Form1.sqlcon1.sqlstr("
                        SELECT factory.facname,sorting_price.char_bj
                        FROM factory,sorting,sorting_price 
                        WHERE sorting.facid = factory.id 
                        AND sorting.pkgid = sorting_price.pkgid
                        AND sorting.facid = sorting_price.facid
                        AND sorting.sort = 1
                        AND sorting.pkgid = " & dr.Item(6), 1)
                        Form1.sqlcon1.dataf.Read()
                        builder.Write(Form1.sqlcon1.dataf.Item(0))
                        builder.InsertCell()
                        builder.CellFormat.Width = 90
                        builder.Write(Form1.sqlcon1.dataf.Item(1))
                        builder.EndRow()
                    End If
                End If
            Next
        End If
        '设置表格格式
        table = builder.EndTable
        table.Alignment = Tables.TableAlignment.Center
        table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 12
        builder.Font.Bold = False
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 15
        builder.Write("    " & Format(Now(), " yyyy年MM月dd日") &
                        "，国网山东省电力公司枣庄供电公司会议听取了公司授权采购项目评审情况的汇报，认为评审结果有效，同意本次评审意见。")
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 12
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Write("公司领导审批签字：")
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")

        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_houxuan(ByVal fp As String)
        Dim i As Integer
        Dim str As String = ""
        Dim dr As DataRow
        Dim sli As New SortedList

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        '填写标题
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        builder.Font.Bold = False
        builder.Font.Name = "方正小标宋_GBK"
        builder.Font.Size = 18
        'sql获取overload表数据
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono 
        FROM overload 
        WHERE overload.id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        builder.Write(Form1.sqlcon1.dataf.Item(0) & vbCrLf & "成交候选人公示")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Font.Bold = False
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 16
        builder.Write("（采购编号：" & Form1.sqlcon1.dataf.Item(1) & "）")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        builder.Writeln("各相关应答人：")
        builder.ParagraphFormat.FirstLineIndent = 28
        builder.Write(Form1.sqlcon1.dataf.Item(0) & "评审工作已结束，现将评审委员会推荐的成交候选人予以公示，公示期3天。应答人或者其他利害关系人若对评审结果有异议的，请在成交候选人公示期间以书面形式（传真）提出。")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 12
        builder.Font.Bold = True
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 10.5
        '表标题
        table = builder.StartTable()
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.RowFormat.Height = 25
        '插入第一行
        builder.InsertCell()
        builder.CellFormat.Width = 35
        builder.Write("序号")
        builder.InsertCell()
        builder.CellFormat.Width = 75
        builder.Write("分标编号")
        builder.InsertCell()
        builder.CellFormat.Width = 95
        builder.Write("包名称")
        builder.InsertCell()
        builder.CellFormat.Width = 70
        builder.Write("推荐的成交候选人")
        builder.InsertCell()
        builder.CellFormat.Width = 35
        builder.Write("排序")
        builder.InsertCell()
        builder.CellFormat.Width = 50
        builder.Write("质量")
        builder.InsertCell()
        builder.CellFormat.Width = 45
        builder.Write("工期/服务期")
        builder.InsertCell()
        builder.CellFormat.Width = 90
        builder.Write("资格能力")
        builder.InsertCell()
        builder.CellFormat.Width = 60
        builder.Write("评审情况")
        builder.EndRow()
        'sql获取bid表数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT 
        bid.bididx,bid.bidname,bid.bidno,package.pkgidx,package.pkgname,
        package_config.char_ps,bid.id,package.id 
        FROM bid,package,package_config 
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.id = package.bidid
        AND package.id = package_config.pkgid 
        ORDER BY bid.id,package.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            i = 1
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                builder.Font.Bold = False
                builder.Font.Name = "方正仿宋_GBK"
                builder.Font.Size = 10.5
                builder.RowFormat.Height = 40
                builder.InsertCell()
                builder.CellFormat.Width = 35
                builder.Write(i)
                i += 1
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.Write(dr.Item(2))
                builder.InsertCell()
                builder.CellFormat.Width = 95
                builder.Write(dr.Item(4))
                builder.InsertCell()
                builder.CellFormat.Width = 70
                '获取package_abort表数据
                Form1.sqlcon1.sqlstr("
                SELECT package_abort.aboid 
                FROM package_abort
                WHERE package_abort.pkgid = " & dr.Item(7), 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    builder.Write("流包")
                    builder.InsertCell()
                    builder.CellFormat.Width = 35
                    builder.Write("")
                    builder.InsertCell()
                    builder.CellFormat.Width = 50
                    builder.Write("")
                    builder.InsertCell()
                    builder.CellFormat.Width = 45
                    builder.Write("")
                    builder.InsertCell()
                    builder.CellFormat.Width = 90
                    builder.Write("")
                    builder.InsertCell()
                    builder.CellFormat.Width = 60
                    builder.Write("")
                    builder.EndRow()
                Else
                    Form1.sqlcon1.sqlstr("
                    SELECT package_config.char_ps 
                    FROM package_config 
                    WHERE package_config.pkgid = " & dr.Item(7), 1)
                    Form1.sqlcon1.dataf.Read()
                    If Form1.sqlcon1.dataf.Item(0) = "最低价法" Then
                        'sql获取sorting_price表数据
                        Form1.sqlcon1.sqlstr("
                        SELECT factory.facname,sorting_price.char_bj 
                        FROM factory,sorting_price 
                        WHERE sorting_price.facid = factory.id 
                        AND sorting_price.sort = 1 
                        AND sorting_price.pkgid = " & dr.Item(7), 1)
                        Form1.sqlcon1.dataf.Read()
                        builder.Write(Form1.sqlcon1.dataf.Item(0))
                        builder.InsertCell()
                        builder.CellFormat.Width = 35
                        builder.Write("1")
                        builder.InsertCell()
                        builder.CellFormat.Width = 50
                        builder.Write("满足技术规范要求")
                        builder.InsertCell()
                        builder.CellFormat.Width = 45
                        builder.Write("满足采购文件要求")
                        builder.InsertCell()
                        builder.CellFormat.Width = 90
                        builder.Write("达到采购文件要求的资格能力")
                        builder.InsertCell()
                        builder.CellFormat.Width = 60
                        builder.Write("综合排序第一名")
                        builder.EndRow()
                    Else
                        'sql获取sorting表数据
                        Form1.sqlcon1.sqlstr("
                        SELECT factory.facname,sorting_price.char_bj
                        FROM factory,sorting,sorting_price 
                        WHERE sorting.facid = factory.id 
                        AND sorting.pkgid = sorting_price.pkgid
                        AND sorting.facid = sorting_price.facid
                        AND sorting.sort = 1
                        AND sorting.pkgid = " & dr.Item(7), 1)
                        Form1.sqlcon1.dataf.Read()
                        builder.Write(Form1.sqlcon1.dataf.Item(0))
                        builder.InsertCell()
                        builder.CellFormat.Width = 35
                        builder.Write("1")
                        builder.InsertCell()
                        builder.CellFormat.Width = 50
                        builder.Write("满足技术规范要求")
                        builder.InsertCell()
                        builder.CellFormat.Width = 45
                        builder.Write("满足采购文件要求")
                        builder.InsertCell()
                        builder.CellFormat.Width = 90
                        builder.Write("达到采购文件要求的资格能力")
                        builder.InsertCell()
                        builder.CellFormat.Width = 60
                        builder.Write("综合排序第一名")
                        builder.EndRow()
                    End If
                End If
            Next
        End If
        '设置表格格式
        table = builder.EndTable
        table.Alignment = Tables.TableAlignment.Center
        table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        builder.ParagraphFormat.FirstLineIndent = 28
        builder.Font.Bold = False
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 15
        sli.Clear()
        sli.Add("1", "本公示公示期3日。")
        sli.Add("2", "根据招标投标法及相关法规规定，应答人和直接参与并且与招投标活动有着直接利害关系的当事人有异议的，有权依法进行质疑，提出质疑时应注意以下事项：")
        sli.Add("3", "1.质疑必须在成交结果公示结束前提出。")
        sli.Add("4", "2．应当提交质疑书，并包括下列内容：")
        sli.Add("5", "（1）质疑人的名称、地址、联系人及有效联系方式；")
        sli.Add("6", "（2）被质疑人的名称；")
        sli.Add("7", "（3）质疑事项的基本事实；")
        sli.Add("8", "（4）有效线索和相关证明材料。")
        sli.Add("9", "3．质疑人为法人的，质疑书必须由其法定代表人或者授权代表签字并加盖公章，同时还需提交授权委托书；质疑人为个人的，质疑书必须由质疑人本人签字，并附有效身份证明，由本人提交。")
        sli.Add("10", "4．下列质疑将不予接收：")
        sli.Add("11", "（1）在成交结果公示结束后提出的；")
        sli.Add("12", "（2）质疑人不能证明是所质疑招标投标活动的应答人和直接参与并且与招投标活动有着直接利害关系的当事人；")
        sli.Add("13", "（3）质疑事项不具体，且未提供有效线索，难以查证的；")
        sli.Add("14", "（4）对质疑事项已经答复，且质疑人没有提出新的证据的。")
        sli.Add("15", "5.质疑人不得以质疑为名排挤竞争对手，进行虚假、恶意质疑，阻碍招标投标活动的正常进行。")
        For i = 1 To 15
            builder.Write(sli(i.ToString))
            builder.InsertBreak(BreakType.ParagraphBreak)
        Next
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right
        'sql获取owner表
        Form1.sqlcon1.sqlstr("
        SELECT owner.owname,owner.owcalln 
        FROM owner,overload
        WHERE owner.id = overload.owid 
        AND overload.id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        builder.Writeln("采购人：" & Form1.sqlcon1.dataf.Item(0))
        builder.Writeln("联系电话：" & Form1.sqlcon1.dataf.Item(1))
        builder.Writeln("联系传真：" & Form1.sqlcon1.dataf.Item(1))
        builder.Writeln("采购代理机构： 山东诚信工程建设监理有限公司")
        builder.Write("日期：" & Format(Now(), " yyyy年MM月dd日"))
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")

        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_gonggao(ByVal fp As String)
        Dim i As Integer
        Dim str As String = ""
        Dim dr As DataRow
        Dim sli As New SortedList

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        '填写标题
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        builder.Font.Bold = False
        builder.Font.Name = "方正小标宋_GBK"
        builder.Font.Size = 18
        'sql获取overload表数据
        Form1.sqlcon1.sqlstr("
        SELECT overload.proname,overload.prono 
        FROM overload 
        WHERE overload.id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        builder.Write(Form1.sqlcon1.dataf.Item(0) & vbCrLf & "成交结果公告")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.Font.Bold = False
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 16
        builder.Write("（采购编号：" & Form1.sqlcon1.dataf.Item(1) & "）")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        builder.ParagraphFormat.FirstLineIndent = 28
        builder.Write(Form1.sqlcon1.dataf.Item(0) & "成交候选人公示活动已经结束。现将成交人名单公告如下：")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 12
        builder.Font.Bold = True
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 10.5
        '表标题
        table = builder.StartTable()
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.RowFormat.Height = 25
        '插入第一行
        builder.InsertCell()
        builder.CellFormat.Width = 35
        builder.Write("序号")
        builder.InsertCell()
        builder.CellFormat.Width = 90
        builder.Write("分标编号")
        builder.InsertCell()
        builder.CellFormat.Width = 45
        builder.Write("包号")
        builder.InsertCell()
        builder.CellFormat.Width = 165
        builder.Write("包名称")
        builder.InsertCell()
        builder.CellFormat.Width = 150
        builder.Write("成交人")
        builder.EndRow()
        'sql获取bid表数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT 
        bid.bididx,bid.bidname,bid.bidno,package.pkgidx,package.pkgname,
        package_config.char_ps,bid.id,package.id 
        FROM bid,package,package_config 
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.id = package.bidid
        AND package.id = package_config.pkgid 
        ORDER BY bid.id,package.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            i = 1
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                builder.Font.Bold = False
                builder.Font.Name = "方正仿宋_GBK"
                builder.Font.Size = 10.5
                builder.RowFormat.Height = 40
                builder.InsertCell()
                builder.CellFormat.Width = 35
                builder.Write(i)
                i += 1
                builder.InsertCell()
                builder.CellFormat.Width = 90
                builder.Write(dr.Item(2))
                builder.InsertCell()
                builder.CellFormat.Width = 45
                builder.Write(dr.Item(3))
                builder.InsertCell()
                builder.CellFormat.Width = 165
                builder.Write(dr.Item(4))
                builder.InsertCell()
                builder.CellFormat.Width = 150
                '获取package_abort表数据
                Form1.sqlcon1.sqlstr("
                SELECT package_abort.aboid 
                FROM package_abort
                WHERE package_abort.pkgid = " & dr.Item(7), 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    builder.Write("流包")
                    builder.EndRow()
                Else
                    Form1.sqlcon1.sqlstr("
                    SELECT package_config.char_ps 
                    FROM package_config 
                    WHERE package_config.pkgid = " & dr.Item(7), 1)
                    Form1.sqlcon1.dataf.Read()
                    If Form1.sqlcon1.dataf.Item(0) = "最低价法" Then
                        'sql获取sorting_price表数据
                        Form1.sqlcon1.sqlstr("
                        SELECT factory.facname,sorting_price.char_bj 
                        FROM factory,sorting_price 
                        WHERE sorting_price.facid = factory.id 
                        AND sorting_price.sort = 1 
                        AND sorting_price.pkgid = " & dr.Item(7), 1)
                        Form1.sqlcon1.dataf.Read()
                        builder.Write(Form1.sqlcon1.dataf.Item(0))
                        builder.EndRow()
                    Else
                        'sql获取sorting表数据
                        Form1.sqlcon1.sqlstr("
                        SELECT factory.facname,sorting_price.char_bj
                        FROM factory,sorting,sorting_price 
                        WHERE sorting.facid = factory.id 
                        AND sorting.pkgid = sorting_price.pkgid
                        AND sorting.facid = sorting_price.facid
                        AND sorting.sort = 1
                        AND sorting.pkgid = " & dr.Item(7), 1)
                        Form1.sqlcon1.dataf.Read()
                        builder.Write(Form1.sqlcon1.dataf.Item(0))
                        builder.EndRow()
                    End If
                End If
            Next
        End If
        '设置表格格式
        table = builder.EndTable
        table.Alignment = Tables.TableAlignment.Center
        table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        builder.Font.Bold = False
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 15
        'sql获取owner表
        Form1.sqlcon1.sqlstr("
        SELECT owner.owname,owner.owcalln 
        FROM owner,overload
        WHERE owner.id = overload.owid 
        AND overload.id = " & Form3.pro_id, 1)
        Form1.sqlcon1.dataf.Read()
        builder.Writeln("采购人：" & Form1.sqlcon1.dataf.Item(0))
        builder.Writeln("采购代理机构： 山东诚信工程建设监理有限公司")
        builder.Write("日期：" & Format(Now(), " yyyy年MM月dd日"))
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Public Sub dc_tongzhishu(ByVal fp As String, ByVal pkgids As Integer)
        Dim i As Integer
        Dim jg As Double
        Dim str As String
        Dim sli As New SortedList

        doc = New Document
        builder = New DocumentBuilder(doc)
        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        '填写标题
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 18
        builder.Font.Bold = False
        builder.Font.Name = "方正小标宋_GBK"
        builder.Font.Size = 22
        builder.Writeln("成交通知书")
        '填写首行
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        builder.Font.Bold = False
        builder.Font.Name = "方正仿宋_GBK"
        builder.Font.Size = 16
        'sql获取sorting表数据
        Form1.sqlcon1.sqlstr("
        SELECT package_config.char_ps 
        FROM package_config 
        WHERE package_config.pkgid = " & pkgids, 1)
        Form1.sqlcon1.dataf.Read()
        If Form1.sqlcon1.dataf.Item(0) = "最低价法" Then
            Form1.sqlcon1.sqlstr("
            SELECT factory.facname,sorting_price.char_bj 
            FROM factory,sorting_price 
            WHERE sorting_price.oveid = " & Form3.pro_id & " 
            AND sorting_price.pkgid = " & pkgids & "
            AND sorting_price.facid = factory.id 
            AND sorting_price.sort = 1", 1)
            Form1.sqlcon1.dataf.Read()
            builder.Writeln(Form1.sqlcon1.dataf.Item(0))
            jg = Form1.sqlcon1.dataf.Item(1)
        Else
            Form1.sqlcon1.sqlstr("
            SELECT factory.facname,sorting_price.char_bj 
            FROM factory,sorting,sorting_price 
            WHERE sorting.oveid = " & Form3.pro_id & "
            AND sorting.pkgid = " & pkgids & "
            AND sorting.pkgid = sorting_price.pkgid
            AND sorting.facid = sorting_price.facid 
            AND sorting.facid = factory.id 
            AND sorting.sort = 1", 1)
            Form1.sqlcon1.dataf.Read()
            builder.Writeln(Form1.sqlcon1.dataf.Item(0))
            jg = Form1.sqlcon1.dataf.Item(1)
        End If
        'sql获取overload,bid,package,package_config表数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT 
        overload.proname,overload.prono,bid.bidno,
        package.pkgidx,package.pkgname,
        package_config.char_bj,package_config.char_ps,
        owner.owname,
        bid.id,package.id,count(engineering.id)
        FROM overload,bid,package,package_config,engineering,owner 
        WHERE overload.id = " & Form3.pro_id & "
        AND package.id = " & pkgids & "
        AND overload.id = bid.oveid 
        AND bid.id = package.bidid 
        AND package.id = package_config.pkgid 
        AND package.id = engineering.pkgid 
        AND overload.owid = owner.id", 1)
        Form1.sqlcon1.dataf.Read()
        str = Form1.sqlcon1.dataf.Item(0) & "（采购编号：" &
            Form1.sqlcon1.dataf.Item(1) & "）的评审工作已结束。根据评审委员会的评审推荐结果，经公司授权采购工作领导小组批准，贵公司被确认为分标编号：" &
            Form1.sqlcon1.dataf.Item(2) & "，" &
            Form1.sqlcon1.dataf.Item(3) & "（包名称：" &
            Form1.sqlcon1.dataf.Item(4) & "）的成交人，成交金额"
        builder.ParagraphFormat.FirstLineIndent = 28
        Select Case Form1.sqlcon1.dataf.Item(5)
            Case "固定总价"
                builder.Writeln(str & "为" & jg & "万元。")
            Case "折扣率"
                builder.Writeln(str & "为折扣率" & jg * 100 & "%。")
            Case "综合单价"
                builder.Writeln(str & "详见附件。")
        End Select
        builder.Writeln("请贵公司在本签约通知书发出之日起30天内，携带所有签订合同所需的资料（包括但不限于法定代表人授权书、技术规范、技术图纸等），与公司订立书面合同。合同签订的安排由公司另行通知。")
        builder.InsertBreak(BreakType.ParagraphBreak)
        builder.InsertBreak(BreakType.ParagraphBreak)
        '填写结尾
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        builder.Writeln("采购人：" & Form1.sqlcon1.dataf.Item(7))
        builder.Writeln("代理机构：山东诚信工程建设监理有限公司")
        builder.Write(Format(Now(), " yyyy年MM月dd日"))
        builder.InsertBreak(BreakType.ParagraphBreak)
        '填写附件
        builder.ParagraphFormat.ClearFormatting()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
        builder.ParagraphFormat.LineSpacing = 15
        '单价附件
        If Form1.sqlcon1.dataf.Item(5) = "综合单价" Then
            builder.InsertBreak(BreakType.SectionBreakNewPage)
            builder.Writeln("附件：")
        End If
        '项目名称附件
        Form1.sqlcon1.sqlstr("
        SELECT count(engineering.id)
        FROM engineering
        WHERE engineering.pkgid = " & pkgids, 1)
        If Form1.sqlcon1.dataf.HasRows Then
            Form1.sqlcon1.dataf.Read()
            If Form1.sqlcon1.dataf.Item(0) > 1 Then
                'sql获取engineering表数据
                Form1.sqlcon1.sqlstr("
                SELECT engineering.engname
                FROM engineering
                WHERE engineering.pkgid = " & pkgids & "
                ORDER BY engineering.id", 1)
                builder.InsertBreak(BreakType.SectionBreakNewPage)
                builder.Writeln("附件：")
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 12
                builder.Font.Bold = True
                builder.Font.Name = "方正仿宋_GBK"
                builder.Font.Size = 10.5
                '表标题
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.RowFormat.Height = 35
                '插入第一行
                builder.InsertCell()
                builder.CellFormat.Width = 35
                builder.Write("序号")
                builder.InsertCell()
                builder.CellFormat.Width = 280
                builder.Write("项目名称")
                builder.InsertCell()
                builder.CellFormat.Width = 110
                builder.Write("成交价格" & vbCrLf & "（万元）")
                builder.EndRow()
                i = 1
                builder.Font.Bold = False
                While Form1.sqlcon1.dataf.Read()
                    builder.InsertCell()
                    builder.CellFormat.Width = 35
                    builder.Write(i)
                    i += 1
                    builder.InsertCell()
                    builder.CellFormat.Width = 280
                    builder.Write(Form1.sqlcon1.dataf.Item(0))
                    builder.InsertCell()
                    builder.CellFormat.Width = 110
                    builder.Write("")
                    builder.EndRow()
                End While
                table = builder.EndTable
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
            End If
        End If
        Try
            'sql获取bid,package表数据
            Form1.sqlcon1.sqlstr("
            SELECT bid.bididx,bid.bidname,package.pkgidx 
            FROM bid,package 
            WHERE package.id = " & pkgids & "
            AND bid.id = package.bidid", 1)
            Form1.sqlcon1.dataf.Read()
            doc.Save(fp & "\" & Form3.TextBox1.Text & "-" &
                     Form1.sqlcon1.dataf.Item(0) & Form1.sqlcon1.dataf.Item(1) &
                     Form1.sqlcon1.dataf.Item(2) & ".docx")

        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
    End Sub

    Public Sub dc_danganfm(ByVal fp As String, ByVal boxidx As String, ByVal dosname As String)
        Dim str, cache, sp() As String
        Dim i, js, ct1, ct2 As Integer
        Dim sli As New SortedList

        doc = New Document
        builder = New DocumentBuilder(doc)
        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        '设置页边距
        builder.PageSetup.TopMargin = ConvertUtil.MillimeterToPoint(5)
        builder.PageSetup.BottomMargin = ConvertUtil.MillimeterToPoint(5)
        builder.PageSetup.LeftMargin = ConvertUtil.MillimeterToPoint(5)
        builder.PageSetup.RightMargin = ConvertUtil.MillimeterToPoint(15)
        '段落剧左
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        cache = ""

        '插入表格
        table = builder.StartTable()
        '表格内文字格式
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Bottom
        builder.Font.Name = "宋体"
        builder.Font.Size = 18
        builder.Font.Bold = True
        '第一行
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(45)
        builder.InsertCell()
        builder.CellFormat.Borders.Top.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(28.8)
        builder.InsertCell()
        builder.CellFormat.Borders.Top.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(33.5)
        builder.Write("档  号:")
        builder.InsertCell()
        builder.CellFormat.Borders.Top.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
        builder.CellFormat.Borders.Bottom.LineWidth = 1.5
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
        builder.Font.Bold = False
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(95)
        '获取dosno
        Form1.sqlcon1.sqlstr("
        SELECT dosno
        FROM cc_dossier
        WHERE dosname = " &
        Chr(34) & dosname & Chr(34), 1)
        Form1.sqlcon1.dataf.Read()
        str = Form1.sqlcon1.dataf.Item(0).ToString & "-" & boxidx
        builder.Write(str)
        builder.InsertCell()
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(35)
        builder.EndRow()
        '第2行
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(25)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(28.8)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(33.5)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(95)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(35)
        builder.EndRow()
        '第3行
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Justify
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(122)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(28.8)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(33.5)
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
        '填写封面内容
        str = dosname
        '填写过程资料
        Form1.sqlcon1.sqlstr("
        SELECT cc_dossier.filena,cc_dossier.filetye
        FROM cc_dossier
        WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
        AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
        AND filetye = " & Chr(34) & "过程资料" & Chr(34) & "
        ORDER BY id", 1)
        If Form1.sqlcon1.dataf.HasRows Then
            While Form1.sqlcon1.dataf.Read()
                str += Form1.sqlcon1.dataf.Item(0).ToString & "、"
            End While
            str = str.Substring(0, Len(str) - 1)
        End If
        '填写采购文件、招标文件

        sli.Clear()
        js = 1
        Form1.sqlcon1.sqlstr("
        SELECT cc_dossier.filena,cc_dossier.filetye,cc_dossier.boxidx
        FROM cc_dossier
        WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
        AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
        AND (
        filetye = " & Chr(34) & "采购文件" & Chr(34) & " OR 
        filetye = " & Chr(34) & "招标文件" & Chr(34) & ")
        ORDER BY id", 1)
        If Form1.sqlcon1.dataf.HasRows Then
            Dim re As New Regex("包(\d+)|包[0-9][0-9]")
            '填写cc_dossier_config_fm
            While Form1.sqlcon1.dataf.Read()
                sp = re.Split(Form1.sqlcon1.dataf.Item(0).ToString)
                Form1.sqlcon1.sqlstr("
                INSERT INTO cc_dossier_config_fm(dosname,boxidx,bidname,pkgname,filetye)
                VALUES(" &
                Chr(34) & dosname & Chr(34) & "," &
                Chr(34) & boxidx & Chr(34) & "," &
                Chr(34) & sp(0) & Chr(34) & "," &
                Chr(34) & re.Match(Form1.sqlcon1.dataf.Item(0)).ToString & Chr(34) & "," &
                Chr(34) & Form1.sqlcon1.dataf.Item(1) & Chr(34) & ")")
            End While
            '验证分标名称跨包情况
            Form1.sqlcon1.sqlstr("
            SELECT Distinct bidname
            FROM cc_dossier_config_fm
            WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
            AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
            ORDER BY id", 1)
            If Form1.sqlcon1.dataf.HasRows Then
                While Form1.sqlcon1.dataf.Read()
                    If Form1.sqlcon1.dataf.Item(0) <> cache Then
                        sli.Add(js, Form1.sqlcon1.dataf.Item(0).ToString)
                        js += 1
                        cache = Form1.sqlcon1.dataf.Item(0)
                    End If
                End While
            End If
            '比较
            For i = 1 To js - 1
                'ct1为分标盒内包数量
                Form1.sqlcon1.sqlstr("
                SELECT count(pkgname)
                FROM cc_dossier_config_fm
                WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
                AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
                AND bidname = " & Chr(34) & sli(i).ToString & Chr(34) & "
                ORDER BY id", 1)
                Form1.sqlcon1.dataf.Read()
                ct1 = Form1.sqlcon1.dataf.Item(0)
                'ct2为分标总包数量
                Form1.sqlcon1.sqlstr("
                SELECT count(filena)
                FROM cc_dossier
                WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
                AND (
                filetye = " & Chr(34) & "采购文件" & Chr(34) & " OR 
                filetye = " & Chr(34) & "招标文件" & Chr(34) & ") 
                AND filena LIKE " & Chr(34) & sli(i).ToString & "%" & Chr(34) & "
                ORDER BY id", 1)
                Form1.sqlcon1.dataf.Read()
                ct2 = Form1.sqlcon1.dataf.Item(0)
                '判断ct1、ct2，填写str
                If ct1 = ct2 Then
                    str += sli(i) & "、"
                Else
                    str += sli(i)
                    If ct1 > 3 Then
                        '填写first
                        Form1.sqlcon1.sqlstr("
                        SELECT bidname,pkgname,filetye
                        FROM cc_dossier_config_fm
                        WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
                        AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
                        AND bidname = " & Chr(34) & sli(i).ToString & Chr(34) & "
                        ORDER BY id", 1)
                        Form1.sqlcon1.dataf.Read()
                        str += Form1.sqlcon1.dataf.Item(1) & "-"
                        '填写last
                        Form1.sqlcon1.sqlstr("
                        SELECT bidname,pkgname,filetye
                        FROM cc_dossier_config_fm
                        WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
                        AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
                        AND bidname = " & Chr(34) & sli(i).ToString & Chr(34) & "
                        ORDER BY id desc", 1)
                        Form1.sqlcon1.dataf.Read()
                        str += Form1.sqlcon1.dataf.Item(1) & "、"
                    Else
                        Form1.sqlcon1.sqlstr("
                        SELECT bidname,pkgname,filetye
                        FROM cc_dossier_config_fm
                        WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
                        AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
                        AND bidname = " & Chr(34) & sli(i).ToString & Chr(34) & "
                        ORDER BY id", 1)
                        While Form1.sqlcon1.dataf.Read()
                            str += Form1.sqlcon1.dataf.Item(1) & "、"
                        End While
                    End If
                End If
            Next
            str = str.Substring(0, Len(str) - 1)
            Form1.sqlcon1.sqlstr("
            SELECT filetye
            FROM cc_dossier_config_fm
            WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
            AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
            ORDER BY id", 1)
            Form1.sqlcon1.dataf.Read()
            str += Form1.sqlcon1.dataf.Item(0)
        End If
        '填写应答文件、投标文件
        Form1.sqlcon1.sqlstr("
        SELECT cc_dossier.filena,cc_dossier.filetye
        FROM cc_dossier
        WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
        AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
        AND (
        filetye = " & Chr(34) & "应答文件报价部分" & Chr(34) & " OR 
        filetye = " & Chr(34) & "应答文件商务部分" & Chr(34) & " OR 
        filetye = " & Chr(34) & "应答文件技术部分" & Chr(34) & " OR 
        filetye = " & Chr(34) & "投标文件报价部分" & Chr(34) & " OR 
        filetye = " & Chr(34) & "投标文件商务部分" & Chr(34) & " OR 
        filetye = " & Chr(34) & "投标文件技术部分" & Chr(34) & ")
        ORDER BY id", 1)
        If Form1.sqlcon1.dataf.HasRows Then
            Form1.sqlcon1.sqlstr("
            SELECT count(DISTINCT cc_dossier.filena)
            FROM cc_dossier
            WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
            AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
            AND (
            filetye = " & Chr(34) & "应答文件报价部分" & Chr(34) & " OR 
            filetye = " & Chr(34) & "应答文件商务部分" & Chr(34) & " OR 
            filetye = " & Chr(34) & "应答文件技术部分" & Chr(34) & " OR 
            filetye = " & Chr(34) & "投标文件报价部分" & Chr(34) & " OR 
            filetye = " & Chr(34) & "投标文件商务部分" & Chr(34) & " OR 
            filetye = " & Chr(34) & "投标文件技术部分" & Chr(34) & ")", 1)
            Form1.sqlcon1.dataf.Read()
            If Form1.sqlcon1.dataf.Item(0) > 3 Then
                '大于3个，first - last
                Form1.sqlcon1.sqlstr("
                SELECT cc_dossier.filena,cc_dossier.filetye
                FROM cc_dossier
                WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
                AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
                AND (
                filetye = " & Chr(34) & "应答文件报价部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "应答文件商务部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "应答文件技术部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "投标文件报价部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "投标文件商务部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "投标文件技术部分" & Chr(34) & ")
                ORDER BY id", 1)
                Form1.sqlcon1.dataf.Read()
                '填写first
                If Form1.sqlcon1.dataf.Item(1) = "应答文件报价部分" Then
                    str += Form1.sqlcon1.dataf.Item(0).ToString & "应答文件-"
                ElseIf Form1.sqlcon1.dataf.Item(1) = "投标文件报价部分" Then
                    str += Form1.sqlcon1.dataf.Item(0).ToString & "投标文件-"
                Else
                    str += Form1.sqlcon1.dataf.Item(0).ToString &
                        Form1.sqlcon1.dataf.Item(1).ToString & "-"
                End If
                Form1.sqlcon1.sqlstr("
                SELECT cc_dossier.filena,cc_dossier.filetye
                FROM cc_dossier
                WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
                AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
                AND (
                filetye = " & Chr(34) & "应答文件报价部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "应答文件商务部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "应答文件技术部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "投标文件报价部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "投标文件商务部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "投标文件技术部分" & Chr(34) & ")
                ORDER BY id DESC", 1)
                Form1.sqlcon1.dataf.Read()
                '填写last
                If Form1.sqlcon1.dataf.Item(1) = "应答文件技术部分" Then
                    str += Form1.sqlcon1.dataf.Item(0).ToString & "应答文件"
                ElseIf Form1.sqlcon1.dataf.Item(1) = "投标文件技术部分" Then
                    str += Form1.sqlcon1.dataf.Item(0).ToString & "投标文件"
                Else
                    str += Form1.sqlcon1.dataf.Item(0).ToString &
                        Form1.sqlcon1.dataf.Item(1).ToString
                End If
            Else
                '小于等于3个，first、middle、last
                Form1.sqlcon1.sqlstr("
                SELECT cc_dossier.filena,cc_dossier.filetye,count(filena)
                FROM cc_dossier
                WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
                AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
                AND (
                filetye = " & Chr(34) & "应答文件报价部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "应答文件商务部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "应答文件技术部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "投标文件报价部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "投标文件商务部分" & Chr(34) & " OR 
                filetye = " & Chr(34) & "投标文件技术部分" & Chr(34) & ")
                GROUP BY filena
                ORDER BY id", 1)
                While Form1.sqlcon1.dataf.Read()
                    Select Case Form1.sqlcon1.dataf.Item(2)
                        Case 1
                            str += Form1.sqlcon1.dataf.Item(0) &
                                Form1.sqlcon1.dataf.Item(1) & "、"
                        Case 2
                            If Form1.sqlcon1.dataf.Item(1) = "应答文件商务部分" Then
                                str += Form1.sqlcon1.dataf.Item(0).ToString &
                                    "应答文件商务部分技术部分、"
                            ElseIf Form1.sqlcon1.dataf.Item(1) = "投标文件商务部分" Then
                                str += Form1.sqlcon1.dataf.Item(0).ToString &
                                    "投标文件、"
                            ElseIf Form1.sqlcon1.dataf.Item(1) = "应答文件报价部分" Then
                                str += Form1.sqlcon1.dataf.Item(0).ToString &
                                    "应答文件报价部分商务部分、"
                            ElseIf Form1.sqlcon1.dataf.Item(1) = "投标文件报价部分" Then
                                str += Form1.sqlcon1.dataf.Item(0).ToString &
                                    "投标文件报价部分商务部分、"
                            End If
                        Case 3
                            If Form1.sqlcon1.dataf.Item(1) = "应答文件报价部分" Then
                                str += Form1.sqlcon1.dataf.Item(0).ToString & "应答文件、"
                            ElseIf Form1.sqlcon1.dataf.Item(1) = "投标文件报价部分" Then
                                str += Form1.sqlcon1.dataf.Item(0).ToString & "投标文件、"
                            End If
                    End Select
                End While
                str = str.Substring(0, Len(str) - 1)
            End If
        End If
        Select Case Len(str)
            Case < 171
                builder.Font.Size = 20
            Case 171
                builder.Font.Size = 18
            Case > 171
                builder.Font.Size = 16
        End Select
        '写入fmstr
        Try
            If Len(str) < 256 Then
                Form1.sqlcon1.sqlstr("
                INSERT INTO cc_dossier_config(dosname,boxidx,fmstr)
                VALUES(" &
                Chr(34) & dosname & Chr(34) & "," &
                Chr(34) & boxidx & Chr(34) & "," &
                Chr(34) & str & Chr(34) & ")")
            End If
        Catch ex As Exception

        End Try
        builder.Font.Bold = True
        builder.Write(str)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(95)
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(35)
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
        builder.EndRow()
        '第4行
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(12)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(28.8)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(33.5)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(95)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(35)
        builder.EndRow()
        '第5-8行
        builder.Font.Size = 18
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Bottom
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(15)
        For i = 1 To 4
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(28.8)
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(33.5)
            builder.Font.Bold = True
            Select Case i
                Case 1
                    builder.Write("立卷单位:")
                Case 2
                    builder.Write("起止日期:")
                Case 3
                    builder.Write("保管期限:")
                Case 4
                    builder.Write("密    级:")
            End Select
            builder.InsertCell()
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Bottom.LineWidth = 1.5
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(95)
            builder.Font.Bold = False
            '获取日期最大值最小值
            Form1.sqlcon1.sqlstr("
            SELECT min(filedat),max(filedat)
            FROM cc_dossier
            WHERE dosname = " &
            Chr(34) & dosname & Chr(34) & "
            AND boxidx = " &
            Chr(34) & boxidx & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            Select Case i
                Case 1
                    builder.Write("山东诚信工程建设监理有限公司")
                Case 2
                    builder.Write(Form1.sqlcon1.dataf.Item(0) & "-" &
                                  Form1.sqlcon1.dataf.Item(1))
                Case 3
                    builder.Write("10年")
            End Select
            builder.InsertCell()
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(35)
            builder.EndRow()
        Next
        table = builder.EndTable
        '保存文件
        Try
            doc.Save(fp & "\1.案卷封面.docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
    End Sub

    Public Sub dc_danganml(ByVal fp As String, ByVal boxidx As String, ByVal dosname As String)
        Dim str As String
        Dim i, js, xh, pageidx As Integer
        Dim sli As New SortedList

        doc = New Document
        builder = New DocumentBuilder(doc)
        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        '设置页边距
        builder.PageSetup.TopMargin = ConvertUtil.MillimeterToPoint(5)
        builder.PageSetup.BottomMargin = ConvertUtil.MillimeterToPoint(25)
        builder.PageSetup.LeftMargin = ConvertUtil.MillimeterToPoint(25)
        builder.PageSetup.RightMargin = ConvertUtil.MillimeterToPoint(15)
        pageidx = 1
        xh = 1
        js = 1

        '段落剧左
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        '获取dosno
        Form1.sqlcon1.sqlstr("
        SELECT dosno
        FROM cc_dossier
        WHERE dosname = " &
        Chr(34) & dosname & Chr(34), 1)
        Form1.sqlcon1.dataf.Read()
        str = Form1.sqlcon1.dataf.Item(0).ToString & "-" & boxidx
        '插入表格
        table = builder.StartTable()
        '第4-last行
        Form1.sqlcon1.sqlstr("
        SELECT  fileno,fileow,filena,filetye,filedat,filepag
        FROM cc_dossier
        WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
        AND boxidx = " & Chr(34) & boxidx & Chr(34) & "
        ORDER BY id", 1)
        '填写表头
        Call danganmlbt(str)
        While Form1.sqlcon1.dataf.Read()
            '填写目录内容
            builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
            builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
            builder.Font.Name = "宋体"
            builder.RowFormat.HeightRule = HeightRule.AtLeast
            builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(15)
            builder.Font.Size = 11
            builder.Font.Bold = False
            builder.InsertCell()
            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Top.LineWidth = 0.5
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Bottom.LineWidth = 0.5
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Left.LineWidth = 1.5
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Right.LineWidth = 0.5
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12)
            builder.Write(xh)
            builder.InsertCell()
            builder.CellFormat.Borders.Left.LineWidth = 0.5
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
            builder.Write(Form1.sqlcon1.dataf.Item(0))
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
            builder.Write(Form1.sqlcon1.dataf.Item(1))
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(70)
            If Form1.sqlcon1.dataf.Item(3).ToString = "过程资料" Then
                builder.Write(Form1.sqlcon1.dataf.Item(2))
            Else
                builder.Write(Form1.sqlcon1.dataf.Item(2) & Form1.sqlcon1.dataf.Item(3))
            End If
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
            builder.Write(Form1.sqlcon1.dataf.Item(4))
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
            builder.Write(Form1.sqlcon1.dataf.Item(5))
            builder.InsertCell()
            builder.CellFormat.Borders.Right.LineWidth = 1.5
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
            builder.EndRow()
            '获取doc.pagecount
            doc.UpdatePageLayout()
            '判断doc.pagecount>pageidx
            If doc.PageCount > pageidx Then
                If xh = 1 Then
                    MsgBox("警告：盒号" & boxidx & "卷内目录格式错误", 0 + 48, "warning")
                    Exit Sub
                End If
                table = builder.EndTable()
                '删除last行
                builder.DeleteRow(pageidx - 1, js + 2)
                '转到last-1行，调整bottom格式
                For i = 1 To 7
                    builder.MoveToCell(pageidx - 1, js + 1, i - 1, 0)
                    builder.CellFormat.Borders.Bottom.LineWidth = 2.25
                    builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                Next
                '插入一页
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                builder.MoveToDocumentEnd()
                builder.InsertBreak(BreakType.PageBreak)
                table = builder.StartTable()
                '填写表头
                Call danganmlbt(str)
                '填写目录内容
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.Font.Name = "宋体"
                builder.RowFormat.HeightRule = HeightRule.AtLeast
                builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(15)
                builder.Font.Size = 11
                builder.Font.Bold = False
                builder.InsertCell()
                builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                builder.CellFormat.Borders.Top.LineWidth = 0.5
                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                builder.CellFormat.Borders.Bottom.LineWidth = 0.5
                builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                builder.CellFormat.Borders.Left.LineWidth = 1.5
                builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                builder.CellFormat.Borders.Right.LineWidth = 0.5
                builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12)
                builder.Write(xh)
                builder.InsertCell()
                builder.CellFormat.Borders.Left.LineWidth = 0.5
                builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
                builder.Write(Form1.sqlcon1.dataf.Item(0))
                builder.InsertCell()
                builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
                builder.Write(Form1.sqlcon1.dataf.Item(1))
                builder.InsertCell()
                builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(70)
                builder.Write(Form1.sqlcon1.dataf.Item(2) & Form1.sqlcon1.dataf.Item(3))
                builder.InsertCell()
                builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
                builder.Write(Form1.sqlcon1.dataf.Item(4))
                builder.InsertCell()
                builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
                builder.Write(Form1.sqlcon1.dataf.Item(5))
                builder.InsertCell()
                builder.CellFormat.Borders.Right.LineWidth = 1.5
                builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
                builder.EndRow()
                pageidx = doc.PageCount
                '获取doc.pagecount
                doc.UpdatePageLayout()
                If doc.PageCount > pageidx Then
                    MsgBox("警告：盒号" & boxidx & "卷内目录格式错误", 0 + 48, "warning")
                    Exit Sub
                End If
                js = 1
            End If
            xh += 1
            js += 1
        End While
        '填写last页空行
        Do While doc.PageCount = pageidx
            builder.InsertCell()
            builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Top.LineWidth = 0.5
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Bottom.LineWidth = 0.5
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Left.LineWidth = 1.5
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Right.LineWidth = 0.5
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12)
            builder.InsertCell()
            builder.CellFormat.Borders.Left.LineWidth = 0.5
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(70)
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
            builder.InsertCell()
            builder.CellFormat.Borders.Right.LineWidth = 1.5
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
            builder.EndRow()
            doc.UpdatePageLayout()
            js += 1
        Loop
        table = builder.EndTable
        '删除last行
        builder.DeleteRow(pageidx - 1, js + 1)
        '转到last行，调整bottom格式
        For i = 1 To 7
            builder.MoveToCell(pageidx - 1, js, i - 1, 0)
            builder.CellFormat.Borders.Bottom.LineWidth = 2.25
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
        Next
        '填写页脚
        builder.MoveToHeaderFooter(HeaderFooterType.FooterPrimary)
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Right
        builder.Font.Name = "宋体"
        builder.Font.Size = 12
        builder.Write("第")
        builder.InsertField("PAGE  \* Arabic  \* MERGEFORMAT")
        builder.Write("页，共" & pageidx & "页")
        builder.MoveToDocumentEnd()
        '保存文件
        Try
            doc.Save(fp & "\2.卷内目录.docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
    End Sub

    Sub danganmlbt(ByVal str As String)

        '表格内文字格式
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.Font.Name = "宋体"
        builder.Font.Size = 24
        builder.Font.Bold = True
        '第一行
        builder.RowFormat.HeightRule = HeightRule.Exactly
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(20)
        builder.InsertCell()
        builder.CellFormat.Borders.Top.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12)
        builder.Write("卷 内 目 录")
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(70)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
        builder.EndRow()
        '第2行
        builder.Font.Size = 11
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(13)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
        builder.Write("档号:")
        builder.InsertCell()
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
        builder.Write(str)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(70)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
        builder.EndRow()
        '第3行
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(20)
        builder.InsertCell()
        builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
        builder.CellFormat.Borders.Top.LineWidth = 1.5
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
        builder.CellFormat.Borders.Left.LineWidth = 1.5
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12)
        builder.Write("序号")
        builder.InsertCell()
        builder.CellFormat.Borders.Left.LineWidth = 0.5
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
        builder.Write("文件编号")
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
        builder.Write("责任者")
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(70)
        builder.Write("文 件 题 目")
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(20)
        builder.Write("日期")
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
        builder.Write("页数")
        builder.InsertCell()
        builder.CellFormat.Borders.Right.LineWidth = 1.5
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(15)
        builder.Write("备注")
        builder.EndRow()

    End Sub

    Public Sub dc_danganbkb(ByVal fp As String, ByVal boxidx As String, ByVal dosname As String)
        Dim str As String
        Dim i As Integer

        doc = New Document
        builder = New DocumentBuilder(doc)
        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Portrait
        '设置页边距
        builder.PageSetup.TopMargin = ConvertUtil.MillimeterToPoint(5)
        builder.PageSetup.BottomMargin = ConvertUtil.MillimeterToPoint(25)
        builder.PageSetup.LeftMargin = ConvertUtil.MillimeterToPoint(25)
        builder.PageSetup.RightMargin = ConvertUtil.MillimeterToPoint(15)
        '段落剧左
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        '插入表格
        table = builder.StartTable()
        '表格内文字格式
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
        builder.Font.Name = "宋体"
        builder.Font.Size = 24
        builder.Font.Bold = True
        '第一行
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(25)
        builder.InsertCell()
        builder.CellFormat.Borders.Top.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12.3)
        builder.Write("卷内备考表")
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(25)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(55.9)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(30)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(40)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(10)
        builder.EndRow()
        '第2行
        builder.Font.Size = 14
        builder.Font.Bold = False
        builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(12)
        builder.InsertCell()
        builder.CellFormat.Borders.Top.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12.3)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(25)
        builder.Write("档  号：")
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(55.9)
        '获取dosno
        Form1.sqlcon1.sqlstr("
        SELECT dosno
        FROM cc_dossier
        WHERE dosname = " &
        Chr(34) & dosname & Chr(34), 1)
        Form1.sqlcon1.dataf.Read()
        str = Form1.sqlcon1.dataf.Item(0).ToString & "-" & boxidx
        builder.Write(str)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(30)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(40)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(10)
        builder.EndRow()
        '第3行
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(12)
        builder.InsertCell()
        builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12.3)
        builder.InsertCell()
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(25)
        builder.Write("互见号：")
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(55.9)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(30)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(40)
        builder.InsertCell()
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(10)
        builder.EndRow()
        '第4-5行
        For i = 1 To 2
            builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(12)
            builder.InsertCell()
            builder.CellFormat.Borders.Top.LineStyle = LineStyle.None
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12.3)
            builder.InsertCell()
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(25)
            If i = 2 Then
                builder.Write("说  明：")
            End If
            builder.InsertCell()
            builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(55.9)
            builder.InsertCell()
            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(30)
            builder.InsertCell()
            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(40)
            builder.InsertCell()
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
            builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(10)
            builder.EndRow()
        Next
        '第6行
        builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Top
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(95)
        builder.InsertCell()
        builder.CellFormat.Borders.Top.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12.3)
        builder.InsertCell()
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.First
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(25)
        '计算件数页数
        Form1.sqlcon1.sqlstr("
        SELECT count(id),sum(filepag)
        FROM cc_dossier
        WHERE dosname = " & Chr(34) & dosname & Chr(34) & "
        AND boxidx = " & Chr(34) & boxidx & Chr(34), 1)
        If Form1.sqlcon1.dataf.HasRows Then
            Form1.sqlcon1.dataf.Read()
            builder.Write("本案卷有" &
                          Form1.sqlcon1.dataf.Item(0) & "份文件，共" &
                          Form1.sqlcon1.dataf.Item(1) & "页。")
        End If
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(55.9)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(30)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.Previous
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(40)
        builder.InsertCell()
        builder.CellFormat.HorizontalMerge = Tables.CellMerge.None
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(10)
        builder.EndRow()
        '第7-11行
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(13)
        For i = 1 To 5
            builder.InsertCell()
            builder.CellFormat.Borders.Top.LineStyle = LineStyle.None
            builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.None
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12.3)
            builder.InsertCell()
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(25)
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(55.9)
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(30)
            Select Case i
                Case 2
                    builder.Write("立 卷 人：")
                Case 4
                    builder.Write("检 查 人：")
            End Select
            builder.InsertCell()
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(40)
            Select Case i
                Case 3
                    builder.Write(" 年  月   日")
                Case 5
                    builder.Write(" 年  月   日")
            End Select
            builder.InsertCell()
            builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
            builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
            builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(10)
            builder.EndRow()
        Next
        'last行
        builder.RowFormat.Height = ConvertUtil.MillimeterToPoint(18)
        builder.InsertCell()
        builder.CellFormat.Borders.Top.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.None
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(12.3)
        builder.InsertCell()
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(25)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(55.9)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(30)
        builder.InsertCell()
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(40)
        builder.InsertCell()
        builder.CellFormat.Borders.Left.LineStyle = LineStyle.None
        builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
        builder.CellFormat.Width = ConvertUtil.MillimeterToPoint(10)
        builder.EndRow()
        table = builder.EndTable
        '保存文件
        Try
            doc.Save(fp & "\3.备考表.docx")
        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
    End Sub

    Public Sub dc_cggonggao(ByVal fp As String)
        Dim str As String
        Dim dataf1 As SQLiteDataReader
        Dim dr As DataRow
        Dim dbl As Double

        '纸张大小A4
        builder.PageSetup.PaperSize = PaperSize.A4
        builder.PageSetup.Orientation = Orientation.Landscape
        'sql获取bid,package表数据
        Form1.sqlcon1.sqlstr("
        SELECT DISTINCT 
        bid.bididx,bid.bidname,bid.bidno,bid.id
        FROM bid
        WHERE bid.oveid = " & Form3.pro_id & "
        ORDER BY bid.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                str = ""
                '设置标题格式
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Left
                '填写分标号分标名
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 12
                builder.InsertBreak(BreakType.ParagraphBreak)
                builder.Font.Bold = True
                builder.Font.Name = "宋体"
                builder.Font.Size = 10.5
                builder.Writeln("分标名称：" & dr.Item(1) & "               " &
                                "分标编号：" & dr.Item(2))
                '设置格式
                builder.ParagraphFormat.ClearFormatting()
                builder.ParagraphFormat.Alignment = ParagraphAlignment.Center
                builder.ParagraphFormat.LineSpacingRule = LineSpacingRule.Multiple
                builder.ParagraphFormat.LineSpacing = 12
                builder.Font.Size = 10
                '设置表格式
                builder.Font.Bold = True
                table = builder.StartTable()
                builder.CellFormat.VerticalAlignment = Tables.CellVerticalAlignment.Center
                builder.CellFormat.Borders.Top.LineStyle = LineStyle.Single
                builder.CellFormat.Borders.Bottom.LineStyle = LineStyle.Single
                builder.CellFormat.Borders.Left.LineStyle = LineStyle.Single
                builder.CellFormat.Borders.Right.LineStyle = LineStyle.Single
                builder.RowFormat.Height = 35
                '插入标题行
                builder.InsertCell()
                builder.CellFormat.Width = 35
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.Write("包号")
                builder.InsertCell()
                builder.CellFormat.Width = 140
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.Write("包名称")
                builder.InsertCell()
                builder.CellFormat.Width = 140
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.Write("项目内容")
                builder.InsertCell()
                builder.CellFormat.Width = 55
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.Write("保证金" & vbCrLf & "（万元）")
                builder.InsertCell()
                builder.CellFormat.Width = 55
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.Write("需求时间")
                builder.InsertCell()
                builder.CellFormat.Width = 200
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.Write("专用资质业绩要求")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.Write("单项最高限价" & vbCrLf & "（万元）")
                builder.InsertCell()
                builder.CellFormat.Width = 75
                builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                builder.Write("包最高限价" & vbCrLf & "（万元）")
                builder.EndRow()
                '填写分包内容
                Form1.sqlcon1.sqlstr("
                SELECT package.id,package.pkgidx,package.pkgname,
                engineering.engname,engineering_config.char_gs,engineering_config.char_xj,
                package_request.char_zz,package_request.char_yj
                FROM package,engineering,engineering_config,package_request
                WHERE package.bidid = " & dr.Item(3) & "
                AND package.id = engineering.pkgid
                AND engineering.id = engineering_config.engid
                AND package.id = package_request.pkgid
                ORDER BY package.id", 1)
                If Form1.sqlcon1.dataf.HasRows Then
                Else
                    Continue For
                End If
                dataf1 = Form1.sqlcon1.dataf
                builder.Font.Bold = False
                While dataf1.Read
                    If str <> dataf1.Item(1) Then
                        str = dataf1.Item(1)
                        '插入非合并数据
                        builder.InsertCell()
                        builder.CellFormat.Width = 35
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        builder.Write(dataf1.Item(1).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 140
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        builder.Write(dataf1.Item(2).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 140
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        builder.Write(dataf1.Item(3).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 55
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        '保证金额计算
                        Form1.sqlcon1.sqlstr("
                        SELECT package.pkgidx,sum(engineering_config.char_gs),sum(engineering_config.char_xj)
                        FROM package,engineering,engineering_config
                        WHERE package.id = " & dataf1.Item(0) & "
                        AND package.id = engineering.pkgid
                        AND engineering.id = engineering_config.engid
                        GROUP BY package.pkgidx
                        ORDER BY package.id", 1)
                        If Form1.sqlcon1.dataf.HasRows Then
                            Form1.sqlcon1.dataf.Read()
                            dbl = CDbl(Form1.sqlcon1.dataf.Item(1)) * 0.016

                            builder.Write(dbl)
                        End If
                        builder.InsertCell()
                        builder.CellFormat.Width = 55
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        builder.Write("详见技术规范")
                        builder.InsertCell()
                        builder.CellFormat.Width = 200
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        builder.Write(dataf1.Item(6).ToString & vbCrLf & dataf1.Item(7).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        builder.Write(dataf1.Item(5).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.First
                        builder.Write(Form1.sqlcon1.dataf.Item(2).ToString)
                        builder.EndRow()
                    Else
                        '插入人合并数据
                        builder.InsertCell()
                        builder.CellFormat.Width = 35
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                        builder.InsertCell()
                        builder.CellFormat.Width = 140
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                        builder.InsertCell()
                        builder.CellFormat.Width = 140
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.Write(dataf1.Item(3).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 55
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                        builder.InsertCell()
                        builder.CellFormat.Width = 55
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.Write("详见技术规范")
                        builder.InsertCell()
                        builder.CellFormat.Width = 200
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.None
                        builder.Write(dataf1.Item(5).ToString)
                        builder.InsertCell()
                        builder.CellFormat.Width = 75
                        builder.CellFormat.VerticalMerge = Tables.CellMerge.Previous
                        builder.EndRow()
                    End If
                End While
                '设置表格格式
                table = builder.EndTable
                table.Alignment = Tables.TableAlignment.Center
                table.AutoFit(Tables.AutoFitBehavior.FixedColumnWidths)
                builder.InsertBreak(BreakType.ParagraphBreak)
            Next
        End If
        Try
            doc.Save(fp & "\" & Form3.TextBox1.Text & ".docx")

        Catch ex As Exception
            MsgBox("警告：指定文件无法编辑", 0 + 48, "warning")
            Exit Sub
        End Try
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

End Class

Public Class wf

    '开启waitform1
    Public Sub wshow()
        Try
            Form3.SplashScreenManager1.ShowWaitForm()
        Catch ex As Exception
            MsgBox("警告：点击过于频繁", 0 + 48, "warning")
        End Try

    End Sub
    '关闭waitform1
    Public Sub wclose()
        Try
            Form3.SplashScreenManager1.CloseWaitForm()
        Catch ex As Exception
            MsgBox("警告：点击过于频繁", 0 + 48, "warning")
            Exit Sub
        End Try

    End Sub
End Class