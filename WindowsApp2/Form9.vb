﻿Public Class Form9

    Private Sub TextBox_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Select Case sender.name
            Case "TextBox1"
                If TextBox1.Text <> "" And IsNumeric(TextBox1.Text) Then
                    PictureBox1.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox1.Image = Form4.ImageList1.Images.Item(1)
                End If
        End Select
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim i As Integer

        '检查textbox1.text数据正确性
        If TextBox1.Text <> "" And IsNumeric(TextBox1.Text) Then
            i = TextBox1.Text
        Else
            MsgBox("警告：报价轮次填写错误", 0 + 48, "warning")
            Exit Sub
        End If
        '检查导入文件格式
        Try
            If Form3.xlscon1.wksheet.GetRow(0).GetCell(1).StringCellValue <> "分标号" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(2).StringCellValue <> "分标名称" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(3).StringCellValue <> "分包号" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(4).StringCellValue <> "应答人" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(5).StringCellValue <> "报价（万元）" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        '确认导入
        If MsgBox("注意：是否确定导入第 " & i & " 轮报价信息", 4 + 32, "caution") = MsgBoxResult.Yes Then
            '判断checkbox删除已存在数据
            If CheckBox1.Checked = True Then
                Call sqlpredel(1)
            Else
                Call sqlpredel(0)
            End If
            '导入数据
            Call dr_baojia()
        End If
    End Sub

    Sub sqlpredel(ByVal i As Integer)
        Dim rou_id As Integer

        Select Case i
            Case 0
                rou_id = TextBox1.Text
                '按轮次删除price_rounds表数据
                Form1.sqlcon1.sqlstr("
                DELETE FROM price_rounds 
                WHERE rouid = " & rou_id & "
                AND preid IN 
                (SELECT price_config.id 
                FROM price_config, factory_config
                WHERE price_config.fngid = factory_config.id 
                AND factory_config.oveid = " & Form3.pro_id & ")"
                                    )
            Case 1
                '按项目删除price_rounds表数据
                Form1.sqlcon1.sqlstr("
                DELETE FROM price_rounds 
                WHERE preid IN 
                (SELECT price_config.id 
                FROM price_config, factory_config
                WHERE price_config.fngid = factory_config.id 
                AND factory_config.oveid = " & Form3.pro_id & ")"
                                    )
                '按项目删除price_config表数据
                Form1.sqlcon1.sqlstr("
                DELETE FROM price_config 
                WHERE id IN
                (SELECT price_config.id 
                FROM price_config, factory_config
                WHERE price_config.fngid = factory_config.id 
                AND factory_config.oveid = " & Form3.pro_id & ")"
                                     )
        End Select

    End Sub

    Sub dr_baojia()
        Dim deep, fng_id, pre_id, i As Integer
        Dim xlsr As Integer

        i = TextBox1.Text
        deep = Form3.xlscon1.wksheet.LastRowNum + 1
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 2 To deep
            '检查cells（xls-1,5）数值正确性
            Try
                If IsNumeric(Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).NumericCellValue) Then
                Else
                    MsgBox("警告：导入文件（6列" & xlsr & "行）格式错误", 0 + 48, "warning")
                    Form1.sqlcon1.sqlstr("END TRANSACTION")
                    Exit Sub
                End If
            Catch ex As Exception
                MsgBox("警告：导入文件（6列" & xlsr & "行）填写错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '获取pkgid
            Try
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM package 
                WHERE pkgidx = " &
                Chr(34) & Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34) & "
                AND bidid = 
                (SELECT id 
                FROM bid 
                WHERE bidname = " &
                Chr(34) & Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue & Chr(34) & "
                AND oveid = " &
                Form3.pro_id & ")", 1
                                    )
                Form1.sqlcon1.dataf.Read()
                Form3.pkg_id = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：导入文件（2-4列" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '获取factory_config_id
            Try
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM factory_config 
                WHERE oveid = " &
                Form3.pro_id & "
                AND pkgid = " &
                Form3.pkg_id & "
                AND facid in 
                (SELECT id 
                FROM factory 
                WHERE facname = " &
                Chr(34) & Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(4).StringCellValue & Chr(34) & ")", 1
                                     )
                Form1.sqlcon1.dataf.Read()
                fng_id = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：导入文件（5列" & xlsr & "行）格式错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '插入更新price_config.couid数据
            Try
                Form1.sqlcon1.sqlstr("
                SELECT couid 
                FROM price_config 
                WHERE fngid = " & fng_id, 1
                                     )
                Form1.sqlcon1.dataf.Read()
                If Form1.sqlcon1.dataf.HasRows Then
                    If Form1.sqlcon1.dataf.Item(0) < i Then
                        Form1.sqlcon1.sqlstr("
                        UPDATE price_config 
                        SET couid = " & i & "
                        WHERE fngid = " & fng_id
                                             )
                    End If
                Else
                    Form1.sqlcon1.sqlstr("
                    INSERT INTO price_config (fngid,couid) 
                    VALUES (" & fng_id & "," & i & ")"
                                         )
                End If
            Catch ex As Exception
                MsgBox("警告：发生未知错误 location:price_config", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '插入更新price_rounds表数据
            Try
                '获取preid
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM price_config 
                WHERE fngid = " & fng_id, 1
                                     )
                Form1.sqlcon1.dataf.Read()
                pre_id = Form1.sqlcon1.dataf.Item(0)
                '插入或更新price_rounds表数据
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM price_rounds 
                WHERE rouid = " & i & "
                AND preid = " & pre_id, 1
                                     )
                Form1.sqlcon1.dataf.Read()
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.sqlstr("
                    UPDATE price_rounds 
                    SET price = " &
                    Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).NumericCellValue & "
                    WHERE rouid = " & i & "
                    AND preid = " & pre_id
                                         )
                Else
                    Form1.sqlcon1.sqlstr("
                    INSERT INTO price_rounds(preid,rouid,price) 
                    VALUES (" & pre_id & "," & i & "," &
                    Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).NumericCellValue & ")"
                                         )
                End If
            Catch ex As Exception
                MsgBox("警告：发生未知错误 location:price_rounds", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        MsgBox("提示：导入文件完成", 0 + 64, "information")
        Me.Close()

    End Sub

    Sub dr_ecp_baojia()
        Dim deep, fng_id, pre_id, i As Integer
        Dim xlsr As Integer
        Dim str As String
        Dim pre As Double

        deep = Form3.xlscon1.wksheet.LastRowNum + 1
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 2 To deep
            '获取pkgid
            Try
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM package 
                WHERE pkgidx = " &
                Chr(34) & Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(5).StringCellValue & Chr(34) & "
                AND bidid = 
                (SELECT id 
                FROM bid 
                WHERE bidname = " &
                Chr(34) & Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(3).StringCellValue & Chr(34) & "
                AND oveid = " &
                Form3.pro_id & ")", 1
                                    )
                Form1.sqlcon1.dataf.Read()
                Form3.pkg_id = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：导入文件（4-6列" & xlsr & "行）数据错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '获取factory_config_id
            Try
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM factory_config 
                WHERE oveid = " &
                Form3.pro_id & "
                AND pkgid = " &
                Form3.pkg_id & "
                AND facid in 
                (SELECT id 
                FROM factory 
                WHERE facname = " &
                Chr(34) & Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(9).StringCellValue & Chr(34) & ")", 1
                                     )
                Form1.sqlcon1.dataf.Read()
                fng_id = Form1.sqlcon1.dataf.Item(0)
            Catch ex As Exception
                MsgBox("警告：导入文件（10列" & xlsr & "行）数据错误", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '插入更新price_config.couid数据
            Try
                str = Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(7).StringCellValue
                i = CInt(str.Substring(1, Len(str) - 2))
                Form1.sqlcon1.sqlstr("
                SELECT couid 
                FROM price_config 
                WHERE fngid = " & fng_id, 1
                                     )
                Form1.sqlcon1.dataf.Read()
                If Form1.sqlcon1.dataf.HasRows Then
                    If Form1.sqlcon1.dataf.Item(0) < i Then
                        Form1.sqlcon1.sqlstr("
                        UPDATE price_config 
                        SET couid = " & i & "
                        WHERE fngid = " & fng_id
                                             )
                    End If
                Else
                    Form1.sqlcon1.sqlstr("
                    INSERT INTO price_config (fngid,couid) 
                    VALUES (" & fng_id & "," & i & ")"
                                         )
                End If
            Catch ex As Exception
                MsgBox("警告：发生未知错误 location:price_config", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
            '插入更新price_rounds表数据
            Try
                '获取preid
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM price_config 
                WHERE fngid = " & fng_id, 1
                                     )
                Form1.sqlcon1.dataf.Read()
                pre_id = Form1.sqlcon1.dataf.Item(0)
                '赋值pre，先总价后折扣率
                If Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(11).StringCellValue = "/" Then
                    If Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(10).StringCellValue = "/" Then
                        Throw New Exception
                    Else
                        str = Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(10).StringCellValue
                        str = str.Substring(0, Len(str) - 1)
                        pre = CDbl(str) / 100
                    End If
                Else
                    str = Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(11).StringCellValue
                    pre = CDbl(str)
                End If
                '插入或更新price_rounds表数据
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM price_rounds 
                WHERE rouid = " & i & "
                AND preid = " & pre_id, 1
                                     )
                Form1.sqlcon1.dataf.Read()
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.sqlstr("
                    UPDATE price_rounds 
                    SET price = " &
                    pre & "
                    WHERE rouid = " & i & "
                    AND preid = " & pre_id
                                         )
                Else
                    Form1.sqlcon1.sqlstr("
                    INSERT INTO price_rounds(preid,rouid,price) 
                    VALUES (" & pre_id & "," & i & "," &
                    pre & ")"
                                         )
                End If
            Catch ex As Exception
                MsgBox("警告：发生未知错误 location:price_data", 0 + 48, "warning")
                Form1.sqlcon1.sqlstr("END TRANSACTION")
                Exit Sub
            End Try
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        MsgBox("提示：导入文件完成", 0 + 64, "information")
        Me.Close()

    End Sub

    Private Sub Form9_Load(sender As Object, e As EventArgs) Handles Me.Load

        TextBox1.Text = ""
        CheckBox1.Checked = False

    End Sub

End Class