﻿
Imports DevExpress.XtraEditors.Controls
Imports DevExpress.XtraGrid.Views.Base
Imports Aspose.Words

Public Class Form10

    Private Sub Form10_Load(sender As Object, e As EventArgs) Handles Me.Load
        Dim dr As DataRow

        '检查bidoc_main表
        'mb-采购文件模板,ht-合同文本,cp-初评,xp-详评,jg-价格,chag-固定服务费,cou_gf-技术规范数量
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bidoc_main(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        oveid   integer                 NOT NULL,
        pkgid   integer                 NOT NULL,
        mbid    integer                 NOT NULL,
        htid    integer                 NOT NULL,
        swcpid  integer                 NOT NULL,
        jscpid  integer                 NOT NULL,
        swxpid  integer                 NOT NULL,
        jsxpid  integer                 NOT NULL,
        jgid    integer                 NOT NULL,
        chag    numeric                 NOT NULL,
        cou_gf  integer                 NOT NULL
        );")
        '检查bidoc_mb,bidoc_ht,bidoc_swcp,bidoc_jscp,bidoc_swxp,bidoc_jsxp,bidoc_jg表
        'na-文件名,cou_pd判断采购文件属性预留接口
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bidoc_mb(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        char_na varchar(255)            NOT NULL,
        cou_pd  integer                 NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bidoc_ht(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        char_na varchar(255)            NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bidoc_swcp(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        char_na varchar(255)            NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bidoc_jscp(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        char_na varchar(255)            NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bidoc_swxp(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        char_na varchar(255)            NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bidoc_jsxp(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        char_na varchar(255)            NOT NULL
        );")
        Form1.sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS bidoc_jg(
        id      integer     PRIMARY KEY AUTOINCREMENT,
        char_na varchar(255)            NOT NULL
        );")
        '检查bidoc_main表oveid,pkgid
        Form1.sqlcon1.sqlstr("
        SELECT package.id
        FROM package,bid,overload
        WHERE overload.id = " & Form3.pro_id & "
        AND bid.oveid = overload.id
        AND bid.id = package.bidid
        ORDER BY package.id", 2)
        If Form1.sqlcon1.datas.Tables(0).Rows.Count > 0 Then
            For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                Form1.sqlcon1.sqlstr("
                INSERT INTO [bidoc_main]
                ([oveid], [pkgid],[mbid],[htid],
                [swcpid],[jscpid],[swxpid],[jsxpid],[jgid],[chag],[cou_gf])
                SELECT " &
                Form3.pro_id & "," &
                dr.Item(0) & "," &
                Chr(34) & Chr(34) & "," &
                Chr(34) & Chr(34) & "," &
                Chr(34) & Chr(34) & "," &
                Chr(34) & Chr(34) & "," &
                Chr(34) & Chr(34) & "," &
                Chr(34) & Chr(34) & "," &
                Chr(34) & Chr(34) & "," &
                Chr(34) & Chr(34) & "," &
                Chr(34) & Chr(34) & "
                WHERE NOT EXISTS
                (SELECT id
                FROM [bidoc_main]
                WHERE [oveid] = " & Form3.pro_id & "
                AND [pkgid] = " & dr.Item(0) & ")")
            Next
        End If
    End Sub

    Sub ButtonEdit1_buttonclick(sender As Object, e As EventArgs) Handles ButtonEdit1.ButtonClick

        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            ButtonEdit1.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Sub ButtonEdit2_buttonclick(sender As Object, e As EventArgs) Handles ButtonEdit2.ButtonClick

        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            ButtonEdit2.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub TextBox_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged,
    TextBox2.TextChanged, TextBox3.TextChanged, TextBox4.TextChanged, TextBox5.TextChanged

        Select Case sender.name
            Case "TextBox1"
                If TextBox1.Text <> "" Then
                    PictureBox1.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox1.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox2"
                If TextBox2.Text <> "" Then
                    PictureBox2.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox2.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox3"
                If TextBox3.Text <> "" Then
                    PictureBox3.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox3.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox4"
                If TextBox4.Text <> "" Then
                    PictureBox4.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox4.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox5"
                If TextBox5.Text <> "" Then
                    PictureBox5.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox5.Image = Form4.ImageList1.Images.Item(1)
                End If
        End Select
    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        Dim i, j, j2 As Integer
        Dim sli As New SortedList
        Dim myfile As String
        Dim dr As DataRow

        '写入sli
        sli.Clear()
        sli.Add("1", "1.采购文件模板")
        sli.Add("2", "2.合同文本")
        sli.Add("3", "3.初评模板")
        sli.Add("4", "4.详评模板")
        sli.Add("5", "5.价格模板")
        sli.Add("6", "6.技术规范书")
        '检查textbox
        If TextBox1.Text = "" Or TextBox2.Text = "" Or TextBox3.Text = "" Or
            TextBox4.Text = "" Or TextBox5.Text = "" Then
            MsgBox("警告：文件基本信息不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '检查buttonedit
        If ButtonEdit1.Text = "" Then
            MsgBox("警告：文件导出路径不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '初始化repcombobox.item
        RepositoryItemComboBox1.Items.Clear()
        RepositoryItemComboBox2.Items.Clear()
        RepositoryItemComboBox3.Items.Clear()
        RepositoryItemComboBox4.Items.Clear()
        RepositoryItemComboBox5.Items.Clear()
        RepositoryItemComboBox6.Items.Clear()
        RepositoryItemComboBox7.Items.Clear()
        '检查路径下文件夹
        For i = 1 To 6
            If Not Dir(ButtonEdit1.Text & "\" & sli(CStr(i)), vbDirectory) <> "" Then
                If MsgBox("注意：是否新建<" & sli(CStr(i)) & ">文件夹", 4 + 32, "caution") = MsgBoxResult.Yes Then
                    Select Case i
                        Case 1
                            '新建1.采购文件模板文件夹
                            MkDir(ButtonEdit1.Text & "\" & sli(CStr(i)))
                        Case 2
                            '新建2.合同文本文件夹
                            MkDir(ButtonEdit1.Text & "\" & sli(CStr(i)))
                        Case 3
                            '新建3.初评模板文件夹
                            MkDir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\商务初评模板")
                            MkDir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\技术初评模板")
                        Case 4
                            '新建4.详评模板文件夹
                            MkDir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\商务详评模板")
                            MkDir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\技术详评模板")
                        Case 5
                            '新建5.价格模板文件夹
                            MkDir(ButtonEdit1.Text & "\" & sli(CStr(i)))
                        Case 6
                            '新建6.技术规范书分包文件夹
                            Form1.sqlcon1.sqlstr("
                            SELECT bid.bididx,bid.bidname,package.pkgidx
                            FROM bid,package
                            WHERE bid.oveid = " & Form3.pro_id & "
                            AND package.bidid = bid.id
                            ORDER BY bid.id,package.id", 1)
                            While Form1.sqlcon1.dataf.Read()
                                MkDir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\" &
                                      Form1.sqlcon1.dataf.Item(0).ToString &
                                      Form1.sqlcon1.dataf.Item(1).ToString &
                                      Form1.sqlcon1.dataf.Item(2).ToString)
                            End While
                    End Select
                Else
                    '选择不新建文件夹退出
                    Exit Sub
                End If
            End If
            '检查是否存在文件
            j = 0
            j2 = 0
            Select Case i
                Case 1
                    myfile = Dir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\*.docx")
                    Do While myfile <> ""
                        '不存在插入bidoc_mb表
                        Form1.sqlcon1.sqlstr("
                        INSERT INTO [bidoc_mb]
                        ([char_na], [cou_pd])
                        SELECT " &
                        Chr(34) & myfile & Chr(34) & "," &
                        1 & "
                        WHERE NOT EXISTS
                        (SELECT id
                        FROM [bidoc_mb]
                        WHERE [char_na] = " &
                        Chr(34) & myfile & Chr(34) & ")")
                        '添加repcombobox.item
                        RepositoryItemComboBox1.Items.Add(myfile)
                        myfile = Dir()
                        j += 1
                    Loop
                    If j <> 0 Then
                        PictureBox7.Image = Form4.ImageList1.Images.Item(3)
                        Application.DoEvents()
                    Else
                        MsgBox("警告：" & sli(CStr(i)) & "不存在文件", 0 + 48, "warning")
                        Exit Sub
                    End If
                Case 2
                    myfile = Dir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\*.docx")
                    Do While myfile <> ""
                        '不存在插入bidoc_ht表
                        Form1.sqlcon1.sqlstr("
                        INSERT INTO [bidoc_ht]
                        ([char_na])
                        SELECT " &
                        Chr(34) & myfile & Chr(34) & "
                        WHERE NOT EXISTS
                        (SELECT id
                        FROM [bidoc_ht]
                        WHERE [char_na] = " &
                        Chr(34) & myfile & Chr(34) & ")")
                        '添加repcombobox.item
                        RepositoryItemComboBox2.Items.Add(myfile)
                        myfile = Dir()
                        j += 1
                    Loop
                    If j <> 0 Then
                        PictureBox8.Image = Form4.ImageList1.Images.Item(3)
                        Application.DoEvents()
                    Else
                        MsgBox("警告：" & sli(CStr(i)) & "不存在文件", 0 + 48, "warning")
                        Exit Sub
                    End If
                Case 3
                    '商务初评模板
                    myfile = Dir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\商务初评模板\*.docx")
                    Do While myfile <> ""
                        '不存在插入bidoc_swcp表
                        Form1.sqlcon1.sqlstr("
                        INSERT INTO [bidoc_swcp]
                        ([char_na])
                        SELECT " &
                        Chr(34) & myfile & Chr(34) & "
                        WHERE NOT EXISTS
                        (SELECT id
                        FROM [bidoc_swcp]
                        WHERE [char_na] = " &
                        Chr(34) & myfile & Chr(34) & ")")
                        '添加repcombobox.item
                        RepositoryItemComboBox3.Items.Add(myfile)
                        myfile = Dir()
                        j += 1
                    Loop
                    '技术初评模板
                    myfile = Dir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\技术初评模板\*.docx")
                    Do While myfile <> ""
                        '不存在插入bidoc_swcp表
                        Form1.sqlcon1.sqlstr("
                        INSERT INTO [bidoc_jscp]
                        ([char_na])
                        SELECT " &
                        Chr(34) & myfile & Chr(34) & "
                        WHERE NOT EXISTS
                        (SELECT id
                        FROM [bidoc_jscp]
                        WHERE [char_na] = " &
                        Chr(34) & myfile & Chr(34) & ")")
                        '添加repcombobox.item
                        RepositoryItemComboBox4.Items.Add(myfile)
                        myfile = Dir()
                        j2 += 1
                    Loop
                    If j <> 0 And j2 <> 0 Then
                        PictureBox9.Image = Form4.ImageList1.Images.Item(3)
                        Application.DoEvents()
                    Else
                        MsgBox("警告：" & sli(CStr(i)) & "不存在文件", 0 + 48, "warning")
                        Exit Sub
                    End If
                Case 4
                    '商务详评模板
                    myfile = Dir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\商务详评模板\*.docx")
                    Do While myfile <> ""
                        '不存在插入bidoc_swxp表
                        Form1.sqlcon1.sqlstr("
                        INSERT INTO [bidoc_swxp]
                        ([char_na])
                        SELECT " &
                        Chr(34) & myfile & Chr(34) & "
                        WHERE NOT EXISTS
                        (SELECT id
                        FROM [bidoc_swxp]
                        WHERE [char_na] = " &
                        Chr(34) & myfile & Chr(34) & ")")
                        '添加repcombobox.item
                        RepositoryItemComboBox5.Items.Add(myfile)
                        myfile = Dir()
                        j += 1
                    Loop
                    '技术详评模板
                    myfile = Dir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\技术详评模板\*.docx")
                    Do While myfile <> ""
                        '不存在插入bidoc_jsxp表
                        Form1.sqlcon1.sqlstr("
                        INSERT INTO [bidoc_jsxp]
                        ([char_na])
                        SELECT " &
                        Chr(34) & myfile & Chr(34) & "
                        WHERE NOT EXISTS
                        (SELECT id
                        FROM [bidoc_jsxp]
                        WHERE [char_na] = " &
                        Chr(34) & myfile & Chr(34) & ")")
                        '添加repcombobox.item
                        RepositoryItemComboBox6.Items.Add(myfile)
                        myfile = Dir()
                        j2 += 1
                    Loop
                    If j <> 0 And j2 <> 0 Then
                        PictureBox10.Image = Form4.ImageList1.Images.Item(3)
                        Application.DoEvents()
                    Else
                        MsgBox("警告：" & sli(CStr(i)) & "不存在文件", 0 + 48, "warning")
                        Exit Sub
                    End If
                Case 5
                    '价格模板
                    myfile = Dir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\*.docx")
                    Do While myfile <> ""
                        '不存在插入bidoc_jg表
                        Form1.sqlcon1.sqlstr("
                        INSERT INTO [bidoc_jg]
                        ([char_na])
                        SELECT " &
                        Chr(34) & myfile & Chr(34) & "
                        WHERE NOT EXISTS
                        (SELECT id
                        FROM [bidoc_jg]
                        WHERE [char_na] = " &
                        Chr(34) & myfile & Chr(34) & ")")
                        '添加repcombobox.item
                        RepositoryItemComboBox7.Items.Add(myfile)
                        myfile = Dir()
                        j += 1
                    Loop
                    If j <> 0 Then
                        PictureBox11.Image = Form4.ImageList1.Images.Item(3)
                        Application.DoEvents()
                    Else
                        MsgBox("警告：" & sli(CStr(i)) & "不存在文件", 0 + 48, "warning")
                        Exit Sub
                    End If
                Case 6
                    'sql刷新bidoc_main.cou_gf
                    Form1.sqlcon1.sqlstr("
                    SELECT bid.bididx,bid.bidname,package.pkgidx,package.id
                    FROM bid,package
                    WHERE bid.oveid = " & Form3.pro_id & "
                    AND package.bidid = bid.id
                    ORDER BY bid.id,package.id", 2)
                    For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
                        j = 0
                        myfile = Dir(ButtonEdit1.Text & "\" & sli(CStr(i)) & "\" &
                                 dr.Item(0) & dr.Item(1) & dr.Item(2) & "\*.doc*")
                        Do While myfile <> ""
                            myfile = Dir()
                            j += 1
                        Loop
                        Form1.sqlcon1.sqlstr("
                        UPDATE bidoc_main
                        SET cou_gf = " & j & "
                        WHERE oveid = " & Form3.pro_id & "
                        AND pkgid = " & dr.Item(3))
                    Next
                    PictureBox12.Image = Form4.ImageList1.Images.Item(3)
                    Application.DoEvents()
            End Select
        Next
        If MsgBox("注意：是否确认采购文件基本信息", 4 + 32, "caution") = MsgBoxResult.Yes Then
            '显示waitform1
            Form3.wf1.wshow()
            Application.DoEvents()
            'sql获取bidoc_main表数据
            Form1.sqlcon1.sqlstr("
            SELECT 
            tb1.bididx,tb1.bidname,tb1.bidno,tb1.pkgidx,tb1.pkgname,
            bidoc_mb.char_na,bidoc_ht.char_na,
            bidoc_swcp.char_na,bidoc_jscp.char_na,
            bidoc_swxp.char_na,bidoc_jsxp.char_na,
            bidoc_jg.char_na,
            tb1.chag,tb1.cou_gf
            FROM
            (SELECT
            bid.bididx,
            bid.bidname,
            bid.bidno,
            package.pkgidx,
            package.pkgname,
            bidoc_main.mbid,
            bidoc_main.htid,
            bidoc_main.swcpid,
            bidoc_main.jscpid,
            bidoc_main.swxpid,
            bidoc_main.jsxpid,
            bidoc_main.jgid,
            bidoc_main.chag,
            bidoc_main.cou_gf
            FROM 
            bid,package,bidoc_main
            WHERE bidoc_main.oveid = " & Form3.pro_id & "
            AND bid.oveid = bidoc_main.oveid
            AND bid.id = package.bidid
            AND package.id = bidoc_main.pkgid
            ORDER BY bid.id,package.id) as tb1
            LEFT JOIN bidoc_mb ON tb1.mbid = bidoc_mb.id
            LEFT JOIN bidoc_ht ON tb1.htid = bidoc_ht.id
            LEFT JOIN bidoc_swcp ON tb1.swcpid = bidoc_swcp.id
            LEFT JOIN bidoc_jscp ON tb1.jscpid = bidoc_jscp.id
            LEFT JOIN bidoc_swxp ON tb1.swxpid = bidoc_swxp.id
            LEFT JOIN bidoc_jsxp ON tb1.jsxpid = bidoc_jsxp.id
            LEFT JOIN bidoc_jg ON tb1.jgid = bidoc_jg.id", 2)
            GridControl1.DataSource = Form1.sqlcon1.datas.Tables(0)
            '设定列名称
            GridView1.Columns(0).Caption = "分标号"
            GridView1.Columns(1).Caption = "分标名称"
            GridView1.Columns(2).Caption = "分标编号"
            GridView1.Columns(3).Caption = "分包号"
            GridView1.Columns(4).Caption = "分包名称"
            GridView1.Columns(5).Caption = "采购文件模板"
            GridView1.Columns(6).Caption = "合同文本"
            GridView1.Columns(7).Caption = "商务初评模板"
            GridView1.Columns(8).Caption = "技术初评模板"
            GridView1.Columns(9).Caption = "商务详评模板"
            GridView1.Columns(10).Caption = "技术详评模板"
            GridView1.Columns(11).Caption = "价格模板"
            GridView1.Columns(12).Caption = "固定服务费"
            GridView1.Columns(13).Caption = "技术规范数量"
            '设置列格式
            GridView1.BestFitColumns()
            '设置列编辑性
            For i = 0 To 4
                GridView1.Columns(i).OptionsColumn.AllowEdit = False
            Next
            GridView1.Columns(13).OptionsColumn.AllowEdit = False
            '添加columnedit
            GridView1.Columns(5).ColumnEdit = RepositoryItemComboBox1
            GridView1.Columns(6).ColumnEdit = RepositoryItemComboBox2
            GridView1.Columns(7).ColumnEdit = RepositoryItemComboBox3
            GridView1.Columns(8).ColumnEdit = RepositoryItemComboBox4
            GridView1.Columns(9).ColumnEdit = RepositoryItemComboBox5
            GridView1.Columns(10).ColumnEdit = RepositoryItemComboBox6
            GridView1.Columns(11).ColumnEdit = RepositoryItemComboBox7
            '卸载waitform1
            Form3.wf1.wclose()
            '初始化buttonedit2
            ButtonEdit2.Text = ButtonEdit1.Text
            Label13.Visible = False
            '显示groupbox2
            GroupBox2.Visible = True
        Else
            Exit Sub
        End If
    End Sub

    Private Sub PictureBox16_Click(sender As Object, e As EventArgs) Handles PictureBox16.Click

        '初始化picturebox
        PictureBox7.Image = Form4.ImageList1.Images.Item(2)
        PictureBox8.Image = Form4.ImageList1.Images.Item(2)
        PictureBox9.Image = Form4.ImageList1.Images.Item(2)
        PictureBox10.Image = Form4.ImageList1.Images.Item(2)
        PictureBox11.Image = Form4.ImageList1.Images.Item(2)
        PictureBox12.Image = Form4.ImageList1.Images.Item(2)
        '显示groupbox1
        GroupBox2.Visible = False
        GroupBox1.Visible = True
    End Sub

    Private Sub RepositoryItemComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RepositoryItemComboBox1.SelectedIndexChanged
        Dim i, j As Integer
        Dim str As String

        Try
            '获取选中值
            str = RepositoryItemComboBox1.Items.Item(sender.SelectedIndex).ToString
            '获取bidoc_mb.id为j
            Form1.sqlcon1.sqlstr("
            SELECT bidoc_mb.id
            FROM bidoc_mb
            WHERE bidoc_mb.char_na = " & Chr(34) & str & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            j = Form1.sqlcon1.dataf.Item(0)
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:bidoc_mb", 0 + 48, "warning")
            Exit Sub
        End Try
        '获取pkgid为i
        Form1.sqlcon1.sqlstr("
        SELECT package.id
        FROM bid,package
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.bidname = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(1).ToString & Chr(34) & "
        AND package.pkgidx = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(3).ToString & Chr(34) & "
        AND bid.id = package.bidid", 1)
        Form1.sqlcon1.dataf.Read()
        i = Form1.sqlcon1.dataf.Item(0)
        '刷新bidoc_main.mbid
        Form1.sqlcon1.sqlstr("
        UPDATE bidoc_main
        SET mbid = " & j & "
        WHERE oveid = " & Form3.pro_id & "
        AND pkgid = " & i)
    End Sub

    Private Sub RepositoryItemComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RepositoryItemComboBox2.SelectedIndexChanged
        Dim i, j As Integer
        Dim str As String

        Try
            '获取选中值
            str = RepositoryItemComboBox2.Items.Item(sender.SelectedIndex).ToString
            '获取bidoc_ht.id为j
            Form1.sqlcon1.sqlstr("
            SELECT bidoc_ht.id
            FROM bidoc_ht
            WHERE bidoc_ht.char_na = " & Chr(34) & str & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            j = Form1.sqlcon1.dataf.Item(0)
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:bidoc_ht", 0 + 48, "warning")
            Exit Sub
        End Try
        '获取pkgid为i
        Form1.sqlcon1.sqlstr("
        SELECT package.id
        FROM bid,package
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.bidname = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(1).ToString & Chr(34) & "
        AND package.pkgidx = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(3).ToString & Chr(34) & "
        AND bid.id = package.bidid", 1)
        Form1.sqlcon1.dataf.Read()
        i = Form1.sqlcon1.dataf.Item(0)
        '刷新bidoc_main.htid
        Form1.sqlcon1.sqlstr("
        UPDATE bidoc_main
        SET htid = " & j & "
        WHERE oveid = " & Form3.pro_id & "
        AND pkgid = " & i)
    End Sub

    Private Sub RepositoryItemComboBox3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RepositoryItemComboBox3.SelectedIndexChanged
        Dim i, j As Integer
        Dim str As String

        Try
            '获取选中值
            str = RepositoryItemComboBox3.Items.Item(sender.SelectedIndex).ToString
            '获取bidoc_swcp.id为j
            Form1.sqlcon1.sqlstr("
            SELECT bidoc_swcp.id
            FROM bidoc_swcp
            WHERE bidoc_swcp.char_na = " & Chr(34) & str & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            j = Form1.sqlcon1.dataf.Item(0)
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:bidoc_swcp", 0 + 48, "warning")
            Exit Sub
        End Try
        '获取pkgid为i
        Form1.sqlcon1.sqlstr("
        SELECT package.id
        FROM bid,package
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.bidname = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(1).ToString & Chr(34) & "
        AND package.pkgidx = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(3).ToString & Chr(34) & "
        AND bid.id = package.bidid", 1)
        Form1.sqlcon1.dataf.Read()
        i = Form1.sqlcon1.dataf.Item(0)
        '刷新bidoc_main.swcpid
        Form1.sqlcon1.sqlstr("
        UPDATE bidoc_main
        SET swcpid = " & j & "
        WHERE oveid = " & Form3.pro_id & "
        AND pkgid = " & i)
    End Sub

    Private Sub RepositoryItemComboBox4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RepositoryItemComboBox4.SelectedIndexChanged
        Dim i, j As Integer
        Dim str As String

        Try
            '获取选中值
            str = RepositoryItemComboBox4.Items.Item(sender.SelectedIndex).ToString
            '获取bidoc_jscp.id为j
            Form1.sqlcon1.sqlstr("
            SELECT bidoc_jscp.id
            FROM bidoc_jscp
            WHERE bidoc_jscp.char_na = " & Chr(34) & str & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            j = Form1.sqlcon1.dataf.Item(0)
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:bidoc_jscp", 0 + 48, "warning")
            Exit Sub
        End Try
        '获取pkgid为i
        Form1.sqlcon1.sqlstr("
        SELECT package.id
        FROM bid,package
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.bidname = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(1).ToString & Chr(34) & "
        AND package.pkgidx = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(3).ToString & Chr(34) & "
        AND bid.id = package.bidid", 1)
        Form1.sqlcon1.dataf.Read()
        i = Form1.sqlcon1.dataf.Item(0)
        '刷新bidoc_main.jscpid
        Form1.sqlcon1.sqlstr("
        UPDATE bidoc_main
        SET jscpid = " & j & "
        WHERE oveid = " & Form3.pro_id & "
        AND pkgid = " & i)
    End Sub

    Private Sub RepositoryItemComboBox5_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RepositoryItemComboBox5.SelectedIndexChanged
        Dim i, j As Integer
        Dim str As String

        Try
            '获取选中值
            str = RepositoryItemComboBox5.Items.Item(sender.SelectedIndex).ToString
            '获取bidoc_swxp.id为j
            Form1.sqlcon1.sqlstr("
            SELECT bidoc_swxp.id
            FROM bidoc_swxp
            WHERE bidoc_swxp.char_na = " & Chr(34) & str & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            j = Form1.sqlcon1.dataf.Item(0)
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:bidoc_swxp", 0 + 48, "warning")
            Exit Sub
        End Try
        '获取pkgid为i
        Form1.sqlcon1.sqlstr("
        SELECT package.id
        FROM bid,package
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.bidname = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(1).ToString & Chr(34) & "
        AND package.pkgidx = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(3).ToString & Chr(34) & "
        AND bid.id = package.bidid", 1)
        Form1.sqlcon1.dataf.Read()
        i = Form1.sqlcon1.dataf.Item(0)
        '刷新bidoc_main.swxpid
        Form1.sqlcon1.sqlstr("
        UPDATE bidoc_main
        SET swxpid = " & j & "
        WHERE oveid = " & Form3.pro_id & "
        AND pkgid = " & i)
    End Sub

    Private Sub RepositoryItemComboBox6_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RepositoryItemComboBox6.SelectedIndexChanged
        Dim i, j As Integer
        Dim str As String

        Try
            '获取选中值
            str = RepositoryItemComboBox6.Items.Item(sender.SelectedIndex).ToString
            '获取bidoc_jsxp.id为j
            Form1.sqlcon1.sqlstr("
            SELECT bidoc_jsxp.id
            FROM bidoc_jsxp
            WHERE bidoc_jsxp.char_na = " & Chr(34) & str & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            j = Form1.sqlcon1.dataf.Item(0)
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:bidoc_jsxp", 0 + 48, "warning")
            Exit Sub
        End Try
        '获取pkgid为i
        Form1.sqlcon1.sqlstr("
        SELECT package.id
        FROM bid,package
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.bidname = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(1).ToString & Chr(34) & "
        AND package.pkgidx = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(3).ToString & Chr(34) & "
        AND bid.id = package.bidid", 1)
        Form1.sqlcon1.dataf.Read()
        i = Form1.sqlcon1.dataf.Item(0)
        '刷新bidoc_main.swxpid
        Form1.sqlcon1.sqlstr("
        UPDATE bidoc_main
        SET jsxpid = " & j & "
        WHERE oveid = " & Form3.pro_id & "
        AND pkgid = " & i)
    End Sub

    Private Sub RepositoryItemComboBox7_SelectedIndexChanged(sender As Object, e As EventArgs) Handles RepositoryItemComboBox7.SelectedIndexChanged
        Dim i, j As Integer
        Dim str As String

        Try
            '获取选中值
            str = RepositoryItemComboBox7.Items.Item(sender.SelectedIndex).ToString
            '获取bidoc_jg.id为j
            Form1.sqlcon1.sqlstr("
            SELECT bidoc_jg.id
            FROM bidoc_jg
            WHERE bidoc_jg.char_na = " & Chr(34) & str & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            j = Form1.sqlcon1.dataf.Item(0)
        Catch ex As Exception
            MsgBox("警告：发生未知错误 location:bidoc_jg", 0 + 48, "warning")
            Exit Sub
        End Try
        '获取pkgid为i
        Form1.sqlcon1.sqlstr("
        SELECT package.id
        FROM bid,package
        WHERE bid.oveid = " & Form3.pro_id & "
        AND bid.bidname = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(1).ToString & Chr(34) & "
        AND package.pkgidx = " &
        Chr(34) & GridView1.GetFocusedDataRow.Item(3).ToString & Chr(34) & "
        AND bid.id = package.bidid", 1)
        Form1.sqlcon1.dataf.Read()
        i = Form1.sqlcon1.dataf.Item(0)
        '刷新bidoc_main.jgid
        Form1.sqlcon1.sqlstr("
        UPDATE bidoc_main
        SET jgid = " & j & "
        WHERE oveid = " & Form3.pro_id & "
        AND pkgid = " & i)
    End Sub

    Private Sub GridView1_CellValueChanged(sender As Object, e As CellValueChangedEventArgs) Handles GridView1.CellValueChanged
        Dim i As Integer

        If GridView1.FocusedColumn.Caption = "固定服务费" Then
            '获取pkgid为i
            Form1.sqlcon1.sqlstr("
            SELECT package.id
            FROM bid,package
            WHERE bid.oveid = " & Form3.pro_id & "
            AND bid.bidname = " &
            Chr(34) & GridView1.GetFocusedDataRow.Item(1).ToString & Chr(34) & "
            AND package.pkgidx = " &
            Chr(34) & GridView1.GetFocusedDataRow.Item(3).ToString & Chr(34) & "
            AND bid.id = package.bidid", 1)
            Form1.sqlcon1.dataf.Read()
            i = Form1.sqlcon1.dataf.Item(0)
            '刷新bidoc_main.chag
            Form1.sqlcon1.sqlstr("
            UPDATE bidoc_main
            SET chag = " & GridView1.GetFocusedDataRow.Item(12) & "
            WHERE oveid = " & Form3.pro_id & "
            AND pkgid = " & i)
        End If
    End Sub

    '采购文件导出
    Private Sub PictureBox13_Click(sender As Object, e As EventArgs) Handles PictureBox13.Click
        Dim r As Integer()
        Dim i As Integer
        Dim str, n As String

        i = 0
        '判断gridview选中,无选中提示
        r = GridView1.GetSelectedRows()
        For Each item In r
            i += 1
        Next
        If i = 0 Then
            MsgBox("警告：未选择分包", 0 + 48, "warning")
            Exit Sub
        End If
        '验证buttonedit2.text
        If ButtonEdit2.Text = "" Then
            MsgBox("警告：文件导出路径不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '判断确定生成
        If MsgBox("注意：是否生成选定分包采购文件", 4 + 32, "caution") = MsgBoxResult.No Then
            Exit Sub
        End If
        Label13.Visible = True
        '验证路径\采购文件，不存在新建
        If Not Dir(ButtonEdit2.Text & "\采购文件", vbDirectory) <> "" Then
            MkDir(ButtonEdit2.Text & "\采购文件")
        End If
        '显示waitform1
        Form3.wf1.wshow()
        '生成采购文件
        For Each item In r
            '修改label13
            Label13.Text = ">正在检查 " &
                GridView1.GetDataRow(item).Item(0).ToString & " " &
                GridView1.GetDataRow(item).Item(1).ToString & " " &
                GridView1.GetDataRow(item).Item(3).ToString & " 文件路径"
            Application.DoEvents()
            '判断判断分标路径
            str = GridView1.GetDataRow(item).Item(1).ToString
            If Not Dir(ButtonEdit2.Text & "\采购文件\" & str, vbDirectory) <> "" Then
                Try
                    MkDir(ButtonEdit2.Text & "\采购文件\" & str)
                Catch ex As Exception
                    MsgBox("警告：创建分标路径失败", 0 + 48, "warning")
                    Form3.wf1.wclose()
                    Exit Sub
                End Try
            End If
            '判断分包路径
            str = GridView1.GetDataRow(item).Item(3).ToString
            n = CInt(str.Substring(1, Len(str) - 1)).ToString
            If Not Dir(ButtonEdit2.Text & "\采购文件\" &
                       GridView1.GetDataRow(item).Item(1).ToString & "\" &
                        n, vbDirectory) <> "" Then
                Try
                    MkDir(ButtonEdit2.Text & "\采购文件\" &
                          GridView1.GetDataRow(item).Item(1).ToString & "\" &
                          n)
                Catch ex As Exception
                    MsgBox("警告：创建分包路径失败", 0 + 48, "warning")
                    Form3.wf1.wclose()
                    Exit Sub
                End Try
            End If
            Call dc_bidoc(item)
            If Label13.Text = ">生成文件路径：" & ButtonEdit2.Text & "\采购文件" Then
            Else
                '卸载waitform1
                Form3.wf1.wclose()
                Exit Sub
            End If
        Next
        '卸载waitform1
        Form3.wf1.wclose()
        MsgBox("提示：已导出指定文件", 0 + 64, "information")
    End Sub

    Private Sub PictureBox15_Click(sender As Object, e As EventArgs) Handles PictureBox15.Click

        '显示waitform1
        Form3.wf1.wshow()
        '隐藏gridcontrol
        GridControl1.Visible = False
        '调用刷新gridview
        Call sqlup_gv()
        '显示gridcontrol
        GridControl1.Visible = True
        '卸载waitform1
        Form3.wf1.wclose()
    End Sub

    Private Sub PictureBox14_Click(sender As Object, e As EventArgs) Handles PictureBox14.Click

        '显示waitform1
        Form3.wf1.wshow()
        If MsgBox("注意：是否清除已有数据并导入", 4 + 32, "caution") = MsgBoxResult.No Then
            '卸载waitform1
            Form3.wf1.wclose()
            Exit Sub
        End If
        '选择导入后执行
        Form3.OpenFileDialog1.Filter = "Excel File (*.xls;*.xlsx)|*.xls;*.xlsx"
        Form3.OpenFileDialog1.ShowDialog()
        If Form3.OpenFileDialog1.FileName <> "" Then
            Try
                '连接excel（文件名，sheet序号）
                Form3.xlscon1.str(Form3.OpenFileDialog1.FileName, 1)
            Catch ex As Exception
                '连接文件已打开报错
                MsgBox("警告：发生未知错误 location:xlscon", 0 + 48, "warning")
                '卸载waitform1
                Form3.wf1.wclose()
                Exit Sub
            End Try
            '调用采购文件信息导入
            Call Dr_cgwjhuizong()
            '初始化OpenFileDialog1.FileName
            Form3.OpenFileDialog1.FileName = ""
        End If
        '卸载waitform1
        Form3.wf1.wclose()
    End Sub

    Sub Dr_cgwjhuizong()
        Dim deep, i, j As Integer
        Dim xlsr, xlsc As Integer
        Dim sli As New SortedList
        Dim str As String

        '验证1-9行标题名称
        Try
            If Form3.xlscon1.wksheet.GetRow(0).GetCell(0).StringCellValue <> "分标号" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(1).StringCellValue <> "分标名称" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(2).StringCellValue <> "分包号" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(3).StringCellValue <> "分包名称" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(4).StringCellValue <> "采购文件模板" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(5).StringCellValue <> "合同模板" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(6).StringCellValue <> "商务初评模板" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(7).StringCellValue <> "技术初评模板" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(8).StringCellValue <> "商务详评模板" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(9).StringCellValue <> "技术详评模板" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(10).StringCellValue <> "价格模板" Or
               Form3.xlscon1.wksheet.GetRow(0).GetCell(11).StringCellValue <> "固定服务费（万元）" Then
                MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("警告：导入文件格式错误", 0 + 48, "warning")
            Exit Sub
        End Try
        'sql写入文件
        deep = Form3.xlscon1.wksheet.LastRowNum + 1
        Form1.sqlcon1.sqlstr("BEGIN TRANSACTION")
        For xlsr = 2 To deep
            '判断1-4列是否存在数据
            For xlsc = 1 To 4
                Try
                    If Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).CellType = NPOI.SS.UserModel.CellType.Blank Then
                        MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                        Form1.sqlcon1.sqlstr("END TRANSACTION")
                        Exit Sub
                    End If
                Catch ex As Exception
                    MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）格式错误", 0 + 48, "warning")
                    Form1.sqlcon1.sqlstr("END TRANSACTION")
                    Exit Sub
                End Try
            Next
            '获取pkgid为i
            Form1.sqlcon1.sqlstr("
            SELECT package.id
            FROM bid,package
            WHERE bid.oveid = " & Form3.pro_id & "
            AND bid.bidname = " &
            Chr(34) & Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(1).StringCellValue.ToString & Chr(34) & "
            AND package.pkgidx = " &
            Chr(34) & Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(2).StringCellValue.ToString & Chr(34) & "
            AND bid.id = package.bidid", 1)
            Form1.sqlcon1.dataf.Read()
            i = Form1.sqlcon1.dataf.Item(0)
            For xlsc = 5 To 12
                sli.Clear()
                sli.Add("0", "mb")
                sli.Add("1", "ht")
                sli.Add("2", "swcp")
                sli.Add("3", "jscp")
                sli.Add("4", "swxp")
                sli.Add("5", "jsxp")
                sli.Add("6", "jg")
                Try
                    '判断单元格为空
                    If Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).CellType = NPOI.SS.UserModel.CellType.Blank Then
                        Select Case xlsc
                            Case 12
                                '刷新bidoc_main表
                                Form1.sqlcon1.sqlstr("
                                UPDATE bidoc_main
                                SET chag = " & Chr(34) & Chr(34) & "
                                WHERE oveid = " & Form3.pro_id & "
                                AND pkgid = " & i)
                            Case Else
                                '刷新bidoc_main表
                                Form1.sqlcon1.sqlstr("
                                UPDATE bidoc_main
                                SET " & sli((xlsc - 5).ToString) & "id = " & Chr(34) & Chr(34) & "
                                WHERE oveid = " & Form3.pro_id & "
                                AND pkgid = " & i)
                        End Select
                        '判断单元格不为空
                    Else
                        Select Case xlsc
                            Case 12
                                '刷新bidoc_main表
                                Form1.sqlcon1.sqlstr("
                                UPDATE bidoc_main
                                SET chag = " & Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).NumericCellValue & "
                                WHERE oveid = " & Form3.pro_id & "
                                AND pkgid = " & i)
                            Case Else
                                str = Form3.xlscon1.wksheet.GetRow(xlsr - 1).GetCell(xlsc - 1).StringCellValue.ToString & ".docx"
                                '获取bidoc_mb.id
                                Form1.sqlcon1.sqlstr("
                                SELECT bidoc_" & sli((xlsc - 5).ToString) & ".id
                                FROM bidoc_" & sli((xlsc - 5).ToString) & "
                                WHERE bidoc_" & sli((xlsc - 5).ToString) & ".char_na = " &
                                Chr(34) & str & Chr(34), 1)
                                Form1.sqlcon1.dataf.Read()
                                j = Form1.sqlcon1.dataf.Item(0)
                                '刷新bidoc_main表
                                Form1.sqlcon1.sqlstr("
                                UPDATE bidoc_main
                                SET " & sli((xlsc - 5).ToString) & "id = " & j & "
                                WHERE oveid = " & Form3.pro_id & "
                                AND pkgid = " & i)
                        End Select
                    End If
                Catch ex As Exception
                    MsgBox("警告：导入文件（" & xlsc & "列，" & xlsr & "行）数据错误", 0 + 48, "warning")
                    Form1.sqlcon1.sqlstr("END TRANSACTION")
                    Exit Sub
                End Try
            Next
        Next
        Form1.sqlcon1.sqlstr("COMMIT")
        Call sqlup_gv()
        MsgBox("提示：导入文件完成", 0 + 64, "information")
    End Sub

    Sub sqlup_gv()
        Dim dr As DataRow
        Dim myfile As String
        Dim j As Integer

        '刷新bidoc_main.cou_gf
        Form1.sqlcon1.sqlstr("
        SELECT bid.bididx,bid.bidname,package.pkgidx,package.id
        FROM bid,package
        WHERE bid.oveid = " & Form3.pro_id & "
        AND package.bidid = bid.id
        ORDER BY bid.id,package.id", 2)
        For Each dr In Form1.sqlcon1.datas.Tables(0).Rows
            j = 0
            myfile = Dir(ButtonEdit1.Text & "\6.技术规范书\" &
                         dr.Item(0) & dr.Item(1) & dr.Item(2) & "\*.doc*")
            Do While myfile <> ""
                myfile = Dir()
                j += 1
            Loop
            Form1.sqlcon1.sqlstr("
            UPDATE bidoc_main
            SET cou_gf = " & j & "
            WHERE oveid = " & Form3.pro_id & "
            AND pkgid = " & dr.Item(3))
        Next
        'sql获取bidoc_main表数据
        Form1.sqlcon1.sqlstr("
            SELECT 
            tb1.bididx,tb1.bidname,tb1.bidno,tb1.pkgidx,tb1.pkgname,
            bidoc_mb.char_na,bidoc_ht.char_na,
            bidoc_swcp.char_na,bidoc_jscp.char_na,
            bidoc_swxp.char_na,bidoc_jsxp.char_na,
            bidoc_jg.char_na,
            tb1.chag,tb1.cou_gf
            FROM
            (SELECT
            bid.bididx,
            bid.bidname,
            bid.bidno,
            package.pkgidx,
            package.pkgname,
            bidoc_main.mbid,
            bidoc_main.htid,
            bidoc_main.swcpid,
            bidoc_main.jscpid,
            bidoc_main.swxpid,
            bidoc_main.jsxpid,
            bidoc_main.jgid,
            bidoc_main.chag,
            bidoc_main.cou_gf
            FROM 
            bid,package,bidoc_main
            WHERE bidoc_main.oveid = " & Form3.pro_id & "
            AND bid.oveid = bidoc_main.oveid
            AND bid.id = package.bidid
            AND package.id = bidoc_main.pkgid
            ORDER BY bid.id,package.id) as tb1
            LEFT JOIN bidoc_mb ON tb1.mbid = bidoc_mb.id
            LEFT JOIN bidoc_ht ON tb1.htid = bidoc_ht.id
            LEFT JOIN bidoc_swcp ON tb1.swcpid = bidoc_swcp.id
            LEFT JOIN bidoc_jscp ON tb1.jscpid = bidoc_jscp.id
            LEFT JOIN bidoc_swxp ON tb1.swxpid = bidoc_swxp.id
            LEFT JOIN bidoc_jsxp ON tb1.jsxpid = bidoc_jsxp.id
            LEFT JOIN bidoc_jg ON tb1.jgid = bidoc_jg.id", 2)
        GridControl1.DataSource = Form1.sqlcon1.datas.Tables(0)
        '设定列名称
        GridView1.Columns(0).Caption = "分标号"
        GridView1.Columns(1).Caption = "分标名称"
        GridView1.Columns(2).Caption = "分标编号"
        GridView1.Columns(3).Caption = "分包号"
        GridView1.Columns(4).Caption = "分包名称"
        GridView1.Columns(5).Caption = "采购文件模板"
        GridView1.Columns(6).Caption = "合同文本"
        GridView1.Columns(7).Caption = "商务初评模板"
        GridView1.Columns(8).Caption = "技术初评模板"
        GridView1.Columns(9).Caption = "商务详评模板"
        GridView1.Columns(10).Caption = "技术详评模板"
        GridView1.Columns(11).Caption = "价格模板"
        GridView1.Columns(12).Caption = "固定服务费"
        GridView1.Columns(13).Caption = "技术规范数量"
        '设置列格式
        GridView1.BestFitColumns()
        '设置列编辑性
        For i = 0 To 4
            GridView1.Columns(i).OptionsColumn.AllowEdit = False
        Next
        GridView1.Columns(13).OptionsColumn.AllowEdit = False
        '添加columnedit
        GridView1.Columns(5).ColumnEdit = RepositoryItemComboBox1
        GridView1.Columns(6).ColumnEdit = RepositoryItemComboBox2
        GridView1.Columns(7).ColumnEdit = RepositoryItemComboBox3
        GridView1.Columns(8).ColumnEdit = RepositoryItemComboBox4
        GridView1.Columns(9).ColumnEdit = RepositoryItemComboBox5
        GridView1.Columns(10).ColumnEdit = RepositoryItemComboBox6
        GridView1.Columns(11).ColumnEdit = RepositoryItemComboBox7
    End Sub

    Sub dc_bidoc(ByVal rowi As Integer)
        Dim doc, insdoc As New Document
        Dim builder As New DocumentBuilder
        Dim table As New Tables.Table(doc)
        Dim ln, str, n, myfile As String
        Dim i As Integer
        Dim sli As New SortedList
        Dim bm As Bookmark

        ln = GridView1.GetDataRow(rowi).Item(0).ToString & " " &
            GridView1.GetDataRow(rowi).Item(1).ToString & " " &
            GridView1.GetDataRow(rowi).Item(3).ToString
        '检查采购文件模板
        Label13.Text = ">正在检查 " & ln & " 采购文件模板"
        Application.DoEvents()
        Try
            '获取指定模板
            doc = New Document(ButtonEdit1.Text & "\1.采购文件模板\" &
                               GridView1.GetDataRow(rowi).Item(5).ToString)
        Catch ex As Exception
            Label13.Text = ">错误:" & ln & " 采购文件模板"
            Application.DoEvents()
            MsgBox("警告：发生未知错误 location:dc_bidoc", 0 + 48, "warning")
            Exit Sub
        End Try
        '检查替换模板文本
        Label13.Text = ">正在生成 " & ln & " 采购文件信息写入"
        Application.DoEvents()
        Try
            'sql获取overload表数据
            Form1.sqlcon1.sqlstr("
            SELECT overload.proname,overload.prono,
            owner.owname,owner.owaddrs,owner.owcallm,owner.owcalln,
            worker.wkname,worker.wkaddrs,worker.wkcallm,worker.wkcalln
            FROM overload,owner,worker
            WHERE overload.id = " & Form3.pro_id & "
            AND overload.owid = owner.id
            AND overload.wkid = worker.id", 1)
            Form1.sqlcon1.dataf.Read()
            '填充sli
            sli.Clear()
            sli.Add("0", "[采购项目名称]")
            sli.Add("1", "[采购编号]")
            sli.Add("2", "[采购人名称]")
            sli.Add("3", "[采购人地址]")
            sli.Add("4", "[采购人联系人]")
            sli.Add("5", "[采购人联系方式]")
            sli.Add("6", "[代理机构名称]")
            sli.Add("7", "[代理机构地址]")
            sli.Add("8", "[代理机构联系人]")
            sli.Add("9", "[代理机构联系方式]")
            sli.Add("10", "[分标号]")
            sli.Add("11", "[分标名称]")
            sli.Add("12", "[分标编号]")
            sli.Add("13", "[分包号]")
            sli.Add("14", "[分包名称]")
            sli.Add("15", "[固定服务费]")
            sli.Add("16", "[文件日期]")
            '替换模板文本
            For i = 0 To 9
                doc.Range.Replace(sli(i.ToString),
                              Form1.sqlcon1.dataf.Item(i).ToString,
                              New Replacing.FindReplaceOptions())
            Next
            For i = 10 To 14
                doc.Range.Replace(sli(i.ToString),
                              GridView1.GetDataRow(rowi).Item(i - 10).ToString,
                              New Replacing.FindReplaceOptions())
            Next
            i = 15
            doc.Range.Replace(sli(i.ToString),
                              GridView1.GetDataRow(rowi).Item(12).ToString,
                              New Replacing.FindReplaceOptions())
            i = 16
            doc.Range.Replace(sli(i.ToString),
                              TextBox5.Text.ToString,
                              New Replacing.FindReplaceOptions())
        Catch ex As Exception
            Label13.Text = ">错误:" & ln & " 信息写入-" & sli(i.ToString)
            Application.DoEvents()
            MsgBox("警告：发生未知错误 location:dc_bidoc", 0 + 48, "warning")
            Exit Sub
        End Try
        '检查插入文档
        Label13.Text = ">正在检查 " & ln & " 采购文件文档插入"
        Application.DoEvents()
        Try
            '填充sli
            sli.Clear()
            i = 0
            For Each bm In doc.Range.Bookmarks
                sli.Add(i.ToString, bm.Name.ToString)
                i += 1
            Next
            If sli.ContainsValue("合同文本") And
                sli.ContainsValue("商务初评模板") And
                sli.ContainsValue("技术初评模板") And
                sli.ContainsValue("商务详评模板") And
                sli.ContainsValue("技术详评模板") And
                sli.ContainsValue("价格模板") Then
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            Label13.Text = ">错误:" & ln & " 文档插入-位置"
            Application.DoEvents()
            MsgBox("警告：未找到全部文档书签", 0 + 48, "warning")
            Exit Sub
        End Try
        Label13.Text = ">正在生成 " & ln & " 采购文件文档插入"
        Application.DoEvents()
        Try
            '填充sli
            sli.Clear()
            sli.Add("0", "合同文本")
            sli.Add("1", "商务初评模板")
            sli.Add("2", "技术初评模板")
            sli.Add("3", "商务详评模板")
            sli.Add("4", "技术详评模板")
            sli.Add("5", "价格模板")
            sli.Add("6", "技术规范书")
            '插入合同文本、初评模板、详评模板、价格模板
            builder = New DocumentBuilder(doc)
            For i = 0 To 5
                builder.MoveToBookmark(sli(i.ToString))
                Select Case i
                    Case 0
                        str = (i + 2).ToString & "." & sli(i.ToString)
                    Case 1
                        str = "3.初评模板\商务初评模板"
                    Case 2
                        str = "3.初评模板\技术初评模板"
                    Case 3
                        str = "4.详评模板\商务详评模板"
                    Case 4
                        str = "4.详评模板\技术详评模板"
                    Case Else
                        str = i.ToString & "." & sli(i.ToString)
                End Select
                insdoc = New Document(ButtonEdit1.Text & "\" & str & "\" &
                                      GridView1.GetDataRow(rowi).Item(i + 6).ToString)
                builder.InsertDocument(insdoc, ImportFormatMode.KeepDifferentStyles)
            Next
            '插入技术规范
            i = 6
            If GridView1.GetDataRow(rowi).Item(13) = 0 Then
            Else
                'If GridView1.GetDataRow(rowi).Item(0).ToString = "" Then
                '    Throw New Exception
                'End If
                str = GridView1.GetDataRow(rowi).Item(0).ToString &
                GridView1.GetDataRow(rowi).Item(1).ToString &
                GridView1.GetDataRow(rowi).Item(3).ToString
                myfile = Dir(ButtonEdit1.Text & "\" &
                             i.ToString & "." & sli(i.ToString) & "\" &
                             str & "\*.doc*")
                Do While myfile <> ""
                    '插入新页
                    builder.MoveToDocumentEnd()
                    builder.InsertBreak(BreakType.SectionBreakNewPage)
                    insdoc = New Document(ButtonEdit1.Text & "\6.技术规范书\" &
                                          str & "\" & myfile)
                    builder.InsertDocument(insdoc, ImportFormatMode.KeepDifferentStyles)
                    myfile = Dir()
                Loop
            End If
        Catch ex As Exception
            Label13.Text = ">错误:" & ln & " 文档插入-" & sli(i.ToString)
            Application.DoEvents()
            MsgBox("警告：发生未知错误 location:dc_bidoc", 0 + 48, "warning")
            Exit Sub
        End Try
        '保存文件
        Try
            str = GridView1.GetDataRow(rowi).Item(3).ToString
            n = CInt(str.Substring(1, Len(str) - 1)).ToString
            str = GridView1.GetDataRow(rowi).Item(1).ToString
            doc.Save(ButtonEdit2.Text & "\采购文件\" &
                     str & "\" & n & "\" &
                     GridView1.GetDataRow(rowi).Item(3).ToString & "-" &
                     GridView1.GetDataRow(rowi).Item(4).ToString &
                     "采购文件.docx", SaveFormat.Docx)
        Catch ex As Exception
            Label13.Text = ">错误:" & ln & " 采购文件未保存"
            Application.DoEvents()
            MsgBox("警告：发生未知错误 location:dc_bidoc", 0 + 48, "warning")
            Exit Sub
        End Try
        '显示生成文件路径
        Label13.Text = ">生成文件路径：" & ButtonEdit2.Text & "\采购文件"
        Application.DoEvents()
    End Sub

End Class