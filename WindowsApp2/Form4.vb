﻿Public Class Form4
    '文字输入图标
    Private Sub TextBox_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged,
    TextBox2.TextChanged, TextBox3.TextChanged, TextBox5.TextChanged, TextBox6.TextChanged,
    TextBox4.TextChanged, TextBox7.TextChanged
        Select Case sender.name
            Case "TextBox1"
                If TextBox1.Text <> "" Then
                    PictureBox1.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox1.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox2"
                If TextBox2.Text <> "" Then
                    PictureBox2.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox2.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox3"
                If TextBox3.Text <> "" Then
                    PictureBox3.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox3.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox5"
                If TextBox5.Text <> "" Then
                    PictureBox9.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox9.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox6"
                If TextBox6.Text <> "" Then
                    PictureBox10.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox10.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox4"
                If TextBox4.Text <> "" Then
                    PictureBox12.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox12.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox7"
                If TextBox7.Text <> "" Then
                    PictureBox13.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox13.Image = ImageList1.Images.Item(1)
                End If
        End Select
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        Dim i As Integer
        '检查分标名、分标编号是否为空
        '检查分标设置是否完成
        If TextBox2.Text = "" Or TextBox3.Text = "" Then
            MsgBox("警告：分标名称或编号不得为空", 0 + 48, "warning")
            Exit Sub
        ElseIf Label10.Visible = False Or Label11.Visible = False Then
            MsgBox("警告：分标属性设置不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '检查是否存在分标名
        Try
            Form1.sqlcon1.sqlstr("SELECT id FROM bid 
            WHERE bidname = " &
            Chr(34) & TextBox2.Text.ToString & Chr(34) &
            "AND oveid = " &
            Form3.pro_id, 1
                                 )
            Form1.sqlcon1.dataf.Read()
            Form3.bid_id = Form1.sqlcon1.dataf.Item(0)
            If MsgBox("注意：新增项目已存在，是否更新已有项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                '更新bid表项目
                Form1.sqlcon1.sqlstr("UPDATE bid 
                SET bididx = " &
                Chr(34) & TextBox1.Text.ToString & Chr(34) & "," &
                "bidno = " &
                Chr(34) & TextBox3.Text.ToString & Chr(34) &
                "WHERE bidname = " &
                Chr(34) & TextBox2.Text.ToString & Chr(34) &
                " AND oveid = " &
                Form3.pro_id
                                     )
            Else
                Exit Sub
            End If
        Catch ex As Exception
            If MsgBox("注意：是否确定新建此项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                '新建bid表项目
                Form1.sqlcon1.sqlstr("INSERT INTO bid(bididx,bidname,bidno,oveid)
                VALUES(" &
                Chr(34) & TextBox1.Text.ToString & Chr(34) & "," &
                Chr(34) & TextBox2.Text.ToString & Chr(34) & "," &
                Chr(34) & TextBox3.Text.ToString & Chr(34) & "," &
                Form3.pro_id & ")"
                                     )
            Else
                Exit Sub
            End If
        End Try
        '获取bidid
        Form1.sqlcon1.sqlstr("SELECT id FROM bid 
                WHERE bidname = " &
                Chr(34) & TextBox2.Text.ToString & Chr(34) &
                " AND oveid = " &
                Form3.pro_id, 1
                             )
        Form1.sqlcon1.dataf.Read()
        i = Form1.sqlcon1.dataf.Item(0)
        '检查bid_config表是否存在data，更新或新建
        Form1.sqlcon1.sqlstr("SELECT id FROM bid_config 
        WHERE bidid = " &
        i, 1
                             )
        Try
            Form1.sqlcon1.dataf.Read()
            i = Form1.sqlcon1.dataf.Item(0)
            '更新bid_config表data
            Form1.sqlcon1.sqlstr("UPDATE bid_config 
            SET char_nr = " &
            Chr(34) &
            Label10.Text.Substring(InStr(Label10.Text, "："), Len(Label10.Text) - InStr(Label10.Text, "：")).ToString &
            Chr(34) & "," &
            "char_xs = " &
            Chr(34) &
            Label11.Text.Substring(InStr(Label11.Text, "："), Len(Label11.Text) - InStr(Label11.Text, "：")).ToString &
            Chr(34) &
            "WHERE id = " &
            i
                                 )
        Catch ex As Exception
            '新建bid_connfig表data
            Form1.sqlcon1.sqlstr("INSERT INTO bid_config(char_nr,char_xs,bidid)
            VALUES(" &
            Chr(34) &
            Label10.Text.Substring(InStr(Label10.Text, "："), Len(Label10.Text) - InStr(Label10.Text, "：")).ToString &
            Chr(34) & "," &
            Chr(34) &
            Label11.Text.Substring(InStr(Label11.Text, "："), Len(Label11.Text) - InStr(Label11.Text, "：")).ToString &
            Chr(34) & "," &
            i & ")"
                                 )
        End Try
        MsgBox("提示：项目已新建或更新完成", 0 + 64, "information")
        Call Form3.tvu(1)
        Me.Close()
    End Sub

    Private Sub SkinComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles SkinComboBox1.SelectedIndexChanged
        Dim str As String
        str = SkinComboBox1.SelectedItem.ToString
        SkinComboBox2.Text = ""
        Select Case str
            Case "采购内容"
                SkinComboBox2.Items.Clear()
                SkinComboBox2.Items.Add("物资采购项目")
                SkinComboBox2.Items.Add("服务采购项目")
                SkinComboBox2.Items.Add("施工采购项目")
            Case "限授情况"
                SkinComboBox2.Items.Clear()
                SkinComboBox2.Items.Add("无限授")
                SkinComboBox2.Items.Add("按概算金额限授")
        End Select
    End Sub

    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        Dim str As String
        If SkinComboBox1.Text = "" Or SkinComboBox2.Text = "" Then
            MsgBox("警告：分标属性设置不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        str = SkinComboBox1.SelectedItem.ToString
        Select Case str
            Case "采购内容"
                Label10.Text = "    " & SkinComboBox1.Text & "：" & SkinComboBox2.Text
                Label10.Visible = True
            Case "限授情况"
                Label11.Text = "    " & SkinComboBox1.Text & "：" & SkinComboBox2.Text
                Label11.Visible = True
        End Select
    End Sub

    Private Sub SkinComboBox4_SelectedIndexChanged(sender As Object, e As EventArgs) Handles SkinComboBox4.SelectedIndexChanged
        Dim str As String
        str = SkinComboBox4.SelectedItem.ToString
        SkinComboBox3.Text = ""
        Select Case str
            Case "公开形式"
                SkinComboBox3.Items.Clear()
                SkinComboBox3.Items.Add("公开")
                SkinComboBox3.Items.Add("邀请")
            Case "采购方式"
                SkinComboBox3.Items.Clear()
                SkinComboBox3.Items.Add("竞争性谈判")
                SkinComboBox3.Items.Add("单一来源")
            Case "报价格式"
                SkinComboBox3.Items.Clear()
                SkinComboBox3.Items.Add("固定总价")
                SkinComboBox3.Items.Add("折扣率")
                SkinComboBox3.Items.Add("综合单价")
            Case "评审方法"
                SkinComboBox3.Items.Clear()
                SkinComboBox3.Items.Add("最低价法")
                SkinComboBox3.Items.Add("综合评审法")
        End Select
    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        Dim str As String
        If SkinComboBox3.Text = "" Or SkinComboBox4.Text = "" Then
            MsgBox("警告：分包属性设置不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        str = SkinComboBox4.SelectedItem.ToString
        Select Case str
            Case "公开形式"
                Label17.Text = "    " & SkinComboBox4.Text & "：" & SkinComboBox3.Text
                Label17.Visible = True
            Case "采购方式"
                Label16.Text = "    " & SkinComboBox4.Text & "：" & SkinComboBox3.Text
                Label16.Visible = True
            Case "报价格式"
                Label15.Text = "    " & SkinComboBox4.Text & "：" & SkinComboBox3.Text
                Label15.Visible = True
            Case "评审方法"
                Label14.Text = "    " & SkinComboBox4.Text & "：" & SkinComboBox3.Text
                Label14.Visible = True
        End Select
    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        Dim i As Integer
        '检查分包号、分标名称是否为空
        '检查分包设置是否完成
        If TextBox5.Text = "" Or TextBox6.Text = "" Then
            MsgBox("警告：分包号或分包名称不得为空", 0 + 48, "warning")
            Exit Sub
        ElseIf Label14.Visible = False Or Label15.Visible = False Or
            Label16.Visible = False Or Label17.Visible = False Then
            MsgBox("警告：分包属性设置不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '检查是否存在分包号
        Try
            Form1.sqlcon1.sqlstr("SELECT id FROM package 
            WHERE pkgidx = " &
            Chr(34) & TextBox6.Text.ToString & Chr(34) &
            "AND bidid = " &
            Form3.bid_id, 1
                                 )
            Form1.sqlcon1.dataf.Read()
            Form3.pkg_id = Form1.sqlcon1.dataf.Item(0)
            If MsgBox("注意：新增项目已存在，是否更新已有项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                '更新package表项目
                Form1.sqlcon1.sqlstr("UPDATE package 
                SET pkgname = " &
                Chr(34) & TextBox5.Text.ToString & Chr(34) &
                "WHERE pkgidx = " &
                Chr(34) & TextBox6.Text.ToString & Chr(34) &
                " AND bidid = " &
                Form3.bid_id
                                     )
            Else
                Exit Sub
            End If
        Catch ex As Exception
            If MsgBox("注意：是否确定新建此项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                '新建package表项目
                Form1.sqlcon1.sqlstr("INSERT INTO package(pkgidx,pkgname,bidid)
                VALUES(" &
                Chr(34) & TextBox6.Text.ToString & Chr(34) & "," &
                Chr(34) & TextBox5.Text.ToString & Chr(34) & "," &
                Form3.bid_id & ")"
                                     )
            Else
                Exit Sub
            End If
        End Try
        '获取pkgid
        Form1.sqlcon1.sqlstr("SELECT id FROM package 
                WHERE pkgidx = " &
                Chr(34) & TextBox6.Text.ToString & Chr(34) &
                " AND bidid = " &
                Form3.bid_id, 1
                             )
        Form1.sqlcon1.dataf.Read()
        i = Form1.sqlcon1.dataf.Item(0)
        '检查package_config表是否存在data，更新或新建
        Form1.sqlcon1.sqlstr("SELECT id FROM package_config 
        WHERE pkgid = " &
        i, 1
                             )
        Try
            Form1.sqlcon1.dataf.Read()
            i = Form1.sqlcon1.dataf.Item(0)
            '更新package_config表data
            Form1.sqlcon1.sqlstr("UPDATE package_config 
            SET char_ps = " &
            Chr(34) &
            Label14.Text.Substring(InStr(Label14.Text, "："), Len(Label14.Text) - InStr(Label14.Text, "：")).ToString &
            Chr(34) & "," &
            "char_bj = " &
            Chr(34) &
            Label15.Text.Substring(InStr(Label15.Text, "："), Len(Label15.Text) - InStr(Label15.Text, "：")).ToString &
            Chr(34) & "," &
            "char_fs = " &
            Chr(34) &
            Label16.Text.Substring(InStr(Label16.Text, "："), Len(Label16.Text) - InStr(Label16.Text, "：")).ToString &
            Chr(34) & "," &
            "char_gk = " &
            Chr(34) &
            Label17.Text.Substring(InStr(Label17.Text, "："), Len(Label17.Text) - InStr(Label17.Text, "：")).ToString &
            Chr(34) &
            "WHERE id = " &
            i
                                 )
        Catch ex As Exception
            '新建package_connfig表data
            Form1.sqlcon1.sqlstr("INSERT INTO package_config(char_gk,char_fs,char_bj,char_ps,pkgid)
            VALUES(" &
            Chr(34) &
            Label17.Text.Substring(InStr(Label17.Text, "："), Len(Label17.Text) - InStr(Label17.Text, "：")).ToString &
            Chr(34) & "," &
            Chr(34) &
            Label16.Text.Substring(InStr(Label16.Text, "："), Len(Label16.Text) - InStr(Label16.Text, "：")).ToString &
            Chr(34) & "," &
            Chr(34) &
            Label15.Text.Substring(InStr(Label15.Text, "："), Len(Label15.Text) - InStr(Label15.Text, "：")).ToString &
            Chr(34) & "," &
            Chr(34) &
            Label14.Text.Substring(InStr(Label14.Text, "："), Len(Label14.Text) - InStr(Label14.Text, "：")).ToString &
            Chr(34) & "," &
            i & ")"
                                 )
        End Try
        MsgBox("提示：项目已新建或更新完成", 0 + 64, "information")
        Call Form3.tvu(1)
        Me.Close()
    End Sub

    Private Sub PictureBox8_Click(sender As Object, e As EventArgs) Handles PictureBox8.Click
        Dim str As String
        If Not IsNumeric(SkinComboBox5.Text) Then
            MsgBox("警告：工程属性设置不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        str = SkinComboBox6.SelectedItem.ToString
        Select Case str
            Case "概算金额"
                Label28.Text = "    " & SkinComboBox6.Text & "：" & SkinComboBox5.Text
                Label28.Visible = True
            Case "最高限价"
                Label27.Text = "    " & SkinComboBox6.Text & "：" & SkinComboBox5.Text
                Label27.Visible = True
        End Select
    End Sub

    Private Sub PictureBox11_Click(sender As Object, e As EventArgs) Handles PictureBox11.Click
        Dim i As Integer
        '检查工程名称是否为空
        '检查工程设置是否完成
        If TextBox4.Text = "" Then
            MsgBox("警告：工程名称不得为空", 0 + 48, "warning")
            Exit Sub
        ElseIf Label28.Visible = False Or Label27.Visible = False Then
            MsgBox("警告：工程属性设置不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '检查是否存在工程名
        Try
            Form1.sqlcon1.sqlstr("SELECT id FROM engineering 
            WHERE engname = " &
            Chr(34) & TextBox4.Text.ToString & Chr(34) &
            "AND pkgid = " &
            Form3.pkg_id, 1
                                 )
            Form1.sqlcon1.dataf.Read()
            Form3.eng_id = Form1.sqlcon1.dataf.Item(0)
            If MsgBox("注意：新增项目已存在，是否更新已有项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                '更新package表项目
                Form1.sqlcon1.sqlstr("UPDATE engineering 
                SET engidx = " &
                Chr(34) & TextBox7.Text.ToString & Chr(34) &
                "WHERE engname = " &
                Chr(34) & TextBox4.Text.ToString & Chr(34) &
                " AND pkgid = " &
                Form3.pkg_id
                                     )
            Else
                Exit Sub
            End If
        Catch ex As Exception
            If MsgBox("注意：是否确定新建此项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                '新建package表项目
                Form1.sqlcon1.sqlstr("INSERT INTO engineering(engidx,engname,pkgid)
                VALUES(" &
                Chr(34) & TextBox7.Text.ToString & Chr(34) & "," &
                Chr(34) & TextBox4.Text.ToString & Chr(34) & "," &
                Form3.pkg_id & ")"
                                     )
            Else
                Exit Sub
            End If
        End Try
        '获取engid
        Form1.sqlcon1.sqlstr("SELECT id FROM engineering 
                WHERE engname = " &
                Chr(34) & TextBox4.Text.ToString & Chr(34) &
                " AND pkgid = " &
                Form3.pkg_id, 1
                             )
        Form1.sqlcon1.dataf.Read()
        i = Form1.sqlcon1.dataf.Item(0)
        '检查engineering_config表是否存在data，更新或新建
        Form1.sqlcon1.sqlstr("SELECT id FROM engineering_config 
        WHERE engid = " &
        i, 1
                             )
        Try
            Form1.sqlcon1.dataf.Read()
            i = Form1.sqlcon1.dataf.Item(0)
            '更新engineering_config表data
            Form1.sqlcon1.sqlstr("UPDATE engineering_config 
            SET char_gs = " &
            Label28.Text.Substring(InStr(Label28.Text, "："), Len(Label28.Text) - InStr(Label28.Text, "：")) &
            "," &
            "char_xj = " &
            Label27.Text.Substring(InStr(Label27.Text, "："), Len(Label27.Text) - InStr(Label27.Text, "：")) &
            " WHERE id = " &
            i
                                 )
        Catch ex As Exception
            '新建package_connfig表data
            Form1.sqlcon1.sqlstr("INSERT INTO engineering_config(char_xj,char_gs,engid)
            VALUES(" &
            Label27.Text.Substring(InStr(Label27.Text, "："), Len(Label27.Text) - InStr(Label27.Text, "：")) &
             "," &
            Label28.Text.Substring(InStr(Label28.Text, "："), Len(Label28.Text) - InStr(Label28.Text, "：")) &
            "," &
            i & ")"
                                 )
        End Try
        MsgBox("提示：项目已新建或更新完成", 0 + 64, "information")
        Call Form3.tvu(1)
        Me.Close()
    End Sub
End Class