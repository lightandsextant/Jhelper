﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form6
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form6))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.SkinComboBox2 = New CCWin.SkinControl.SkinComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.SkinTextBox2 = New CCWin.SkinControl.SkinTextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.SkinComboBox1 = New CCWin.SkinControl.SkinComboBox()
        Me.SkinTextBox1 = New CCWin.SkinControl.SkinTextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.SkinComboBox2)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.Button3)
        Me.GroupBox1.Controls.Add(Me.Button2)
        Me.GroupBox1.Controls.Add(Me.Button1)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.SkinTextBox2)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.SkinComboBox1)
        Me.GroupBox1.Controls.Add(Me.SkinTextBox1)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(541, 365)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "应答人废标设置"
        '
        'SkinComboBox2
        '
        Me.SkinComboBox2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.SkinComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.SkinComboBox2.FormattingEnabled = True
        Me.SkinComboBox2.Location = New System.Drawing.Point(371, 72)
        Me.SkinComboBox2.Name = "SkinComboBox2"
        Me.SkinComboBox2.Size = New System.Drawing.Size(119, 22)
        Me.SkinComboBox2.TabIndex = 24
        Me.SkinComboBox2.WaterText = "请选择专家"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(288, 75)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(89, 12)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = "提出专家姓名："
        '
        'Button3
        '
        Me.Button3.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Button3.Location = New System.Drawing.Point(333, 336)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 22
        Me.Button3.Text = "取消废标"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Button2.Location = New System.Drawing.Point(233, 336)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 21
        Me.Button2.Text = "技术废标"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom
        Me.Button1.Location = New System.Drawing.Point(133, 336)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 20
        Me.Button1.Text = "商务废标"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(6, 225)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(113, 12)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "应答文件存在问题："
        '
        'SkinTextBox2
        '
        Me.SkinTextBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SkinTextBox2.BackColor = System.Drawing.Color.Transparent
        Me.SkinTextBox2.DownBack = Nothing
        Me.SkinTextBox2.Icon = Nothing
        Me.SkinTextBox2.IconIsButton = False
        Me.SkinTextBox2.IconMouseState = CCWin.SkinClass.ControlState.Normal
        Me.SkinTextBox2.IsPasswordChat = Global.Microsoft.VisualBasic.ChrW(0)
        Me.SkinTextBox2.IsSystemPasswordChar = False
        Me.SkinTextBox2.Lines = New String(-1) {}
        Me.SkinTextBox2.Location = New System.Drawing.Point(3, 250)
        Me.SkinTextBox2.Margin = New System.Windows.Forms.Padding(0)
        Me.SkinTextBox2.MaxLength = 32767
        Me.SkinTextBox2.MinimumSize = New System.Drawing.Size(28, 28)
        Me.SkinTextBox2.MouseBack = Nothing
        Me.SkinTextBox2.MouseState = CCWin.SkinClass.ControlState.Normal
        Me.SkinTextBox2.Multiline = True
        Me.SkinTextBox2.Name = "SkinTextBox2"
        Me.SkinTextBox2.NormlBack = Nothing
        Me.SkinTextBox2.Padding = New System.Windows.Forms.Padding(5)
        Me.SkinTextBox2.ReadOnly = False
        Me.SkinTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.SkinTextBox2.Size = New System.Drawing.Size(535, 70)
        '
        '
        '
        Me.SkinTextBox2.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SkinTextBox2.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SkinTextBox2.SkinTxt.Font = New System.Drawing.Font("微软雅黑", 9.75!)
        Me.SkinTextBox2.SkinTxt.Location = New System.Drawing.Point(5, 5)
        Me.SkinTextBox2.SkinTxt.Multiline = True
        Me.SkinTextBox2.SkinTxt.Name = "BaseText"
        Me.SkinTextBox2.SkinTxt.Size = New System.Drawing.Size(525, 60)
        Me.SkinTextBox2.SkinTxt.TabIndex = 0
        Me.SkinTextBox2.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.SkinTextBox2.SkinTxt.WaterText = ""
        Me.SkinTextBox2.TabIndex = 18
        Me.SkinTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.SkinTextBox2.WaterColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.SkinTextBox2.WaterText = ""
        Me.SkinTextBox2.WordWrap = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 115)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(89, 12)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "采购文件要求："
        '
        'SkinComboBox1
        '
        Me.SkinComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.SkinComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.SkinComboBox1.FormattingEnabled = True
        Me.SkinComboBox1.Location = New System.Drawing.Point(89, 72)
        Me.SkinComboBox1.Name = "SkinComboBox1"
        Me.SkinComboBox1.Size = New System.Drawing.Size(192, 22)
        Me.SkinComboBox1.TabIndex = 16
        Me.SkinComboBox1.WaterText = "请选择废标模板"
        '
        'SkinTextBox1
        '
        Me.SkinTextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.SkinTextBox1.BackColor = System.Drawing.Color.Transparent
        Me.SkinTextBox1.DownBack = Nothing
        Me.SkinTextBox1.Icon = Nothing
        Me.SkinTextBox1.IconIsButton = False
        Me.SkinTextBox1.IconMouseState = CCWin.SkinClass.ControlState.Normal
        Me.SkinTextBox1.IsPasswordChat = Global.Microsoft.VisualBasic.ChrW(0)
        Me.SkinTextBox1.IsSystemPasswordChar = False
        Me.SkinTextBox1.Lines = New String(-1) {}
        Me.SkinTextBox1.Location = New System.Drawing.Point(3, 140)
        Me.SkinTextBox1.Margin = New System.Windows.Forms.Padding(0)
        Me.SkinTextBox1.MaxLength = 32767
        Me.SkinTextBox1.MinimumSize = New System.Drawing.Size(28, 28)
        Me.SkinTextBox1.MouseBack = Nothing
        Me.SkinTextBox1.MouseState = CCWin.SkinClass.ControlState.Normal
        Me.SkinTextBox1.Multiline = True
        Me.SkinTextBox1.Name = "SkinTextBox1"
        Me.SkinTextBox1.NormlBack = Nothing
        Me.SkinTextBox1.Padding = New System.Windows.Forms.Padding(5)
        Me.SkinTextBox1.ReadOnly = False
        Me.SkinTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.SkinTextBox1.Size = New System.Drawing.Size(535, 70)
        '
        '
        '
        Me.SkinTextBox1.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.SkinTextBox1.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SkinTextBox1.SkinTxt.Font = New System.Drawing.Font("微软雅黑", 9.75!)
        Me.SkinTextBox1.SkinTxt.Location = New System.Drawing.Point(5, 5)
        Me.SkinTextBox1.SkinTxt.Multiline = True
        Me.SkinTextBox1.SkinTxt.Name = "BaseText"
        Me.SkinTextBox1.SkinTxt.Size = New System.Drawing.Size(525, 60)
        Me.SkinTextBox1.SkinTxt.TabIndex = 0
        Me.SkinTextBox1.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.SkinTextBox1.SkinTxt.WaterText = ""
        Me.SkinTextBox1.TabIndex = 15
        Me.SkinTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.SkinTextBox1.WaterColor = System.Drawing.Color.FromArgb(CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer), CType(CType(127, Byte), Integer))
        Me.SkinTextBox1.WaterText = ""
        Me.SkinTextBox1.WordWrap = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(6, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 12)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "废标模板："
        '
        'Label2
        '
        Me.Label2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(288, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(125, 12)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "（<分标名称><包号>）"
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Enabled = False
        Me.TextBox1.Location = New System.Drawing.Point(89, 32)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(193, 21)
        Me.TextBox1.TabIndex = 12
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(6, 35)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(77, 12)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "应答人名称："
        '
        'Form6
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lavender
        Me.ClientSize = New System.Drawing.Size(565, 389)
        Me.Controls.Add(Me.GroupBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form6"
        Me.Text = "废标设置"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label4 As Label
    Friend WithEvents SkinComboBox1 As CCWin.SkinControl.SkinComboBox
    Friend WithEvents SkinTextBox1 As CCWin.SkinControl.SkinTextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents SkinTextBox2 As CCWin.SkinControl.SkinTextBox
    Friend WithEvents Button3 As Button
    Friend WithEvents SkinComboBox2 As CCWin.SkinControl.SkinComboBox
    Friend WithEvents Label6 As Label
End Class
