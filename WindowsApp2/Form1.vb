﻿Imports System.ComponentModel
Imports System.Data.SQLite
Imports System.IO
Imports System.Text.RegularExpressions
Public Class Form1
    '》》》》》》定义版本号《《《《《《
    Public ver As String = "2.1.14"
    Public sqlcon1 As New sqlcon
    Public appath As String = System.Environment.CurrentDirectory
    Public thread1 As New Threading.Thread(AddressOf jupdates)
    Public Sub Form1_Load(sender As Object, e As EventArgs) Handles Me.Load
        '检查软件多开
        If UBound(Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName)) > 0 Then
            MsgBox("提示：软件已经运行", 0 + 64, "information")
            Dim targetkill() As Process
            targetkill = Process.GetProcessesByName(Process.GetCurrentProcess.ProcessName)
            targetkill(0).Kill()
        End If
        LabelControl1.Text = "当前版本: " & ver
        '连接sqlite
        '检查overload表，不存在创建,dele-0未删除，-1删除
        '检查owner表，不存在创建
        '检查worker表，不存在创建
        sqlcon1.conn(appath & "\main.db") '全局连接main数据库
        sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS overload(
        id      integer     PRIMARY KEY     AUTOINCREMENT,
        proname varchar(255)    UNIQUE      NOT NULL,
        prono   varchar(255)                NOT NULL,
        protm1  datetime                    NOT NULL,
        protm2  datetime                    NOT NULL,
        protm3  datetime                    NOT NULL,
        rectime datetime                    NOT NULL,
        owid   integer                      NOT NULL,
        wkid  integer                       NOT NULL
        );")
        sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS owner(
        id      integer     PRIMARY KEY     AUTOINCREMENT,
        owname  varchar(255)                NOT NULL,
        owaddrs varchar(255)                NOT NULL,
        owcallm varchar(50)                 NOT NULL,
        owcalln varchar(50)                 NOT NULL,
        owmail  varchar(50)                 NOT NULL,
        rectime datetime                    NOT NULL
        );")
        sqlcon1.sqlstr("CREATE TABLE IF NOT EXISTS worker(
        id      integer     PRIMARY KEY     AUTOINCREMENT,
        wkname  varchar(255)                NOT NULL,
        wkaddrs varchar(255)                NOT NULL,
        wkcallm varchar(50)                 NOT NULL,
        wkcalln varchar(50)                 NOT NULL,
        wkmail  varchar(50)                 NOT NULL,
        rectime datetime                    NOT NULL
        );")
        '刷新listview1
        Call ulv(1)

        '开启新线程检查更新
        thread1.Start()

    End Sub

    Private Sub Form1_Closing(sender As Object, e As CancelEventArgs) Handles Me.Closing
        If MsgBox("注意：是否确定退出J评审辅助", 4 + 32, "caution") = MsgBoxResult.Yes Then
            sqlcon1.cls()
        Else
            e.Cancel = True
        End If
    End Sub

    '软件自动更新
    Sub jupdates()
        Dim ftp1 As Net.FtpWebRequest
        Dim ftpres1 As Net.FtpWebResponse
        Dim str() As String

        '默认等待10秒
        Threading.Thread.Sleep(10000)
        '创建FTP连接
        Try
            '设置ftp1
            ftp1 = CType(Net.FtpWebRequest.Create("ftp://172.16.5.35/"), Net.FtpWebRequest)
            ftp1.Method = Net.WebRequestMethods.Ftp.ListDirectory
            ftp1.UseBinary = True
            ftp1.Credentials = New Net.NetworkCredential("", "")

            Dim strm As Stream
            Dim strd As StreamReader
            '赋值ftp相应ftpres1
            ftpres1 = CType(ftp1.GetResponse, Net.FtpWebResponse)
            '定义stream和streamreader
            strm = ftpres1.GetResponseStream
            strd = New StreamReader(strm)
            '检查版本号
            str = Split(strd.ReadToEnd, vbCrLf)
            '定义版本号正则表达式0-9.正整数.正整数
            Dim reg As Regex = New Regex("^([0-9]).([0-9]\d*).([0-9]\d*)$")
            For Each item In str
                If reg.IsMatch(item) Then
                    If ver <> item Then
                        If MsgBox("检查到新本版，是否安装更新", 4 + 32, "caution") = MsgBoxResult.Yes Then
                            Call dlver(item)
                        End If
                    End If
                End If
            Next
            '上传本地数据库main
            Call ulmain()
            ftp1.Abort()
        Catch ex As Exception
            '未连接服务器报错

        End Try
        '结束进程
        thread1.Abort()

    End Sub

    Sub dlver(ByVal s As String)
        Dim ftp1 As Net.FtpWebRequest
        Dim ftpres1 As Net.FtpWebResponse
        Dim i As Integer
        Dim bte(1024) As Byte

        '检查updata文件夹
        If Directory.Exists(appath & "\updata") Then
            Directory.Delete(appath & "\updata", True)
            Threading.Thread.Sleep(2000)
            Directory.CreateDirectory(appath & "\updata")
        Else
            Directory.CreateDirectory(appath & "\updata")
        End If
        '下载更新安装包
        Try
            '设置ftp1
            ftp1 = CType(Net.WebRequest.Create("ftp://172.16.5.35/" & s & "/J评审辅助.msi"), Net.FtpWebRequest)
            ftp1.Method = Net.WebRequestMethods.Ftp.DownloadFile
            ftp1.UseBinary = True
            ftp1.Credentials = New Net.NetworkCredential("", "")
            '新建文件流fs1
            Dim fs1 As New FileStream(appath & "\updata\自动更新包.msi", FileMode.Create)
            '设置ftpres1
            ftpres1 = CType(ftp1.GetResponse, Net.FtpWebResponse)
            Do
                i = ftpres1.GetResponseStream.Read(bte, 0, bte.Length)
                fs1.Write(bte, 0, i)
            Loop While i > 0
            fs1.Close()
            '验证更新安装包是否完成下载
            If File.Exists(appath & "\updata\自动更新包.msi") Then
                If MsgBox("是否关闭完成更新", 4 + 32, "caution") = MsgBoxResult.Yes Then
                    '运行安装包msi
                    System.Diagnostics.Process.Start(appath & "\updata\自动更新包.msi")
                    '结束当前进程
                    Dim targetkill() As Process
                    targetkill = Process.GetProcessesByName("J评审辅助")
                    targetkill(0).Kill()
                End If
            Else
                Throw New Exception
            End If
        Catch ex As Exception
            MsgBox("警告：更新未完成，请检查网络连接", 0 + 48, "warning")
            Exit Sub
        End Try

    End Sub

    Sub ulmain()
        Dim ftp1 As Net.FtpWebRequest
        Dim ftpres1 As Net.FtpWebResponse
        Dim i As Integer
        Dim bte(1024) As Byte
        Dim str, str0 As String

        '检查upload文件夹
        If Directory.Exists(appath & "\upload") Then
            Directory.Delete(appath & "\upload", True)
            Threading.Thread.Sleep(2000)
            Directory.CreateDirectory(appath & "\upload")
        Else
            Directory.CreateDirectory(appath & "\upload")
        End If
        '上传数据库main
        Try
            '复制文件main
            Dim finfo As New FileInfo(appath & "\main.db")
            str0 = finfo.Length.ToString
            str0 = Format(Now, "yyyy-MM-dd-HH-mm-ss") & "-" & str0 & ".db"
            str = appath & "\upload\" & str0
            File.Copy(appath & "\main.db", str)
            '设置ftp1
            ftp1 = CType(Net.FtpWebRequest.Create("ftp://172.16.5.35/upload/"), Net.FtpWebRequest)
            ftp1.Method = Net.WebRequestMethods.Ftp.UploadFileWithUniqueName
            ftp1.UseBinary = True
            ftp1.Credentials = New Net.NetworkCredential("", "")

            '定义上传文件流
            Dim fs1 As New FileStream(str, FileMode.Open, FileAccess.Read)
            Dim strm As Stream
            strm = ftp1.GetRequestStream
            '上传文件
            Do
                i = fs1.Read(bte, 0, bte.Length)
                strm.Write(bte, 0, i)
            Loop While i > 0
            strm.Close()
            '返回ftp响应
            ftpres1 = CType(ftp1.GetResponse, Net.FtpWebResponse)
            ftpres1.Close()
            ftp1.Abort()
        Catch ex As Exception

        End Try

    End Sub

    '鼠标经过label效果
    Private Sub PictureBox_MouseMove(sender As Object, e As MouseEventArgs) Handles PictureBox1.MouseMove,
    PictureBox2.MouseMove, PictureBox3.MouseMove, PictureBox4.MouseMove, PictureBox5.MouseMove, PictureBox6.MouseMove
        Select Case sender.name
            Case "PictureBox1"
                Label1.Font = New Font("宋体", 10)
            Case "PictureBox2"
                Label2.Font = New Font("宋体", 10)
            Case "PictureBox3"
                Label3.Font = New Font("宋体", 10)
            Case "PictureBox4"
                Label4.Font = New Font("宋体", 10)
            Case "PictureBox5"
                Label5.Font = New Font("宋体", 10)
            Case "PictureBox6"
                Label6.Font = New Font("宋体", 10)
        End Select

    End Sub

    Private Sub PictureBox1_MouseLeave(sender As Object, e As EventArgs) Handles PictureBox1.MouseLeave,
    PictureBox2.MouseLeave, PictureBox3.MouseLeave, PictureBox4.MouseLeave, PictureBox5.MouseLeave, PictureBox6.MouseLeave
        Select Case sender.name
            Case "PictureBox1"
                Label1.Font = New Font("宋体", 9)
            Case "PictureBox2"
                Label2.Font = New Font("宋体", 9)
            Case "PictureBox3"
                Label3.Font = New Font("宋体", 9)
            Case "PictureBox4"
                Label4.Font = New Font("宋体", 9)
            Case "PictureBox5"
                Label5.Font = New Font("宋体", 9)
            Case "PictureBox6"
                Label6.Font = New Font("宋体", 9)
        End Select

    End Sub

    '新增项目，显示form2
    Private Sub PictureBox2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click
        '初始化form2.groupbox1
        Form2.TextBox1.Text = ""
        Form2.TextBox2.Text = ""
        Form2.ShowDialog()
    End Sub

    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        MsgBox("提示：此功能暂未开放", 0 + 64, "information")
    End Sub

    '修改项目显示form2
    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        Dim str As String

        Try
            '获取选中行item
            str = ListView1.SelectedItems(0).Text
            If MsgBox("注意：是否确定删除选择项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                '获取pro_id
                sqlcon1.sqlstr("SELECT id FROM overload
                WHERE proname = " &
                Chr(34) & str & Chr(34), 1
                        )
                sqlcon1.dataf.Read()
                Form3.pro_id = sqlcon1.dataf.Item(0)
                '尝试删除bid,package,engineering系列表
                Try
                    Call Form3.sqldel()
                Catch ex As Exception

                End Try
                '删除overload表
                sqlcon1.sqlstr("DELETE FROM overload
                WHERE proname = " &
                Chr(34) & str & Chr(34)
                                )

                '刷新listview1
                Call ulv(1)
                MsgBox("提示：已删除选择项目", 0 + 64, "information")
            End If
        Catch ex As Exception
            MsgBox("警告：请选择要删除的项目", 0 + 48, "warning")
        End Try
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Dim str As String
        Try
            '获取选中行item
            str = ListView1.SelectedItems(0).Text
            sqlcon1.sqlstr("SELECT prono,id FROM overload
            WHERE proname = " &
            Chr(34) & str & Chr(34), 1
                        )
            sqlcon1.dataf.Read()
            Form2.TextBox1.Text = str
            Form2.TextBox2.Text = sqlcon1.dataf.Item(0)
            'Form3.pro_id = sqlcon1.dataf.Item(1)
            Form2.ShowDialog()
        Catch ex As Exception
            MsgBox("警告：请选择要更新的项目", 0 + 48, "warning")
        End Try
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        Dim str As String
        Try
            '获取选中行item
            str = ListView1.SelectedItems(0).Text
            If MsgBox("注意：是否确定进入编辑模式", 4 + 32, "caution") = MsgBoxResult.Yes Then
                Form3.Show()
                Form3.SkinListView1.HideSelection = True
                Me.Hide()
            End If
        Catch ex As Exception
            MsgBox("警告：请选择要编辑的项目", 0 + 48, "warning")
        End Try
    End Sub

    Sub ulv(ByVal s As Int32)
        Dim dataf1 As SQLiteDataReader
        ListView1.Items.Clear()
        sqlcon1.sqlstr("SELECT * FROM overload", 1)
        dataf1 = sqlcon1.dataf
        While dataf1.Read()
            '定义新item类型
            Dim lvi As New ListViewItem(dataf1.Item(1).ToString, 0)
            Dim lvis0 As New ListViewItem.ListViewSubItem
            Dim lvis1 As New ListViewItem.ListViewSubItem
            Dim lvis2 As New ListViewItem.ListViewSubItem
            '添加item和subitem
            lvis0.Text = dataf1.Item(2).ToString
            lvi.SubItems.Add(lvis0)
            lvis1.Text = Format(dataf1.Item(5), "yyyy年MM月dd日 HH:mm:ss").ToString
            lvi.SubItems.Add(lvis1)
            sqlcon1.sqlstr("SELECT owname FROM overload,owner 
                            WHERE 
                            overload.proname = " & Chr(34) & dataf1.Item(1).ToString & Chr(34) & "AND 
                            overload.owid = owner.id", 1)
            sqlcon1.dataf.Read()
            lvis2.Text = sqlcon1.dataf.Item(0).ToString()
            lvi.SubItems.Add(lvis2)
            ListView1.Items.Add(lvi)
        End While
    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        Form11.Show()

        Me.Hide()
    End Sub
End Class

'查询函数
Public Class sqlcon
    '连接sqlite并增删改查
    Dim con As SQLiteConnection
    Dim sqls As SQLiteCommand
    Dim sqladp As New SQLiteDataAdapter
    Public dataf As SQLiteDataReader
    Public datas As New DataSet

    Sub conn(ByVal s As String)
        con = New SQLiteConnection("Data Source=" & s)
        con.Open()
    End Sub

    Sub sqlstr(ByVal s As String, Optional e As Integer = 0)
        sqls = New SQLiteCommand(s, con)
        If e = 0 Then
            sqls.ExecuteNonQuery()
        ElseIf e = 1 Then
            dataf = sqls.ExecuteReader()
        Else
            datas = New DataSet
            sqladp.SelectCommand = sqls
            sqladp.Fill(datas, "test")
        End If
    End Sub

    Sub cls()
        con.Dispose()
        con = Nothing
    End Sub

End Class

Public Class sqlqust
    Public Function isexist(ByVal tabn As String,
                            ByVal coln As String,
                            ByVal exn As String,
                            Optional ByVal excoln As String = "",
                            Optional ByVal qustn As Integer = 0) As Integer
        Dim resl As Integer
        Try
            If excoln = "" Then
                Form1.sqlcon1.sqlstr("
                SELECT id FROM " & tabn & "
                WHERE " & coln & " = " & Chr(34) & exn & Chr(34), 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    resl = Form1.sqlcon1.dataf.Item(0)
                    Return resl
                Else
                    Return 0
                End If
            Else
                Form1.sqlcon1.sqlstr("
                SELECT id FROM " & tabn & "
                WHERE " & coln & " = " & Chr(34) & exn & Chr(34) & "
                AND " & excoln & " = " & qustn, 1)
                If Form1.sqlcon1.dataf.HasRows Then
                    Form1.sqlcon1.dataf.Read()
                    resl = Form1.sqlcon1.dataf.Item(0)
                    Return resl
                Else
                    Return 0
                End If
            End If


        Catch ex As Exception
            Throw New InvalidCastException("sqlquest.isexist非法参数")
        End Try
    End Function

End Class

Public Class computdata
    '>>>>>>>>>>>>>>>>未完成<<<<<<<<<<<<<
    Function fuwufei(ByVal price As Double, engtype As Integer)
        Dim fwf As Double

        '0-物资类，1-服务类，2-施工类
        Select Case price
            Case Is < 100
                Select Case engtype
                    Case 0
                        fwf = Math.Round(price * 0.015, 4)
                    Case 1
                        fwf = Math.Round(price * 0.015, 4)
                    Case 2
                        fwf = Math.Round(price * 0.01, 4)
                End Select
            Case 100 To 500
                price -= 100
                Select Case engtype
                    Case 0
                        fwf = 1.5 + Math.Round(price * 0.011, 4)
                    Case 1
                        fwf = 1.5 + Math.Round(price * 0.008, 4)
                    Case 2
                        fwf = 1 + Math.Round(price * 0.007, 4)
                End Select
            Case 500 To 1000
                price -= 500




        End Select

        Return fwf
    End Function



End Class