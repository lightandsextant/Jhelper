﻿Public Class Form7

    Private Sub TextBox_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged,
    TextBox2.TextChanged, TextBox5.TextChanged, TextBox6.TextChanged, TextBox7.TextChanged, TextBox8.TextChanged,
    TextBox9.TextChanged, TextBox10.TextChanged, TextBox11.TextChanged
        Select Case sender.name
            Case "TextBox1"
                If TextBox1.Text <> "" Then
                    PictureBox1.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox1.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox2"
                If TextBox2.Text <> "" Then
                    PictureBox2.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox2.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox5"
                If TextBox5.Text <> "" Then
                    PictureBox5.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox5.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox6"
                If TextBox6.Text <> "" Then
                    PictureBox6.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox6.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox7"
                If TextBox7.Text <> "" Then
                    PictureBox7.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox7.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox8"
                If TextBox8.Text <> "" Then
                    PictureBox8.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox8.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox9"
                If TextBox9.Text <> "" Then
                    PictureBox9.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox9.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox10"
                If TextBox10.Text <> "" Then
                    PictureBox10.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox10.Image = Form4.ImageList1.Images.Item(1)
                End If
            Case "TextBox11"
                If TextBox11.Text <> "" Then
                    PictureBox11.Image = Form4.ImageList1.Images.Item(0)
                Else
                    PictureBox11.Image = Form4.ImageList1.Images.Item(1)
                End If
        End Select
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim str As String

        SkinListView1.Columns.Clear()
        If TextBox3.Text = "" And TextBox4.Text = "" Then
            MsgBox("警告：专家姓名或身份证号不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        str = "
        SELECT expname,expiden,expprof,expadds,expline 
        FROM expert "
        If TextBox3.Text = "" Then
            str = str & "
            WHERE expiden = " &
            Chr(34) & TextBox4.Text & Chr(34)
        ElseIf TextBox4.Text = "" Then
            str = str & "
            WHERE expname = " &
            Chr(34) & TextBox3.Text & Chr(34)
        Else
            str = str & "
            WHERE expname = " &
            Chr(34) & TextBox3.Text & Chr(34) & "
            AND expiden = " &
            Chr(34) & TextBox4.Text & Chr(34)
        End If
        '刷新skinlistview1
        SkinListView1.Columns.Add("专家姓名", 100)
        SkinListView1.Columns.Add("身份证号", 250)
        SkinListView1.Columns.Add("专业名称", 250)
        SkinListView1.Columns.Add("工作单位", 250)
        SkinListView1.Columns.Add("联系方式", 250)
        '写入skinlistview1数据
        Dim dataf1 As SQLite.SQLiteDataReader
        SkinListView1.Items.Clear()
        Form1.sqlcon1.sqlstr(str, 1)
        dataf1 = Form1.sqlcon1.dataf
        While dataf1.Read()
            '定义新item类型
            Dim lvi As New ListViewItem(dataf1.Item(0).ToString, 0)
            Dim lvis0 As New ListViewItem.ListViewSubItem
            Dim lvis1 As New ListViewItem.ListViewSubItem
            Dim lvis2 As New ListViewItem.ListViewSubItem
            Dim lvis3 As New ListViewItem.ListViewSubItem

            lvis0.Text = dataf1.Item(1).ToString
            lvi.SubItems.Add(lvis0)
            lvis1.Text = dataf1.Item(2).ToString
            lvi.SubItems.Add(lvis1)
            lvis2.Text = dataf1.Item(3).ToString
            lvi.SubItems.Add(lvis2)
            lvis3.Text = dataf1.Item(4).ToString
            lvi.SubItems.Add(lvis3)
            SkinListView1.Items.Add(lvi)
        End While

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Dim str As String

        If SkinListView1.SelectedItems.Count = 0 Then
            TextBox5.Text = ""
            TextBox6.Text = ""
            TextBox7.Text = ""
            TextBox8.Text = ""
            TextBox9.Text = ""
            TextBox10.Text = ""
            TextBox11.Text = ""
        Else
            str = SkinListView1.SelectedItems(0).SubItems(1).Text
            Form1.sqlcon1.sqlstr("
            SELECT expname,expiden,expprof,expadds,expline,expbnka,expbnkn
            FROM expert 
            WHERE expiden = " &
            Chr(34) & str & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            TextBox5.Text = Form1.sqlcon1.dataf.Item(0)
            TextBox6.Text = Form1.sqlcon1.dataf.Item(1)
            TextBox7.Text = Form1.sqlcon1.dataf.Item(2)
            TextBox8.Text = Form1.sqlcon1.dataf.Item(3)
            TextBox9.Text = Form1.sqlcon1.dataf.Item(4)
            TextBox10.Text = Form1.sqlcon1.dataf.Item(5)
            TextBox11.Text = Form1.sqlcon1.dataf.Item(6)
        End If
        GroupBox2.Visible = True
        SkinListView1.Items.Clear()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If TextBox5.Text = "" Or TextBox6.Text = "" Or TextBox7.Text = "" Or
        TextBox8.Text = "" Or TextBox9.Text = "" Then
            MsgBox("警告：标星项目不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        If MsgBox("注意：是否确定新增或更改评审专家信息", 4 + 32, "caution") = MsgBoxResult.Yes Then
            Form1.sqlcon1.sqlstr("
            SELECT id 
            FROM expert 
            WHERE expiden = " &
            Chr(34) & TextBox6.Text & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            If Form1.sqlcon1.dataf.HasRows Then
                Form1.sqlcon1.sqlstr("
                UPDATE expert 
                SET expname = " &
                Chr(34) & TextBox5.Text & Chr(34) & "
                , expprof = " &
                Chr(34) & TextBox7.Text & Chr(34) & "
                ,expadds = " &
                Chr(34) & TextBox8.Text & Chr(34) & "
                ,expline = " &
                Chr(34) & TextBox9.Text & Chr(34) & "
                ,expbnka = " &
                Chr(34) & TextBox10.Text & Chr(34) & "
                ,expbnkn = " &
                Chr(34) & TextBox11.Text & Chr(34) & "
                WHERE expiden = " &
                Chr(34) & TextBox6.Text & Chr(34))
            Else
                Form1.sqlcon1.sqlstr("
                INSERT INTO expert(expname,expiden,expprof,expadds,expline,expbnka,expbnkn) 
                VALUES (" &
                Chr(34) & TextBox5.Text & Chr(34) & "," &
                Chr(34) & TextBox6.Text & Chr(34) & "," &
                Chr(34) & TextBox7.Text & Chr(34) & "," &
                Chr(34) & TextBox8.Text & Chr(34) & "," &
                Chr(34) & TextBox9.Text & Chr(34) & "," &
                Chr(34) & TextBox10.Text & Chr(34) & "," &
                Chr(34) & TextBox11.Text & Chr(34) & ")")
            End If
            MsgBox("提示：新增或修改评审专家信息完成", 0 + 64, "information")
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim str As String
        Dim i As Integer

        Try
            str = SkinListView1.SelectedItems(0).SubItems(1).Text
            If MsgBox("注意：是否确定删除评审专家信息", 4 + 32, "caution") = MsgBoxResult.Yes Then
                '获取expid
                Form1.sqlcon1.sqlstr("
                SELECT id 
                FROM expert
                WHERE expiden = " &
                Chr(34) & str & Chr(34), 1)
                Form1.sqlcon1.dataf.Read()
                i = Form1.sqlcon1.dataf.Item(0)
                '删除expert_config
                Form1.sqlcon1.sqlstr("
                DELETE FROM expert_config
                WHERE expid = " & i)
                '删除expert
                Form1.sqlcon1.sqlstr("
                DELETE FROM expert 
                WHERE expiden = " &
                Chr(34) & str & Chr(34))
                SkinListView1.Items.Clear()
                MsgBox("提示：已删除评审专家信息", 0 + 64, "information")
            End If
        Catch ex As Exception
            MsgBox("警告：请选择要删除的评审专家信息", 0 + 48, "warning")
        End Try
        '刷新tvu
        Form3.SkinListView1.Items.Clear()
        Form3.TreeView1.Nodes.Clear()
        Call Form3.tvu(2)
    End Sub

    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        Dim str As String
        Dim i As Integer

        If TextBox1.Text = "" Or TextBox2.Text = "" Then
            MsgBox("警告：评审组名称和组内职位不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        If SkinListView1.SelectedItems.Count < 1 Then
            MsgBox("警告：请选择要添加的评审专家", 0 + 48, "warning")
            Exit Sub
        End If
        If MsgBox("注意：是否确定新增本项目评审专家", 4 + 32, "caution") = MsgBoxResult.Yes Then
            '获取exp_id
            str = SkinListView1.SelectedItems(0).SubItems(1).Text
            Form1.sqlcon1.sqlstr("
            SELECT id 
            FROM expert 
            WHERE expiden = " &
            Chr(34) & str & Chr(34), 1)
            Form1.sqlcon1.dataf.Read()
            i = Form1.sqlcon1.dataf.Item(0)
            '判断专家是否已添加
            Form1.sqlcon1.sqlstr("
            SELECT id 
            FROM expert_config 
            WHERE expid = " & i & "
            AND oveid = " & Form3.pro_id, 1)
            Form1.sqlcon1.dataf.Read()
            If Form1.sqlcon1.dataf.HasRows Then
                MsgBox("警告：评审专家已存在无法新增", 0 + 48, "warning")
                Exit Sub
            End If
            '写入expert_config数据
            Form1.sqlcon1.sqlstr("
            INSERT INTO 
            [expert_config]([char_gp],[char_po],[expid],[oveid]) 
            SELECT " &
            Chr(34) & TextBox1.Text & Chr(34) & "," &
            Chr(34) & TextBox2.Text & Chr(34) & "," &
            i & "," &
            Form3.pro_id & "
            WHERE NOT EXISTS 
            (SELECT *
            FROM [expert_config]
            WHERE [expid] = " & i & "
            AND [oveid] = " & Form3.pro_id & ")")
            MsgBox("提示：已新增本项目评审专家", 0 + 64, "information")
        End If
        '刷新tvu
        Form3.SkinListView1.Items.Clear()
        Form3.TreeView1.Nodes.Clear()
        Call Form3.tvu(2)
        Me.Close()
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        GroupBox1.Visible = True
        GroupBox2.Visible = False
    End Sub

End Class