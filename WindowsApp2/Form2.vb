﻿Public Class Form2
    Dim owid As Integer
    Dim wkid As Integer

    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles Me.Load
        '初始化groupbox1
        PictureBox1.Image = ImageList1.Images.Item(1)
        PictureBox2.Image = ImageList1.Images.Item(1)
        DateTimePicker1.CustomFormat = "yyyy年MM月dd日 HH:mm:ss"
        DateTimePicker2.CustomFormat = "yyyy年MM月dd日 HH:mm:ss"
        DateTimePicker3.CustomFormat = "yyyy年MM月dd日 HH:mm:ss"
        '隐藏groupbox2、groupbox3
        GroupBox2.Visible = False
        GroupBox3.Visible = False
    End Sub
    Private Sub TextBox_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged,
    TextBox2.TextChanged, TextBox3.TextChanged, TextBox4.TextChanged, TextBox5.TextChanged,
    TextBox6.TextChanged, TextBox7.TextChanged, TextBox8.TextChanged, TextBox9.TextChanged,
    TextBox10.TextChanged, TextBox11.TextChanged, TextBox12.TextChanged
        '文本框验证输入图片
        Select Case sender.name
            Case "TextBox1"
                If TextBox1.Text <> "" Then
                    PictureBox1.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox1.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox2"
                If TextBox2.Text <> "" Then
                    PictureBox2.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox2.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox3"
                If TextBox3.Text <> "" Then
                    PictureBox4.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox4.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox4"
                If TextBox4.Text <> "" Then
                    PictureBox5.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox5.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox5"
                If TextBox5.Text <> "" Then
                    PictureBox6.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox6.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox6"
                If TextBox6.Text <> "" Then
                    PictureBox7.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox7.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox7"
                If TextBox7.Text <> "" Then
                    PictureBox8.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox8.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox8"
                If TextBox8.Text <> "" Then
                    PictureBox11.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox11.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox9"
                If TextBox9.Text <> "" Then
                    PictureBox12.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox12.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox10"
                If TextBox10.Text <> "" Then
                    PictureBox13.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox13.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox11"
                If TextBox11.Text <> "" Then
                    PictureBox14.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox14.Image = ImageList1.Images.Item(1)
                End If
            Case "TextBox12"
                If TextBox12.Text <> "" Then
                    PictureBox15.Image = ImageList1.Images.Item(0)
                Else
                    PictureBox15.Image = ImageList1.Images.Item(1)
                End If
        End Select

    End Sub
    Private Sub PictureBox3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click
        '判断项目名称或项目编号是否为空
        If TextBox1.Text = "" Or TextBox2.Text = "" Then
            MsgBox("警告：项目名称或项目编号不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        GroupBox2.Visible = True
        '初始化groupbox2
        PictureBox4.Image = ImageList1.Images.Item(1)
        PictureBox5.Image = ImageList1.Images.Item(1)
        PictureBox6.Image = ImageList1.Images.Item(1)
        PictureBox7.Image = ImageList1.Images.Item(1)
        PictureBox8.Image = ImageList1.Images.Item(1)
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
    End Sub

    Private Sub PictureBox10_Click(sender As Object, e As EventArgs) Handles PictureBox10.Click
        '初始化groupbox1
        GroupBox2.Visible = False
        TextBox1.Text = ""
        TextBox2.Text = ""
    End Sub

    Private Sub PictureBox9_Click(sender As Object, e As EventArgs) Handles PictureBox9.Click
        '判断采购人和地址是否为空
        If TextBox3.Text = "" Or TextBox4.Text = "" Then
            MsgBox("警告：采购人名称或地址不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '判断采购人和地址是否为空
        If TextBox5.Text = "" Or TextBox6.Text = "" Or TextBox7.Text = "" Then
            MsgBox("警告：采购联系人信息不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '插入main数据库owner表
        Form1.sqlcon1.sqlstr("SELECT * FROM owner 
        WHERE 
        owname=" & Chr(34) & TextBox3.Text.ToString & Chr(34) & "AND 
        owcallm=" & Chr(34) & TextBox5.Text.ToString & Chr(34), 1)
        '判断owner表更新或插入
        If Form1.sqlcon1.dataf.Read = False Then
            Form1.sqlcon1.sqlstr("INSERT INTO 
            owner(owname,owaddrs,owcallm,owcalln,owmail,rectime)
            VALUES(" &
            Chr(34) & TextBox3.Text.ToString & Chr(34) & "," &
            Chr(34) & TextBox4.Text.ToString & Chr(34) & "," &
            Chr(34) & TextBox5.Text.ToString & Chr(34) & "," &
            Chr(34) & TextBox6.Text.ToString & Chr(34) & "," &
            Chr(34) & TextBox7.Text.ToString & Chr(34) & "," &
            Chr(34) & Format(Now(), "yyyy-MM-dd HH:mm:ss") & Chr(34) &
                ")"
                    )
        Else
            Form1.sqlcon1.sqlstr("UPDATE owner SET " &
            "owaddrs = " &
            Chr(34) & TextBox4.Text.ToString & Chr(34) & "," &
            "owcalln = " &
            Chr(34) & TextBox6.Text.ToString & Chr(34) & "," &
            "owmail = " &
            Chr(34) & TextBox7.Text.ToString & Chr(34) & "," &
            "rectime = " &
            Chr(34) & Format(Now(), "yyyy-MM-dd HH:mm:ss") & Chr(34) &
            "WHERE " &
            "owname = " &
            Chr(34) & TextBox3.Text.ToString & Chr(34) & "AND " &
            "owcallm = " &
            Chr(34) & TextBox5.Text.ToString & Chr(34)
                           )
        End If
        '获取owid
        Form1.sqlcon1.sqlstr("SELECT id FROM owner 
        WHERE 
        owname=" & Chr(34) & TextBox3.Text.ToString & Chr(34) & "AND 
        owcallm=" & Chr(34) & TextBox5.Text.ToString & Chr(34), 1)
        Form1.sqlcon1.dataf.Read()
        owid = Form1.sqlcon1.dataf.Item(0)
        '初始化groupbox3
        PictureBox11.Image = ImageList1.Images.Item(1)
        PictureBox12.Image = ImageList1.Images.Item(1)
        PictureBox13.Image = ImageList1.Images.Item(1)
        PictureBox14.Image = ImageList1.Images.Item(1)
        PictureBox15.Image = ImageList1.Images.Item(1)
        TextBox8.Text = ""
        TextBox9.Text = ""
        TextBox10.Text = ""
        TextBox11.Text = ""
        TextBox12.Text = ""
        GroupBox3.Visible = True
    End Sub

    Private Sub PictureBox17_Click(sender As Object, e As EventArgs) Handles PictureBox17.Click
        '初始化groupbox2
        GroupBox3.Visible = False
        TextBox3.Text = ""
        TextBox4.Text = ""
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
    End Sub

    Private Sub PictureBox16_Click(sender As Object, e As EventArgs) Handles PictureBox16.Click
        '判断代理机构和地址是否为空
        If TextBox3.Text = "" Or TextBox4.Text = "" Then
            MsgBox("警告：代理机构名称或地址不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '判断代理机构和地址是否为空
        If TextBox5.Text = "" Or TextBox6.Text = "" Or TextBox7.Text = "" Then
            MsgBox("警告：代理机构联系人信息不得为空", 0 + 48, "warning")
            Exit Sub
        End If
        '插入main数据库worker表
        Form1.sqlcon1.sqlstr("SELECT * FROM worker 
        WHERE 
        wkname=" & Chr(34) & TextBox8.Text.ToString & Chr(34) & "AND 
        wkcallm=" & Chr(34) & TextBox10.Text.ToString & Chr(34), 1)
        '判断owner表更新或插入
        If Form1.sqlcon1.dataf.Read = False Then
            Form1.sqlcon1.sqlstr("INSERT INTO 
            worker(wkname,wkaddrs,wkcallm,wkcalln,wkmail,rectime)
            VALUES(" &
            Chr(34) & TextBox8.Text.ToString & Chr(34) & "," &
            Chr(34) & TextBox9.Text.ToString & Chr(34) & "," &
            Chr(34) & TextBox10.Text.ToString & Chr(34) & "," &
            Chr(34) & TextBox11.Text.ToString & Chr(34) & "," &
            Chr(34) & TextBox12.Text.ToString & Chr(34) & "," &
            Chr(34) & Format(Now(), "yyyy-MM-dd HH:mm:ss") & Chr(34) &
            ")"
                    )
        Else
            Form1.sqlcon1.sqlstr("UPDATE worker SET " &
            "wkaddrs = " &
            Chr(34) & TextBox9.Text.ToString & Chr(34) & "," &
            "wkcalln = " &
            Chr(34) & TextBox11.Text.ToString & Chr(34) & "," &
            "wkmail = " &
            Chr(34) & TextBox12.Text.ToString & Chr(34) & "," &
            "rectime = " &
            Chr(34) & Format(Now(), "yyyy-MM-dd HH:mm:ss") & Chr(34) &
            "WHERE " &
            "wkname = " &
            Chr(34) & TextBox8.Text.ToString & Chr(34) & "AND " &
            "wkcallm = " &
            Chr(34) & TextBox10.Text.ToString & Chr(34)
                           )
        End If
        '获取wkid
        Form1.sqlcon1.sqlstr("SELECT id FROM worker 
        WHERE 
        wkname=" & Chr(34) & TextBox8.Text.ToString & Chr(34) & "AND 
        wkcallm=" & Chr(34) & TextBox10.Text.ToString & Chr(34), 1)
        Form1.sqlcon1.dataf.Read()
        wkid = Form1.sqlcon1.dataf.Item(0)
        '判断main中是否存在项目
        Form1.sqlcon1.sqlstr("SELECT id FROM overload 
        WHERE 
        proname=" & Chr(34) & TextBox1.Text.ToString & Chr(34), 1)
        If Form1.sqlcon1.dataf.Read = True Then
            If MsgBox("注意：项目名称已存在，是否更新已有项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                Form1.sqlcon1.sqlstr("UPDATE overload SET " &
                "prono = " &
                Chr(34) & TextBox2.Text.ToString & Chr(34) & "," &
                "protm1 = " &
                Chr(34) & Format(DateTimePicker1.Value, "yyyy-MM-dd HH:mm:ss") & Chr(34) & "," &
                "protm2 = " &
                Chr(34) & Format(DateTimePicker2.Value, "yyyy-MM-dd HH:mm:ss") & Chr(34) & "," &
                "protm3 = " &
                Chr(34) & Format(DateTimePicker3.Value, "yyyy-MM-dd HH:mm:ss") & Chr(34) & "," &
                "rectime = " &
                Chr(34) & Format(Now(), "yyyy-MM-dd HH:mm:ss") & Chr(34) & "," &
                "owid = " &
                owid & "," &
                "wkid = " &
                wkid &
                " WHERE " &
                "proname = " &
                Chr(34) & TextBox1.Text.ToString & Chr(34)
                               )
            Else
                Exit Sub
            End If
        Else
            If MsgBox("注意：是否确定新建此项目", 4 + 32, "caution") = MsgBoxResult.Yes Then
                Form1.sqlcon1.sqlstr("INSERT INTO 
                overload(proname,prono,protm1,protm2,protm3,rectime,owid,wkid)
                VALUES(" &
                Chr(34) & TextBox1.Text.ToString & Chr(34) & "," &
                Chr(34) & TextBox2.Text.ToString & Chr(34) & "," &
                Chr(34) & Format(DateTimePicker1.Value, "yyyy-MM-dd HH:mm:ss") & Chr(34) & "," &
                Chr(34) & Format(DateTimePicker2.Value, "yyyy-MM-dd HH:mm:ss") & Chr(34) & "," &
                Chr(34) & Format(DateTimePicker3.Value, "yyyy-MM-dd HH:mm:ss") & Chr(34) & "," &
                Chr(34) & Format(Now(), "yyyy-MM-dd HH:mm:ss") & Chr(34) & "," &
                owid & "," &
                wkid & ")"
                               )
            Else
                Exit Sub
            End If
        End If
        MsgBox("提示：项目已新建或更新完成", 0 + 64, "information")
        '刷新listview
        Call Form1.ulv(1)
        Me.Close()
    End Sub
End Class